package aoc2017
import common.grid._

import scala.annotation.tailrec

object day22 extends App {
  case class State(infected: Set[Point], pos: Point = Point(0, 0), dir: Point = Point.Up) {
    def next: (Boolean, State) = {
      if (infected contains pos)
        (false, State(infected - pos, pos + dir.rotateRight, dir.rotateRight))
      else
        (true, State(infected + pos, pos + dir.rotateLeft, dir.rotateLeft))
    }
  }
  def part1(input: String, count: Int = 10000): Int = {
    val lines = input.split("\n")

    val initial = (for {
      y <- -(lines.size / 2) to (lines.size / 2)
      x <- -(lines(0).length / 2) to (lines(0).length / 2)
      if lines(y + (lines.size / 2))(x + (lines(0).length / 2)) == '#'
    } yield Point(x, y)).toSet

    @tailrec
    def loop(state: State, remain: Int = count, infected: Int = 0): Int =
      if (remain <= 0) infected
      else
        state.next match {
          case (true, n)  => loop(n, remain - 1, infected + 1)
          case (false, n) => loop(n, remain - 1, infected)
        }

    loop(State(initial))
  }

  case class StateEvolved(
      infected: Set[Point],
      weaken: Set[Point] = Set.empty,
      flagged: Set[Point] = Set.empty,
      pos: Point = Point.Origin,
      dir: Point = Point.Up
  ) {
    def next: (Boolean, StateEvolved) = {
      if (weaken contains pos)
        (
          true,
          StateEvolved(
            infected + pos,
            weaken - pos,
            flagged,
            pos + dir,
            dir
          )
        )
      else if (infected contains pos)
        (
          false,
          StateEvolved(
            infected - pos,
            weaken,
            flagged + pos,
            pos + dir.rotateRight,
            dir.rotateRight
          )
        )
      else if (flagged contains pos)
        (
          false,
          StateEvolved(
            infected,
            weaken,
            flagged - pos,
            pos + dir.opposite,
            dir.opposite
          )
        )
      else
        (
          false,
          StateEvolved(
            infected,
            weaken + pos,
            flagged,
            pos + dir.rotateLeft,
            dir.rotateLeft
          )
        )
    }
  }

  def part2(input: String, count: Int = 10000000): Int = {
    val lines = input.split("\n")

    val initial = (for {
      y <- -(lines.size / 2) to (lines.size / 2)
      x <- -(lines(0).length / 2) to (lines(0).length / 2)
      if lines(y + (lines.size / 2))(x + (lines(0).length / 2)) == '#'
    } yield Point(x, y)).toSet

    @tailrec
    def loop(state: StateEvolved, remain: Int = count, infected: Int = 0): Int =
      if (remain <= 0) infected
      else
        state.next match {
          case (true, n)  => loop(n, remain - 1, infected + 1)
          case (false, n) => loop(n, remain - 1, infected)
        }

    loop(StateEvolved(initial))
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
