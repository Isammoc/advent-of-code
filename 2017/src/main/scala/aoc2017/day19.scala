package aoc2017

import common.grid._

import scala.annotation.tailrec

object day19 extends App {

  implicit class MyDirection(direction: Point) {
    def turn(p: Point): List[(Point, Point)] = direction match {
      case Point.Up => List(Point.Left, Point.Right).map(d => (d, p + d))
      case Point.Down => List(Point.Left, Point.Right).map(d => (d, p + d))
      case Point.Right => List(Point.Up, Point.Down).map(d => (d, p + d))
      case Point.Left => List(Point.Up, Point.Down).map(d => (d, p + d))
    }
  }

  def part1(input: String): String = {
    def map = input.split("\n")

    @tailrec
    def loop(p: Point, dir: Point, found: List[Char]): String = {
      map(p.y)(p.x) match {
        case ' ' =>
          found.reverse.mkString
        case '+' => // turn
          val (d, p2) = dir
            .turn(p)
            .filter { case (_, p2) =>
              0 <= p2.y && p2.y < map.length && 0 <= p2.x && p2.x < map(0).length && map(
                p2.y
              )(p2.x) != ' '
            }
            .head
          loop(p2, d, found)
        case c if c == '|' || c == '-' => // continue
          loop(p + dir, dir, found)
        case c => // letter
          loop(p + dir, dir, c :: found)
      }
    }

    loop(Point(map(0).indexOf('|'), 0), Point.Down, Nil)
  }

  def part2(input: String): Long = {
    def map = input.split("\n")

    @tailrec
    def loop(p: Point, dir: Point, count: Long): Long = {
      map(p.y)(p.x) match {
        case ' ' =>
          count
        case '+' => // turn
          val (d, p2) = dir
            .turn(p)
            .filter { case (_, p2) =>
              0 <= p2.y && p2.y < map.length && 0 <= p2.x && p2.x < map(0).length && map(
                p2.y
              )(p2.x) != ' '
            }
            .head
          loop(p2, d, count + 1)
        case _ => // continue
          loop(p + dir, dir, count + 1)
      }
    }

    loop(Point(map(0).indexOf('|'), 0), Point.Down, 0)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
