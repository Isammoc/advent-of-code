package aoc2019

object day08 extends App {

  def part1(input: String) = {
    val layer = input.grouped(25 * 6).minBy(_.count(_ == '0'))
    layer.count(_ == '1') * layer.count(_ == '2')
  }

  def part2(input: String) = {
    val layers = input.grouped(25 * 6).toList.transpose
    layers
      .map(_.dropWhile(_ == '2').headOption.getOrElse('2'))
      .map {
        case '0' => ' '
        case '1' => 'X'
      }
      .grouped(25)
      .map(_.mkString)
      .mkString("\n")
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = \n" + part2(input))
}
