package aoc2019

object day16 extends App {
  def coeff(i: Int): LazyList[Int] = {
    val start = List(0, 1, 0, -1)
    val cycle = start.flatMap(z => List.fill(i)(z))
    LazyList.continually(cycle).flatten.tail
  }

  def step(row: Array[Int]) =
    for {
      (_, idx) <- row.zipWithIndex
    } yield (for {
      (a, b) <- row.zip(coeff(idx + 1))
    } yield a * b).sum.abs % 10

  def part1(input: String) = {
    LazyList
      .iterate(input.split("").map(_.toInt))(step)
      .drop(100)
      .head
      .take(8)
      .mkString
  }

  def part2(input: String) = {
    val input2 = (input * 1000).split("").map(_.toInt)
    val offset = input.take(7).toInt - input.length * 9000

    LazyList
      .iterate(input2.reverse)(_.scanLeft(0)((acc, x) => acc + x).tail.map(_.abs % 10))
      .drop(100)
      .head
      .reverse
      .slice(offset, offset + 8)
      .mkString
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
