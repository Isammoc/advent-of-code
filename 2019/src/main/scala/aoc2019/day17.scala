package aoc2019

import aoc2019.intcode.Interpreter
import common.grid.Point

object day17 extends App {

  def alignmentParameters(camera: String): Int = {
    val points = (for {
      (line, y) <- camera.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
    } yield Point(x, y) -> c).toMap

    def around(p: Point) = (for {
      dir <- Point.OrthogonalDirections
      to = p + dir if points.contains(to) && points(to) == '#'
    } yield to).size

    (for {
      (p, scaffold) <- points if scaffold == '#' && around(p) > 3
    } yield p.x * p.y).sum
  }

  def part1(input: String): Int = {
    alignmentParameters(Interpreter(input).execute._2.output.map(_.toChar).mkString)
  }

  def part2(input: String) = {
    val logic =
      """A,B,A,B,C,A,B,C,A,C
        |R,6,L,10,R,8
        |R,8,R,12,L,8,L,8
        |L,10,R,6,R,6,L,8
        |n
        |""".stripMargin

    val original = Interpreter(input)
    val (_, result) = original.copy(ram = original.ram + (0L -> 2), input = logic.toList.map(_.toLong)).execute
    result.output.last
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
