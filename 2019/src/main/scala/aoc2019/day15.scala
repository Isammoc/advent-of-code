package aoc2019

import aoc2019.intcode.Interpreter
import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day15 extends App {

  sealed abstract class PositionType

  case object WALL extends PositionType

  case object FREE extends PositionType

  case object OXYGEN_SYSTEM extends PositionType

  object PositionType {
    def apply(i: Int): PositionType = i match {
      case 0 => WALL
      case 1 => FREE
      case 2 => OXYGEN_SYSTEM
    }
  }

  case class Status(posType: Int, distance: Int, parent: Point)

  case class Robot(intCode: Interpreter, current: Point = Point(0, 0)) {
    def go(dir: Point): (Robot, PositionType, Point) = {
      val dirInt = dir match {
        case Point.Up => 1L
        case Point.Down => 2L
        case Point.Left  => 3L
        case Point.Right  => 4L
      }
      intCode.copy(input = List(dirInt)).execute match {
        case (_, newIntCode) =>
          val posType = PositionType(newIntCode.output.head.toInt)
          posType match {
            case WALL =>
              (this.copy(intCode = newIntCode.copy(output = Nil)), WALL, current + dir)
            case _ =>
              (
                this.copy(
                  intCode = newIntCode.copy(output = Nil),
                  current = current + dir
                ),
                posType,
                current + dir
              )
          }
      }
    }
  }

  case class Plan(known: Map[Point, PositionType] = Map(Point(0, 0) -> FREE)) {
    lazy val accessibleUnknowns: Set[Point] =
      for {
        free <- known.keySet if known(free) == FREE
        dir <- Point.OrthogonalDirections
        p = free + dir if !known.contains(p)
      } yield p

    def pathFrom(origin: Point): Map[Point, List[Point]] = {
      @tailrec
      def loop(toVisit: Queue[Point], result: Map[Point, List[Point]]): Map[Point, List[Point]] = toVisit match {
        case p +: ps if !known.contains(p) =>
          loop(ps, result)
        case p +: ps if known(p) == WALL =>
          loop(ps, result)
        case p +: ps =>
          val ns = for {
            dir <- Point.OrthogonalDirections
            n = p + dir if !result.contains(n)
          } yield (n, dir)
          loop(
            ps ++ ns.map(_._1),
            result ++ ns.map { case (n, dir) =>
              n -> (dir :: result(p))
            }
          )
        case _ => result
      }

      loop(Queue(origin), Map(origin -> Nil))
    }

    def contains(positionType: PositionType): Boolean = known.values.toSet.contains(positionType)

    def withResult(p: Point, positionType: PositionType): Plan = Plan(known + (p -> positionType))

    def print(): Unit = {
      val minx = known.keySet.map(_.x).min
      val maxx = known.keySet.map(_.x).max
      val miny = known.keySet.map(_.y).min
      val maxy = known.keySet.map(_.y).max

      println((for {
        y <- miny to maxy
      } yield (for {
        x <- minx to maxx
      } yield known.get(Point(x, y)) match {
        case None                => " "
        case Some(FREE)          => "."
        case Some(WALL)          => "#"
        case Some(OXYGEN_SYSTEM) => "O"
      }).mkString).mkString("\n"))
    }
  }

  case class Part1(robot: Robot, plan: Plan = Plan()) {
    def discoverNearest: Part1 = {
      val pathes = plan.pathFrom(robot.current)

      val toVisit = plan.accessibleUnknowns.minBy(p => pathes(p).size)

      @tailrec
      def loop(todo: List[Point] = pathes(toVisit).reverse, robot: Robot = robot): Part1 = todo match {
        case last :: Nil =>
          val (newRobot, posType, p) = robot.go(last)
          this.copy(robot = newRobot, plan.withResult(p, posType))
        case dir :: t =>
          val (newRobot, _, _) = robot.go(dir)
          loop(t, newRobot)
      }

      loop()
    }
  }

  def part1(input: String) = {
    val initial = Part1(Robot(Interpreter(input)))

    @tailrec
    def loop(part1: Part1 = initial): Int = if (part1.plan.contains(OXYGEN_SYSTEM)) {
      val pathes = part1.plan.pathFrom(Point(0, 0))
      val target = part1.plan.known.find(_._2 == OXYGEN_SYSTEM).get._1
      pathes(target).size
    } else {
      loop(part1.discoverNearest)
    }

    loop()
  }

  def part2(input: String) = {
    val initial = Part1(Robot(Interpreter(input)))

    @tailrec
    def discoverAll(part1: Part1 = initial): Part1 =
      if (part1.plan.accessibleUnknowns.isEmpty)
        part1
      else {
        discoverAll(part1.discoverNearest)
      }

    val fullPlan = discoverAll().plan
    val oxygenSystem = fullPlan.known.find(_._2 == OXYGEN_SYSTEM).get._1

    val pathes = fullPlan.pathFrom(oxygenSystem)

    pathes.map(_._2.size).max
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
