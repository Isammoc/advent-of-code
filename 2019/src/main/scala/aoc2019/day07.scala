package aoc2019

import aoc2019.intcode.{ Interpreter, NeedInput, Ok }

import scala.annotation.tailrec

object day07 extends App {

  def ampl(input: String)(phase: Long)(in: Long) = {
    Interpreter(input, List(phase, in)).execute._2.output.head
  }

  def part1(input: String) = {
    val possibleValues = Set(0, 1, 2, 3, 4)
    (for {
      pa <- possibleValues
      bPos = possibleValues - pa
      pb <- bPos
      cPos = bPos - pb
      pc <- cPos
      dPos = cPos - pc
      pd <- dPos
      ePos = dPos - pd
      pe <- ePos
    } yield {
      val a = ampl(input)(pa)(0)
      val b = ampl(input)(pb)(a)
      val c = ampl(input)(pc)(b)
      val d = ampl(input)(pd)(c)
      ampl(input)(pe)(d)
    }).max
  }

  def part2Execute(input: String, a: Long, b: Long, c: Long, d: Long, e: Long) = {
    @tailrec
    def loop(ampls: List[Interpreter], current: Long = 0): Long = ampls match {
      case Nil => current
      case h :: t =>
        h.copy(input = List(current)).execute match {
          case (Ok, interpreter)        => loop(t, interpreter.output.head)
          case (NeedInput, interpreter) => loop(t :+ interpreter.copy(output = Nil), interpreter.output.head)
        }
    }

    loop(
      List(
        Interpreter(input, List(a)).execute._2,
        Interpreter(input, List(b)).execute._2,
        Interpreter(input, List(c)).execute._2,
        Interpreter(input, List(d)).execute._2,
        Interpreter(input, List(e)).execute._2
      )
    )
  }

  def part2(input: String): Long = {
    val executions = for {
      Seq(a, b, c, d, e) <- (5 to 9).permutations
    } yield part2Execute(input, a, b, c, d, e)

    executions.max
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
