package aoc2019

import aoc2019.intcode.Interpreter

import scala.annotation.tailrec

object day19 extends App {

  def part1(input: String): Int = {
    val drone = Drone(input)

    val result = for {
      y <- 0 until 50
      x <- 0 until 50
    } yield drone.isInBeam(x, y)

    result.count(b => b)
  }

  def testPart(drone: Drone, x: Int, y: Int): (Int, Int) = {
    @tailrec
    def loopX(xx: Int = x): Int =
      if (drone.isInBeam(xx, y)) {
        loopX(xx + 1)
      } else {
        xx - x
      }

    @tailrec
    def loopY(yy: Int = y): Int =
      if (drone.isInBeam(x, yy)) {
        loopY(yy + 1)
      } else {
        yy - y
      }

    (loopX(), loopY())
  }

  case class Drone(interpreter: Interpreter) {
    def isInBeam(x: Int, y: Int): Boolean = interpreter.copy(input = List(x.toLong, y.toLong)).execute._2.output.head == 1
  }

  object Drone {
    def apply(str: String) = new Drone(Interpreter(str))
  }

  def part2(input: String) = {
    val drone = Drone(input)

    // Skip flaky first lines
    val minX = LazyList.from(0).dropWhile(!drone.isInBeam(_, 100)).head

    @tailrec
    def loop(x: Int = minX, y: Int = 100): Int = {
      val (xx, yy) = testPart(drone, x, y)
      if (xx >= 100 && yy >= 100) x * 10000 + y
      else {
        if (xx >= yy) {
          loop(x + 1, y)
        } else {
          loop(x, y + 1)
        }
      }
    }

    loop()
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
