package aoc2019

import aoc2019.intcode.{ ExitCode, Interpreter, NeedInput, Ok }
import common.grid.Point

import scala.annotation.tailrec

object day11 extends App {

  case class EmergencyHullPaintingRobot(
      computer: Interpreter,
      whites: Set[Point] = Set.empty,
      pos: Point = Point(0, 0),
      dir: Point = Point.Up,
      painted: Set[Point] = Set.empty
  ) {
    val BLACK = 0
    val WHITE = 1
    val LEFT = 0
    val RIGHT = 1

    def step: (ExitCode, EmergencyHullPaintingRobot) = {
      val current = if (whites.contains(pos)) WHITE else BLACK
      this.computer.copy(input = List(current), output = Nil).execute match {
        case (exitCode, newComputer) =>
          newComputer.output match {
            case color :: turn :: Nil =>
              val newDir = turn match {
                case LEFT =>
                  dir.rotateLeft
                case RIGHT =>
                  dir.rotateRight
              }
              (
                exitCode,
                this.copy(
                  computer = newComputer,
                  whites = if (color == WHITE) whites + pos else whites - pos,
                  pos = pos + newDir,
                  dir = newDir,
                  painted = if (color == WHITE) painted + pos else painted
                )
              )
          }
      }
    }

    def run: EmergencyHullPaintingRobot = {
      @tailrec
      def loop(current: EmergencyHullPaintingRobot = this): EmergencyHullPaintingRobot = current.step match {
        case (NeedInput, n) => loop(n)
        case (Ok, n)        => n
      }
      loop()
    }
  }

  def part1(input: String) = {
    EmergencyHullPaintingRobot(Interpreter(input)).run.painted.size
  }

  def part2(input: String) = {
    val whites = EmergencyHullPaintingRobot(Interpreter(input), whites = Set(Point(0, 0))).run.whites
    val minx = whites.map(_.x).min
    val maxx = whites.map(_.x).max
    val miny = whites.map(_.y).min
    val maxy = whites.map(_.y).max

    (for {
      y <- miny to maxy
    } yield (for {
      x <- minx to maxx
    } yield if (whites.contains(Point(x, y))) "X" else " ").mkString).mkString("\n")
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = \n" + part2(input))
}
