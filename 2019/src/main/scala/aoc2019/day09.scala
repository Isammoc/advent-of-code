package aoc2019

import aoc2019.intcode.Interpreter

object day09 extends App {

  def part1(input: String) =
    Interpreter(input, 1).execute._2.output.head

  def part2(input: String) = Interpreter(input, 2).execute._2.output.head

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
