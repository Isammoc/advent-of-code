package aoc2019

import aoc2019.intcode.{ ExitCode, Interpreter }

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day25 extends App {

  case class AsciiComputer(interpreter: Interpreter, debug: Boolean = false) {
    def run(str: String = ""): (ExitCode, String, AsciiComputer) = {
      if (debug) Console.err.println(str)
      val (exitCode, afterExecute) = interpreter.copy(input = str.toList.map(_.toLong) :+ 10).execute
      val output = afterExecute.output.map(_.toChar).mkString
      if (debug) Console.out.println(output)
      (exitCode, output, this.copy(afterExecute.copy(output = Nil)))
    }

    def runAndRead(str: String = ""): (String, AsciiComputer) = {
      val (_, o, a) = this.run(str)
      (o, a)
    }

    def runAndDrop(str: String = ""): AsciiComputer = this.run(str)._3
  }

  case class Room(title: String, directions: Set[String], items: Set[String]) {
    def remove(item: String): Room = this.copy(items = items - item)

    def add(item: String): Room = this.copy(items = items + item)
  }

  object Room {
    def apply(str: String): Room = new Room(extractRoomName(str), extractDirections(str), extractItems(str))

    def extractRoomName(str: String): String = str.split("\n").filter(_.startsWith("== ")).last

    def extractDirections(str: String): Set[String] = for {
      line: String <- str.split("\n").toSet
      dir = line match {
        case "- north" => "north"
        case "- south" => "south"
        case "- east"  => "east"
        case "- west"  => "west"
        case _         => "other"
      } if dir != "other"
    } yield dir

    def extractItems(str: String): Set[String] = {
      val forbiddenItems = Set("escape pod", "infinite loop", "giant electromagnet", "molten lava", "photons")
      if (str.contains("Items here:\n")) {
        str.split("Items here:\n")(1).split("\n").toList.takeWhile(_.startsWith("- ")).map(_.drop(2)).toSet -- forbiddenItems
      } else {
        Set.empty
      }
    }
  }

  class Droid(val computer: AsciiComputer, val carrying: Set[String], val currentRoom: Room) {
    def take(item: String): Droid = new Droid(computer.runAndDrop("take " + item), carrying + item, currentRoom.remove(item))

    def drop(item: String): Droid = new Droid(computer.runAndDrop("drop " + item), carrying - item, currentRoom.add(item))

    def drop(items: Set[String]): Droid = items.foldLeft(this)(_ drop _)

    def dropAll: Droid = this.drop(carrying)

    def takeAll: Droid = currentRoom.items.foldLeft(this)(_ take _)

    def go(direction: String): Droid = {
      val (output, comp) = computer.runAndRead(direction)
      val room = Room(output)
      new Droid(comp, carrying, room)
    }

    def inventory: Set[String] = {
      val str = computer.runAndRead()._1
      if (str.contains("Items in your inventory:\n")) {
        str.split("Items in your inventory:\n")(1).split("\n").toList.takeWhile(_.startsWith("- ")).map(_.drop(2)).toSet
      } else {
        Set.empty
      }
    }

    def canEqual(a: Any): Boolean = a.isInstanceOf[Droid]

    override def equals(obj: Any): Boolean = obj match {
      case that: Droid => that.canEqual(this) && this.computer == that.computer
    }

    override def hashCode(): Int = this.computer.hashCode()

    override def toString: String = "new Droid(" + computer + ", " + carrying + ", " + currentRoom + ")"
  }

  object Droid {
    def apply(input: String): Droid = {
      val (output, after) = AsciiComputer(interpreter = Interpreter(input)).runAndRead()
      new Droid(after, Set.empty, Room(output))
    }
  }

  @tailrec
  def securityWithAllObjects(toVisit: Queue[Droid], visited: Set[Droid] = Set.empty, result: Droid = Droid(input)): Droid = toVisit match {
    case current +: other =>
      if (visited.contains(current)) {
        securityWithAllObjects(other, visited, result)
      } else if (current.currentRoom.title == "== Security Checkpoint ==") {
        if (current.carrying.size > result.carrying.size) {
          securityWithAllObjects(other, visited + current, current)
        } else {
          securityWithAllObjects(other, visited + current, result)
        }
      } else {
        println(toVisit.size)
        val newDroids = (for {
          dir <- current.currentRoom.directions
        } yield current.go(dir).takeAll) -- visited
        securityWithAllObjects(toVisit ++ newDroids, visited + current, result)
      }
    case _ => result
  }

  def manageSecurity(droid: Droid) = {
    val deadEnd = Set(
      "A loud, robotic voice says \"Alert! Droids on this ship are lighter than the detected value!\" and you are ejected back to the checkpoint.",
      "A loud, robotic voice says \"Alert! Droids on this ship are heavier than the detected value!\" and you are ejected back to the checkpoint."
    )
    val carrying = droid.carrying

    val found = carrying
      .subsets()
      .map { current =>
        val (s, _) = droid.drop(current).computer.runAndRead("west")
        s
      }
      .find(s => deadEnd.forall(f => !s.contains(f)))
      .get

    val AnswerR = "by typing (.*) on the keypad".r
    AnswerR.findFirstMatchIn(found).get.group(1)
  }

  def part1(input: String) = {
    val result = securityWithAllObjects(toVisit = Queue(Droid(input)))
    manageSecurity(result)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
}
