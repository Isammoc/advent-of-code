package aoc2019.intcode

sealed abstract class Operation {
  def exec(ram: Interpreter): Either[ExitCode, Interpreter]
}

case object Halt extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] = Left(Ok)
}

case class SumOperation(arg1: Argument, arg2: Argument, arg3: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    for {
      a <- arg1.get(ram)
      b <- arg2.get(ram)
      res <- arg3.set(ram, a + b)
    } yield res.copy(p = res.p + 4)
}

case class MultOperation(arg1: Argument, arg2: Argument, arg3: Argument) extends Operation {
  override def exec(interpreter: Interpreter): Either[ExitCode, Interpreter] =
    for {
      a <- arg1.get(interpreter)
      b <- arg2.get(interpreter)
      res <- arg3.set(interpreter, a * b)
    } yield res.copy(p = res.p + 4)
}

case class InputOperation(arg: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    ram.input match {
      case Nil    => Left(NeedInput)
      case h :: t => arg.set(ram, h).map(_.copy(p = ram.p + 2, input = t))
    }
}

case class OutputOperation(arg: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    for {
      i <- arg.get(ram)
    } yield ram.copy(p = ram.p + 2, output = ram.output :+ i)
}

case class JumpIfTrue(arg1: Argument, arg2: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] = {
    for {
      a <- arg1.get(ram)
      p2 <- arg2.get(ram)
    } yield if (a == 0) ram.copy(p = ram.p + 3) else ram.copy(p = p2)
  }
}
case class JumpIfFalse(arg1: Argument, arg2: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] = {
    for {
      a <- arg1.get(ram)
      p2 <- arg2.get(ram)
    } yield if (a == 0) ram.copy(p = p2) else ram.copy(p = ram.p + 3)
  }
}

case class LessThan(arg1: Argument, arg2: Argument, arg3: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    for {
      a <- arg1.get(ram)
      b <- arg2.get(ram)
      res = if (a < b) 1 else 0
      newInterpreter <- arg3.set(ram, res)
    } yield newInterpreter.copy(p = newInterpreter.p + 4)

}

case class Equals(arg1: Argument, arg2: Argument, arg3: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    for {
      a <- arg1.get(ram)
      b <- arg2.get(ram)
      res = if (a == b) 1 else 0
      newInterpreter <- arg3.set(ram, res)
    } yield newInterpreter.copy(p = newInterpreter.p + 4)
}

case class AdjustRelativeBase(arg1: Argument) extends Operation {
  override def exec(ram: Interpreter): Either[ExitCode, Interpreter] =
    for {
      a <- arg1.get(ram)
    } yield ram.copy(relativeBase = ram.relativeBase + a, p = ram.p + 2)
}

object Operation {
  def apply(ram: Interpreter): Either[ExitCode, Operation] = {
    for {
      intCode <- ram.get(ram.p)
      op <- fromIntCode(intCode, ram.p)
    } yield op
  }

  private def fromIntCode(intCode: Long, addr: Long): Either[ExitCode, Operation] = {
    val args = arguments(intCode / 100, addr + 1)
    intCode % 100 match {
      case 1 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
          arg2 <- args(2)
        } yield SumOperation(arg0, arg1, arg2)
      case 2 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
          arg2 <- args(2)
        } yield MultOperation(arg0, arg1, arg2)
      case 3 =>
        for {
          arg0 <- args.head
        } yield InputOperation(arg0)
      case 4 =>
        for {
          arg0 <- args.head
        } yield OutputOperation(arg0)
      case 5 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
        } yield JumpIfTrue(arg0, arg1)
      case 6 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
        } yield JumpIfFalse(arg0, arg1)
      case 7 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
          arg2 <- args(2)
        } yield LessThan(arg0, arg1, arg2)
      case 8 =>
        for {
          arg0 <- args.head
          arg1 <- args(1)
          arg2 <- args(2)
        } yield Equals(arg0, arg1, arg2)
      case 9 =>
        for {
          arg0 <- args.head
        } yield AdjustRelativeBase(arg0)
      case 99 => Right(Halt)
      case _  => Left(UnknownOperation)
    }
  }

  private def arguments(current: Long, addr: Long): LazyList[Either[ExitCode, Argument]] =
    LazyList.cons(
      Argument(current % 10, addr),
      arguments(current / 10, addr + 1)
    )
}
