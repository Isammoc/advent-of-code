package aoc2019.intcode

sealed abstract class ExitCode

case object Ok extends ExitCode

case object OutOfMemory extends ExitCode

case object UnknownOperation extends ExitCode

case object NeedInput extends ExitCode

case object UnknownArgumentMode extends ExitCode
