package aoc2019.intcode

sealed abstract class Argument {
  def get(ram: Interpreter): Either[ExitCode, Long]

  def set(ram: Interpreter, value: Long): Either[ExitCode, Interpreter]
}

case class ImmediateArgument(addr: Long) extends Argument {
  override def get(ram: Interpreter): Either[ExitCode, Long] = ram.get(addr)

  override def set(ram: Interpreter, value: Long): Either[ExitCode, Interpreter] = ram.set(addr, value)
}

case class PositionArgument(addr: Long) extends Argument {
  override def get(ram: Interpreter): Either[ExitCode, Long] = ram.get(addr).flatMap(ram.get)

  override def set(ram: Interpreter, value: Long): Either[ExitCode, Interpreter] = ram.get(addr).flatMap(ram.set(_, value))
}

case class RelativeArgument(addr: Long) extends Argument {
  override def get(ram: Interpreter): Either[ExitCode, Long] = ram.get(addr).flatMap(i => ram.get(i + ram.relativeBase))

  override def set(ram: Interpreter, value: Long): Either[ExitCode, Interpreter] = ram.get(addr).flatMap(i => ram.set(i + ram.relativeBase, value))
}

object Argument {
  def apply(det: Long, addr: Long): Either[ExitCode, Argument] =
    det match {
      case 0 => Right(PositionArgument(addr))
      case 1 => Right(ImmediateArgument(addr))
      case 2 => Right(RelativeArgument(addr))
      case _ => Left(UnknownArgumentMode)
    }
}
