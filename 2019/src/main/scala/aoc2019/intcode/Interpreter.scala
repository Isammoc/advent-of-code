package aoc2019.intcode

import scala.annotation.tailrec

case class Interpreter(ram: Map[Long, Long], p: Long, relativeBase: Long = 0, input: List[Long] = Nil, output: List[Long] = Nil) {
  def get(addr: Long): Either[ExitCode, Long] =
    if (addr < 0) {
      Left(OutOfMemory)
    } else {
      Right(ram.getOrElse(addr, 0))
    }

  def set(addr: Long, value: Long): Either[ExitCode, Interpreter] =
    if (addr >= 0)
      Right(this.copy(ram = ram + (addr -> value)))
    else
      Left(OutOfMemory)

  def step: Either[ExitCode, Interpreter] =
    for {
      op <- Operation(this)
      newInterpreter <- op.exec(this)
    } yield newInterpreter

  @tailrec
  final def execute: (ExitCode, Interpreter) =
    step match {
      case Right(step) => step.execute
      case Left(stop)  => (stop, this)
    }
}

object Interpreter {
  def apply(input: String): Interpreter = {
    val ram = input
      .split(",")
      .map(_.toLong)
      .zipWithIndex
      .map { case (ramValue, idx) => idx.toLong -> ramValue }
      .toMap
    new Interpreter(ram, 0)
  }

  def apply(str: String, input: List[Long]): Interpreter = Interpreter(str).copy(input = input)

  def apply(str: String, input: Int*): Interpreter = Interpreter(str).copy(input = input.toList.map(_.toLong))
}
