package aoc2019

import aoc2019.intcode.Interpreter

import scala.annotation.tailrec

object day23 extends App {

  def part1(input: String) = {
    val nics = (for {
      i <- 0 until 50
    } yield i.toLong -> Interpreter(input, i).execute._2).toMap

    @tailrec
    def loop(queue: Iterable[List[Long]], nics: Map[Long, Interpreter]): Long =
      queue match {
        case Nil =>
          val newNics = nics.map { case (i, nic) =>
            i -> nic.copy(output = Nil, input = if (nic.input.isEmpty) List(-1) else nic.input).execute._2
          }
          loop(newNics.values.flatMap(_.output.grouped(3)), newNics)
        case List(255, _, y) :: _ => y
        case List(address: Long, x, y) :: t if address >= 0 && address < 50 =>
          val currentNic = nics(address)
          loop(t, nics + (address -> currentNic.copy(input = currentNic.input :+ x :+ y)))
        case _ :: t =>
          loop(t, nics)
      }

    loop(Nil, nics)
  }

  def part2(input: String) = {
    val nics = (for {
      i <- 0 until 50
    } yield i.toLong -> Interpreter(input, i).execute._2).toMap

    @tailrec
    def nat(queue: Iterable[List[Long]], nics: Map[Long, Interpreter], lastX: Long, lastY: Long, oldY: Long): Long =
      queue match {
        case Nil =>
          if (nics.values.forall(_.input.isEmpty)) {
            if (lastY == oldY) lastY
            else {
              val newNics = nics.map {
                case (0, nic) => 0L -> nic.copy(output = Nil, input = List(lastX, lastY)).execute._2
                case (i, nic) => i -> nic.copy(output = Nil, input = List(-1)).execute._2
              }
              nat(newNics.values.flatMap(_.output.grouped(3)), newNics, lastX, lastY, lastY)
            }
          } else {
            val newNics = nics.map { case (i, nic) =>
              i -> nic.copy(output = Nil, input = if (nic.input.isEmpty) List(-1) else nic.input).execute._2
            }
            nat(newNics.values.flatMap(_.output.grouped(3)), newNics, lastX, lastY, oldY)
          }
        case List(255, x, y) :: t =>
          nat(t, nics, x, y, oldY)
        case List(address: Long, x, y) :: t if address >= 0 && address < 50 =>
          val currentNic = nics(address)
          nat(t, nics + (address -> currentNic.copy(input = currentNic.input :+ x :+ y)), lastX, lastY, oldY)
        case _ :: t =>
          nat(t, nics, lastX, lastY, oldY)
      }

    nat(Nil, nics, 0, 0, -1)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
