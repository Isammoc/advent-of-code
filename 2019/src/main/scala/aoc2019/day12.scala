package aoc2019

import common.algo.integers.LCM

import scala.annotation.tailrec

object day12 extends App {

  case class Point3(x: Int, y: Int, z: Int) {
    def add(other: Point3): Point3 = Point3(x + other.x, y + other.y, z + other.z)

    def energy: Int = x.abs + y.abs + z.abs
  }

  case class Moon(pos: Point3, velocity: Point3 = Point3(0, 0, 0)) {
    def energy: Int = pos.energy * velocity.energy

    def x: (BigInt, BigInt) = (pos.x, velocity.x)

    def y: (BigInt, BigInt) = (pos.y, velocity.y)

    def z: (BigInt, BigInt) = (pos.z, velocity.z)
  }

  case class Jupiter(moons: List[Moon]) {
    def energy: Int = moons.map(_.energy).sum

    def step: Jupiter = {
      val gravities = for {
        moon <- moons
      } yield (for {
        other <- moons if other != moon
      } yield Point3(
        (other.pos.x - moon.pos.x).sign,
        (other.pos.y - moon.pos.y).sign,
        (other.pos.z - moon.pos.z).sign
      )).reduce(_.add(_))

      val withNewVelocity = moons.zip(gravities).map { case (moon, g) =>
        moon.copy(velocity = moon.velocity.add(g))
      }

      val newMoons =
        for (moon <- withNewVelocity)
          yield moon.copy(
            pos = moon.pos.add(moon.velocity)
          )
      this.copy(moons = newMoons)
    }

    def x: List[(BigInt, BigInt)] = moons.map(_.x)

    def y: List[(BigInt, BigInt)] = moons.map(_.y)

    def z: List[(BigInt, BigInt)] = moons.map(_.z)
  }

  def readInput(input: String): List[Point3] = {
    val PointReg = "<x=(-?\\d+), y=(-?\\d+), z=(-?\\d+)>".r
    input.split("\n").toList.map { case PointReg(x, y, z) =>
      Point3(x.toInt, y.toInt, z.toInt)
    }
  }

  def part1(input: String, step: Int = 1000): Int = {
    val initial = Jupiter(readInput(input).map(p => Moon(p)))

    @scala.annotation.tailrec
    def loop(current: Jupiter = initial, remainingStep: Int = step): Jupiter =
      if (remainingStep <= 0) current
      else loop(current.step, remainingStep - 1)

    loop().energy
  }

  def part2(input: String): BigInt = {
    val initial = Jupiter(readInput(input).map(p => Moon(p)))

    @tailrec
    def loop(
        extract: Jupiter => List[(BigInt, BigInt)],
        current: Jupiter = initial,
        count: Long = 0,
        visited: Map[List[(BigInt, BigInt)], Long] = Map.empty
    ): Long =
      if (visited.keySet.contains(extract(current))) {
        count - visited(extract(current))
      } else {
        loop(extract, current.step, count + 1, visited + (extract(current) -> count))
      }

    val x: BigInt = loop(_.x)
    val y: BigInt = loop(_.y)
    val z: BigInt = loop(_.z)
    LCM(Seq(x, y, z))
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
