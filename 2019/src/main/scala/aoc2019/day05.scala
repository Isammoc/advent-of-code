package aoc2019

import aoc2019.intcode.Interpreter

object day05 extends App {

  def part1(input: String) = Interpreter(input, List(1L)).execute._2.output.dropWhile(_ == 0).head

  def part2(input: String) = Interpreter(input, List(5L)).execute._2.output.head

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
