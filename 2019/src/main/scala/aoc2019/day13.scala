package aoc2019

import aoc2019.intcode.{ ExitCode, Interpreter, NeedInput, Ok }
import common.grid.Point

import scala.annotation.tailrec

object day13 extends App {

  @scala.annotation.tailrec
  def loopRun(current: Arcade): Arcade =
    current.nextStep match {
      case (Ok, result) => result
      case (NeedInput, result) =>
        result.print()
        val ball = result.tiles.filter(_._2 == current.BALL).maxBy(_._1.y)._1
        val paddle = result.tiles.filter(_._2 == current.HORIZONTAL_PADDLE).minBy(_._1.x)._1

        val todo =
          if (ball.x == paddle.x) 0L
          else if (ball.x < paddle.x) -1L
          else 1L

        loopRun(result.copy(intCode = result.intCode.copy(input = List(todo))))
    }

  @tailrec
  def loopStep(remain: List[Int], current: Arcade): Arcade =
    remain match {
      case -1 :: 0 :: score :: t => loopStep(t, current.copy(score = score))
      case x :: y :: tileId :: t =>
        tileId match {
          case current.EMPTY => loopStep(t, current.copy(tiles = current.tiles - Point(x, y)))
          case _             => loopStep(t, current.copy(tiles = current.tiles + (Point(x, y) -> tileId)))
        }
      case _ => current.copy(intCode = current.intCode.copy(output = remain.map(_.toLong)))
    }

  case class Arcade(intCode: Interpreter, tiles: Map[Point, Int] = Map.empty, score: Int = 0) {
    val EMPTY = 0
    val WALL = 1
    val BLOCK = 2
    val HORIZONTAL_PADDLE = 3
    val BALL = 4

    def nextStep: (ExitCode, Arcade) = {
      intCode.execute match {
        case (exitCode, result) =>
          (exitCode, loopStep(result.output.map(_.toInt), this.copy(intCode = result)))
      }
    }

    def run: Int = {
      loopRun(this).score
    }

    def print(): Unit = {
      val minx = this.tiles.keySet.map(_.x).min
      val maxx = this.tiles.keySet.map(_.x).max
      val miny = this.tiles.keySet.map(_.y).min
      val maxy = this.tiles.keySet.map(_.y).max

      println((for {
        y <- miny to maxy
      } yield (for {
        x <- minx to maxx
      } yield tiles.get(Point(x, y)) match {
        case None                    => ' '
        case Some(WALL)              => '#'
        case Some(BLOCK)             => 'x'
        case Some(HORIZONTAL_PADDLE) => '='
        case Some(BALL)              => 'o'
      }).mkString).mkString("\n"))
    }
  }

  def part1(input: String) = {
    val (exitCode, arcade) = Arcade(Interpreter(input)).nextStep
    (exitCode, arcade.tiles.values.count(_ == arcade.BLOCK))
  }

  def part2(input: String) = {
    val interpreter = Interpreter(input)
    val (_, arcade) = Arcade(interpreter.copy(ram = interpreter.ram + (0L -> 2L))).nextStep
    arcade.run
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
