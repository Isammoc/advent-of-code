package aoc2019

import scala.annotation.tailrec

object day14 extends App {

  type Quantity = Map[String, Long]

  implicit class QuantityImproved(quantity: Quantity) {
    def +(other: Quantity): Quantity = {
      quantity ++ other.map { case (name, s) =>
        name -> (quantity.getOrElse(name, 0L) + s)
      }
    }
  }

  object Quantity {
    def apply(input: String): Quantity = {
      val Ingredient = "(\\d+) (.+)".r
      input
        .split(", ")
        .map { case Ingredient(q, name) =>
          name -> q.toLong
        }
        .toMap
    }
  }

  case class Recipe(before: Quantity, after: Quantity)

  object Recipe {
    def apply(input: String): Recipe = {
      val BeforeAfter = "(.*) => (.*)".r
      input match {
        case BeforeAfter(before, after) => Recipe(Quantity(before), Quantity(after))
      }
    }
  }

  case class State(quantity: Quantity, score: Long) {
    def next(recipes: Set[Recipe]): Set[State] = {
      @tailrec
      def loop(recipes: List[Recipe], result: Set[State] = Set.empty): Set[State] = recipes match {
        case Nil => result
        case Recipe(before, after) :: t if before.keySet == Set("ORE") =>
          loop(t, result + this.add(after, before("ORE")))
        case Recipe(before, _) :: t if this.contains(before) =>
          loop(t, result + this.remove(before))
        case _ :: t => loop(t, result)
      }

      loop(recipes.toList)
    }

    def add(q: Quantity, score: Long): State = this.copy(
      quantity = quantity + q,
      score = this.score + score
    )

    def contains(q: Quantity): Boolean = q.forall { case (name, q) =>
      quantity.getOrElse(name, 0L) > q
    }

    def remove(q: Quantity): State = this.copy(quantity = quantity ++ q.map { case (name, count) =>
      name -> (quantity.getOrElse(name, 0L) - count)
    })
  }

  def part1(input: String, target: Quantity = Map("FUEL" -> 1L)): Long = {
    val originalRecipes = input.split("\n").map(Recipe.apply).toSet

    @tailrec
    def loop(target: Quantity = target, leftOver: Quantity = Map.empty): Long = {
      if (target.keySet == Set("ORE")) {
        target("ORE")
      } else if (leftOver.keySet.exists(target.keySet.contains)) {
        val toRemove = leftOver.keys.filter(target.keySet.contains).head
        if (target(toRemove) > leftOver(toRemove)) {
          loop(target + (toRemove -> (target(toRemove) - leftOver(toRemove))), leftOver - toRemove)
        } else {
          loop(target - toRemove, (leftOver + (toRemove -> (leftOver(toRemove) - target(toRemove)))).filter(_._2 > 0))
        }
      } else {
        val todo = target.keys.find(_ != "ORE").get
        val recipe = originalRecipes.find(_.after.contains(todo)).get

        val resultByRecipe = recipe.after(todo)
        val amountTodo = target(todo)

        val minTodo = amountTodo / resultByRecipe

        val count = if (minTodo * resultByRecipe >= amountTodo) {
          minTodo
        } else {
          minTodo + 1
        }
        loop(
          (target - todo) + recipe.before.map { case (name, q) => name -> q * count },
          leftOver + (todo -> (recipe.after(todo) * count - amountTodo))
        )
      }
    }

    loop()
  }

  def part2(input: String): Long = {
    val limit = 1000000000000L

    @tailrec
    def loop(start: Long, end: Long): Long = if (start >= end - 1) {
      start
    } else {
      val middle = (end - start) / 2 + start
      val result = part1(input, Map("FUEL" -> middle))
      if (result > limit) {
        loop(start, middle)
      } else {
        loop(middle, end)
      }
    }

    loop(0, limit)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
