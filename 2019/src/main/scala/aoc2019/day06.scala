package aoc2019

import scala.annotation.tailrec

object day06 extends App {

  def part1(input: String) = {
    val directOrbits = input
      .split("\n")
      .map(_.split("\\)"))
      .map { case Array(a, b) => a -> b }
      .groupBy(_._1)
      .view
      .mapValues(_.map(_._2))
      .toMap

    @tailrec
    def loop(toVisit: Set[String] = Set("COM"), visited: Set[String] = Set.empty, dist: Map[String, Int] = Map("COM" -> 0), total: Int = 0): Int = if (
      toVisit.isEmpty
    ) total
    else {
      val current = toVisit.head
      if (visited.contains(current))
        loop(toVisit - current, visited, dist, total)
      else {
        loop(
          toVisit ++ directOrbits.getOrElse(current, Array.empty[String]),
          visited + current,
          dist ++ directOrbits.getOrElse(current, Array.empty[String]).map((_, dist(current) + 1)),
          total + dist(current)
        )
      }
    }

    loop()
  }

  def part2(input: String) = {
    val directParent = input
      .split("\n")
      .map(_.split("\\)"))
      .map { case Array(a, b) => b -> a }
      .toMap

    @tailrec
    def loopPath(current: String, path: List[String] = List.empty): List[String] =
      if (current == "COM") "COM" :: path else loopPath(directParent(current), current :: path)

    val youPath = loopPath("YOU")
    val sanPath = loopPath("SAN")
    sanPath.reverse.takeWhile(i => !youPath.contains(i)).size + youPath.reverse.takeWhile(i => !sanPath.contains(i)).size - 2
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
