package aoc2019

import aoc2019.intcode.Interpreter

object day02 extends App {

  def part1(input: String, a: Long, b: Long, pos: Long = 0) = {
    val interpreter = Interpreter(input)
    val realInterpreter = interpreter.copy(ram = interpreter.ram + (1L -> a) + (2L -> b))

    val (_, result) = realInterpreter.execute
    result.ram(pos)
  }

  def part2(input: String) = {
    val res = for {
      i <- 0 to 99
      j <- 0 to 99
    } yield
      if (part1(input, i, j) == 19690720)
        Some(100 * i + j)
      else None

    res.flatten.head
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input, 12, 2))
  println("part2 = " + part2(input))
}
