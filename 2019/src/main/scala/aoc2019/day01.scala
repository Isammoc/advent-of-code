package aoc2019

import scala.annotation.tailrec

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed =  Array[Int]

  object solution extends common.Solution[Parsed]("2019", "01") {
    override def parse(input: String): Parsed = input.split("\n").map(_.toInt)

    override def part1(parsed: Parsed): Any = parsed.map(_ / 3 - 2).sum

    @tailrec
    def fuel(i: Int, current: Int = 0): Int =
      if (i <= 6) current
      else {
        val f = i / 3 - 2
        fuel(f, current + f)
      }

    override def part2(parsed: Parsed): Any = parsed.map(fuel(_)).sum
  }
}

