package aoc2019

import common.grid.Point

import scala.annotation.tailrec

object day24 extends App {

  def part1(input: String) = {
    val alive = (for {
      y <- 0 until 5
      x <- 0 until 5
      if input.split("\n")(y)(x) == '#'
    } yield Point(x, y)).toSet

    def nextIsBug(current: Set[Point], x: Int, y: Int): Boolean = {
      val p = Point(x, y)
      val neighbors = Point.OrthogonalDirections.count(dir => current.contains(p + dir))
      (current.contains(p) && neighbors == 1) || (!current.contains(p) && (neighbors == 1 || neighbors == 2))
    }

    @tailrec
    def loop(current: Set[Point], visited: Set[Set[Point]]): Int = if (visited.contains(current)) {
      (for {
        y <- 0 until 5
        x <- 0 until 5 if current.contains(Point(x, y))
      } yield BigInt(2).pow(y * 5 + x)).sum.toInt
    } else {
      loop(
        (for {
          y <- 0 until 5
          x <- 0 until 5 if nextIsBug(current, x, y)
        } yield Point(x, y)).toSet,
        visited + current
      )
    }

    loop(alive, Set.empty)
  }

  case class Point3(x: Int, y: Int, z: Int) {
    lazy val neighbors: Set[Point3] = {
      val west = this match {
        case Point3(0, _, z) => Set(Point3(1, 2, z - 1))
        case Point3(3, 2, z) =>
          (for {
            y <- 0 until 5
          } yield Point3(4, y, z + 1)).toSet
        case Point3(x, y, z) => Set(Point3(x - 1, y, z))
      }
      val east = this match {
        case Point3(4, _, z) => Set(Point3(3, 2, z - 1))
        case Point3(1, 2, z) =>
          (for {
            y <- 0 until 5
          } yield Point3(0, y, z + 1)).toSet
        case Point3(x, y, z) => Set(Point3(x + 1, y, z))
      }
      val north = this match {
        case Point3(_, 0, z) => Set(Point3(2, 1, z - 1))
        case Point3(2, 3, z) =>
          (for {
            x <- 0 until 5
          } yield Point3(x, 4, z + 1)).toSet
        case Point3(x, y, z) => Set(Point3(x, y - 1, z))
      }
      val south = this match {
        case Point3(_, 4, z) => Set(Point3(2, 3, z - 1))
        case Point3(2, 1, z) =>
          (for {
            x <- 0 until 5
          } yield Point3(x, 0, z + 1)).toSet
        case Point3(x, y, z) => Set(Point3(x, y + 1, z))
      }

      west ++ east ++ north ++ south
    }

    def nextIsBug(current: Set[Point3]): Boolean = {
      val ns = this.neighbors.intersect(current).size
      (current.contains(this) && ns == 1) || (!current.contains(this) && (ns == 1 || ns == 2))
    }
  }

  def step3(current: Set[Point3]): Set[Point3] = {
    val minz = current.map(_.z).min - 1
    val maxz = current.map(_.z).max + 1

    for {
      z: Int <- (minz to maxz).toSet
      y <- 0 until 5
      x <- 0 until 5 if x != 2 || y != 2
      p = Point3(x, y, z) if p.nextIsBug(current)
    } yield p
  }

  def part2(input: String, steps: Int = 200) = {
    val alive = (for {
      y <- 0 until 5
      x <- 0 until 5
      if input.split("\n")(y)(x) == '#'
    } yield Point3(x, y, 0)).toSet

    @tailrec
    def loop(current: Set[Point3], remain: Int): Int = if (remain <= 0) current.size
    else {
      loop(step3(current), remain - 1)
    }

    loop(alive, steps)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
