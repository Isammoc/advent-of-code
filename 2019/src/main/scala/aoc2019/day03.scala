package aoc2019

import common.grid.Point

import scala.annotation.tailrec

object day03 extends App {

  def wire(str: String): Map[Point, Int] = {
    val North = "U(\\d+)".r
    val South = "D(\\d+)".r
    val East = "R(\\d+)".r
    val West = "L(\\d+)".r

    @tailrec
    def loop(remain: List[String], current: Point = Point(0, 0), distance: Int = 0, found: Map[Point, Int] = Map(Point(0, 0) -> 0)): Map[Point, Int] =
      remain match {
        case Nil => found
        case h :: t =>
          val (d, p, more) = h match {
            case North(d) =>
              val points = for {
                y <- 1 to d.toInt
                p = current.copy(y = current.y - y) if !found.contains(p)
              } yield (p, distance + y)
              (distance + d.toInt, current.copy(y = current.y - d.toInt), points)
            case South(d) =>
              val points = for {
                y <- 1 to d.toInt
                p = current.copy(y = current.y + y) if !found.contains(p)
              } yield (p, distance + y)
              (distance + d.toInt, current.copy(y = current.y + d.toInt), points)
            case East(d) =>
              val points = for {
                x <- 1 to d.toInt
                p = current.copy(x = current.x + x) if !found.contains(p)
              } yield (p, distance + x)
              (distance + d.toInt, current.copy(x = current.x + d.toInt), points)
            case West(d) =>
              val points = for {
                x <- 1 to d.toInt
                p = current.copy(x = current.x - x) if !found.contains(p)
              } yield (p, distance + x)
              (distance + d.toInt, current.copy(x = current.x - d.toInt), points)
          }
          loop(t, p, d, found ++ more)
      }

    loop(str.split(",").toList)
  }

  def part1(input: String) = {
    val Array(a, b) = input.split("\n").map(wire)
    a.keys.toSet.intersect(b.keys.toSet).map(p => p.x.abs + p.y.abs).filter(_ != 0).min
  }

  def part2(input: String) = {
    val Array(a, b) = input.split("\n").map(wire)
    println(a)
    println(b)
    a.keys.toSet.intersect(b.keys.toSet).map(p => a(p) + b(p)).filter(_ != 0).min
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
