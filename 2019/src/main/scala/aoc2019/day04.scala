package aoc2019

import scala.annotation.tailrec

object day04 extends App {
  @tailrec
  def containsDouble(str: List[Char], currentChar: Option[Char] = None, current: Int = 0): Boolean = str match {
    case Nil                               => current == 2
    case h :: t if currentChar.contains(h) => containsDouble(t, currentChar, current + 1)
    case _ if current == 2                 => true
    case h :: t                            => containsDouble(t, Some(h), 1)
  }

  def isValidPart1(input: Int) = {
    val one = input.toString
    one.zip(one.tail).forall { case (x, y) => y >= x } && one.zip(one.tail).exists { case (x, y) => x == y }
  }

  def isValidPart2(input: Int) = {
    val one = input.toString
    one.zip(one.tail).forall { case (x, y) => y >= x } && containsDouble(one.toList)
  }

  def part1(input: String) = {
    val Array(min, max) = input.split("-").map(_.toInt)
    val valids = for (x <- min to max if isValidPart1(x)) yield x
    valids.size
  }

  def part2(input: String) = {
    val Array(min, max) = input.split("-").map(_.toInt)
    val valids = for (x <- min to max if isValidPart2(x)) yield x
    valids.size
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
