package aoc2019

import aoc2019.intcode.Interpreter

object day21 extends App {

  def execute(input: String, springCode: String): Long = {
    val interpreter = Interpreter(input)

    val (_, result) = interpreter.copy(input = springCode.toList.map(_.toLong)).execute
    result.output.filterNot(_ < 256).headOption.getOrElse(0L)
  }

  def part1(input: String) = {
    val springCode =
      """NOT A T
        |NOT T T
        |AND B T
        |AND C T
        |NOT T J
        |AND D J
        |WALK
        |""".stripMargin
    execute(input, springCode)
  }

  def part2(input: String) = {
    val springCode =
      """NOT A T
        |NOT T T
        |AND B T
        |AND C T
        |NOT T J
        |AND D J
        |NOT E T
        |NOT T T
        |OR  H T
        |AND T J
        |RUN
        |""".stripMargin
    execute(input, springCode)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
