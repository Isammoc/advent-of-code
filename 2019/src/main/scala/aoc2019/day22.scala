package aoc2019

import scala.annotation.tailrec

object day22 extends App {

  case class Module(m: BigInt) {

    def toModulo(before: BigInt): BigInt = ((before % m) + m) % m

    case class Linear(a: BigInt, b: BigInt) {
      def apply(x: BigInt): BigInt = toModulo(a * x + b)

      def apply(arr: List[BigInt]): List[BigInt] = arr.map(this.apply)

      def compose(other: Linear): Linear = {
        Linear(toModulo(a * other.a), toModulo(a * other.b + b))
      }

      def reverse: Linear = Linear(a.modInverse(m), toModulo(-b * a.modInverse(m)))

      def pow(n: BigInt): Linear = {
        @tailrec
        def loop(remain: BigInt, current: Linear = this, result: Linear = Linear.identity): Linear =
          if (remain <= 0) {
            result
          } else if (remain % 2 == 1) {
            loop(remain / 2, current.compose(current), result.compose(current))
          } else {
            loop(remain / 2, current.compose(current), result)
          }

        loop(n)
      }
    }

    object Linear {
      private val CutR = "cut (-?\\d+)".r
      private val DealWithIncrementR = "deal with increment (\\d+)".r

      def apply(str: String): Linear = {
        str match {
          case "deal into new stack" => Linear(toModulo(-1), toModulo(m - 1))
          case CutR(n)               => Linear(toModulo(1), toModulo(BigInt(n)))
          case DealWithIncrementR(n) => Linear(toModulo(BigInt(n).modInverse(m)), 0)
        }
      }

      val identity: Linear = Linear(1, 0)

      def compose(str: String): Linear = {
        str.split("\n").toList.foldLeft(identity) { (acc, current) =>
          acc.compose(apply(current))
        }
      }
    }

  }

  def part1(input: String) = {
    Module(10007).Linear.compose(input).reverse.apply(2019)
  }

  def part2(input: String) = {
    Module(BigInt("119315717514047")).Linear.compose(input).pow(BigInt("101741582076661")).apply(2020)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
