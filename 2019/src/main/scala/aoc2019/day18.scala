package aoc2019

import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.collection.mutable

object day18 extends App {

  sealed abstract class Position

  case object Wall extends Position

  case object Free extends Position

  case class Key(c: Char) extends Position

  case class Door(c: Char) extends Position

  case class World(map: Map[Point, Position]) {
    def edges(p: Point): List[Point] = map(p) match {
      case Free =>
        Point.OrthogonalDirections
          .filter(dir => map.contains(p + dir))
          .filter(dir =>
            map(p + dir) match {
              case Free    => true
              case Key(_)  => true
              case Wall    => false
              case Door(_) => false
            }
          )
      case _ => Nil
    }

    def accessibles(current: Point): Map[Point, Int] = {
      @tailrec
      def loop(toVisit: Queue[Point], result: Map[Point, List[Point]]): Map[Point, List[Point]] = toVisit match {
        case p +: ps if !map.contains(p) =>
          loop(ps, result)
        case p +: ps =>
          val ns = for {
            dir <- edges(p)
            n = p + dir if !result.contains(n)
          } yield (n, dir)
          loop(
            ps ++ ns.map(_._1),
            result ++ ns.map { case (n, dir) =>
              n -> (dir :: result(p))
            }
          )
        case _ => result
      }

      loop(Queue(current), Map(current -> Nil)).map { case (i, v) => i -> v.size }
    }

    val keys: Set[Point] = map.filter {
      case (_, Key(_)) => true
      case _           => false
    }.keySet

    def withPoint(p: Point): World = {
      val Key(c) = map(p)

      this.copy(
        map = map.map { case (k, v) =>
          k -> (v match {
            case Key(k) if k == c  => Free
            case Door(k) if k == c => Free
            case other             => other
          })
        }
      )
    }

    override def toString: String = {
      val minx = map.keys.map(_.x).min
      val maxx = map.keys.map(_.x).max
      val miny = map.keys.map(_.y).min
      val maxy = map.keys.map(_.y).max

      (for {
        y <- miny to maxy
      } yield (for {
        x <- minx to maxx
      } yield map(Point(x, y)) match {
        case Free    => '.'
        case Wall    => '#'
        case Key(c)  => (c - 'A' + 'a').toChar
        case Door(c) => c
      }).mkString).mkString("\n")
    }
  }

  case class State(positions: Set[Point], world: World) {
    def next: List[(State, Int)] = for {
      old <- positions.toList
      (newP, s) <- world.accessibles(old).toList if world.keys.contains(newP)
    } yield (State(positions - old + newP, world.withPoint(newP)), s)
  }

  case class Node(state: State, steps: Int) {
    val keys: Set[Point] = state.world.keys

    def isEnd: Boolean = keys.isEmpty

    def next: List[Node] =
      for {
        (newS, cost) <- state.next
      } yield Node(newS, steps + cost)
  }

  def readWorld(input: String): World = {
    val map = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
    } yield {
      val position = c match {
        case '.'                       => Free
        case '@'                       => Free
        case '#'                       => Wall
        case _ if c >= 'a' && c <= 'z' => Key((c + 'A' - 'a').toChar)
        case _                         => Door(c)
      }
      Point(x, y) -> position
    }).toMap
    World(map)
  }

  def result(initial: Node): Int = {
    val toVisit = mutable.PriorityQueue(initial)(Ordering.by(-_.steps))

    @tailrec
    def loop(visited: Set[State]): Int = {
      val current = toVisit.dequeue()
      if (visited.contains(current.state)) {
        loop(visited)
      } else if (current.isEnd) {
        current.steps
      } else {
        toVisit ++= current.next
        loop(visited + current.state)
      }
    }

    loop(Set.empty)
  }

  def part1(input: String): Int = {
    val initialWorld = readWorld(input)

    val current = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex if c == '@'
    } yield Point(x, y)).head

    result(Node(State(Set(current), initialWorld), 0))
  }

  def part2(input: String) = {
    val inputWorld = readWorld(input)

    val current = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex if c == '@'
    } yield Point(x, y)).head

    val initialWorld = World(
      inputWorld.map +
        (Point(current.x - 1, current.y) -> Wall) +
        (Point(current.x + 1, current.y) -> Wall) +
        (Point(current.x, current.y) -> Wall) +
        (Point(current.x, current.y - 1) -> Wall) +
        (Point(current.x, current.y + 1) -> Wall)
    )

    result(
      Node(
        State(
          Set(
            Point(current.x + 1, current.y + 1),
            Point(current.x + 1, current.y - 1),
            Point(current.x - 1, current.y + 1),
            Point(current.x - 1, current.y - 1)
          ),
          initialWorld
        ),
        0
      )
    )
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
