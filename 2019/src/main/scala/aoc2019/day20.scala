package aoc2019

import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day20 extends App {

  case class Edge(to: Point, level: Int)

  case class World(start: Point, end: Point, edges: Map[Point, List[Edge]])

  def readWorld(input: String): World = {
    val points = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
    } yield Point(x, y) -> c).toMap

    val maxx = points.keySet.map(_.x).max
    val maxy = points.keySet.map(_.y).max

    val markers = List(
      (List(Point.Up, Point.Up), List(Point.Up)),
      (List(Point.Down), List(Point.Down, Point.Down)),
      (List(Point.Left, Point.Left), List(Point.Left)),
      (List(Point.Right), List(Point.Right, Point.Right))
    )
    val portals = for {
      (origin, free) <- points if free == '.'
      (a, b) <- markers
      pa = a.foldLeft(origin)(_ + _) if points.contains(pa) && points(pa) >= 'A' && points(pa) <= 'Z'
      pb = b.foldLeft(origin)(_ + _) if points.contains(pb) && points(pb) >= 'A' && points(pb) <= 'Z'
    } yield {
      val level = if (origin.y == 2 || origin.y == maxy - 2 || origin.x == 2 || origin.x == maxx - 2) -1 else 1
      origin -> ((points(pa).toString + points(pb).toString, level))
    }

    val edges = for {
      (p, free) <- points if free == '.'
    } yield {
      val near = for {
        dir <- Point.OrthogonalDirections
        dist = p + dir if points(dist) == '.'
      } yield Edge(dist, 0)

      val byPortals = if (portals.contains(p)) {
        for {
          (origin, (name, _)) <- portals if name == portals(p)._1 && origin != p
        } yield Edge(origin, portals(p)._2)
      } else {
        Nil
      }

      p -> (near ++ byPortals)
    }

    val start = portals.find(_._2._1 == "AA").get._1
    val end = portals.find(_._2._1 == "ZZ").get._1

    World(start, end, edges)
  }

  def part1(input: String) = {
    val world = readWorld(input)

    @tailrec
    def loop(toVisit: Queue[(Point, Int)], visited: Set[Point]): Int = toVisit match {
      case (p, _) +: ps if visited.contains(p) => loop(ps, visited)
      case (p, s) +: _ if p == world.end       => s
      case (p, s) +: ps =>
        loop(ps ++ world.edges(p).map(_.to -> (s + 1)), visited + p)
    }

    loop(Queue((world.start, 0)), Set.empty)
  }

  def part2(input: String) = {
    val world = readWorld(input)

    @tailrec
    def loop(toVisit: Queue[(Edge, Int)], visited: Set[Edge]): Int = toVisit match {
      case (e, _) +: ps if visited.contains(e)              => loop(ps, visited)
      case (Edge(p, l), s) +: _ if p == world.end && l == 0 => s
      case (Edge(p, l), s) +: ps =>
        val newEdges = for {
          Edge(newP, diffL) <- world.edges(p)
          newL = l + diffL if newL >= 0
        } yield Edge(newP, newL) -> (s + 1)

        loop(ps ++ newEdges, visited + Edge(p, l))
    }

    loop(Queue((Edge(world.start, 0), 0)), Set.empty)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
