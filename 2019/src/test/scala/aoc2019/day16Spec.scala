package aoc2019

import common.AocSpec

class day16Spec extends AocSpec {

  import day16._

  "day16 2019" can {
    "part1" should {
      "example" in {
        part1("80871224585914546619083218645595") shouldEqual "24176176"
        part1("19617804207202209144916044189917") shouldEqual "73745418"
        part1("69317163492948606335995924319873") shouldEqual "52432133"
      }
    }
  }
}
