package aoc2019

import common.AocSpec
import org.scalatest.Assertion

class day22Spec extends AocSpec {

  import day22._

  implicit class ExampleString(str: String) {
    def shouldEqual10(result: List[Int]): Assertion =
      Module(10).Linear.compose(str).apply(List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(BigInt.apply)) shouldEqual result.map(BigInt.apply)
  }

  "day22 2019" can {
    "part1" should {
      "deal into new stack" in {
        "deal into new stack" shouldEqual10 List(9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
      }

      "cut N" should {
        "N = 3" in {
          "cut 3" shouldEqual10 List(3, 4, 5, 6, 7, 8, 9, 0, 1, 2)
        }
        "N = -4" in {
          "cut -4" shouldEqual10 List(6, 7, 8, 9, 0, 1, 2, 3, 4, 5)
        }
      }

      "deal with increment N" should {
        "N = 3" in {
          "deal with increment 3" shouldEqual10 List(0, 7, 4, 1, 8, 5, 2, 9, 6, 3)
        }
      }

      "compose" should {
        "sample1" in {
          """deal with increment 7
            |deal into new stack
            |deal into new stack""".stripMargin shouldEqual10 List(0, 3, 6, 9, 2, 5, 8, 1, 4, 7)
        }

        "sample2" in {
          """cut 6
            |deal with increment 7
            |deal into new stack""".stripMargin shouldEqual10 List(3, 0, 7, 4, 1, 8, 5, 2, 9, 6)
        }

        "sample3" in {
          """deal with increment 7
            |deal with increment 9
            |cut -2""".stripMargin shouldEqual10 List(6, 3, 0, 7, 4, 1, 8, 5, 2, 9)
        }

        "sample4" in {
          """deal into new stack
            |cut -2
            |deal with increment 7
            |cut 8
            |cut -4
            |deal with increment 7
            |cut 3
            |deal with increment 9
            |deal with increment 3
            |cut -1""".stripMargin shouldEqual10 List(9, 2, 5, 8, 1, 4, 7, 0, 3, 6)
        }
      }
    }
  }
}
