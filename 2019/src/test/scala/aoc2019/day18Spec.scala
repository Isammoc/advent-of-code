package aoc2019

import common.AocSpec

class day18Spec extends AocSpec {

  import day18._

  "day18 2019" can {
    "part1" should {
      "without door" in {
        part1("""########
                |#b.@.ac#
                |########""".stripMargin) shouldEqual 7
      }
      "example1" in {
        part1("""#########
                |#b.A.@.a#
                |#########""".stripMargin) shouldEqual 8
      }
      "example2" in {
        part1("""########################
                |#f.D.E.e.C.b.A.@.a.B.c.#
                |######################.#
                |#d.....................#
                |########################""".stripMargin) shouldEqual 86
      }
      "example3" in {
        part1("""########################
                |#...............b.C.D.f#
                |#.######################
                |#.....@.a.B.c.d.A.e.F.g#
                |########################""".stripMargin) shouldEqual 132
      }
      "example4" in {
        part1("""#################
                |#i.G..c...e..H.p#
                |########.########
                |#j.A..b...f..D.o#
                |########@########
                |#k.E..a...g..B.n#
                |########.########
                |#l.F..d...h..C.m#
                |#################""".stripMargin) shouldEqual 136
      }
      "example5" in {
        part1("""########################
                |#@..............ac.GI.b#
                |###d#e#f################
                |###A#B#C################
                |###g#h#i################
                |########################""".stripMargin) shouldEqual 81
      }
    }

    "part2" should {
      "example" in {
        // part2(sample) shouldEqual 0
        pending
      }
    }
  }
}
