package aoc2019

import common.AocSpec

class day24Spec extends AocSpec {

  import day24._

  "day24 2019" can {

    "part1" should {
      "example" in {
        part1("""....#
                |#..#.
                |#..##
                |..#..
                |#....""".stripMargin) shouldEqual 2129920
      }
    }
    "Point3" should {
      "Simple adjacents" in {
        Point3(3, 3, 0).neighbors shouldEqual Set(Point3(3, 2, 0), Point3(2, 3, 0), Point3(4, 3, 0), Point3(3, 4, 0))
        Point3(1, 1, 1).neighbors shouldEqual Set(
          Point3(1, 0, 1),
          Point3(0, 1, 1),
          Point3(2, 1, 1),
          Point3(1, 2, 1)
        )
      }

      "4 adjacents upper" in {
        Point3(3, 0, 1).neighbors shouldEqual Set(
          Point3(2, 1, 0),
          Point3(2, 0, 1),
          Point3(4, 0, 1),
          Point3(3, 1, 1)
        )
      }

      "4 adjacents upper-right" in {
        Point3(4, 0, 1).neighbors shouldEqual Set(
          Point3(2, 1, 0),
          Point3(3, 0, 1),
          Point3(3, 2, 0),
          Point3(4, 1, 1)
        )
      }

      "8 adjacents" in {
        Point3(3, 2, 0).neighbors shouldEqual Set(
          Point3(3, 1, 0),
          Point3(4, 0, 1),
          Point3(4, 1, 1),
          Point3(4, 2, 1),
          Point3(4, 3, 1),
          Point3(4, 4, 1),
          Point3(4, 2, 0),
          Point3(3, 3, 0)
        )
      }
    }

    "part2" should {
      "example" in {
        part2(
          """....#
            |#..#.
            |#..##
            |..#..
            |#....""".stripMargin,
          10
        ) shouldEqual 99
      }
    }
  }
}
