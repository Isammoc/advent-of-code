package aoc2019

class day01Spec extends common.AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """12
      |14
      |1969
      |100756""".stripMargin

  "day01 2019" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual (2 + 2 + 654 + 33583)
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual (2 + 2 + 966 + 50346)
      }
    }
  }
}

