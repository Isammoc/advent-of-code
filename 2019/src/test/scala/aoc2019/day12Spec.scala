package aoc2019

import common.AocSpec

class day12Spec extends AocSpec {

  import day12._

  "day12 2019" can {
    "part1" should {
      val sample =
        """<x=-1, y=0, z=2>
          |<x=2, y=-10, z=-7>
          |<x=4, y=-8, z=8>
          |<x=3, y=5, z=-1>""".stripMargin
      "simple sample" in {
        readInput(sample) shouldEqual List(Point3(-1, 0, 2), Point3(2, -10, -7), Point3(4, -8, 8), Point3(3, 5, -1))

        Jupiter(readInput(sample).map(p => Moon(p))).step shouldEqual Jupiter(
          List(
            Moon(Point3(2, -1, 1), Point3(3, -1, -1)),
            Moon(Point3(3, -7, -4), Point3(1, 3, 3)),
            Moon(Point3(1, -7, 5), Point3(-3, 1, -3)),
            Moon(Point3(2, 2, 0), Point3(-1, -3, 1))
          )
        )
      }
      "example" in {
        part1(sample, 10) shouldEqual 179
        val secondExample = """<x=-8, y=-10, z=0>
                              |<x=5, y=5, z=10>
                              |<x=2, y=-7, z=3>
                              |<x=9, y=-8, z=-3>""".stripMargin
        part1(secondExample, 100) shouldEqual 1940
      }
    }

    "part2" should {
      "example" in {
        val sample = """<x=-8, y=-10, z=0>
                       |<x=5, y=5, z=10>
                       |<x=2, y=-7, z=3>
                       |<x=9, y=-8, z=-3>""".stripMargin
        part2(sample) shouldEqual BigInt("4686774924")
      }
    }
  }
}
