package aoc2019
import common.AocSpec

class day03Spec extends AocSpec {
  import day03._

  "day03 2019" can {
    val sample =
      """R8,U5,L5,D3
        |U7,R6,D4,L4""".stripMargin

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6
        part1("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83") shouldEqual 159
        part1("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7") shouldEqual 135
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 30
        part2("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83") shouldEqual 610
        part2("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7") shouldEqual 410
      }
    }
  }
}
