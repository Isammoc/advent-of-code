package aoc2019

import common.AocSpec

class day06Spec extends AocSpec {

  import day06._

  "day06 2019" can {
    "part1" should {
      "example" in {
        part1("""COM)B
                |B)C
                |C)D
                |D)E
                |E)F
                |B)G
                |G)H
                |D)I
                |E)J
                |J)K
                |K)L""".stripMargin) shouldEqual 42
      }
    }

    "part2" should {
      "example" in {
        part2("""COM)B
                |B)C
                |C)D
                |D)E
                |E)F
                |B)G
                |G)H
                |D)I
                |E)J
                |J)K
                |K)L
                |K)YOU
                |I)SAN""".stripMargin) shouldEqual 4
      }
    }
  }
}
