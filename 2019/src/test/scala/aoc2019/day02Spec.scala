package aoc2019
import common.AocSpec

class day02Spec extends AocSpec {
  import day02._

  "day02 2019" can {
    "part1" should {
      "example" taggedAs IntCodeTag in {
        part1("1,9,10,3,2,3,11,0,99,30,40,50", 9, 10) shouldEqual 3500
        part1("1,0,0,0,99", 0, 0) shouldEqual 2
        part1("2,3,0,3,99", 3, 0, 3) shouldEqual 6
        part1("2,4,4,5,99,0", 4, 4, 5) shouldEqual 9801
        part1("1,1,1,4,99,5,6,0,99", 1, 1) shouldEqual 30
      }
    }
  }
}
