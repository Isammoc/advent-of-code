package aoc2019

import common.AocSpec

class day17Spec extends AocSpec {

  import day17._

  "day17 2019" can {
    "alignmentParameter" should {
      "example" in {
        alignmentParameters("""..#..........
                              |..#..........
                              |#######...###
                              |#.#...#...#.#
                              |#############
                              |..#...#...#..
                              |..#####...^..""".stripMargin) shouldEqual 76
      }
    }
  }
}
