package aoc2019
import aoc2019.intcode.{ Interpreter, Ok }
import common.AocSpec

class day05Spec extends AocSpec {

  "day05 2019" can {
    "part1" should {
      "input" taggedAs IntCodeTag in {
        val (exitCode, res) = Interpreter("3,3,99,0").copy(input = List(25)).execute
        exitCode shouldEqual Ok
        res.ram shouldEqual Map(0 -> 3, 1 -> 3, 2 -> 99, 3 -> 25)
      }
      "ouput" taggedAs IntCodeTag in {
        val (exitCode, res) = Interpreter("4,3,99,42").execute
        exitCode shouldEqual Ok
        res.output should equal(List(42))
      }
    }

    "part2" should {
      "equalTo8" taggedAs IntCodeTag in {
        val equalTo8 = "3,9,8,9,10,9,4,9,99,-1,8"

        Interpreter(equalTo8, List(8L)).execute._2.output shouldEqual List(1)
        Interpreter(equalTo8, List(6L)).execute._2.output shouldEqual List(0)
      }

      "less than 8" taggedAs IntCodeTag in {
        val lessThan8 = "3,9,7,9,10,9,4,9,99,-1,8"
        Interpreter(lessThan8, List(3L)).execute._2.output shouldEqual List(1)
        Interpreter(lessThan8, List(13L)).execute._2.output shouldEqual List(0)
      }

      "equals to 8 (2)" taggedAs IntCodeTag in {
        val equalTo8 = "3,3,1108,-1,8,3,4,3,99"
        Interpreter(equalTo8, List(8L)).execute._2.output shouldEqual List(1)
        Interpreter(equalTo8, List(6L)).execute._2.output shouldEqual List(0)
      }

      "larger" taggedAs IntCodeTag in {
        val larger =
          "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"

        Interpreter(larger, List(1L)).execute._2.output.head shouldEqual 999
        Interpreter(larger, List(8L)).execute._2.output.head shouldEqual 1000
        Interpreter(larger, List(13L)).execute._2.output.head shouldEqual 1001
      }
    }
  }
}
