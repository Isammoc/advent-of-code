package aoc2019
import common.AocSpec

class day04Spec extends AocSpec {
  import day04._

  "day04 2019" can {
    "part1" should {
      "example" in {
        part1("111111-111111") shouldEqual 1
        part1("223450-223450") shouldEqual 0
        part1("123789-123789") shouldEqual 0
      }
    }

    "part2" should {
      "example" in {
        part2("112233-112233") shouldEqual 1
        part2("123444-123444") shouldEqual 0
        part2("111122-111122") shouldEqual 1
      }
    }
  }
}
