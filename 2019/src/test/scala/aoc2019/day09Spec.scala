package aoc2019
import aoc2019.intcode.{ Interpreter, Ok }
import common.AocSpec

class day09Spec extends AocSpec {

  "day09 2019" can {
    "part1" should {
      "quine" taggedAs IntCodeTag in {
        val (exitCode, result) = Interpreter("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99").execute
        exitCode shouldBe Ok
        result.output shouldEqual List(109L, 1L, 204L, -1L, 1001L, 100L, 1L, 100L, 1008L, 100L, 16L, 101L, 1006L, 101L, 0L, 99L)
      }
      "large number" taggedAs IntCodeTag in {
        val (exitCode, result) = Interpreter("1102,34915192,34915192,7,4,7,99,0").execute
        exitCode shouldBe Ok
        val List(l) = result.output
        l.toString.length shouldEqual 16
      }
      "large number input" taggedAs IntCodeTag in {
        val (exitCode, result) = Interpreter("104,1125899906842624,99").execute
        exitCode shouldBe Ok
        val List(l) = result.output
        l.toString shouldEqual "1125899906842624"
      }
    }
  }
}
