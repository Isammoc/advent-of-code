package aoc2018

import common.algo.graph.Dijkstra
import common.grid.Point

import scala.annotation.tailrec
import common.grid.Point.Implicits._

object day15 extends App {
  sealed abstract class Kind {
    val enemy: Kind
  }

  case object Elf extends Kind {
    override val enemy: Kind = Goblin
  }

  case object Goblin extends Kind {
    override val enemy: Kind = Elf
  }

  case class Character(kind: Kind, location: Point, hitPoint: Int = 200)

  def parse(input: String): (List[Character], Set[Point]) = {
    val grid = for {
      (line, y) <- input.split("\n").toList.zipWithIndex
      (c, x) <- line.toList.zipWithIndex
    } yield (Point(x, y), c)

    @tailrec
    def loop(remain: List[(Point, Char)], units: List[Character] = Nil, walls: Set[Point] = Set.empty): (List[Character], Set[Point]) =
      remain match {
        case Nil => (units, walls)
        case (p, '#') :: t => loop(t, units, walls + p)
        case (p, 'E') :: t => loop(t, Character(Elf, p) :: units, walls)
        case (p, 'G') :: t => loop(t, Character(Goblin, p) :: units, walls)
        case _ :: t => loop(t, units, walls)
      }

    loop(grid)
  }

  def move(current: Character, enemies: Set[Character], blocked: Set[Point]): Character = {
    Point.OrthogonalDirections.map(current.location + _).toSet
    val reachableEnemies =
      enemies.filter(_.location.manhattan(current.location) == 1)
    if (reachableEnemies.isEmpty) {
      val dijkstra = Dijkstra.simpleDistance(current.location)(p => Point.OrthogonalDirections.map(p + _).filterNot(blocked.contains))
      val inRanges = for {
        enemy <- enemies
        pos = enemy.location
        range <- Point.OrthogonalDirections.map(pos + _) if !blocked.contains(range)
      } yield range

      if (inRanges.flatMap(dijkstra.get).isEmpty) {
        current
      } else {
        val minDistance = inRanges.flatMap(dijkstra.get).min
        val chosen =
          inRanges.filter(dijkstra(_) == minDistance).minBy(p => (p.y, p.x))
        val distanceFromChosen = Dijkstra.simpleDistance(chosen)(p => Point.OrthogonalDirections.map(p + _).filterNot(blocked.contains))
        val possibleMoves =
          Point.OrthogonalDirections.map(current.location + _).filterNot(blocked.contains)
        val minDistanceToChosen =
          possibleMoves.map(distanceFromChosen.apply).min
        val moves =
          possibleMoves.filter(distanceFromChosen(_) == minDistanceToChosen)
        val toMove = moves.min
        current.copy(location = toMove)
      }
    } else {
      current
    }
  }

  def battle(initialUnits: List[Character], walls: Set[Point], attackElf: Int = 3): (Int, List[Character]) = {
    @tailrec
    def loop(count: Int = 0, toMove: List[Character] = initialUnits.sortBy(_.location), moved: List[Character] = Nil): (Int, List[Character]) = {
      toMove match {
        case Nil =>
          loop(count + 1, moved.sortBy(_.location), Nil)
        case current :: t =>
          val otherUnits = t.toSet ++ moved
          val enemies = otherUnits.filter(_.kind == current.kind.enemy)
          if (enemies.isEmpty) {
            (count, toMove ++ moved)
          } else {
            val currentMoved =
              move(current, enemies, walls ++ otherUnits.map(_.location))

            val targets =
              enemies.filter(_.location.manhattan(currentMoved.location) == 1)
            if (targets.isEmpty) {
              loop(count, t, currentMoved :: moved)
            } else {
              val attack = if (current.kind == Elf) attackElf else 3
              val target =
                targets.minBy(t => (t.hitPoint, t.location.y, t.location.x))
              if (target.hitPoint <= attack) {
                loop(
                  count,
                  t.filterNot(_ == target),
                  currentMoved :: moved.filterNot(_ == target)
                )
              } else {
                def withAttack(before: List[Character]): List[Character] = before.map {
                  case a if a == target =>
                    a.copy(hitPoint = a.hitPoint - attack)
                  case a => a
                }

                loop(count, withAttack(t), currentMoved :: withAttack(moved))
              }
            }
          }
      }
    }

    loop()
  }

  def part1(input: String): Int = {
    val (initialUnits, walls) = parse(input)
    val (count, units) = battle(initialUnits, walls)
    val hps = units.map(_.hitPoint).sum
    count * hps
  }

  def part2(input: String): Int = {
    val (initialUnits, walls) = parse(input)
    val initialElfs = initialUnits.count(_.kind == Elf)
    val (count, units) = LazyList
      .from(4)
      .map { attack => battle(initialUnits, walls, attack) }
      .dropWhile { case (_, result) =>
        result.count(_.kind == Elf) != initialElfs
      }
      .head
    val hps = units.map(_.hitPoint).sum
    count * hps
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
