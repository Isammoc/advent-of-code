package aoc2018
import common.grid._

import scala.annotation.tailrec
import common.grid.Point.Implicits._

object day13 extends App {
  val input = io.Source.stdin.getLines().mkString("\n")

  def part1(input: String): String = {
    val paths = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      path <- Path(c)
    } yield Point(x, y) -> path).toMap

    val carts = for {
      (line, y) <- input.split("\n").toList.zipWithIndex
      (c, x) <- line.zipWithIndex
      dir <- parseDirection(c)
    } yield Cart(Point(x, y), dir, Left)

    @tailrec
    def loop(carts: List[Cart] = carts.sortBy(_.location), next: List[Cart] = Nil, blocked: Set[Point] = carts.map(_.location).toSet): String =
      carts match {
        case Nil => loop(next.sortBy(_.location), Nil, blocked)
        case current :: _
            if blocked contains current
              .move(paths(current.location))
              .location =>
          val crash = current.move(paths(current.location)).location
          s"${crash.x},${crash.y}"
        case current :: t =>
          loop(
            t,
            current.move(paths(current.location)) :: next,
            blocked - current.location + current
              .move(paths(current.location))
              .location
          )
      }
    loop()
  }

  def part2(input: String): String = {
    val paths = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      path <- Path(c)
    } yield Point(x, y) -> path).toMap

    val carts = for {
      (line, y) <- input.split("\n").toList.zipWithIndex
      (c, x) <- line.zipWithIndex
      dir <- parseDirection(c)
    } yield Cart(Point(x, y), dir, Left)

    @tailrec
    def loop(
        carts: List[Cart] = carts.sortBy(_.location),
        next: List[Cart] = Nil,
        blocked: Set[Point] = carts.map(_.location).toSet,
        startTick: Boolean
    ): String = {
      carts match {
        case finalCart :: Nil if startTick =>
          s"${finalCart.location.x},${finalCart.location.y}"
        case Nil =>
          loop(next.sortBy(_.location), Nil, blocked, startTick = true)
        case current :: t
            if blocked contains current
              .move(paths(current.location))
              .location =>
          val crash = current.move(paths(current.location)).location
          loop(
            t.filterNot(_.location == crash),
            next.filterNot(_.location == crash),
            blocked - crash - current.location,
            startTick = false
          )
        case current :: t =>
          loop(
            t,
            current.move(paths(current.location)) :: next,
            blocked - current.location + current
              .move(paths(current.location))
              .location,
            startTick = false
          )
      }
    }
    loop(startTick = true)
  }

  sealed abstract class Way {
    val next: Way

    def turn(dir: Point): Point
  }

  sealed abstract class Path

  case class Cart(location: Point, direction: Point, way: Way = Left) {
    def move(path: Path): Cart = path match {
      case Pipe =>
        Cart(location + direction, direction, way)
      case Caret =>
        Cart(location + direction, direction, way)
      case Slash =>
        direction match {
          case Point.Up => Cart(location + Point.Right, Point.Right, way)
          case Point.Down => Cart(location + Point.Left, Point.Left, way)
          case Point.Right  => Cart(location + Point.Up, Point.Up, way)
          case Point.Left  => Cart(location + Point.Down, Point.Down, way)
        }
      case AntiSlash =>
        direction match {
          case Point.Up => Cart(location + Point.Left, Point.Left, way)
          case Point.Down => Cart(location + Point.Right, Point.Right, way)
          case Point.Right  => Cart(location + Point.Down, Point.Down, way)
          case Point.Left  => Cart(location + Point.Up, Point.Up, way)
        }
      case Plus =>
        val nextDirection = way.turn(direction)
        Cart(location + nextDirection, nextDirection, way.next)
    }
  }

  def parseDirection(c: Char): Option[Point] = c match {
    case '^' => Some(Point.Up)
    case 'v' => Some(Point.Down)
    case '<' => Some(Point.Left)
    case '>' => Some(Point.Right)
    case _   => None
  }

  case object Left extends Way {
    val next: Way = Straight

    override def turn(dir: Point): Point = dir match {
      case Point.Up => Point.Left
      case Point.Left  => Point.Down
      case Point.Down => Point.Right
      case Point.Right  => Point.Up
    }
  }

  case object Straight extends Way {
    val next: Way = Right

    override def turn(dir: Point): Point = dir
  }

  case object Right extends Way {
    val next: Way = Left

    override def turn(dir: Point): Point = dir match {
      case Point.Up => Point.Right
      case Point.Left  => Point.Up
      case Point.Down => Point.Left
      case Point.Right  => Point.Down
    }
  }

  case object Pipe extends Path

  case object Caret extends Path

  case object Slash extends Path

  case object AntiSlash extends Path

  case object Plus extends Path

  object Path {
    def apply(c: Char): Option[Path] = c match {
      case '|'  => Some(Pipe)
      case '^'  => Some(Pipe)
      case 'v'  => Some(Pipe)
      case '-'  => Some(Caret)
      case '>'  => Some(Caret)
      case '<'  => Some(Caret)
      case '/'  => Some(Slash)
      case '\\' => Some(AntiSlash)
      case '+'  => Some(Plus)
      case _    => None
    }
  }
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
