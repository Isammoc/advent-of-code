package aoc2018
import common.algo.graph.Dijkstra
import common.grid.Point

import scala.annotation.tailrec

object day20 extends App {
  @tailrec
  def parseInput(
      remain: List[Char],
      current: Point = Point(0, 0),
      stack: List[Point] = List(Point(0, 0)),
      edges: Map[Point, Set[Point]] = Map.empty
  ): Map[Point, Set[Point]] = remain match {
    case Nil      => edges
    case '^' :: t => parseInput(t, current, stack, edges)
    case '$' :: t => parseInput(t, current, stack, edges)
    case '(' :: t => parseInput(t, current, current :: stack, edges)
    case ')' :: t => parseInput(t, stack.head, stack.tail, edges)
    case '|' :: t => parseInput(t, stack.head, stack, edges)
    case w :: t =>
      val next = current + charToDir(w)
      parseInput(
        t,
        next,
        stack,
        edges + (current -> (edges
          .getOrElse(current, Set.empty) + next)) + (next -> (edges
          .getOrElse(next, Set.empty) + current))
      )
  }

  private def charToDir(w: Char): Point = w match {
    case 'N' => Point.Up
    case 'W' => Point.Left
    case 'E' => Point.Right
    case 'S' => Point.Down
  }

  def part1(input: String): Long = {
    val neighbors = parseInput(input.toList)
    val dijkstra = Dijkstra.simpleDistance(Point(0, 0))(neighbors.apply)

    dijkstra.values.max
  }

  def part2(input: String): Int = {
    val neighbors = parseInput(input.toList)
    val dijkstra = Dijkstra.simpleDistance(Point(0, 0))(neighbors.apply)

    dijkstra.values.count(_ >= 1000)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
