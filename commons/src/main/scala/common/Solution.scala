package common

import common.solution.{ InputFetcher, TimedFunction }

abstract class Solution[Parsed](val year: String, val day: String) {
  def parse(input: String): Parsed
  def part1(parsed: Parsed): Any
  def part2(parsed: Parsed): Any
}

abstract class AocApp[Parsed](solution: Solution[Parsed]) extends App {
  val input = InputFetcher(solution.year, solution.day)
  val (timePrep, parsed) = TimedFunction(solution.parse(input))
  println(s"********** Preparation $timePrep **********")
  val (part1Time, part1) = TimedFunction(solution.part1(parsed))
  println()
  println(s"**********   Part 1    $part1Time **********")
  println(part1)
  println()
  val (part2Time, part2) = TimedFunction(solution.part2(parsed))
  println(s"**********   Part 2    $part2Time **********")
  println(part2)
}
