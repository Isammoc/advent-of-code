package common.grid

object Point {
  object Implicits {
    implicit val pointOrdering: Ordering[Point] = Ordering.by(p => (p.y, p.x))
  }

  val Up: Point = Point(0, -1)
  val Down: Point = Point(0, 1)
  val Left: Point = Point(-1, 0)
  val Right: Point = Point(1, 0)

  val OrthogonalDirections: List[Point] = List(Up, Right, Down, Left)

  val Origin: Point = Point(0, 0)
}

case class Point(x: Int, y: Int) {

  def manhattan(xx: Int, yy: Int): Int = (xx - x).abs + (yy - y).abs

  def manhattan(that: Point): Int = this.manhattan(that.x, that.y)

  def rotateRight: Point = Point(-y, x)
  def rotateLeft: Point = Point(y, -x)
  def opposite: Point = Point(-x, -y)

  def +(that: Point): Point = Point(x + that.x, y + that.y)
  def -(that: Point): Point = Point(x - that.x, y - that.y)

  def *(size: Int): Point = Point(x * size, y * size)
}
