package common.algo.integers

object LCM {
  def apply(list: Seq[BigInt]): BigInt = list.foldLeft(BigInt(1))((a, b) => (a / GCD(a, b)) * b)
}
