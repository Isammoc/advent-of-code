package common.algo.integers

import scala.annotation.tailrec

object GCD {
  @tailrec
  def apply(a: BigInt, b: BigInt): BigInt = if (b == 0) a.abs else GCD(b, a % b)
}
