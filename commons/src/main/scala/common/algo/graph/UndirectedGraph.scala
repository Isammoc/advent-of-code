package common.algo.graph

import scala.collection.mutable.ListBuffer

object UndirectedGraph {
  def fromEdge[K](edges: Iterable[(K, K)]): UndirectedGraph[K] =
    UndirectedGraph((edges ++ edges.map(_.swap)).groupMapReduce(_._1)(tuple => Set(tuple._2))(_ ++ _))
}

final case class UndirectedGraph[K](_neighbors: Map[K, Set[K]]) {
  def neighbors(key: K): Set[K] = _neighbors.getOrElse(key, Set.empty)

  def nodes: Set[K] = _neighbors.keySet

  def findLargestClique: Set[K] = {
    // from https://gist.github.com/Chen-Zhe/ade6cb064fc32b9bb013a05fe482fc0b
    def bronKerbosch(R: Set[K], P_var: Set[K], X_var: Set[K], result: ListBuffer[Set[K]]): Unit = {
      var P = P_var
      var X = X_var
      if (P.isEmpty && X.isEmpty) {
        result.append(R)
      } else {
        val pivot = P.union(X).head
        for (v <- P.diff(_neighbors(pivot))) {
          val Nv = _neighbors(v)
          bronKerbosch(R + v, P.intersect(Nv), X.intersect(Nv), result)
          X += v
          P -= v
        }
      }
    }

    val result = ListBuffer[Set[K]]()
    bronKerbosch(Set.empty, nodes, Set.empty, result)

    result.maxBy(_.size)
  }
}
