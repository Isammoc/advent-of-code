package common.algo.graph
import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.collection.mutable

object Dijkstra {
  def simpleDistance[Node](
      start: Node
  )(implicit neighbors: Node => Iterable[Node]): Map[Node, Long] = {
    @tailrec
    def loop(toVisit: Queue[Node] = Queue(start), visited: Set[Node] = Set(start), result: Map[Node, Long] = Map(start -> 0)): Map[Node, Long] =
      toVisit match {
        case x +: xs =>
          val ns = neighbors(x).filterNot(visited.contains)
          loop(xs ++ ns, visited ++ ns, result ++ ns.map(_ -> (result(x) + 1)))
        case _ => result
      }
    loop().withDefault(_ => Long.MaxValue)
  }

  def reach[Node, Cost](start: Node)(target: Node)(neighbors: Node => Iterable[(Node, Cost)])(implicit Cost: Numeric[Cost]): Cost =
    reach(start, neighbors, (node: Node) => node == target)

  def reach[Cost, Node](start: Node, neighbors: Node => Iterable[(Node, Cost)], isTarget: Node => Boolean)(implicit Cost: Numeric[Cost]): Cost = {
    val queue = mutable.PriorityQueue((start, Cost.zero))(Ordering.by[(Node, Cost), Cost](_._2).reverse)

    @tailrec
    def loop(visited: Set[Node]): Cost = queue.dequeue() match {
      case (t, res) if isTarget(t)       => res
      case (v, _) if visited.contains(v) => loop(visited)
      case (current, distance) =>
        for {
          (n, w) <- neighbors(current)
        } {
          queue.enqueue(n -> Cost.plus(distance, w))
        }
        loop(visited + current)
    }
    loop(Set.empty)
  }

  def all[Cost, Node](start: Node, neighbors: Node => Iterable[(Node, Cost)])(implicit Cost: Numeric[Cost]): Map[Node, Cost] =
    all(Set(start), neighbors)

  def all[Cost, Node](start: Set[Node], neighbors: Node => Iterable[(Node, Cost)])(implicit Cost: Numeric[Cost]): Map[Node, Cost] = {
    @tailrec
    def loop(toVisit: Set[Node], result: Map[Node, Cost]): Map[Node, Cost] = if (toVisit.isEmpty) {
      result
    } else {
      val current = toVisit.minBy(result)
      val currentCost = result(current)

      val (newResult, newToVisit) = neighbors(current).foldLeft((result, List.empty[Node])) { case ((acc, nextToVisit), (nextNode, cost)) =>
        if (result.get(nextNode).exists(oldCost => Cost.compare(oldCost, Cost.plus(currentCost, cost)) <= 0))
          (acc, nextToVisit)
        else
          (acc + (nextNode -> Cost.plus(currentCost, cost)), nextNode :: nextToVisit)
      }
      loop(toVisit - current ++ newToVisit, newResult)
    }
    loop(start, start.map(_ -> Cost.zero).toMap)
  }
}
