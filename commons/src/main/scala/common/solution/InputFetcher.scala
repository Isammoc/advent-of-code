package common.solution

import java.net.URI
import java.nio.file.{Files, Path}
import scala.io.BufferedSource
import scala.util.{Try, Using}

object InputFetcher {
  val cacheDir: Path = sys.env.get("AOC_CACHE_DIR").map(str => Path.of(str)).getOrElse(Path.of(sys.props("user.home"), ".cache", "aoc"))

  val sessionKey: Option[String] = sys.env.get("AOC_SESSION_KEY")

  def filePath(year: String, day: String): Path = cacheDir.resolve(Path.of(year, day))

  def fileResource(path: Path): Option[BufferedSource] =
    if (Files.isRegularFile(path)) {
      Some(io.Source.fromURI(path.toUri))
    } else None

  def urlResource(url: String, sessionKey: String): Option[BufferedSource] = Try {
    val connection = URI.create(url).toURL.openConnection()
    connection.setRequestProperty("Cookie", s"session=${sessionKey}")
    io.Source.fromInputStream(connection.getInputStream)
  }.toOption

  def adventOfCodeResource(year: String, day: String): Option[BufferedSource] =
    sessionKey.flatMap(sessionKey => urlResource(s"https://adventofcode.com/$year/day/${day.toInt}/input", sessionKey))

  def stdinResource: BufferedSource = {
    println("No input cached and cannot connect to adventofcode site, please paste your input")
    io.Source.stdin
  }

  def apply(year: String, day: String): String = {
    val path = filePath(year, day)
    Files.createDirectories(path.getParent)

    val content = Using(fileResource(path).orElse(adventOfCodeResource(year, day)).getOrElse(stdinResource)) {
      _.getLines().mkString("\n")
    }.get

    // cache
    Files.writeString(path, content)

    content
  }
}
