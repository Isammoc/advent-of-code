package common.solution

object TimedFunction {
  def format(time: Long): String = {
    val elapsedMS = (time / 1000).toDouble / 1000
    val withoutColor = f"[$elapsedMS%10.2fms]"

    val chosenColor = if (elapsedMS < 500) {
      Console.GREEN
    } else if (elapsedMS < 1000) {
      Console.YELLOW
    } else Console.RED

    chosenColor + withoutColor + Console.RESET
  }
  def apply[A](f: => A): (String, A) = {
    val start = System.nanoTime()
    val result = f
    val end = System.nanoTime()

    (format(end - start), result)
  }
}
