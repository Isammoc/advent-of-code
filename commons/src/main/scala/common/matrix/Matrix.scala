package common.matrix

case class Matrix[N](rows: List[List[N]])(implicit N: Numeric[N]) {
  private def padTo(width: Int)(string: String): String =
    " " * (width - string.length) + string

  override def toString: String = {
    val width = rows.flatten.map(_.toString.length).max
    rows.map(_.map(_.toString).map(padTo(width)).mkString(" ")).mkString("\n")
  }

  def -(other: Matrix[N]): Matrix[N] =
    Matrix(rows.zip(other.rows).map { case (a, b) => a.zip(b).map((N.minus _).tupled) })

  def +(other: Matrix[N]): Matrix[N] =
    Matrix(rows.zip(other.rows).map { case (a, b) => a.zip(b).map((N.plus _).tupled) })

  def scalarMult(mult: N): Matrix[N] =
    Matrix(rows.map(_.map(a => N.times(a, mult))))

  def *(other: Matrix[N]): Matrix[N] = {
    val cols = other.rows.transpose
    val resultRows = rows.map(row => cols.map(col => row.zip(col).map((N.times _).tupled).sum))
    Matrix(resultRows)
  }
}
