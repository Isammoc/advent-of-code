package common.priorityqueue

object +: {

}

object PriorityQueue {

  def empty[A](implicit ord: Ordering[A]): PriorityQueue[A] = new Empty[A]

  def single[A](elem: A)(implicit ord: Ordering[A]): PriorityQueue[A] = Tree[A](1, 1, elem, empty, empty)

  private[priorityqueue] def merge[A](a: PriorityQueue[A], b: PriorityQueue[A])(implicit ord: Ordering[A]): PriorityQueue[A] = a match {
    case empty if empty.length == 0 => b
    case Tree(_, _, ae, al, ar) => b match {
      case empty if empty.length == 0 => a
      case Tree(_, _, be, bl, br) =>
        if (ord.compare(ae, be) > 0)
          makeT(ae, al, merge(ar, b))
        else
          makeT(be, bl, merge(br, a))
    }
  }

  private[priorityqueue] def makeT[A](head: A, a: PriorityQueue[A], b: PriorityQueue[A])(implicit ord: Ordering[A]): PriorityQueue[A] =
    if (a.rank >= b.rank)
      Tree(a.length + b.length + 1, b.rank + 1, head, a, b)
    else
      Tree(a.length + b.length + 1, a.rank + 1, head, b, a)

  def unapply[A](queue: PriorityQueue[A]): Option[(A, PriorityQueue[A])] =
    if (queue.length == 0) None
    else Some(queue.head -> queue.tail)
}

abstract class PriorityQueue[A](implicit val ord: Ordering[A]) {
  private[priorityqueue] def rank: Int

  def head: A

  def tail: PriorityQueue[A]

  def +(q: PriorityQueue[A]): PriorityQueue[A] =
    if (ord == q.ord)
      PriorityQueue.merge(this, q)
    else
      throw new RuntimeException("ordering should equal")

  def +(elem: A): PriorityQueue[A] = PriorityQueue.merge(this, PriorityQueue.single(elem))

  def +(iter: Iterable[A]): PriorityQueue[A] = iter.foldLeft(this)(_ + _)

  def length: Int
}

private[priorityqueue] final class Empty[A](implicit override val ord: Ordering[A])
  extends PriorityQueue[A] {
  override val length = 0
  override val rank = 0

  override def head = throw new NoSuchElementException("head on empty priority queue")

  override def tail = throw new NoSuchElementException("tail on empty priority queue")
}

private[priorityqueue] case class Tree[A](
                                           override val length: Int,
                                           override val rank: Int,
                                           override val head: A,
                                           left: PriorityQueue[A],
                                           right: PriorityQueue[A])
                                         (implicit override val ord: Ordering[A])
  extends PriorityQueue[A] {
  override def tail: PriorityQueue[A] = PriorityQueue.merge(left, right)
}
