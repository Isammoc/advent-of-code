package common.util

object Memoize {
  def memoize[I, O](f: I => O): I => O = new Memoization(Map.empty[I, O], f)

  private class Memoization[I, O](var known: Map[I, O], function: I => O) extends (I => O) {
    def apply(key: I): O = known.get(key) match {
      case Some(result) => result
      case None =>
        val result = function(key)
        known = known + (key -> result)
        result
    }
  }
}
