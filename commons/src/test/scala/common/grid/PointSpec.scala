package common.grid

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PointSpec  extends AnyWordSpec with Matchers {
  "Point" should {
    "turnRight" in {
      Point.Up.rotateRight shouldEqual Point.Right
      Point.Right.rotateRight shouldEqual Point.Down
      Point.Down.rotateRight shouldEqual Point.Left
      Point.Left.rotateRight shouldEqual Point.Up
    }

    "turnLeft" in {
      Point.Up.rotateLeft shouldEqual Point.Left
      Point.Left.rotateLeft shouldEqual Point.Down
      Point.Down.rotateLeft shouldEqual Point.Right
      Point.Right.rotateLeft shouldEqual Point.Up
    }

    "opposite" in {
      Point.Up.opposite shouldEqual Point.Down
      Point.Down.opposite shouldEqual Point.Up

      Point.Left.opposite shouldEqual Point.Right
      Point.Right.opposite shouldEqual Point.Left
    }
  }
}
