package aoc2024

import common.algo.graph.Dijkstra
import common.grid.Point
import common.util.Repeat

object day21 extends common.AocApp(Day21Solution.solution)

object Day21Solution {
  type Parsed = List[String]

  private final case class Pad(charToPoint: Map[Char, Point], points: Set[Point])

  private val Directions: List[(Point, Char)] = List(
    (Point.Up, '^'),
    (Point.Right, '>'),
    (Point.Down, 'v'),
    (Point.Left, '<')
  )

  private def stringToPad(input: String): Pad = {
    val charToPoint = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      if c != ' '
    } yield c -> Point(x, y)).toMap
    Pad(charToPoint, charToPoint.values.toSet)
  }

  private val NumericPad: Pad =
    stringToPad("""789
                  |456
                  |123
                  | 0A""".stripMargin)

  private val DirectionPad: Pad =
    stringToPad(""" ^A
                  |<v>""".stripMargin)

  private type CostMap = Map[(Char, Char), BigInt]
  private val InitialCost: CostMap = (for {
    dummy <- DirectionPad.charToPoint.keySet
    wanted <- DirectionPad.charToPoint.keySet
  } yield (dummy, wanted)).map(_ -> BigInt(1)).toMap

  private final case class Node(from: Char, current: Point)

  private def nextPad(previousCost: CostMap)(pad: Pad): CostMap = {
    for {
      (start, startPoint) <- pad.charToPoint.toList
      (end, endPoint) <- pad.charToPoint.toList
    } yield {
      (start, end) -> (if (start == end) {
                         previousCost('A' -> 'A')
                       } else {
                         Dijkstra.reach(
                           Node('A', startPoint),
                           (current: Node) =>
                             if (current.current == endPoint) List(Node('A', endPoint) -> previousCost(current.from -> 'A'))
                             else {
                               Directions
                                 .map { case (dir, c) =>
                                   Node(c, current.current + dir) -> previousCost(current.from -> c)
                                 }
                                 .filter(node => pad.points.contains(node._1.current))
                             },
                           (node: Node) => node.from == 'A' && node.current == endPoint
                         )
                       })
    }
  }.toMap

  object solution extends common.Solution[Parsed]("2024", "21") {
    override def parse(input: String): Parsed = input.split("\n").toList

    private def complexitySum(costMap: CostMap, parsed: Parsed): BigInt =
      parsed.map { line =>
        ("A" + line).zip(line).map(costMap).sum * line.filterNot(_ == 'A').toLong
      }.sum

    override def part1(parsed: Parsed): Any =
      complexitySum(nextPad(nextPad(nextPad(InitialCost)(DirectionPad))(DirectionPad))(NumericPad), parsed)

    override def part2(parsed: Parsed): Any =
      complexitySum(nextPad(Repeat(25)(InitialCost)(nextPad(_)(DirectionPad)))(NumericPad), parsed)
  }
}
