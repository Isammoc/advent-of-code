package aoc2024

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  type Parsed = List[List[Int]]

  object solution extends common.Solution[Parsed]("2024", "02") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.split("\\s+").map(_.toInt).toList)

    def isIncreasing(list: List[Int]): Boolean =
      list.zip(list.tail).forall { case (a,b) => a < b}

    def isDecreasing(list: List[Int]): Boolean =
      list.zip(list.tail).forall { case (a,b) => a > b}

    def acceptableAdjacents(list: List[Int]): Boolean =
      list.zip(list.tail).forall { case (a,b) => 1 <= (a-b).abs && (a-b).abs <= 3}

    def isSafe(list: List[Int]): Boolean =
      (isIncreasing(list) || isDecreasing(list)) && acceptableAdjacents(list)

    override def part1(parsed: Parsed): Any = parsed.count(isSafe)

    def mostlySafe(list: List[Int]): Boolean = (0 to list.size).exists{i =>
      isSafe(list.take(i) ++ list.drop(i+1))
    }

    override def part2(parsed: Parsed): Any = parsed.count(mostlySafe)
  }
}
