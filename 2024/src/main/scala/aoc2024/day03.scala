package aoc2024

import scala.annotation.tailrec

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {
  type Parsed = String

  object solution extends common.Solution[Parsed]("2024", "03") {
    val MulR = """mul\((\d+),(\d+)\)""".r

    override def parse(input: String): Parsed = input

    override def part1(parsed: Parsed): Long =
      MulR.findAllMatchIn(parsed).map{ ma =>
        ma.group(1).toLong * ma.group(2).toLong
      }.sum

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(remain: String, current: Long): Long = {
        val index = remain.indexOf("don't()")
        if(index >= 0) {
          val parsed = remain.substring(0, index)
          val after = {
            val index2 = remain.indexOf("do()", index)
            if(index2 <0) "" else remain.substring(index2)
          }
          loop(after, current + part1(parsed))
        } else {
          current + part1(remain)
        }
      }
      loop(parsed, 0)
    }
  }
}
