package aoc2024

import aoc2024.Day24Solution.Operator.{ AND, OR, XOR }

import scala.annotation.tailrec

object day24 extends common.AocApp(Day24Solution.solution)

object Day24Solution {
  sealed trait Operator {
    def on(iterable: Iterable[Int]): Int
  }
  object Operator {
    case object XOR extends Operator {
      override def on(iterable: Iterable[Int]): Int = iterable.reduce(_ ^ _)
    }
    case object AND extends Operator {
      override def on(iterable: Iterable[Int]): Int = iterable.reduce(_ & _)
    }
    case object OR extends Operator {
      override def on(iterable: Iterable[Int]): Int = iterable.reduce(_ | _)
    }
  }

  val GateR = "(.*)\\s+(.*)\\s+(.*)\\s+->\\s+(.*)".r
  final case class Gate(operands: Set[String], op: Operator, result: String) {
    private def swapResult(a: String, b: String): Gate =
      if (result == a) copy(result = b)
      else if (result == b) copy(result = a)
      else this

    def swap(a: String, b: String): Gate = swapResult(a, b)

    def alias(before: String, after: String): Gate =
      if (operands.contains(before)) copy(operands = operands - before + after)
      else if (result == before) copy(result = after)
      else this

    override def toString: String = operands.toList.sorted.mkString(s" $op ") + " -> " + result

    def level: Int =
      (for {
        text <- operands + result
        num <- text.takeRight(2).toIntOption
      } yield num).maxOption.getOrElse(Int.MaxValue)
  }

  final case class Device(wires: Map[String, Int], gates: List[Gate])
  type Parsed = Device

  object solution extends common.Solution[Parsed]("2024", "24") {
    override def parse(input: String): Parsed = {
      val Array(wires, gates) = input.split("\n\n")

      def strToOperator: String => Operator = {
        case "OR"  => Operator.OR
        case "XOR" => Operator.XOR
        case "AND" => Operator.AND
      }

      Device(
        wires = wires
          .split("\n")
          .map { line =>
            val Array(name, value) = line.split(": ")
            name -> value.toInt
          }
          .toMap,
        gates = gates.split("\n").toList.map { case GateR(left, op, right, result) =>
          Gate(Set(left, right), strToOperator(op), result)
        }
      )
    }

    def compute(parsed: Parsed): Option[BigInt] = {
      @tailrec
      def loop(remain: List[Gate], unknown: List[Gate], known: Map[String, Int]): Option[BigInt] = remain match {
        case Nil =>
          if (unknown.map(_.result).exists(_.startsWith("z"))) None
          else Some(known.toList.filter(_._1.startsWith("z")).sortBy(_._1).reverse.map(_._2).foldLeft(BigInt(0))((acc, current) => acc * 2 + current))
        case Gate(operands, op, result) :: t if operands.forall(known.contains) =>
          loop(t ++ unknown, Nil, known + (result -> op.on(operands.toList.map(known))))
        case g :: t => loop(t, g :: unknown, known)
      }
      loop(parsed.gates, Nil, parsed.wires)
    }

    override def part1(parsed: Parsed): BigInt =
      compute(parsed).get

    override def part2(parsed: Parsed): Any = {
      val found = List(
        "kth" -> "z12",
        "gsd" -> "z26",
        "tbt" -> "z32",
        "vpm" -> "qnf"
      )

      val swapped = found.foldLeft(parsed.gates) { case (acc, (a, b)) =>
        acc.map(_.swap(a, b))
      }

      val aliasAnd = (0 to 44).toList.foldLeft(swapped) { (acc, current) =>
        acc.find(gate => gate.operands == Set(f"x$current%02d", f"y$current%02d") && gate.op == AND) match {
          case None    => acc
          case Some(g) => acc.map(_.alias(g.result, f"AND$current%02d"))
        }
      }

      val aliasXor = (0 to 44).toList.foldLeft(aliasAnd) { (acc, current) =>
        acc.find(gate => gate.operands == Set(f"x$current%02d", f"y$current%02d") && gate.op == XOR) match {
          case None    => acc
          case Some(g) => acc.map(_.alias(g.result, f"XOR$current%02d"))
        }
      }

      val initiated = aliasXor.map(_.alias("AND00", "CARRY00"))

      val fin = (1 to 44).toList.foldLeft(initiated) { (acc, current) =>
        val withIntermediate = acc.find(gate => gate.operands == Set(f"XOR$current%02d", f"CARRY${current - 1}%02d") && gate.op == AND) match {
          case None    => acc
          case Some(g) => acc.map(_.alias(g.result, f"CARRY_INTERMEDIATE$current%02d"))
        }
        withIntermediate.find(gate => gate.operands == Set(f"CARRY_INTERMEDIATE$current%02d", f"AND$current%02d") && gate.op == OR) match {
          case None    => withIntermediate
          case Some(g) => withIntermediate.map(_.alias(g.result, f"CARRY$current%02d"))
        }
      }

      fin.groupBy(_.level).toList.sortBy(_._1).map(_._2.sortWith(lt).mkString("\n")).mkString("\n\n")

      (for {
        (a, b) <- found
        v <- List(a, b)
      } yield v).sorted.mkString(",")
    }

    def lt(a: Gate, b: Gate): Boolean =
      if (a.result.startsWith("AND")) true
      else if (b.result.startsWith("AND")) false
      else if (a.result.startsWith("XOR")) true
      else if (b.result.startsWith("XOR")) false
      else if (a.result.startsWith("z")) true
      else if (b.result.startsWith("z")) false
      else if (a.result.startsWith("CARRY_INTERMEDIATE")) true
      else if (b.result.startsWith("CARRY_INTERMEDIATE")) false
      else if (a.result.startsWith("CARRY")) true
      else if (b.result.startsWith("CARRY")) false
      else false
  }
}
