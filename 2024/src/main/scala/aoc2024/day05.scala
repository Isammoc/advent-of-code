package aoc2024

import scala.annotation.tailrec

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  final case class Rule(left: Int, right: Int)
  final case class Parsed(mustBeBefore: Map[Int, Set[Int]], updates: List[List[Int]]) {
    @tailrec
    private def isValidLoop(remain: List[Int], seen: Set[Int]): Boolean = remain match {
      case Nil => true
      case h :: t =>
        if(mustBeBefore.get(h).exists(_.exists(seen))) {
          false
        } else {
          isValidLoop(t, seen + h)
        }
    }

    def isValid(updates: List[Int]) = isValidLoop(updates, Set.empty)
  }


  object solution extends common.Solution[Parsed]("2024", "05") {
    override def parse(input: String): Parsed = {
      val Array(rulesLines, updatesLines) = input.split("\\n\\n")

      Parsed(
        mustBeBefore = rulesLines.split("\n").toList.map {
          case s"$a|$b" => Rule(a.toInt, b.toInt)
        }.groupMapReduce(_.left)(rule => Set(rule.right))(_ ++ _),
        updates = updatesLines.split("\n").toList.map(_.split(",").map(_.toInt).toList)
      )
    }

    override def part1(parsed: Parsed): Any = {
      parsed.updates.filter(parsed.isValid).map{update => update.drop(update.size/2).head}.sum
    }

    override def part2(parsed: Parsed): Any = {
      val incorrectlyOrderedUpdates = parsed.updates.filterNot(parsed.isValid)

      def sortLoop(remain: Set[Int], placed: List[Int]): Int = if(remain.isEmpty) placed.drop(placed.size/2).head
      else {
        val next = remain.find{ current =>
          parsed.mustBeBefore.get(current).forall(toTest => !remain.exists(toTest))
        }.get
        sortLoop(remain - next, next::placed)
      }

      incorrectlyOrderedUpdates.map(update => sortLoop(update.toSet, Nil)).sum
    }
  }
}
