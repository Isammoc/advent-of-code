package aoc2024

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed = List[List[Int]]

  object solution extends common.Solution[Parsed]("2024", "01") {
    override def parse(input: String): Parsed = input.split("\n").toList.map{ line =>
       line.split("\\s+").map(_.toInt)
    }.transpose

    override def part1(parsed: Parsed): Any = {
      parsed.head.sorted.zip(parsed.tail.head.sorted).map { case (a,b) => (a-b).abs}.sum
    }

    override def part2(parsed: Parsed): Any = {
      val left = parsed.head.groupMapReduce(identity)(_ => 1)(_ + _)
      val right = parsed.tail.head.groupMapReduce(identity)(_ => 1)(_ + _)

      val common = left.keySet.intersect(right.keySet)

      common.map{ k =>
        k * left(k) * right(k)
      }.sum
    }
  }
}
