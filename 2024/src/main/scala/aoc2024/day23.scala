package aoc2024

import common.algo.graph.UndirectedGraph

object day23 extends common.AocApp(Day23Solution.solution)

object Day23Solution {
  type Parsed = UndirectedGraph[String]

  object solution extends common.Solution[Parsed]("2024", "23") {
    override def parse(input: String): Parsed =
      UndirectedGraph.fromEdge(input.split("\n").map { input =>
        val Array(a, b) = input.split("-")
        a -> b
      })

    override def part1(parsed: Parsed): Any =
      (for {
        first <- parsed.nodes.filter(_.startsWith("t"))
        second <- parsed.neighbors(first)
        third <- parsed.neighbors(first)
        if parsed.neighbors(second).contains(third)
      } yield Set(first, second, third)).size

    override def part2(parsed: Parsed): Any =
      parsed.findLargestClique.toList.sorted.mkString(",")
  }
}
