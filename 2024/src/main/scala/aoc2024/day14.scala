package aoc2024

import common.grid.Point
import common.util.Repeat

import scala.annotation.tailrec

object day14 extends common.AocApp(Day14Solution.solution)

object Day14Solution {
  final case class Robot(pos: Point, velocity: Point) {
    def move: Robot = Robot(pos + velocity, velocity)
  }
  type Parsed = List[Robot]

  object solution extends common.Solution[Parsed]("2024", "14") {
    override def parse(input: String): Parsed = input
      .split("\n")
      .map { case s"p=${px},${py} v=${vx},${vy}" =>
        Robot(Point(px.toInt, py.toInt), Point(vx.toInt, vy.toInt))
      }
      .toList

    def productByQuadrant(wide: Int, height: Int)(parsed: Parsed): Long = {
      def resizeMap(robot: Robot): Robot = robot.copy(pos = Point((robot.pos.x + wide) % wide, (robot.pos.y + height) % height))

      val results = Repeat(100)(parsed)(_.map(_.move).map(resizeMap))

      val x = wide / 2
      val y = height / 2

      val nw = results.count(robot => robot.pos.x < x && robot.pos.y < y)
      val ne = results.count(robot => robot.pos.x > x && robot.pos.y < y)
      val sw = results.count(robot => robot.pos.x < x && robot.pos.y > y)
      val se = results.count(robot => robot.pos.x > x && robot.pos.y > y)

      nw * ne * sw * se
    }

    override def part1(parsed: Parsed): Any = productByQuadrant(101, 103)(parsed)
    def part1_test(parsed: Parsed): Long = productByQuadrant(11, 7)(parsed)

    def countClose(robots: Parsed): Int = {
      val robotPos = robots.map(_.pos).toSet

      robots.map(_.pos).count(ref => Point.OrthogonalDirections.map(_ + ref).exists(robotPos))
    }

    override def part2(parsed: Parsed): Any = {
      val wide = 101
      val height = 103

      def display(robots: Parsed): Unit = println((for {
        y <- 0 until height
      } yield (for {
        x <- 0 until wide
      } yield {
        val count = robots.count(_.pos == Point(x, y))
        if (count == 0) " "
        else "#"
      }).mkString("")).mkString("\n"))

      def resizeMap(robot: Robot): Robot = robot.copy(pos = Point((robot.pos.x + wide) % wide, (robot.pos.y + height) % height))

      @tailrec
      def loop(i: Int, current: List[Robot]): Int =
        if (countClose(current) > (current.size / 2)) {
          display(current)
          i
        } else {
          loop(i + 1, current.map(_.move).map(resizeMap))
        }

      loop(0, parsed)
    }
  }
}
