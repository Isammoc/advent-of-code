package aoc2024

import scala.annotation.tailrec

object day17 extends common.AocApp(Day17Solution.solution)

object Day17Solution {
  object Computer {
    object Instruction {
      val Adv: Long = 0
      val Bxl: Long = 1
      val Bst: Long = 2
      val Jnz: Long = 3
      val Bxc: Long = 4
      val Out: Long = 5
      val Bdv: Long = 6
      val Cdv: Long = 7
    }

    @tailrec
    def nextOutput(computer: Computer): Option[(Long, Computer)] =
      if (computer.isFinished) None
      else {
        val (next, output) = computer.next
        output match {
          case Some(out) => Some((out, next))
          case _         => nextOutput(next)
        }
      }

    def lazyRun(computer: Computer): LazyList[Long] = LazyList.unfold(computer)(nextOutput)

    def run(computer: Computer): (Computer, List[Long]) = {
      @tailrec
      def loop(current: Computer, reversedOutput: List[Long]): (Computer, List[Long]) =
        if (current.isFinished) (current, reversedOutput.reverse)
        else {
          val (next, out) = current.next
          loop(next, out.fold(reversedOutput)(_ :: reversedOutput))
        }
      loop(computer, Nil)
    }
  }
  final case class Computer(a: Long, b: Long, c: Long, ip: Int, instructions: Array[Long]) {
    import Computer._
    import Instruction._

    def isFinished: Boolean = ip < 0 || ip >= (instructions.length - 1)

    def combo(v: Long): Long = v match {
      case 0L => 0L
      case 1L => 1L
      case 2L => 2L
      case 3L => 3L
      case 4L => a
      case 5L => b
      case 6L => c
    }

    def next: (Computer, Option[Long]) = if (isFinished) this -> None
    else {
      (instructions(ip), instructions(ip + 1)) match {
        case (Adv, v) => Computer(a >> combo(v).toInt, b, c, ip + 2, instructions) -> None
        case (Bxl, v) => Computer(a, b ^ v, c, ip + 2, instructions) -> None
        case (Bst, v) => Computer(a, combo(v) % 8, c, ip + 2, instructions) -> None
        case (Jnz, v) => (if (a == 0) Computer(a, b, c, ip + 2, instructions) else Computer(a, b, c, v.toInt, instructions)) -> None
        case (Bxc, _) => Computer(a, b ^ c, c, ip + 2, instructions) -> None
        case (Out, v) => Computer(a, b, c, ip + 2, instructions) -> Some(combo(v) % 8)
        case (Bdv, v) => Computer(a, a >> combo(v).toInt, c, ip + 2, instructions) -> None
        case (Cdv, v) => Computer(a, b, a >> combo(v).toInt, ip + 2, instructions) -> None
      }
    }
  }
  type Parsed = Computer

  object solution extends common.Solution[Parsed]("2024", "17") {
    override def parse(input: String): Parsed = {
      val Array(a, b, c, _, program) = input.split("\n")
      Computer(
        a.split(": ")(1).toLong,
        b.split(": ")(1).toLong,
        c.split(": ")(1).toLong,
        0,
        program.split(": ")(1).split(",").map(_.toLong)
      )
    }

    override def part1(parsed: Parsed): Any = Computer.run(parsed)._2.mkString(",")

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Long, toFound: LazyList[Long]): Long =
        if (Computer.lazyRun(parsed.copy(a = current)) == toFound) {
          if (toFound.size == parsed.instructions.length) current
          else {
            loop(current << 3, LazyList.from(parsed.instructions.drop(parsed.instructions.length - (toFound.size + 1))))
          }
        } else {
          loop(current + 1, toFound)
        }

      loop(0, LazyList.from(parsed.instructions.drop(parsed.instructions.length - 1)))
    }
  }
}
