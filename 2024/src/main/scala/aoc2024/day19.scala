package aoc2024

import scala.annotation.tailrec

object day19 extends common.AocApp(Day19Solution.solution)

object Day19Solution {
  final case class Parsed(towels: Set[String], wanted: List[String]) {
    def countArrangement(wanted: String): Long = {
      @tailrec
      def loop(current: Int, result: Map[String, Long]): Long = if (current >= wanted.length) result.getOrElse(wanted, 0L)
      else {
        val currentPossible = result.getOrElse(wanted.take(current), 0L)

        loop(
          current + 1,
          towels
            .map(wanted.take(current) + _)
            .foldLeft(result) { (acc, current) =>
              acc.updatedWith(current)(opt => Some(opt.getOrElse(0L) + currentPossible))
            }
        )
      }

      loop(0, Map("" -> 1))
    }
  }

  object solution extends common.Solution[Parsed]("2024", "19") {
    override def parse(input: String): Parsed = {
      val Array(towels, wanted) = input.split("\n\n")

      Parsed(towels.split(", ").toSet, wanted.split("\n").toList)
    }

    override def part1(parsed: Parsed): Any = parsed.wanted.count(wanted => parsed.countArrangement(wanted) != 0)

    override def part2(parsed: Parsed): Any = parsed.wanted.map(parsed.countArrangement).sum
  }
}
