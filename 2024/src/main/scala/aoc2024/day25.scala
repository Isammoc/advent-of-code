package aoc2024

object day25 extends common.AocApp(Day25Solution.solution)

object Day25Solution {
  final case class LockKeys(locks: List[List[Int]], keys: List[List[Int]])

  type Parsed = LockKeys

  object solution extends common.Solution[Parsed]("2024", "25") {
    override def parse(input: String): Parsed = {
      val (locks, keys) = input.split("\n\n").toList.partition(_(0) == '#')

      LockKeys(
        locks = locks.map(_.split("\n").toList.transpose.map(_.count(_ == '#'))),
        keys = keys.map(_.split("\n").toList.transpose.map(_.count(_ == '#')))
      )
    }

    override def part1(parsed: Parsed): Any =
      parsed.locks.map { lock =>
        parsed.keys.count {
          _.zip(lock).forall { case (a, b) =>
            a + b <= 7
          }
        }
      }.sum

    override def part2(parsed: Parsed): Any = "NOT IMPLEMENTED"
  }
}
