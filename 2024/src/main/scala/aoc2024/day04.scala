package aoc2024

import common.grid.Point

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {
  type Parsed = Map[Point, Char]

  object solution extends common.Solution[Parsed]("2024", "04") {
    override def parse(input: String): Parsed =
      (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x,y) -> c).toMap

    private val AllDirections = List(
      Point(-1,-1),
      Point(-1,0),
      Point(-1,1),
      Point(0,1),
      Point(0,-1),
      Point(1,-1),
      Point(1,0),
      Point(1,1)
    )

    private val AllDiagonals = List(
      Point(-1,-1),
      Point(-1,1),
      Point(1,-1),
      Point(1,1)
    )

    override def part1(parsed: Parsed): Any = {
      def wordFor(start: Point, direction: Point): String =
        LazyList.unfold(start)(pos => parsed.get(pos).map(c => (c -> (pos + direction)))).take(4).mkString("")

      (for {
        start <- parsed.keys.toList
        dir <- AllDirections
      } yield wordFor(start, dir)).count(_ == "XMAS")
    }

    override def part2(parsed: Parsed): Any = {
      val results = (for {
        start <- parsed.keys.toList if parsed(start) == 'A'
      } yield {
        AllDiagonals.count{ dir =>
          parsed.get(start + dir).contains('M') &&
            parsed.get(start + dir.rotateRight).contains('M') &&
            parsed.get(start + dir.opposite).contains('S') &&
            parsed.get(start + dir.rotateLeft).contains('S')
        }
      }).groupMapReduce(identity)(Function.const(1))(_ + _)

      results(1)
    }
  }
}
