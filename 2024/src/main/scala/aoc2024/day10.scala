package aoc2024

import common.grid.Point

import scala.annotation.tailrec

object day10 extends common.AocApp(Day10Solution.solution)

object Day10Solution {
  type Parsed = Map[Char, Set[Point]]

  object solution extends common.Solution[Parsed]("2024", "10") {
    override def parse(input: String): Parsed = (
      for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield c->Set(Point(x,y))
    ).groupMapReduce(_._1)(_._2)(_ ++ _)

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Char, trails: Map[Point, Set[Point]]): Int =
        if(current == '0') {
          trails.values.map(_.size).sum
        } else {
          val next = (current - 1).toChar
          val nextTrails = (for {
            toConsider <- parsed(next)
          } yield toConsider -> Point.OrthogonalDirections.toSet.map(toConsider + _).flatMap(trails.get).flatten).toMap
          loop(next, nextTrails)
        }

      loop('9', parsed('9').map(i => i -> Set(i)).toMap)
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Char, trails: Map[Point, Int]): Int =
        if(current == '0') {
          trails.values.sum
        } else {
          val next = (current - 1).toChar
          val nextTrails = (for {
            toConsider <- parsed(next)
          } yield toConsider -> Point.OrthogonalDirections.map(toConsider + _).map(trails.getOrElse(_, 0)).sum).toMap
          loop(next, nextTrails)
        }

      loop('9', parsed('9').map(_ -> 1).toMap)
    }
  }
}
