package aoc2024

import common.grid.Point

import scala.annotation.tailrec

object day06 extends common.AocApp(Day06Solution.solution)

object Day06Solution {
  final case class Guard(pos: Point, facing: Point) {
    def forward: Guard = Guard(pos = pos + facing, facing = facing)
    def turnRight: Guard = Guard(pos, facing.rotateRight)
  }
  final case class Parsed(area: Map[Point, Char], start: Guard) {
    private def moveGuard(current: Guard) =
      if(area.get(current.forward.pos).contains('#')) {
        current.turnRight
      } else {
        current.forward
      }

    lazy val visited: Set[Point] = {
      @tailrec
      def loop(current: Guard, visited: Set[Point]): Set[Point] =
        area.get(current.pos) match {
          case None => visited
          case _ =>
            loop(moveGuard(current), visited + current.pos)
        }

      loop(start, Set.empty)
    }

    def isLoop: Boolean = {
      @tailrec
      def loop(current: Guard, visited: Set[Guard]): Boolean = {
        if(visited.contains(current)) true
        else if (area.contains(current.pos)) loop(moveGuard(current), visited + current)
        else false
      }
      loop(start, Set.empty)
    }

    def withNewObstacle(newObstacle: Point): Parsed = this.copy(area = area + (newObstacle -> '#'))
  }

  object solution extends common.Solution[Parsed]("2024", "06") {
    override def parse(input: String): Parsed = {
      val area = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x,y) -> c).toMap

      val startPos = area.find(_._2 == '^').get._1

      Parsed(
        area + (startPos -> '.'),
        Guard(startPos, Point.Up)
      )
    }

    override def part1(parsed: Parsed): Any = parsed.visited.size

    override def part2(parsed: Parsed): Any = (parsed.visited - parsed.start.pos).count{ newObstacle =>
        parsed.withNewObstacle(newObstacle).isLoop
      }
  }
}
