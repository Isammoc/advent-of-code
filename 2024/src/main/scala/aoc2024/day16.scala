package aoc2024

import common.algo.graph.Dijkstra
import common.grid.Point

object day16 extends common.AocApp(Day16Solution.solution)

object Day16Solution {
  final case class Maze(walls: Set[Point], start: Point, end: Point)
  final case class Reindeer(pos: Point, facing: Point) {
    def rotateRight: Reindeer = copy(facing = facing.rotateRight)
    def rotateLeft: Reindeer = copy(facing = facing.rotateLeft)
    def forward: Reindeer = copy(pos = pos + facing)
    def backward: Reindeer = copy(pos = pos - facing)

    def neighboors(walls: Set[Point]): List[(Reindeer, Int)] = List(
      Some((rotateRight, 1000)),
      Some((rotateLeft, 1000)),
      Some(forward).filterNot(reindeer => walls.contains(reindeer.pos)).map(_ -> 1)
    ).flatten

    def ancestors(walls: Set[Point]): List[(Reindeer, Int)] = List(
      Some((rotateRight, 1000)),
      Some((rotateLeft, 1000)),
      Some(backward).filterNot(reindeer => walls.contains(reindeer.pos)).map(_ -> 1)
    ).flatten
  }
  type Parsed = Maze

  object solution extends common.Solution[Parsed]("2024", "16") {
    override def parse(input: String): Parsed = {
      val all = for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c
      Maze(all.filter(_._2 == '#').map(_._1).toSet, all.find(_._2 == 'S').get._1, all.find(_._2 == 'E').get._1)
    }

    override def part1(parsed: Parsed): Int =
      Dijkstra.reach[Int, Reindeer](Reindeer(parsed.start, Point.Right), (_: Reindeer).neighboors(parsed.walls), (_: Reindeer).pos == parsed.end)

    override def part2(parsed: Parsed): Any = {
      val minimumToReach = Dijkstra.all(Point.OrthogonalDirections.map(Reindeer(parsed.end, _)).toSet, (_: Reindeer).ancestors(parsed.walls))

      val fromStart = Dijkstra.all(Reindeer(parsed.start, Point.Right), (_: Reindeer).neighboors(parsed.walls))

      val realCost = fromStart.map { case (reindeer, cost) =>
        reindeer -> (cost + minimumToReach(reindeer))
      }

      val min = realCost.values.min

      realCost.filter(_._2 == min).keySet.map(_.pos).size
    }
  }
}
