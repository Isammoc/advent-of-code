package aoc2024

import common.algo.integers.GCD
import common.grid.Point

import scala.annotation.tailrec

object day08 extends common.AocApp(Day08Solution.solution)

object Day08Solution {
  implicit class QuotientPoint(thiz: Point) {
    def smallest: Point = {
      val gcd = GCD(thiz.x, thiz.y).toInt
      Point(thiz.x / gcd, thiz.y / gcd)
    }
  }

  case class Area(antennas: Map[Char, Set[Point]], max: Point)
  type Parsed = Area

  object solution extends common.Solution[Parsed]("2024", "08") {
    def isInArea(parsed: Parsed): Point => Boolean = {
      case Point(x, y) =>
        0 <= x && x <= parsed.max.x &&
        0 <= y && y <= parsed.max.y
    }

    def computeAntinodesPair(one: Point, another: Point): Set[Point] = Set(
      one + (another - one) * 2,
      another + (one - another) * 2
    )

    def computeAntinodesLine(parsed: Parsed)(one: Point, another: Point): Set[Point] = {
      val vector = (one - another).smallest

      LazyList.iterate(one)(_ + vector).takeWhile(isInArea(parsed)).toSet ++
        LazyList.iterate(one)(_ - vector).takeWhile(isInArea(parsed))
    }

    def computeAllAntinodes(antinodesFor: (Point, Point) => Set[Point])(antennas: Set[Point]) = {
      @tailrec
      def loop(antennas: Set[Point], antinodes: Set[Point]): Set[Point] =
        if (antennas.isEmpty) antinodes
        else {
          val one = antennas.head
          val others = antennas - one

          val newAntinodes = for {
            other <- others
            antinode <- antinodesFor(one, other)
          } yield antinode

          loop(others, antinodes ++ newAntinodes)
        }
      loop(antennas, Set.empty)
    }

    override def parse(input: String): Area = {
      val antennas = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c).groupMapReduce(_._2) { case (p, _) => Set(p) }(_ ++ _)

      Area(
        antennas,
        Point(
          antennas.values.flatten.map(_.x).max,
          antennas.values.flatten.map(_.y).max
        )
      )
    }

    override def part1(parsed: Parsed): Any = (for {
      (c, antennas) <- parsed.antennas if c != '.'
      antinode <- computeAllAntinodes(computeAntinodesPair)(antennas)
      if isInArea(parsed)(antinode)
    } yield antinode).toSet.size

    override def part2(parsed: Parsed): Any = (for {
      (c, antennas) <- parsed.antennas if c != '.'
      antinode <- computeAllAntinodes(computeAntinodesLine(parsed))(antennas)
    } yield antinode).toSet.size
  }
}
