package aoc2024

import scala.annotation.tailrec

object day09 extends common.AocApp(Day09Solution.solution)

object Day09Solution {
  final case class File(id: Int, size: Int)
  type Parsed = List[File]

  object solution extends common.Solution[Parsed]("2024", "09") {
    private def checksum(diskMap: List[File]): Long = {
      @tailrec
      def loop(index: Int, remain: List[File], result: Long): Long = remain match {
        case Nil                 => result
        case File(_, 0) :: t     => loop(index, t, result)
        case File(-1, size) :: t => loop(index + size, t, result)
        case File(id, size) :: t => loop(index + 1, File(id, size - 1) :: t, result + id * index)
      }
      loop(0, diskMap, 0)
    }

    override def parse(input: String): Parsed = for {
      (size, index) <- input.map(_ - '0').toList.zipWithIndex
    } yield File(id = if (index % 2 == 0) index / 2 else -1, size = size)

    override def part1(parsed: Parsed): Any = {
      val DefragmentedSize = parsed.zipWithIndex.filter(_._2 % 2 == 0).map(_._1.size).sum

      @tailrec
      def loop(index: Int, frontRemain: List[File], backRemain: List[File], reverseResult: List[File]): List[File] =
        if (index >= DefragmentedSize) reverseResult.reverse
        else {
          (frontRemain, backRemain) match {
            case (File(_, 0) :: t, _)  => loop(index, t, backRemain, reverseResult)
            case (_, File(_, 0) :: t)  => loop(index, frontRemain, t, reverseResult)
            case (_, File(-1, _) :: t) => loop(index, frontRemain, t, reverseResult)
            case (File(-1, sizeFront) :: t, File(id, sizeBack) :: t2) if sizeFront >= sizeBack =>
              loop(index + sizeBack, File(-1, sizeFront - sizeBack) :: t, t2, File(id, sizeBack) :: reverseResult)
            case (File(-1, sizeFront) :: t, File(id, sizeBack) :: t2) =>
              loop(index + sizeFront, t, File(id, sizeBack - sizeFront) :: t2, File(id, sizeFront) :: reverseResult)
            case (file :: t, _) if index + file.size >= DefragmentedSize =>
              loop(index + file.size, t, backRemain, File(file.id, DefragmentedSize - index) :: reverseResult)
            case (file :: t, _) => loop(index + file.size, t, backRemain, file :: reverseResult)
          }
        }

      checksum(loop(0, parsed, parsed.reverse, Nil))
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def glueEmpties(remain: List[File], reverseResult: List[File]): List[File] = remain match {
        case Nil                                     => reverseResult.reverse
        case File(-1, size1) :: File(-1, size2) :: t => glueEmpties(File(-1, size1 + size2) :: t, reverseResult)
        case h :: t                                  => glueEmpties(t, h :: reverseResult)
      }

      @tailrec
      def loop(current: List[File], idToMove: Int): List[File] =
        if (idToMove <= 0) {
          current
        } else {
          val currentPlace = current.indexWhere(_.id == idToMove, 0)
          val fileToMove = current.find(_.id == idToMove).get
          val possiblePlace = current.indexWhere(file => file.id == -1 && file.size >= fileToMove.size, 0)
          if (possiblePlace != -1 && possiblePlace < currentPlace) {
            val (before, empty :: inside) = current.splitAt(possiblePlace)

            val replaced =
              if (empty.size == fileToMove.size) fileToMove :: Nil
              else fileToMove :: File(-1, empty.size - fileToMove.size) :: Nil

            val after = glueEmpties(
              inside.map {
                case `fileToMove` => File(-1, fileToMove.size)
                case other        => other
              },
              Nil
            )

            loop(before ++ replaced ++ after, idToMove - 1)
          } else {
            loop(current, idToMove - 1)
          }
        }

      checksum(loop(parsed, parsed.map(_.id).max))
    }
  }
}
