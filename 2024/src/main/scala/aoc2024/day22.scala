package aoc2024

import common.util.Repeat

import scala.annotation.tailrec

object day22 extends common.AocApp(Day22Solution.solution)

object Day22Solution {
  type Parsed = List[Long]

  object solution extends common.Solution[Parsed]("2024", "22") {
    def nextSecret(secret: Long): Long = {
      val secret2 = ((secret * 64) ^ secret)     % 16777216
      val secret3 = ((secret2 / 32) ^ secret2)   % 16777216
      val secret4 = ((secret3 * 2048) ^ secret3) % 16777216
      secret4
    }

    override def parse(input: String): Parsed = input.split("\n").map(_.toLong).toList

    override def part1(parsed: Parsed): Any =
      parsed.map(line => Repeat(2000)(line)(nextSecret)).sum

    def toSequenceWin(input: Long): LazyList[((Long, Long, Long, Long), Long)] = {
      val prices = LazyList.iterate(input)(nextSecret).take(2000).map(_ % 10)

      val changes = prices.zip(prices.tail).map { case (a, b) =>
        (b - a, b)
      }

      changes.zip(changes.drop(1)).zip(changes.drop(2).zip(changes.drop(3))).map { case (((a, _), (b, _)), ((c, _), (d, p))) =>
        (a, b, c, d) -> p
      }
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(
          remain: List[Long],
          currentWin: LazyList[((Long, Long, Long, Long), Long)],
          maxUntilNow: Long,
          currentVisited: Set[(Long, Long, Long, Long)],
          values: Map[(Long, Long, Long, Long), Long]
      ): Long =
        currentWin match {
          case l if l.isEmpty =>
            remain match {
              case Nil    => maxUntilNow
              case h :: t => loop(t, toSequenceWin(h), maxUntilNow, Set.empty, values)
            }
          case (h, _) #:: t if currentVisited.contains(h) => loop(remain, t, maxUntilNow, currentVisited, values)
          case (h, v) #:: t =>
            val nextValue = v + values.getOrElse(h, 0L)
            loop(remain, t, math.max(maxUntilNow, nextValue), currentVisited + h, values + (h -> nextValue))
        }

      loop(parsed, LazyList.empty, -20, Set.empty, Map.empty)
    }
  }
}
