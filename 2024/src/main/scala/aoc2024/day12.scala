package aoc2024

import common.grid.Point

import scala.annotation.tailrec

object day12 extends common.AocApp(Day12Solution.solution)

object Day12Solution {
  type Parsed = List[Set[Point]]

  object solution extends common.Solution[Parsed]("2024", "12") {
    override def parse(input: String): Parsed = {
      val initial = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c).toMap

      def findRegion(start: Point): Set[Point] = {
        val wanted = initial(start)
        @tailrec
        def innerLoop(toVisit: List[Point], result: Set[Point]): Set[Point] = toVisit match {
          case Nil                                        => result
          case h :: t if !initial.get(h).contains(wanted) => innerLoop(t, result)
          case h :: t if result(h)                        => innerLoop(t, result)
          case h :: t                                     => innerLoop(Point.OrthogonalDirections.map(h + _) ++ t, result + h)
        }
        innerLoop(List(start), Set.empty)
      }

      @tailrec
      def loop(toVisit: List[Point], visited: Set[Point], result: List[Set[Point]]): List[Set[Point]] = toVisit match {
        case Nil                  => result
        case h :: t if visited(h) => loop(t, visited, result)
        case h :: t =>
          val region = findRegion(h)
          loop(t, visited ++ region, region :: result)
      }

      loop(initial.keys.toList, Set.empty, Nil)
    }

    override def part1(parsed: Parsed): Any =
      parsed.map { points =>
        val perimeter = points.toList.map { point =>
          Point.OrthogonalDirections.count(dir => !points.contains(dir + point))
        }.sum
        perimeter * points.size
      }.sum

    override def part2(parsed: Parsed): Any = {
      def numberOfSides(points: Set[Point]): Int = {
        val minX = points.map(_.x).min
        val maxX = points.map(_.x).max
        val minY = points.map(_.y).min
        val maxY = points.map(_.y).max

        (for {
          y <- (minY - 1) to maxY
          x <- (minX - 1) to maxX
        } yield Point(x, y)).map { p =>
          List(p, p + Point.Right, p + Point.Right + Point.Down, p + Point.Down).count(points) match {
            case 0 => 0
            case 1 => 1
            case 2 => if (points.contains(p) == points.contains(p + Point.Right + Point.Down)) 2 else 0
            case 3 => 1
            case 4 => 0
          }
        }.sum
      }

      parsed.map { points =>
        points.size * numberOfSides(points)
      }.sum
    }
  }
}
