package aoc2024

object day13 extends common.AocApp(Day13Solution.solution)

object Day13Solution {
  case class Point(x: BigInt, y: BigInt) {
    def +(that: Point): Point = Point(x + that.x, y + that.y)

    def *(size: BigInt): Point = Point(x * size, y * size)
  }

  final case class Claw(a: Point, b: Point, prize: Point)
  type Parsed = List[Claw]

  object solution extends common.Solution[Parsed]("2024", "13") {
    private val ClawR = "Button\\s+\\w:\\s+X\\+(\\d+),\\s+Y\\+(\\d+)".r
    private val PrizeR = "Prize:\\s+X=(\\d+),\\s+Y=(\\d+)".r

    override def parse(input: String): Parsed = {
      input.split("\n\n").map { claw =>
        val Array(buttonA, buttonB, prize) = claw.split("\n").map {
          case ClawR(x, y)  => Point(x.toInt, y.toInt)
          case PrizeR(x, y) => Point(x.toInt, y.toInt)
        }
        Claw(buttonA, buttonB, prize)
      }
    }.toList

    def result(claw: Claw): (BigInt, BigInt) = {
      val maybeA = (claw.prize.y * claw.b.x - claw.prize.x * claw.b.y) / (claw.a.y * claw.b.x - claw.a.x * claw.b.y)
      val maybeB = (claw.prize.x - claw.a.x * maybeA) / claw.b.x

      if (maybeA * claw.a.x + maybeB * claw.b.x == claw.prize.x && maybeA * claw.a.y + maybeB * claw.b.y == claw.prize.y) {
        (maybeA, maybeB)
      } else
        (0, 0)
    }

    override def part1(parsed: Parsed): Any =
      parsed.map { claw =>
        val (a, b) = result(claw)
        if (a <= 100 && b <= 100) a * 3 + b
        else BigInt(0)
      }.sum

    override def part2(parsed: Parsed): Any = {
      val l = 10000000000000L
      parsed
        .map(claw => claw.copy(prize = claw.prize + Point(l, l)))
        .map { claw =>
          val (a, b) = result(claw)
          a * 3 + b
        }
        .sum
    }
  }
}
