package aoc2024

import common.algo.graph.Dijkstra
import common.grid.Point

object day20 extends common.AocApp(Day20Solution.solution)

object Day20Solution {
  final case class RaceTrack(path: Set[Point], start: Point, end: Point) {
    def neighbors(point: Point): List[Point] = Point.OrthogonalDirections.map(_ + point).filter(path)
    lazy val fromStart: Map[Point, Long] = Dijkstra.simpleDistance(start)(neighbors)
    lazy val fromEnd: Map[Point, Long] = Dijkstra.simpleDistance(end)(neighbors)
    def cost: Long = Dijkstra.reach(start)(end)(neighbors(_).map(_ -> 1))
  }
  type Parsed = RaceTrack

  object solution extends common.Solution[Parsed]("2024", "20") {
    override def parse(input: String): Parsed = {
      val iter = for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c

      RaceTrack(
        path = iter.filterNot(_._2 == '#').map(_._1).toSet,
        start = iter.find(_._2 == 'S').get._1,
        end = iter.find(_._2 == 'E').get._1
      )
    }

    def part1(parsed: Parsed, threshold: Int): Int = {
      val total = parsed.fromStart(parsed.end)
      val toBeat = total - threshold

      (for {
        s <- parsed.path
        w1 <- Point.OrthogonalDirections.map(s + _) if !parsed.path.contains(w1)
        s2 <- Point.OrthogonalDirections.map(w1 + _) if parsed.path.contains(s2)
        if parsed.fromStart(s) + 1 + parsed.fromEnd(s2) <= toBeat
      } yield (s, s2)).size
    }

    override def part1(parsed: Parsed): Any = part1(parsed, 100)

    def part2(parsed: Parsed, threshold: Int): Int = {
      val total = parsed.fromStart(parsed.end)
      val toBeat = total - threshold

      (for {
        s <- parsed.path
        s2 <- parsed.path
        if s.manhattan(s2) <= 20
      } yield (s, s2)).count { case (s, s2) =>
        parsed.fromStart(s) + s.manhattan(s2) + parsed.fromEnd(s2) <= toBeat
      }
    }

    override def part2(parsed: Parsed): Any = part2(parsed, 100)
  }
}
