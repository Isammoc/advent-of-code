package aoc2024

import common.algo.graph.Dijkstra
import common.grid.Point

import scala.annotation.tailrec

object day18 extends common.AocApp(Day18Solution.solution)

object Day18Solution {
  type Parsed = List[Point]

  object solution extends common.Solution[Parsed]("2024", "18") {
    override def parse(input: String): Parsed =
      input.split("\n").toList.map { case s"$a,$b" =>
        Point(a.toInt, b.toInt)
      }

    def part1(parsed: Parsed, fallen: Int, max: Int): Any = {
      val walls = parsed.take(fallen).toSet

      Dijkstra.reach(Point(0, 0))(Point(max, max)) { point =>
        Point.OrthogonalDirections
          .map(_ + point)
          .filterNot(walls)
          .filter { p =>
            0 <= p.x && p.x <= max && 0 <= p.y && p.y <= max
          }
          .map(_ -> 1)
      }
    }

    override def part1(parsed: Parsed): Any = part1(parsed, 1024, 70)

    def part2(parsed: Parsed, max: Int): String = {
      @tailrec
      def loop(good: Int, bad: Int): String =
        if (bad - good == 1) {
          val head = parsed.drop(good).head
          "" + head.x + "," + head.y
        } else {
          val current = good + (bad - good) / 2
          val walls = parsed.take(current).toSet

          if (
            Dijkstra.simpleDistance(Point(0, 0)) { point =>
              Point.OrthogonalDirections.map(_ + point).filterNot(walls).filter { p =>
                0 <= p.x && p.x <= max && 0 <= p.y && p.y <= max
              }
            }(Point(max, max)) == Long.MaxValue
          ) {
            loop(good, current)
          } else {
            loop(current, bad)
          }
        }

      loop(0, parsed.size)
    }

    override def part2(parsed: Parsed): Any = part2(parsed, 70)
  }
}
