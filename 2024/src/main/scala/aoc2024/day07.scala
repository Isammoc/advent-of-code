package aoc2024

import scala.annotation.tailrec

object day07 extends common.AocApp(Day07Solution.solution)

object Day07Solution {
  final case class Calibration(testValue: BigInt, values: List[BigInt])
  type Parsed = List[Calibration]

  object solution extends common.Solution[Parsed]("2024", "07") {
    override def parse(input: String): Parsed = input.split("\n").toList.map { line =>
      val Array(testValue, values) = line.split(": ")
      Calibration(BigInt(testValue), values.split(" ").toList.map(BigInt.apply))
    }

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(remain: List[BigInt], current: Set[BigInt]): Set[BigInt] = remain match {
        case Nil    => current
        case h :: t => loop(t, current.map(_ + h) ++ current.map(_ * h))
      }

      parsed
        .filter(calibration => loop(calibration.values.tail, Set(calibration.values.head)).contains(calibration.testValue))
        .map(_.testValue)
        .sum
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(testValue: BigInt, remain: List[BigInt], current: Set[BigInt]): Set[BigInt] = remain match {
        case Nil => current
        case h :: t =>
          loop(testValue, t, (current.map(_ + h) ++ current.map(_ * h) ++ current.map(value => BigInt(value.toString() + h.toString))).filter(_ <= testValue))
      }

      parsed
        .filter(calibration => loop(calibration.testValue, calibration.values.tail, Set(calibration.values.head)).contains(calibration.testValue))
        .map(_.testValue)
        .sum
    }
  }
}
