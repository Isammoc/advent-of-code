package aoc2024

import common.grid.Point
import common.grid.Point._

import scala.annotation.tailrec

object day15 extends common.AocApp(Day15Solution.solution)

object Day15Solution {
  final case class Warehouse(walls: Set[Point], boxes: Set[Point], robot: Point)
  final case class Parsed(warehouse: Warehouse, plan: List[Point])

  def gps(point: Point): Long = point.y * 100 + point.x

  object solution extends common.Solution[Parsed]("2024", "15") {
    override def parse(input: String): Parsed = {
      val Array(warehouse, actions) = input.split("\n\n")

      val warehouse2 = for {
        (line, y) <- warehouse.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c

      val walls = warehouse2.filter(_._2 == '#').map(_._1).toSet
      val boxes = warehouse2.filter(_._2 == 'O').map(_._1).toSet
      val robot = warehouse2.find(_._2 == '@').get._1

      Parsed(
        Warehouse(walls = walls, boxes = boxes, robot = robot),
        actions
          .split("\n")
          .flatMap(_.map {
            case '>' => Right
            case '<' => Left
            case 'v' => Down
            case '^' => Up
          })
          .toList
      )
    }

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(remain: List[Point], current: Point, boxes: Set[Point]): Long = remain match {
        case Nil => boxes.map(gps).sum
        case h :: t =>
          @tailrec
          def innerLoop(toConsider: Point, result: List[Point]): Option[List[Point]] =
            if (parsed.warehouse.walls.contains(toConsider)) None
            else if (boxes.contains(toConsider)) innerLoop(toConsider + h, toConsider :: result)
            else Some(result)

          innerLoop(current + h, Nil) match {
            case Some(boxesToMove) => loop(t, current + h, boxes -- boxesToMove ++ boxesToMove.map(_ + h))
            case _                 => loop(t, current, boxes)
          }
      }

      loop(parsed.plan, parsed.warehouse.robot, parsed.warehouse.boxes)
    }

    override def part2(parsed: Parsed): Any = {
      val walls2 = for {
        wall <- parsed.warehouse.walls
        w2 <- List(Point(wall.x * 2, wall.y), Point(wall.x * 2 + 1, wall.y))
      } yield w2
      val leftBoxes = for {
        box <- parsed.warehouse.boxes
      } yield Point(box.x * 2, box.y)
      val rightBoxes = for {
        box <- parsed.warehouse.boxes
      } yield Point(box.x * 2 + 1, box.y)

      @tailrec
      def loop(remain: List[Point], current: Point, leftBoxes: Set[Point], rightBoxes: Set[Point]): Long =
        remain match {
          case Nil => leftBoxes.map(gps).sum
          case h :: t =>
            @tailrec
            def innerLoop(involved: Set[Point]): Set[Point] = {
              val moved = involved.map(_ + h)
              val leftInvolved = moved.intersect(leftBoxes) -- involved
              val rightInvolved = moved.intersect(rightBoxes) -- involved

              if (leftInvolved.isEmpty && rightInvolved.isEmpty) involved
              else {
                innerLoop(involved ++ leftInvolved ++ leftInvolved.map(_ + Right) ++ rightInvolved ++ rightInvolved.map(_ + Left))
              }
            }

            val involved = innerLoop(Set(current))

            if (involved.map(_ + h).exists(walls2)) loop(t, current, leftBoxes, rightBoxes)
            else {
              loop(
                t,
                current + h,
                leftBoxes -- leftBoxes.intersect(involved) ++ leftBoxes.intersect(involved).map(_ + h),
                rightBoxes -- rightBoxes.intersect(involved) ++ rightBoxes.intersect(involved).map(_ + h)
              )
            }
        }

      loop(parsed.plan, Point(parsed.warehouse.robot.x * 2, parsed.warehouse.robot.y), leftBoxes, rightBoxes)
    }
  }
}
