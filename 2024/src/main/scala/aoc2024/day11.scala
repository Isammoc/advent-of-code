package aoc2024

import common.util.Repeat

object day11 extends common.AocApp(Day11Solution.solution)

object Day11Solution {
  final case class Arrangement(stones: Map[Long, Long]) {
    def next(stone: Long) =
      if (stone == 0L) List(1L)
      else if (stone.toString.length % 2 == 0) {
        val (one, second) = stone.toString.splitAt(stone.toString.length / 2)
        List(one.toLong, second.toLong)
      } else List(stone * 2024)

    def next: Arrangement = Arrangement((for {
      (stone, amount) <- stones.toList
      newStone <- next(stone)
    } yield newStone -> amount).groupMapReduce(_._1)(_._2)(_ + _))

    def size: Long = stones.values.sum
  }
  type Parsed = Arrangement

  object solution extends common.Solution[Parsed]("2024", "11") {
    override def parse(input: String): Parsed = Arrangement(input.split("\\s+").toList.map(_.toLong).map(_ -> 1L).toMap)

    override def part1(parsed: Parsed): Any = Repeat(25)(parsed)(_.next).size

    override def part2(parsed: Parsed): Any = Repeat(75)(parsed)(_.next).size
  }
}
