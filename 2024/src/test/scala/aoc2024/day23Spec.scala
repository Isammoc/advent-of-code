package aoc2024

class day23Spec extends common.AocSpec {
  import Day23Solution._

  val sampleInput: String =
    """kh-tc
      |qp-kh
      |de-cg
      |ka-co
      |yn-aq
      |qp-ub
      |cg-tb
      |vc-aq
      |tb-ka
      |wh-tc
      |yn-cg
      |kh-ub
      |ta-co
      |de-co
      |tc-td
      |tb-wq
      |wh-td
      |ta-ka
      |td-qp
      |aq-cg
      |wq-ub
      |ub-vc
      |de-ta
      |wq-aq
      |wq-vc
      |wh-yn
      |ka-de
      |kh-ta
      |co-tc
      |wh-qp
      |tb-vc
      |td-yn
      |""".stripMargin

  "day23 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 7
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "co,de,ka,ta"
      }
    }
  }
}
