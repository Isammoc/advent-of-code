package aoc2024

class day21Spec extends common.AocSpec {
  import Day21Solution._

  val sampleInput: String =
    """029A
      |980A
      |179A
      |456A
      |379A
      |""".stripMargin

  "day21 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 126384
      }
      "lastCode" in {
        part1(List("379A")) shouldEqual (64 * 379)
      }
    }
  }
}
