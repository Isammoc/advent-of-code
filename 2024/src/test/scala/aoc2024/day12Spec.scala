package aoc2024

class day12Spec extends common.AocSpec {
  import Day12Solution._

  val first = """AAAA
                |BBCD
                |BBCC
                |EEEC
                |""".stripMargin
  val second = """OOOOO
                 |OXOXO
                 |OOOOO
                 |OXOXO
                 |OOOOO
                 |""".stripMargin
  val sampleInput: String =
    """RRRRIICCFF
      |RRRRIICCCF
      |VVRRRCCFFF
      |VVRCCCJFFF
      |VVVVCJJCFE
      |VVIVCCJJEE
      |VVIIICJJEE
      |MIIIIIJJEE
      |MIIISIJEEE
      |MMMISSJEEE
      |""".stripMargin

  "day12 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "first small example" in {
        part1(parse(first)) shouldEqual 140
      }

      "second small example" in {
        part1(parse(second)) shouldEqual 772
      }

      "example" in {
        part1(sample) shouldEqual 1930
      }
    }

    "part2" should {
      "first small example" in {
        part2(parse(first)) shouldEqual 80
      }

      "second small example" in {
        part2(parse(second)) shouldEqual 436
      }

      "E sample" in {
        part2(parse("""EEEEE
                      |EXXXX
                      |EEEEE
                      |EXXXX
                      |EEEEE
                      |""".stripMargin)) shouldEqual 236
      }

      "other sample" in {
        part2(parse("""AAAAAA
                      |AAABBA
                      |AAABBA
                      |ABBAAA
                      |ABBAAA
                      |AAAAAA
                      |""".stripMargin)) shouldEqual 368
      }
      "example" in {
        part2(sample) shouldEqual 1206
      }
    }
  }
}
