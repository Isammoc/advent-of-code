package aoc2024

class day11Spec extends common.AocSpec {
  import Day11Solution._

  val sampleInput: String =
    """125 17""".stripMargin

  "day11 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 55312
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }
  }
}
