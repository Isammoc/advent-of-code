package aoc2024

class day25Spec extends common.AocSpec {
  import Day25Solution._

  val sampleInput: String =
    """#####
      |.####
      |.####
      |.####
      |.#.#.
      |.#...
      |.....
      |
      |#####
      |##.##
      |.#.##
      |...##
      |...#.
      |...#.
      |.....
      |
      |.....
      |#....
      |#....
      |#...#
      |#.#.#
      |#.###
      |#####
      |
      |.....
      |.....
      |#.#..
      |###..
      |###.#
      |###.#
      |#####
      |
      |.....
      |.....
      |.....
      |#....
      |#.#..
      |#.#.#
      |#####
      |""".stripMargin

  "day25 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 3
      }
    }
  }
}
