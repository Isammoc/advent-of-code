package aoc2024

class day03Spec extends common.AocSpec {
  import Day03Solution._

  "day03 2024" can {
    import solution._

    "part1" should {
      val sample = parse("""xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))""".stripMargin)
      "example" in {
        part1(sample) shouldEqual 161
      }
    }

    "part2" should {
      val sample = parse("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))")
      "example" in {
        part2(sample) shouldEqual 48
      }
    }
  }
}
