package aoc2024

class day09Spec extends common.AocSpec {
  import Day09Solution._

  val sampleInput: String =
    """2333133121414131402""".stripMargin

  "day09 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 1928
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 2858
      }
    }
  }
}
