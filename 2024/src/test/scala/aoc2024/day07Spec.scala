package aoc2024

class day07Spec extends common.AocSpec {
  import Day07Solution._

  val sampleInput: String =
    """190: 10 19
      |3267: 81 40 27
      |83: 17 5
      |156: 15 6
      |7290: 6 8 6 15
      |161011: 16 10 13
      |192: 17 8 14
      |21037: 9 7 18 13
      |292: 11 6 16 20
      |""".stripMargin

  "day07 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 3749
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 11387
      }
    }
  }
}
