package aoc2024

class day17Spec extends common.AocSpec {
  import Day17Solution._

  val sampleInput: String =
    """Register A: 729
      |Register B: 0
      |Register C: 0
      |
      |Program: 0,1,5,4,3,0
      |""".stripMargin

  "day17 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "examples instructions" should {


      "If register C contains 9, the program 2,6 would set register B to 1." in {
        val before = Computer(a = 0, b = 0, c = 9, ip = 0, instructions = Array(2,6))
        val result = before.next
        result._1.b shouldEqual 1
      }
      "If register A contains 10, the program 5,0,5,1,5,4 would output 0,1,2." in {
        val before = Computer(a = 10, b = 0, c = 0, ip = 0, instructions = Array(5,0,5,1,5,4))
        val result = Computer.run(before)._2
        result shouldEqual List(0,1,2)
      }
      "If register A contains 2024, the program 0,1,5,4,3,0 would output 4,2,5,6,7,7,7,7,3,1,0 and leave 0 in register A." in {
        val before = Computer(a = 2024, b = 0, c = 0, ip = 0, instructions = Array(0,1,5,4,3,0))
        val (after, output) = Computer.run(before)
        after.a shouldEqual 0
        output shouldEqual List(4,2,5,6,7,7,7,7,3,1,0)
      }
      "If register B contains 29, the program 1,7 would set register B to 26." in {
        val before = Computer(a = 0, b = 29, c = 0, ip = 0, instructions = Array(1,7))
        val (after, _) = Computer.run(before)
        after.b shouldEqual 26
      }
      "If register B contains 2024 and register C contains 43690, the program 4,0 would set register B to 44354." in {
        val before = Computer(a = 0, b = 2024, c = 43690, ip = 0, instructions = Array(4,0))
        val (after, _) = Computer.run(before)
        after.b shouldEqual 44354
      }
    }

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "4,6,3,5,6,3,5,2,1,0"
      }
    }

    "part2" should {
      "example" in {
        val sample = parse("""Register A: 2024
                             |Register B: 0
                             |Register C: 0
                             |
                             |Program: 0,3,5,4,3,0
                             |""".stripMargin)
        part2(sample) shouldEqual 117440
      }
    }
  }

  /*
  2,4 B = A % 8
  1,5 B = B ^ 101
  7,5 C = A >> B
  1,6 B = B ^ 110
  0,3 A = A >> 3
  4,3 B = B ^ C
  5,5 Out B
  3,0 Go back to beginning
   */
}
