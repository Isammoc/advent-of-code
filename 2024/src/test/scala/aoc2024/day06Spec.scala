package aoc2024

class day06Spec extends common.AocSpec {
  import Day06Solution._

  val sampleInput: String =
    """....#.....
      |.........#
      |..........
      |..#.......
      |.......#..
      |..........
      |.#..^.....
      |........#.
      |#.........
      |......#...
      |""".stripMargin

  "day06 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 41
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 6
      }
    }
  }
}
