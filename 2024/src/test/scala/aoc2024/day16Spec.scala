package aoc2024

class day16Spec extends common.AocSpec {
  import Day16Solution._

  val firstInput: String =
    """###############
      |#.......#....E#
      |#.#.###.#.###.#
      |#.....#.#...#.#
      |#.###.#####.#.#
      |#.#.#.......#.#
      |#.#.#####.###.#
      |#...........#.#
      |###.#.#####.#.#
      |#...#.....#.#.#
      |#.#.#.###.#.#.#
      |#.....#...#.#.#
      |#.###.#.#.#.#.#
      |#S..#.....#...#
      |###############
      |""".stripMargin
  val sampleInput: String =
    """#################
      |#...#...#...#..E#
      |#.#.#.#.#.#.#.#.#
      |#.#.#.#...#...#.#
      |#.#.#.#.###.#.#.#
      |#...#.#.#.....#.#
      |#.#.#.#.#.#####.#
      |#.#...#.#.#.....#
      |#.#.#####.#.###.#
      |#.#.#.......#...#
      |#.#.###.#####.###
      |#.#.#...#.....#.#
      |#.#.#.#####.###.#
      |#.#.#.........#.#
      |#.#.#.#########.#
      |#S#.............#
      |#################
      |""".stripMargin

  "day16 2024" can {
    import solution._

    val first = parse(firstInput)
    val sample = parse(sampleInput)

    "part1" should {
      "first example" in {
        part1(first) shouldEqual 7036
      }
      "example" in {
        part1(sample) shouldEqual 11048
      }
    }

    "part2" should {
      "first example" in {
        part2(first) shouldEqual 45
      }
      "example" in {
        part2(sample) shouldEqual 64
      }
    }
  }
}
