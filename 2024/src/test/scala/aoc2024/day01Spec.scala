package aoc2024

class day01Spec extends common.AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """3   4
      |4   3
      |2   5
      |1   3
      |3   9
      |3   3
      |""".stripMargin

  "day01 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 11
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 31
      }
    }
  }
}
