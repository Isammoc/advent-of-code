package aoc2024

class day19Spec extends common.AocSpec {
  import Day19Solution._

  val sampleInput: String =
    """r, wr, b, g, bwu, rb, gb, br
      |
      |brwrr
      |bggr
      |gbbr
      |rrbgbr
      |ubwu
      |bwurrg
      |brgr
      |bbrgwb
      |""".stripMargin

  "day19 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 16
      }
    }
  }
}
