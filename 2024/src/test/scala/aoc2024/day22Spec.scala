package aoc2024

class day22Spec extends common.AocSpec {
  import Day22Solution._

  "day22 2024" can {
    import solution._

    "nextSecret" should {
      "find first" in {
        nextSecret(123) shouldEqual 15887950
      }
    }

    "part1" should {
      val sampleInput: String =
        """1
          |10
          |100
          |2024
          |""".stripMargin
      val sample = parse(sampleInput)
      "example" in {
        part1(sample) shouldEqual 37327623
      }
    }

    "part2" should {
      "123" in {
        toSequenceWin(123) should contain ((-1,-1,0,2) -> 6)
      }


      val sampleInput: String =
        """1
          |2
          |3
          |2024
          |""".stripMargin
      val sample = parse(sampleInput)
      "example" in {
        part2(sample) shouldEqual 23
      }
    }
  }
}
