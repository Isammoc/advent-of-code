package aoc2024

class day20Spec extends common.AocSpec {
  import Day20Solution._

  val sampleInput: String =
    """###############
      |#...#...#.....#
      |#.#.#.#.#.###.#
      |#S#...#.#.#...#
      |#######.#.#.###
      |#######.#.#...#
      |#######.#.###.#
      |###..E#...#...#
      |###.#######.###
      |#...###...#...#
      |#.#####.#.###.#
      |#.#...#.#.#...#
      |#.#.#.#.#.#.###
      |#...#...#...###
      |###############
      |""".stripMargin

  "day20 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample, 60) shouldEqual 1
        part1(sample, 38) shouldEqual 3
        part1(sample, 36) shouldEqual 4
        part1(sample, 20) shouldEqual 5
        part1(sample, 12) shouldEqual 8
        part1(sample) shouldEqual 0
      }
    }

    "part2" should {
      "example" in {
        part2(sample, 76) shouldEqual 3
        part2(sample, 74) shouldEqual 7
        part2(sample, 72) shouldEqual 29
        part2(sample, 70) shouldEqual 41
        part2(sample, 68) shouldEqual 55
        part2(sample, 66) shouldEqual 67
        part2(sample, 64) shouldEqual 86
        part2(sample, 62) shouldEqual 106
        part2(sample, 60) shouldEqual 129
        part2(sample, 58) shouldEqual 154
        part2(sample, 56) shouldEqual 193
        part2(sample, 54) shouldEqual 222
      }
    }
  }
}
