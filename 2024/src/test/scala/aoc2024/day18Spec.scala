package aoc2024

class day18Spec extends common.AocSpec {
  import Day18Solution._

  val sampleInput: String =
    """5,4
      |4,2
      |4,5
      |3,0
      |2,1
      |6,3
      |2,4
      |1,5
      |0,6
      |3,3
      |2,6
      |5,1
      |1,2
      |5,5
      |2,5
      |6,5
      |1,4
      |0,4
      |6,4
      |1,1
      |6,1
      |1,0
      |0,5
      |1,6
      |2,0
      |""".stripMargin

  "day18 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample, 12, 6) shouldEqual 22
      }
    }

    "part2" should {
      "example" in {
        part2(sample, 6) shouldEqual "6,1"
      }
    }
  }
}
