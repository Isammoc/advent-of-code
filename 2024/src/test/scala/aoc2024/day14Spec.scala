package aoc2024

class day14Spec extends common.AocSpec {
  import Day14Solution._

  val sampleInput: String =
    """p=0,4 v=3,-3
      |p=6,3 v=-1,-3
      |p=10,3 v=-1,2
      |p=2,0 v=2,-1
      |p=0,0 v=1,3
      |p=3,0 v=-2,-2
      |p=7,6 v=-1,-3
      |p=3,0 v=-1,-2
      |p=9,3 v=2,3
      |p=7,3 v=-1,2
      |p=2,4 v=2,-3
      |p=9,5 v=-3,-3
      |""".stripMargin

  "day14 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1_test(sample) shouldEqual 12
      }
    }
  }
}
