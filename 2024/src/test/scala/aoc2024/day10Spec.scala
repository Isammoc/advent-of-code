package aoc2024

class day10Spec extends common.AocSpec {
  import Day10Solution._

  val sampleInput: String =
    """89010123
      |78121874
      |87430965
      |96549874
      |45678903
      |32019012
      |01329801
      |10456732
      |""".stripMargin

  "day10 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 36
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 81
      }
    }
  }
}
