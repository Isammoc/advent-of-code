package aoc2024

class day04Spec extends common.AocSpec {
  import Day04Solution._

  val sampleInput: String =
    """MMMSXXMASM
      |MSAMXMSMSA
      |AMXSXMAAMM
      |MSAMASMSMX
      |XMASAMXAMM
      |XXAMMXXAMA
      |SMSMSASXSS
      |SAXAMASAAA
      |MAMMMXMMMM
      |MXMXAXMASX
      |""".stripMargin

  "day04 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 18
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 9
      }
    }
  }
}
