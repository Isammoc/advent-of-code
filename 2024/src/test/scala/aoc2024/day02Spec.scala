package aoc2024

class day02Spec extends common.AocSpec {
  import Day02Solution._

  val sampleInput: String =
    """7 6 4 2 1
      |1 2 7 8 9
      |9 7 6 2 1
      |1 3 2 4 5
      |8 6 4 4 1
      |1 3 6 7 9
      |""".stripMargin

  "day02 2024" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 2
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 4
      }
    }
  }
}
