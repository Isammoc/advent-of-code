package aoc2015

object Day21Solution {
  final case class Opponent(hp: Int, damage: Int, armor: Int)
  final case class Item(cost: Int, damage: Int, armor: Int) {
    def +(other: Item): Item = Item(this.cost + other.cost, this.damage + other.damage, this.armor + other.armor)
  }

  val weapons = List(
    Item(8, 4, 0), // Dagger
    Item(10, 5, 0), // Shortsword
    Item(25, 6, 0), // Warhammer
    Item(40, 7, 0), // Longsword
    Item(74, 8, 0) // Greataxe
  )

  private val nothing = Item(0, 0, 0)

  val armors = List(
    nothing,
    Item(13, 0, 1), // Leather
    Item(31, 0, 2), // Chainmail
    Item(53, 0, 3), // Splintmail
    Item(75, 0, 4), // Bandedmail
    Item(102, 0, 5) // Platemail
  )

  val rings = List(
    nothing,
    nothing,
    Item(25, 1, 0), // Damage +1
    Item(50, 2, 0), // Damage +2
    Item(100, 3, 0), // Damage +3
    Item(20, 0, 1), // Defense +1
    Item(40, 0, 2), // Defense +2
    Item(80, 0, 3) // Defense +3
  )

  def battle(player: Opponent, boss: Opponent): Boolean = {
    val playerDamage = math.max(1, player.damage - boss.armor)
    val bossDamage = math.max(1, boss.damage - player.armor)

    val playerTurnsToKill = boss.hp / playerDamage + (if (boss.hp % playerDamage == 0) 0 else 1)
    val bossTurnsToKill = player.hp / bossDamage + (if (player.hp % bossDamage == 0) 0 else 1)

    playerTurnsToKill <= bossTurnsToKill
  }

  private val possibilities = (for {
    weapon <- weapons
    armor <- nothing :: armors
    i <- 0 until rings.size - 1
    j <- i + 1 until rings.size
    ring1 = rings(i)
    ring2 = rings(j)
  } yield {
    val inventory = weapon + armor + ring1 + ring2
    inventory.cost -> Opponent(100, inventory.damage, inventory.armor)
  }).sortBy(_._1)

  def parse(input: String): Opponent = {
    val Array(hp, damage, armor) = input.split("\n").map(_.split(":")(1).trim.toInt)
    Opponent(hp, damage, armor)
  }

  def part1(input: String): Int = {
    val boss = parse(input)

    possibilities
      .find { case (_, player) =>
        battle(player, boss)
      }
      .get
      ._1
  }

  def part2(input: String): Int = {
    val boss = parse(input)

    possibilities
      .filter { case (_, player) =>
        !battle(player, boss)
      }
      .map(_._1)
      .max
  }
}

object day21 extends App {
  import Day21Solution._

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
