package aoc2015

import scala.collection.mutable

object Day22Solution {

  sealed trait Effect
  final case object ShieldEffect extends Effect
  final case object PoisonEffect extends Effect
  final case object RechargeEffect extends Effect

  sealed abstract class Spell(val cost: Int) {
    def canApply(battle: Battle): Boolean = battle.player.mana >= cost
    def apply(battle: Battle): Battle = battle.copy(player = battle.player.copy(mana = battle.player.mana - cost))
  }

  object Spell {

    /**
     * Magic Missile costs 53 mana. It instantly does 4 damage.
     */
    final case object MagicMissile extends Spell(53) {
      override def apply(battle: Battle): Battle = super.apply(battle.copy(boss = battle.boss.damage(4)))
    }

    /**
     * Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
     */
    final case object Drain extends Spell(73) {
      override def apply(battle: Battle): Battle =
        super.apply(battle.copy(boss = battle.boss.damage(2), player = battle.player.heal(2)))
    }

    /**
     * Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
     */
    final case object Shield extends Spell(113) {
      override def canApply(battle: Battle): Boolean = super.canApply(battle) && !battle.effects.isDefinedAt(ShieldEffect)
      override def apply(battle: Battle): Battle = super.apply(battle.startEffect(ShieldEffect, 6))
    }

    /**
     * Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
     */
    final case object Poison extends Spell(173) {
      override def canApply(battle: Battle): Boolean = super.canApply(battle) && !battle.effects.isDefinedAt(PoisonEffect)
      override def apply(battle: Battle): Battle = super.apply(battle.startEffect(PoisonEffect, 6))
    }

    /**
     * Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
     */
    final case object Recharge extends Spell(229) {
      override def canApply(battle: Battle): Boolean = super.canApply(battle) && !battle.effects.isDefinedAt(RechargeEffect)
      override def apply(battle: Battle): Battle = super.apply(battle.startEffect(RechargeEffect, 5))
    }

    def All: List[Spell] = List(MagicMissile, Drain, Shield, Poison, Recharge)
  }

  final case class Player(hp: Int, mana: Int, armor: Int) {
    def recharge: Player = this.copy(mana = mana + 101)
    def damage(amount: Int): Player = this.copy(hp = hp - amount)
    def heal(amount: Int): Player = this.copy(hp = hp + amount)
    def isDead: Boolean = hp <= 0
    def armored: Player = this.copy(armor = 7)
    def nonArmored: Player = this.copy(armor = 0)
  }
  final case class Boss(hp: Int, damage: Int) {
    def poison: Boss = this.damage(3)
    def damage(amount: Int): Boss = this.copy(hp = hp - amount)
    def isDead: Boolean = hp <= 0
  }

  final case class Battle(player: Player, boss: Boss, effects: Map[Effect, Int]) {
    def startEffect(effect: Effect, turn: Int): Battle = this.copy(effects = effects + (effect -> turn))
    def decreaseEffect(effect: Effect): Map[Effect, Int] = if (effects(effect) == 1) effects - effect else effects + (effect -> (effects(effect) - 1))
    def applyRecharge: Battle = if (effects.isDefinedAt(RechargeEffect)) {
      this.copy(player = player.recharge, effects = decreaseEffect(RechargeEffect))
    } else this
    def applyPoison: Battle = if (effects.isDefinedAt(PoisonEffect)) {
      this.copy(boss = boss.poison, effects = decreaseEffect(PoisonEffect))
    } else this
    def applyArmor: Battle = if (effects.isDefinedAt(ShieldEffect)) {
      this.copy(effects = decreaseEffect(ShieldEffect), player = player.armored)
    } else this.copy(player = player.nonArmored)

    def isEnd: Boolean = boss.isDead

    def applyEffects: Battle = this.applyArmor.applyPoison.applyRecharge

    def bossAttack: Battle = {
      val bossDamage = math.max(1, boss.damage - player.armor)
      this.copy(player = player.damage(bossDamage))
    }
  }

  case class Turn(cost: Int, battle: Battle) {
    def isEnd: Boolean = battle.isEnd
    def next(hard: Boolean): List[Turn] = {
      val currentBattle = if (hard) battle.copy(player = battle.player.damage(1)) else battle
      if (currentBattle.player.isDead) List()
      else {
        // player effects
        val afterPlayerEffects = currentBattle.applyEffects
        if (afterPlayerEffects.isEnd) List(Turn(cost, afterPlayerEffects))
        else {
          val afterPlayerSpells = for {
            spell <- Spell.All if spell.canApply(afterPlayerEffects)
          } yield Turn(cost + spell.cost, spell(afterPlayerEffects))

          for {
            turn <- afterPlayerSpells
            afterBossTurn <- if (turn.isEnd) Some(turn) else turn.bossTurn
          } yield afterBossTurn
        }
      }
    }

    def bossTurn: Option[Turn] = {
      val afterBossEffects = battle.applyEffects
      if (afterBossEffects.isEnd) Some(Turn(cost, afterBossEffects))
      else {
        val afterBossAttack = afterBossEffects.bossAttack

        if (afterBossAttack.player.isDead) None
        else Some(Turn(cost, afterBossAttack))
      }
    }
  }

  def parse(str: String): Boss = {
    val Array(hp, damage) = str.split("\n").map(_.split(":")(1).trim.toInt)
    Boss(hp, damage)
  }

  def play(input: String, hardMode: Boolean): Int = {
    val boss = parse(input)
    val initial = Turn(0, Battle(Player(50, 500, 0), boss, Map.empty))

    val pq = mutable.PriorityQueue(initial)(Ordering.by(-_.cost))
    while (pq.nonEmpty) {
      val current = pq.dequeue()
      if (current.isEnd) return current.cost
      pq.enqueue(current.next(hardMode): _*)
    }
    -1
  }

  def part1(input: String): Int = play(input, hardMode = false)

  def part2(input: String): Int = play(input, hardMode = true)
}

object day22 extends App {
  import Day22Solution._

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
