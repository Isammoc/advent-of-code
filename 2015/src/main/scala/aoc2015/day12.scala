package aoc2015

import play.api.libs.json._

import scala.annotation.tailrec

object day12 extends App {

  def part1(input: String) = {
    @tailrec
    def loop(toVisit: List[JsValue], res: Int): Int = toVisit match {
      case Nil                   => res
      case JsBoolean(_) :: t     => loop(t, res)
      case JsString(_) :: t      => loop(t, res)
      case JsNumber(number) :: t => loop(t, res + number.toInt)
      case JsArray(values) :: t  => loop(values.toList ::: t, res)
      case JsObject(obj) :: t    => loop(obj.values.toList ::: t, res)
    }
    loop(Json.parse(input) :: Nil, 0)
  }

  def part2(input: String) = {
    @tailrec
    def loop(toVisit: List[JsValue], res: Int): Int = toVisit match {
      case Nil                                                           => res
      case JsBoolean(_) :: t                                             => loop(t, res)
      case JsString(_) :: t                                              => loop(t, res)
      case JsNumber(number) :: t                                         => loop(t, res + number.toInt)
      case JsArray(values) :: t                                          => loop(values.toList ::: t, res)
      case JsObject(obj) :: t if obj.values.exists(_ == JsString("red")) => loop(t, res)
      case JsObject(obj) :: t                                            => loop(obj.values.toList ::: t, res)
    }
    loop(Json.parse(input) :: Nil, 0)
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
