package aoc2015

import scala.annotation.tailrec

final case class Rule(from: String, to: String)
final case class Genetics(molecule: String, rules: Set[Rule])

object Day19Solution {
  def parse(input: String): Genetics = {
    val RuleR = "(.*?) => (.*)".r
    val Array(rules, molecule) = input.split("\\n\\n")
    Genetics(
      molecule,
      rules
        .split("\n")
        .map { case RuleR(from, to) =>
          Rule(from, to)
        }
        .toSet
    )
  }

  def nextMolecules(rules: Set[Rule])(molecule: String): Set[String] = for {
    rule <- rules
    result <- LazyList
      .iterate(-1)(i => molecule.indexOf(rule.from, i + 1))
      .tail
      .takeWhile(_ != -1)
      .map(i => molecule.substring(0, i) + rule.to + molecule.substring(i + rule.from.length))
  } yield result

  def part1(input: String): Int = {
    val genetics = parse(input)
    nextMolecules(genetics.rules)(genetics.molecule).size
  }

  def part2(input: String): Int = {
    val genetics = parse(input)
    val rules = genetics.rules.map(rule => Rule(rule.to, rule.from))

    @tailrec
    def loop(candidates: Vector[String], count: Int): Int =
      if (candidates.contains("e")) count
      else loop(candidates.flatMap(nextMolecules(rules)).sortBy(_.length).take(10), count + 1)

    loop(Vector(genetics.molecule), 0)
  }

}

object day19 extends App {
  import Day19Solution._

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
