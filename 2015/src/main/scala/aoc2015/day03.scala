package aoc2015
import common.grid.Point

object day03 extends App {

  def parseInput(input: String): List[Point] =
    input.toList.map {
      case '^' => Point.Up
      case 'v' => Point.Down
      case '<' => Point.Left
      case '>' => Point.Right
    }

  def part1(input: String): Int = {
    val start = Point(0, 0)
    parseInput(input)
      .foldLeft((start, Set(start))) { case ((current, visited), dir) =>
        (current + dir, visited + (current + dir))
      }
      ._2
      .size
  }

  def part2(input: String): Int = {
    val start = Point(0, 0)
    parseInput(input).zipWithIndex
      .foldLeft((start, start, Set(start))) {
        case ((santa, robo, visited), (dir, i)) if i % 2 == 0 => (santa + dir, robo, visited + (santa + dir))
        case ((santa, robo, visited), (dir, _)) => (santa, robo + dir, visited + (robo + dir))
      }
      ._3
      .size
  }

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
