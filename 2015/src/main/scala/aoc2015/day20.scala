package aoc2015

import scala.annotation.tailrec

object Day20Solution {
  def part1(input: String): Int = {
    val max = input.toLong

    @tailrec
    def loop(current: Int): Int = {
      val elves = LazyList.from(2).takeWhile(i => i * i < current).filter(current % _ == 0).flatMap(i => List(i, current / i))
      if (elves.sum * 10 + 10 + current * 10 >= max) current
      else loop(current + 1)
    }

    loop(1)
  }

  def part2(input: String): Int = {
    val max = input.toLong

    @tailrec
    def loop(current: Int): Int = {
      val elves = (1 to 50).filter(current % _ == 0).map(current / _)
      if (elves.sum * 11 >= max) current
      else loop(current + 1)
    }

    loop(1)
  }
}

object day20 extends App {
  import Day20Solution._

  val input = io.Source.stdin.getLines().mkString("\n")
  println("part1 = " + part1(input))
  println("part2 = " + part2(input))
}
