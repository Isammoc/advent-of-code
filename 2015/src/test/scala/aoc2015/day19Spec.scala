package aoc2015
import common.AocSpec

class day19Spec extends AocSpec {
  import Day19Solution._

  "day19 2015" can {
    val sample = """e => H
                   |e => O
                   |H => HO
                   |H => OH
                   |O => HH
                   |
                   |HOHOHO""".stripMargin

    /*
     * e
     * H
     * HO
     * HHH
     * HOHOHO
     */
    "part1" should {
      "example" in {
        part1(sample) shouldEqual 7
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 6
      }
    }
  }
}
