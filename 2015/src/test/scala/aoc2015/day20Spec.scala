package aoc2015
import common.AocSpec

class day20Spec extends AocSpec {
  import Day20Solution._

  "day20 2015" can {
    val sample = """100""".stripMargin

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 6
      }
    }
  }
}
