package aoc2015
import common.AocSpec

class day21Spec extends AocSpec {
  import Day21Solution._

  "day21 2015" can {
    "sample battle" in {
      battle(Opponent(8, 5, 5), Opponent(12, 7, 2)) shouldEqual true
    }
  }
}
