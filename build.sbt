import Dependencies._

ThisBuild / organization := "net.isammoc"
ThisBuild / scalaVersion := "2.13.12"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalacOptions ++= List(
  "-deprecation",
  "-Wconf:cat=other-match-analysis:silent", // I want to play, not be exhaustive
  "-Ypatmat-exhaust-depth",
  "off",
  "-Xfatal-warnings",
  "-Xlint"
)
lazy val commons = (project in file("commons"))
  .settings(
    name := "advent-of-code-commons",
    libraryDependencies += scalaTest % Test
  )

def aocYear(project: Project) = project
  .dependsOn(commons % "compile->compile;test->test")

lazy val aoc2015 = (project in file("2015"))
  .configure(aocYear)
  .settings(
    libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.2"
  )

lazy val aoc2016 = (project in file("2016")).configure(aocYear)

lazy val aoc2017 = (project in file("2017")).configure(aocYear)

lazy val aoc2018 = (project in file("2018")).configure(aocYear)

lazy val aoc2019 = (project in file("2019")).configure(aocYear)

lazy val aoc2020 = (project in file("2020")).configure(aocYear)

lazy val aoc2021 = (project in file("2021")).configure(aocYear)

lazy val aoc2022 = (project in file("2022")).configure(aocYear)

lazy val aoc2023 = (project in file("2023")).configure(aocYear)

lazy val aoc2024 = (project in file("2024")).configure(aocYear)
