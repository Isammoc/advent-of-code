package aoc2022

class day14Spec extends common.AocSpec {

  import Day14Solution._

  val sampleInput: String =
    """498,4 -> 498,6 -> 496,6
      |503,4 -> 502,4 -> 502,9 -> 494,9
      |""".stripMargin

  "day14 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 24
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 93
      }
    }
  }
}

