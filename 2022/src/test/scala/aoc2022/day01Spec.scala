package aoc2022

class day01Spec extends common.AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """1000
      |2000
      |3000
      |
      |4000
      |
      |5000
      |6000
      |
      |7000
      |8000
      |9000
      |
      |10000""".stripMargin

  "day01 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 24000
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 45000
      }
    }
  }
}

