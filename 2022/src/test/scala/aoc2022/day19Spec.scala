package aoc2022

class day19Spec extends common.AocSpec {

  import Day19Solution._

  val sampleInput: String =
    """Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
      |Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.""".stripMargin

  "day19 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 33
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual (56 * 62)
      }
    }
  }
}

