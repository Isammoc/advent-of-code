package aoc2022

class day17Spec extends common.AocSpec {

  import Day17Solution._

  val sampleInput: String =
    """>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>""".stripMargin

  "day17 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 3068
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1514285714288L
      }
    }
  }
}

