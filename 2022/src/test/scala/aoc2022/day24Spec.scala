package aoc2022

class day24Spec extends common.AocSpec {
  import Day24Solution._

  val sampleInput: String =
    """#.######
      |#>>.<^<#
      |#.<..<<#
      |#>v.><>#
      |#<^v^^>#
      |######.#
      |""".stripMargin

  "day24 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 18
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 54
      }
    }
  }

  "LinearBlizzard" can {
    val initialPos = 2
    val limit = 5
    val direction = 1
    val underTest = LinearBlizzard(initialPos, direction)
    "return initial pos" in {
      underTest.at(0, limit) shouldEqual initialPos
    }
  }
}

