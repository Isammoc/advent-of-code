package aoc2022

class day23Spec extends common.AocSpec {

  import Day23Solution._

  val sampleInput: String =
    """....#..
      |..###.#
      |#...#.#
      |.#...##
      |#.###..
      |##.#.##
      |.#..#..
      |""".stripMargin

  "day23 2022" can {
    import solution._

    "mini sample" in {
      val initial = parse(
        """.....
          |..##.
          |..#..
          |.....
          |..##.
          |.....""".stripMargin)
      val afterFirstTurn = parse(
        """..##.
          |.....
          |..#..
          |...#.
          |..#..
          |.....
          |""".stripMargin)
      initial.turn.elves shouldEqual afterFirstTurn.elves
    }

    val sample = parse(sampleInput)

    "part1 round 1" in {
      sample.turn.toString shouldEqual
        """.....#...
          |...#...#.
          |.#..#.#..
          |.....#..#
          |..#.#.##.
          |#..#.#...
          |#.#.#.##.
          |.........
          |..#..#...""".stripMargin
    }

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 110
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 20
      }
    }
  }
}

