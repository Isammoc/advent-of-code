package aoc2022

class day02Spec extends common.AocSpec {
  import Day02Solution._

  val sampleInput: String =
    """A Y
      |B X
      |C Z""".stripMargin

  "day02 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 15
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 12
      }
    }
  }
}

