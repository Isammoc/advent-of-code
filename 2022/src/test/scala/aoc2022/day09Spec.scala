package aoc2022

class day09Spec extends common.AocSpec {
  import Day09Solution._

  val sampleInput: String =
    """R 4
      |U 4
      |L 3
      |D 1
      |R 4
      |D 1
      |L 5
      |R 2""".stripMargin

  "day09 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 13
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1
      }
      "extra sample" in {
        part2(parse("""R 5
                      |U 8
                      |L 8
                      |D 3
                      |R 17
                      |D 10
                      |L 25
                      |U 20
                      |""".stripMargin)) shouldEqual 36
      }
    }
  }
}

