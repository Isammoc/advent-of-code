package aoc2022

class day18Spec extends common.AocSpec {

  import Day18Solution._

  val sampleInput: String =
    """2,2,2
      |1,2,2
      |3,2,2
      |2,1,2
      |2,3,2
      |2,2,1
      |2,2,3
      |2,2,4
      |2,2,6
      |1,2,5
      |3,2,5
      |2,1,5
      |2,3,5""".stripMargin

  "day18 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 64
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 58
      }
    }
  }
}

