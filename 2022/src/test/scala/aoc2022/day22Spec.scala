package aoc2022

import common.grid.Point

class day22Spec extends common.AocSpec {

  import Day22Solution._

  val sampleInput: String =
    """        ...#
      |        .#..
      |        #...
      |        ....
      |...#.......#
      |........#...
      |..#....#....
      |..........#.
      |        ...#....
      |        .....#..
      |        .#......
      |        ......#.
      |
      |10R5L5R10L4R5L5
      |""".stripMargin

  "day22 2022" can {
    import solution._

    val sample = parseSample(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6032
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 5031
      }
    }

    import common.grid.Point.{Up, Down, Left, Right}

    "UnitPatron" that {
      "sample" can {
        // sample
        //   A
        // BCD
        //   EF
        val A = Point(2, 0)
        val B = Point(0, 1)
        val C = Point(1, 1)
        val D = Point(2, 1)
        val E = Point(2, 2)
        val F = Point(3, 2)

        val samplePatron = UnitPatron.from(Set(A, B, C, D, E, F))

        "corners" in {
          samplePatron.forward(Position(A, Left)) shouldEqual Position(C, Down)
          samplePatron.forward(Position(A, Up)) shouldEqual Position(B, Down)
          samplePatron.forward(Position(C, Up)) shouldEqual Position(A, Right)
          samplePatron.forward(Position(C, Down)) shouldEqual Position(E, Right)
          samplePatron.forward(Position(A, Left)) shouldEqual Position(C, Down)
          samplePatron.forward(Position(A, Left)) shouldEqual Position(C, Down)
        }
      }
    }

    "Patron" that {
      val samplePatron = sample._1.patron
      "given samples" in {
        samplePatron.forward(Position(Point(11, 5), Right)) shouldEqual Position(Point(14, 8), Down)
        samplePatron.forward(Position(Point(14, 8), Up)) shouldEqual Position(Point(11, 5), Left)
        samplePatron.forward(Position(Point(10, 11), Down)) shouldEqual Position(Point(1, 7), Up)
        samplePatron.forward(Position(Point(6, 5), Up)) shouldEqual Position(Point(8, 2), Right)
      }
    }
  }
}
