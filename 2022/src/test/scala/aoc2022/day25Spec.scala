package aoc2022

class day25Spec extends common.AocSpec {

  import Day25Solution._

  val sampleInput: String =
    """1=-0-2
      |12111
      |2=0=
      |21
      |2=01
      |111
      |20012
      |112
      |1=-1=
      |1-12
      |12
      |1=
      |122
      |""".stripMargin

  "Snafu" can {
    "toLong" in {
      Snafu.toLong("1=-0-2") shouldEqual 1747
      Snafu.toLong("12111") shouldEqual 906
      Snafu.toLong("2=0=") shouldEqual 198
      Snafu.toLong("21") shouldEqual 11
      Snafu.toLong("2=01") shouldEqual 201
      Snafu.toLong("111") shouldEqual 31
      Snafu.toLong("20012") shouldEqual 1257
      Snafu.toLong("112") shouldEqual 32
      Snafu.toLong("1=-1=") shouldEqual 353
      Snafu.toLong("1-12") shouldEqual 107
      Snafu.toLong("12") shouldEqual 7
      Snafu.toLong("1=") shouldEqual 3
      Snafu.toLong("122") shouldEqual 37
    }

    "fromLong" in {
      Snafu.fromLong(1) shouldEqual "1"
      Snafu.fromLong(2) shouldEqual "2"
      Snafu.fromLong(3) shouldEqual "1="
      Snafu.fromLong(4) shouldEqual "1-"
      Snafu.fromLong(5) shouldEqual "10"
      Snafu.fromLong(6) shouldEqual "11"
      Snafu.fromLong(7) shouldEqual "12"
      Snafu.fromLong(8) shouldEqual "2="
      Snafu.fromLong(9) shouldEqual "2-"
      Snafu.fromLong(10) shouldEqual "20"
      Snafu.fromLong(15) shouldEqual "1=0"
      Snafu.fromLong(20) shouldEqual "1-0"
      Snafu.fromLong(2022) shouldEqual "1=11-2"
      Snafu.fromLong(12345) shouldEqual "1-0---0"
      Snafu.fromLong(314159265) shouldEqual "1121-1110-1=0"
    }
  }

  "day25 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "2=-1=0"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }
  }
}

