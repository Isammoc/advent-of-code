package aoc2022

class day06Spec extends common.AocSpec {
  import Day06Solution._

  val sampleInput: String =
    """mjqjpqmgbljsphdztnvjfqwrcgsmlb""".stripMargin

  "day06 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 7
        part1(parse("bvwbjplbgvbhsrlpgdmjqwftvncz")) shouldEqual 5
        part1(parse("nppdvjthqldpwncqszvftbrmjlhg")) shouldEqual 6
        part1(parse("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")) shouldEqual 10
        part1(parse("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")) shouldEqual 11
      }
    }

    "part2" should {
      "example" in {
        part2(parse("mjqjpqmgbljsphdztnvjfqwrcgsmlb")) shouldEqual 19
        part2(parse("bvwbjplbgvbhsrlpgdmjqwftvncz")) shouldEqual 23
        part2(parse("nppdvjthqldpwncqszvftbrmjlhg")) shouldEqual 23
        part2(parse("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")) shouldEqual 29
        part2(parse("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")) shouldEqual 26
      }
    }
  }
}

