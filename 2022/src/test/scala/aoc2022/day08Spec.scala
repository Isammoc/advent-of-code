package aoc2022

class day08Spec extends common.AocSpec {
  import Day08Solution._

  val sampleInput: String =
    """30373
      |25512
      |65332
      |33549
      |35390""".stripMargin

  "day08 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 21
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 8
      }
    }
  }
}

