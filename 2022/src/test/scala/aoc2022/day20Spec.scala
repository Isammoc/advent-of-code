package aoc2022

class day20Spec extends common.AocSpec {
  import Day20Solution._

  val sampleInput: String =
    """1
      |2
      |-3
      |3
      |-2
      |0
      |4""".stripMargin

  "day20 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 3
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1623178306
      }
    }
  }
}

