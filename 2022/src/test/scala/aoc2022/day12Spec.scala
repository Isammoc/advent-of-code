package aoc2022

class day12Spec extends common.AocSpec {
  import Day12Solution._

  val sampleInput: String =
    """Sabqponm
      |abcryxxl
      |accszExk
      |acctuvwj
      |abdefghi""".stripMargin

  "day12 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 31
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 29
      }
    }
  }
}

