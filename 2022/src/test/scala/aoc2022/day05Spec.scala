package aoc2022

class day05Spec extends common.AocSpec {

  import Day05Solution._

  // what a shame that IntelliJ does not avoid to trim multiline strings
  val sampleInput: String =
    "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2\n"

  "day05 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "CMZ"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "MCD"
      }
    }
  }
}

