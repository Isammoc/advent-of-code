package aoc2022

class day04Spec extends common.AocSpec {

  import Day04Solution._

  val sampleInput: String =
    """2-4,6-8
      |2-3,4-5
      |5-7,7-9
      |2-8,3-7
      |6-6,4-6
      |2-6,4-8
      |""".stripMargin

  "day04 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 2
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 4
      }
    }
  }
}

