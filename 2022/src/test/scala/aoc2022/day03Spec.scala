package aoc2022

class day03Spec extends common.AocSpec {

  import Day03Solution._

  val sampleInput: String =
    """vJrwpWtwJgWrhcsFMMfFFhFp
      |jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
      |PmmdzqPrVvPwwTWBwg
      |wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
      |ttgJtRGJQctTZtZT
      |CrZsJsPPZsGzwwsLwLmpwMDw""".stripMargin

  "day03 2022" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "line1" in {
        part1(parse("vJrwpWtwJgWrhcsFMMfFFhFp")) shouldEqual 16
      }
      "line2" in {
        part1(parse("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")) shouldEqual 38
      }
      "line3" in {
        part1(parse("PmmdzqPrVvPwwTWBwg")) shouldEqual 42
      }
      "line4" in {
        part1(parse("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn")) shouldEqual 22
      }
      "line5" in {
        part1(parse("ttgJtRGJQctTZtZT")) shouldEqual 20
      }
      "line6" in {
        part1(parse("CrZsJsPPZsGzwwsLwLmpwMDw")) shouldEqual 19
      }
      "example" in {
        part1(sample) shouldEqual 157
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 70
      }
    }
  }
}

