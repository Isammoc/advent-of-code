package aoc2022

import common.grid.Point

import scala.annotation.tailrec

object day22 extends common.AocApp(Day22Solution.solution)

object Day22Solution {

  final case class Patron(points: Set[Point], size: Int) {
    private def reduce(p: Point): Point = Point(p.x / size, p.y / size)

    private def reduce(position: Position): Position = position.copy(pos = reduce(position.pos))

    private val unitPatron = UnitPatron.from(points.map(reduce))

    def forward(position: Position): Position = {
      val unitResult = unitPatron.forward(reduce(position))

      val distance = position match {
        case Position(Point(x, _), Point.Up) => x % size
        case Position(Point(x, _), Point.Down) => (size - 1) - (x % size)
        case Position(Point(_, y), Point.Right) => y % size
        case Position(Point(_, y), Point.Left) => (size - 1) - (y % size)
      }

      unitResult match {
        case Position(unitPos, Point.Up) => Position(unitPos * size + Point(distance, size - 1), Point.Up)
        case Position(unitPos, Point.Down) => Position(unitPos * size + Point((size - 1) - distance, 0), Point.Down)
        case Position(unitPos, Point.Right) => Position(unitPos * size + Point(0, distance), Point.Right)
        case Position(unitPos, Point.Left) => Position(unitPos * size + Point(size - 1, (size - 1) - distance), Point.Left)
      }
    }
  }

  object UnitPatron {
    // A
    // BDEF
    // C
    private val refA = Point(0, 0)
    private val refB = Point(0, 1)
    private val refC = Point(0, 2)
    private val refD = Point(1, 1)
    private val refE = Point(2, 1)
    private val refF = Point(3, 1)

    private val refForward: Position => Position = Map(
      Position(refA, Point.Up) -> Position(refE, Point.Down),
      Position(refA, Point.Right) -> Position(refD, Point.Down),
      Position(refA, Point.Down) -> Position(refB, Point.Down),
      Position(refA, Point.Left) -> Position(refF, Point.Down),
      Position(refB, Point.Up) -> Position(refA, Point.Up),
      Position(refB, Point.Right) -> Position(refD, Point.Right),
      Position(refB, Point.Down) -> Position(refC, Point.Down),
      Position(refB, Point.Left) -> Position(refF, Point.Left),
      Position(refC, Point.Up) -> Position(refB, Point.Up),
      Position(refC, Point.Right) -> Position(refD, Point.Up),
      Position(refC, Point.Down) -> Position(refE, Point.Up),
      Position(refC, Point.Left) -> Position(refF, Point.Up),
      Position(refD, Point.Up) -> Position(refA, Point.Left),
      Position(refD, Point.Right) -> Position(refE, Point.Right),
      Position(refD, Point.Down) -> Position(refC, Point.Left),
      Position(refD, Point.Left) -> Position(refB, Point.Left),
      Position(refE, Point.Up) -> Position(refA, Point.Down),
      Position(refE, Point.Right) -> Position(refF, Point.Right),
      Position(refE, Point.Down) -> Position(refC, Point.Up),
      Position(refE, Point.Left) -> Position(refD, Point.Left),
      Position(refF, Point.Up) -> Position(refA, Point.Right),
      Position(refF, Point.Right) -> Position(refB, Point.Right),
      Position(refF, Point.Down) -> Position(refC, Point.Right),
      Position(refF, Point.Left) -> Position(refE, Point.Left),
    )

    def from(original: Set[Point]): UnitPatron = {
      @tailrec
      def loop(toVisit: List[Point], visited: Set[Point], toUnitCube: Map[Position, Position]): Map[Position, Position] = {
        if (visited == original) toUnitCube
        else {
          toVisit match {
            case current :: other =>
              val toAdd = (for {
                dir <- Point.OrthogonalDirections
                next = current + dir if original.contains(next) && !visited.contains(next)
              } yield {
                val originalPos = Position(current, dir)
                val nextOriginPos = Position(next, dir)
                val currentUnitPos = toUnitCube(originalPos)
                val nextUnitPos = refForward(currentUnitPos)
                List(
                  nextOriginPos -> nextUnitPos,
                  nextOriginPos.turnRight -> nextUnitPos.turnRight,
                  nextOriginPos.turnLeft -> nextUnitPos.turnLeft,
                  nextOriginPos.opposite -> nextUnitPos.opposite,
                )
              }).flatten.toMap

              val neighbors = toAdd.keySet.map(_.pos)
              loop(neighbors ++: other, visited ++ neighbors, toUnitCube ++ toAdd)
          }
        }
      }

      val Init = original.head

      val toCube = loop(List(Init), Set(Init), (for {
        dir <- Point.OrthogonalDirections
      } yield Position(Init, dir) -> Position(refA, dir)
        ).toMap)
      UnitPatron(toCube)
    }
  }

  case class UnitPatron(toCube: Map[Position, Position]) {
    private def fromCube: Map[Position, Position] = toCube.map { case (a, b) => b -> a }

    def forward(original: Position): Position = fromCube(UnitPatron.refForward(toCube(original)))
  }

  object Grid {
    private def findAll(str: String, toFind: Char): Set[Point] = (for {
      (line, y) <- str.split("\n").zipWithIndex
      (c, x) <- line.toArray.zipWithIndex
      if c == toFind
    } yield Point(x, y)).toSet

    def parse(str: String, size: Int): Grid = {
      Grid(findAll(str, '.'), findAll(str, '#'), size)
    }
  }

  object Instruction {
    def parse(str: String): List[Instruction] = {
      @tailrec
      def loop(remain: List[Char], forward: Int, reverse: List[Instruction]): List[Instruction] = remain match {
        case c :: other if '0' <= c && c <= '9' => loop(other, forward * 10 + (c - '0'), reverse)
        case _ if forward != 0 => loop(remain, 0, Forward(forward) :: reverse)
        case Nil => reverse.reverse
        case 'R' :: other => loop(other, 0, TurnRight :: reverse)
        case 'L' :: other => loop(other, 0, TurnLeft :: reverse)
      }

      loop(str.trim.toList, 0, Nil)
    }
  }

  sealed trait Instruction

  final case class Forward(value: Int) extends Instruction

  case object TurnRight extends Instruction

  case object TurnLeft extends Instruction

  object Position {
    def initial(pos: Point): Position = Position(pos, Point.Right)
  }

  final case class Position(pos: Point, facing: Point) {
    private def facingScore: Int = facing match {
      case Point.Right => 0
      case Point.Down => 1
      case Point.Left => 2
      case Point.Up => 3
    }

    def score: Int = (pos.y + 1) * 1000 + 4 * (pos.x + 1) + facingScore

    def turnLeft: Position = Position(pos, facing.rotateLeft)

    def turnRight: Position = Position(pos, facing.rotateRight)

    def opposite: Position = Position(pos, facing.opposite)

    def forward: Position = Position(pos + facing, facing)
  }

  final case class Grid(paths: Set[Point], walls: Set[Point], size: Int) {
    private lazy val allPoints: Set[Point] = paths ++ walls

    def go(position: Position): Position = {
      val wanted = if (allPoints.contains(position.forward.pos)) position.forward
      else position.facing match {
        case Point.Right => position.copy(pos = allPoints.filter(_.y == position.pos.y).minBy(_.x))
        case Point.Left => position.copy(pos = allPoints.filter(_.y == position.pos.y).maxBy(_.x))
        case Point.Down => position.copy(pos = allPoints.filter(_.x == position.pos.x).minBy(_.y))
        case Point.Up => position.copy(pos = allPoints.filter(_.x == position.pos.x).maxBy(_.y))
      }

      if (paths.contains(wanted.pos)) wanted else position
    }

    lazy val patron: Patron = Patron(allPoints, size)

    def goCube(position: Position): Position = {
      val wanted = if (allPoints.contains(position.forward.pos)) position.forward
      else patron.forward(position)

      if (paths.contains(wanted.pos)) wanted else position
    }

    lazy val Initial: Position = Position.initial(paths.minBy(p => 1000 * p.y + p.x))
  }

  type Parsed = (Grid, List[Instruction])

  object solution extends common.Solution[Parsed]("2022", "22") {
    def parseSample(input: String): Parsed = {
      val Array(grid, instructions) = input.split("\n\n")

      Grid.parse(grid, 4) -> Instruction.parse(instructions)
    }

    override def parse(input: String): Parsed = {
      val Array(grid, instructions) = input.split("\n\n")

      Grid.parse(grid, 50) -> Instruction.parse(instructions)
    }

    override def part1(parsed: Parsed): Any = {
      val (grid, instructions) = parsed

      @tailrec
      def loop(remain: List[Instruction], position: Position, visited: Map[Point, Point]): Int = remain match {
        case Nil => position.score
        case TurnLeft :: other => loop(other, position.turnLeft, visited + (position.pos -> position.facing))
        case TurnRight :: other => loop(other, position.turnRight, visited + (position.pos -> position.facing))
        case Forward(0) :: other => loop(other, position, visited)
        case Forward(f) :: other =>
          val newPos = grid.go(position)
          if (newPos == position) loop(other, position, visited)
          else loop(Forward(f - 1) :: other, newPos, visited + (position.pos -> position.facing))
      }

      loop(instructions, grid.Initial, Map.empty)
    }

    override def part2(parsed: Parsed): Any = {
      val (grid, instructions) = parsed

      @tailrec
      def loop(remain: List[Instruction], position: Position, visited: Map[Point, Point]): Int = remain match {
        case Nil => position.score
        case TurnLeft :: other => loop(other, position.turnLeft, visited + (position.pos -> position.facing))
        case TurnRight :: other => loop(other, position.turnRight, visited + (position.pos -> position.facing))
        case Forward(0) :: other => loop(other, position, visited)
        case Forward(f) :: other =>
          val newPos = grid.goCube(position)
          if (newPos == position) loop(other, position, visited)
          else loop(Forward(f - 1) :: other, newPos, visited + (position.pos -> position.facing))
      }

      loop(instructions, grid.Initial, Map.empty)
    }
  }
}
