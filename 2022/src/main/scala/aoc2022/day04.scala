package aoc2022

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {
  object Range extends (String => Range) {
    val RangeR = "(\\d+)-(\\d+)".r

    def apply(str: String): Range = str match {
      case RangeR(min, max) => Range(min.toInt, max.toInt)
    }
  }

  final case class Range(min: Int, max: Int) {
    def include(other: Range): Boolean = min <= other.min && other.max <= max

    def distinct(other: Range): Boolean = other.max < min || max < other.min

    def overlap(other: Range): Boolean = !distinct(other)
  }

  type Parsed = List[List[Range]]

  object solution extends common.Solution[Parsed]("2022", "04") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.split(",").toList.map(Range))

    override def part1(parsed: Parsed): Any = parsed.count { case List(a, b) => a.include(b) || b.include(a) }

    override def part2(parsed: Parsed): Any = parsed.count { case List(a, b) => a.overlap(b) }
  }
}

