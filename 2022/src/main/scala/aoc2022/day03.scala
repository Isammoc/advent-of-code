package aoc2022

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {
  type Parsed = List[List[Char]]

  def priority(c: Char): Int =
    if (c >= 'a' && c <= 'z')
      c - 'a' + 1
    else
      c - 'A' + 27

  object solution extends common.Solution[Parsed]("2022", "03") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.toList)

    override def part1(parsed: Parsed): Any =
      parsed.map { line =>
        val (first, second) = line.splitAt(line.size / 2)
        first.intersect(second).map(priority).toSet.sum
      }.sum

    override def part2(parsed: Parsed): Any = parsed.grouped(3).map { group =>
      group.map(_.toSet).reduce(_ intersect _).map(priority).sum
    }.sum
  }
}

