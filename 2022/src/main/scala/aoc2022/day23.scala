package aoc2022

import common.grid.Point
import common.util.Repeat

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day23 extends common.AocApp(Day23Solution.solution)

object Day23Solution {
  object Grid {
    def parse(str: String): Grid = Grid((for {
      (line, y) <- str.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      if c == '#'
    } yield Point(x, y)).toSet, InitialDirections)

    private val InitialDirections = Queue(Point.OrthogonalDirections: _*)
  }

  final case class Grid(elves: Set[Point], directions: Queue[Point]) {
    private def oneElfWant(elf: Point): Option[Point] = {
      val maybe = (for {
        raw <- directions.toSet[Point]
        dir <- List(raw, raw + raw.rotateRight)
      } yield elf + dir).exists(elves.contains)

      if (maybe) {
        (for {
          raw <- directions
          if !List(raw + raw.rotateLeft, raw, raw + raw.rotateRight).map(_ + elf).exists(elves.contains)
        } yield elf + raw).headOption
      } else {
        None
      }
    }

    def turn: Grid = Grid(elves.groupMap(oneElfWant)(identity).collect {
      case (Some(target), wantedElves) if wantedElves.sizeIs == 1 => wantedElves.head -> target
    }.foldLeft(elves) {
      case (before, (start, target)) => before - start + target
    }, directions.tail :+ directions.head)

    override def toString: String = {
      val (minX: Int, maxX: Int, minY: Int, maxY: Int) = minmax

      (for {
        y <- minY to maxY
      } yield {
        (for {
          x <- minX to maxX
        } yield if (elves.contains(Point(x, y))) '#' else '.').mkString
      }
        ).mkString("\n")
    }

    private def minmax: (Int, Int, Int, Int) = {
      val minX = elves.map(_.x).min
      val maxX = elves.map(_.x).max
      val minY = elves.map(_.y).min
      val maxY = elves.map(_.y).max
      (minX, maxX, minY, maxY)
    }

    def score: Long = {
      val (minX: Int, maxX: Int, minY: Int, maxY: Int) = minmax

      (maxX - minX + 1) * (maxY - minY + 1) - elves.size
    }
  }

  type Parsed = Grid

  object solution extends common.Solution[Parsed]("2022", "23") {
    override def parse(input: String): Parsed = Grid.parse(input)

    override def part1(parsed: Parsed): Any = Repeat(10)(parsed)(_.turn).score

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Grid, turns: Int): Int = {
        val next = current.turn

        if (next.elves == current.elves) turns
        else loop(next, turns + 1)
      }

      loop(parsed, 1)
    }
  }
}
