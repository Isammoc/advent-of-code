package aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day21 extends common.AocApp(Day21Solution.solution)

object Day21Solution {
  sealed trait MathOperation {
    def result(left: Long, right: Long): Long

    def left(result: Long, right: Long): Long

    def right(result: Long, left: Long): Long
  }

  case object Add extends MathOperation {
    override def result(left: Long, right: Long): Long = left + right

    override def left(result: Long, right: Long): Long = result - right

    override def right(result: Long, left: Long): Long = result - left
  }

  case object Subtract extends MathOperation {
    override def result(left: Long, right: Long): Long = left - right

    override def left(result: Long, right: Long): Long = result + right

    override def right(result: Long, left: Long): Long = left - result
  }

  case object Times extends MathOperation {
    override def result(left: Long, right: Long): Long = left * right

    override def left(result: Long, right: Long): Long = result / right

    override def right(result: Long, left: Long): Long = result / left
  }

  case object Division extends MathOperation {
    override def result(left: Long, right: Long): Long = left / right

    override def left(result: Long, right: Long): Long = result * right

    override def right(result: Long, left: Long): Long = left / result
  }

  final case class Association(result: String, left: String, right: String, op: MathOperation) {
    def find(name: String, known: Map[String, Long]): Long =
      if (name == result) op.result(known(left), known(right))
      else if (name == left) op.left(known(result), known(right))
      else op.right(known(result), known(left))

    lazy val AllNames: Set[String] = Set(result, left, right)
  }

  type Parsed = (Map[String, Long], Map[String, Set[Association]])

  object solution extends common.Solution[Parsed]("2022", "21") {
    override def parse(input: String): Parsed = {
      val YellMonkeyR = "(\\w+):\\s+(\\d+)".r
      val AssociationR = "(\\w+):\\s+(\\w+)\\s+(.)\\s+(\\w+)".r

      @tailrec
      def loop(remain: List[String], known: Map[String, Long], associations: List[Association]): Parsed = remain match {
        case Nil =>
          known -> (for {
            association@Association(result, left, right, _) <- associations
            name <- List(result, left, right)
          } yield name -> association).groupMap(_._1)(_._2).view.mapValues(_.toSet).toMap

        case YellMonkeyR(name, value) :: other => loop(other, known + (name -> value.toLong), associations)
        case AssociationR(result, left, "+", right) :: other => loop(other, known, Association(result, left, right, op = Add) :: associations)
        case AssociationR(result, left, "-", right) :: other => loop(other, known, Association(result, left, right, op = Subtract) :: associations)
        case AssociationR(result, left, "*", right) :: other => loop(other, known, Association(result, left, right, op = Times) :: associations)
        case AssociationR(result, left, "/", right) :: other => loop(other, known, Association(result, left, right, op = Division) :: associations)
      }

      loop(input.split("\n").toList, Map.empty, List.empty)
    }

    override def part1(parsed: Parsed): Any = {
      val (alreadyKnown, associations) = parsed

      val RootName = "root"
      val ROOT = associations(RootName).find(_.result == RootName).get

      @tailrec
      def loop(toVisit: List[Association], known: Map[String, Long]): Long = toVisit match {
        case Association(a, b, c, _) :: other if List(a, b, c).forall(known.contains) => loop(other, known)
        case Association(result, left, right, op) :: _ if known.contains(left) && known.contains(right) =>
          if (result == RootName) {
            op.result(known(left), known(right))
          } else {
            loop(toVisit, known + (result -> op.result(known(left), known(right))))
          }
        case (current@Association(_, left, right, _)) :: _ =>
          val leftSet = associations(left).filter(_.result == left)
          val rightSet = associations(right).filter(_.result == right)

          loop(toVisit.prependedAll(leftSet ++ rightSet - current), known)
      }

      loop(List(ROOT), alreadyKnown)
    }

    override def part2(parsed: Parsed): Any = {
      val (alreadyKnown, associations) = parsed

      val RootName = "root"
      val HumanName = "humn"
      val ROOT = associations(RootName).find(_.result == RootName).get

      @tailrec
      def loop(toVisit: Queue[Association], known: Map[String, Long]): Long = {
        if (known.contains(HumanName)) known(HumanName)
        else toVisit match {
          case ROOT +: other =>
            if (known.contains(ROOT.left)) {
              loop(other, known + (ROOT.right -> known(ROOT.left)))
            } else if (known.contains(ROOT.right)) {
              loop(other, known + (ROOT.left -> known(ROOT.right)))
            } else {
              loop(other :+ ROOT, known)
            }
          case toRemove +: other if toRemove.AllNames.forall(known.contains) => loop(other, known)
          case toCompute +: other if toCompute.AllNames.count(known.contains) == 2 =>
            val unknown = toCompute.AllNames.find(c => !known.contains(c)).get

            loop(other, known + (unknown -> toCompute.find(unknown, known)))
          case turn +: other => loop(other :+ turn, known)
        }
      }

      loop(Queue.from(associations.values.flatten.toSet), alreadyKnown - HumanName)
    }
  }
}

