package aoc2022

object day06 extends common.AocApp(Day06Solution.solution)

object Day06Solution {
  type Parsed = List[Char]

  def check(size: Int)(str: List[Char], current: Int = 0): Int =
    if(str.take(size).toSet.sizeIs == size)
      current + size
    else
      check(size)(str.tail, current + 1)

  object solution extends common.Solution[Parsed]("2022", "06") {
    override def parse(input: String): Parsed = input.toList

    override def part1(parsed: Parsed): Any = check(4)(parsed)

    override def part2(parsed: Parsed): Any = check(14)(parsed)
  }
}

