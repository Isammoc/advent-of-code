package aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day11 extends common.AocApp(Day11Solution.solution)

object Day11Solution {
  final case class MonkeyId(value: Int) extends AnyVal

  final case class WorryLevel(value: Long) extends AnyVal

  object Operation {
    object Value {
      def parse(str: String): Value = str match {
        case "old" => OldValue
        case _ => FixedValue(str.toLong)
      }
    }

    sealed trait Value extends (WorryLevel => Long)

    case object OldValue extends Value {
      def apply(worryLevel: WorryLevel): Long = worryLevel.value
    }

    final case class FixedValue(value: Long) extends Value {
      def apply(worryLevel: WorryLevel): Long = value
    }

    object OpImpl {
      private val AddR = "(.+) \\+ (.+)".r
      private val MultiplyR = "(.+) \\* (.+)".r

      def parse(str: String): OpImpl = str match {
        case AddR(left, right) => Add(Value.parse(left), Value.parse(right))
        case MultiplyR(left, right) => Multiply(Value.parse(left), Value.parse(right))
      }
    }

    sealed trait OpImpl extends (WorryLevel => WorryLevel)

    case class Add(left: Value, right: Value) extends OpImpl {
      override def apply(worryLevel: WorryLevel): WorryLevel = WorryLevel(left(worryLevel) + right(worryLevel))
    }

    case class Multiply(left: Value, right: Value) extends OpImpl {
      override def apply(worryLevel: WorryLevel): WorryLevel = WorryLevel(left(worryLevel) * right(worryLevel))
    }

    private val OperationR = "  Operation: new = (.+)".r

    def parse(str: String): WorryLevel => WorryLevel = str match {
      case OperationR(op) => OpImpl.parse(op)
    }
  }

  object Action {
    private val TestR = "  Test: divisible by (\\d+)".r
    private val IfTrueR = "    If true: throw to monkey (\\d+)".r
    private val IfFalseR = "    If false: throw to monkey (\\d+)".r

    def parse(testLine: String, trueLine: String, falseLine: String): Action = (testLine, trueLine, falseLine) match {
      case (TestR(divider), IfTrueR(trueMonkey), IfFalseR(falseMonkey)) =>
        val dividerN = divider.toLong
        val trueMonkeyId = MonkeyId(trueMonkey.toInt)
        val falseMonkeyId = MonkeyId(falseMonkey.toInt)
        Action(dividerN, trueMonkeyId, falseMonkeyId)
    }
  }

  final case class Action(divider: Long, ifTrue: MonkeyId, ifFalse: MonkeyId) extends (WorryLevel => MonkeyId) {
    def apply(worryLevel: WorryLevel): MonkeyId =
      if (worryLevel.value % divider == 0) ifTrue else ifFalse
  }

  object Monkey {
    val MonkeyIdR = "Monkey (\\d+):".r
    val StartingItemsR = "  Starting items: (.*)".r

    def parse(str: String): Monkey = {
      val Array(monkeyLine, startingItemsLine, operationLine, testLine, trueLine, falseLine) = str.split("\n")
      val id = monkeyLine match {
        case MonkeyIdR(id) => MonkeyId(id.toInt)
      }
      val startingItems = startingItemsLine match {
        case StartingItemsR(items) => Queue.from(items.split(", ").map(_.toLong).map(WorryLevel))
      }
      val operation = Operation.parse(operationLine)
      val action = Action.parse(testLine, trueLine, falseLine)

      Monkey(id, startingItems, operation, action, inspectionCount = 0)
    }
  }

  final case class Monkey(id: MonkeyId, items: Queue[WorryLevel], operation: WorryLevel => WorryLevel, action: Action, inspectionCount: Long) {
    def enqueue(worryLevel: WorryLevel): Monkey = this.copy(items = items.enqueue(worryLevel))

    def oneItem(boring: WorryLevel => WorryLevel): (Monkey, Option[(MonkeyId, WorryLevel)]) = items match {
      case current +: other =>
        val nextWorryLevel = boring(operation(current))
        this.copy(items = other, inspectionCount = inspectionCount + 1) -> Some(action(nextWorryLevel) -> nextWorryLevel)
      case _ => this -> None
    }
  }

  type Parsed = List[Monkey]

  def dontWorry(parsed: Parsed, turnAmount: Int, boring: WorryLevel => WorryLevel): Long = {
    val monkeyIds = parsed.map(_.id).sortBy(_.value)

    @tailrec
    def turn(remainIds: List[MonkeyId], current: Map[MonkeyId, Monkey]): Map[MonkeyId, Monkey] = remainIds match {
      case Nil => current
      case currentId :: otherIds =>
        current(currentId).oneItem(boring) match {
          case (nextMonkey, None) => turn(otherIds, current + (currentId -> nextMonkey))
          case (nextMonkey, Some(toMonkeyId -> worryLevel)) =>
            turn(remainIds, current + (currentId -> nextMonkey) + (toMonkeyId -> current(toMonkeyId).enqueue(worryLevel)))
        }
    }

    @tailrec
    def loop(remainTurns: Int, current: Map[MonkeyId, Monkey]): Long =
      if (remainTurns <= 0)
        current.values.map(_.inspectionCount).toList.sorted.reverse.take(2).product
      else loop(remainTurns - 1, turn(monkeyIds, current))

    loop(turnAmount, parsed.map(m => m.id -> m).toMap)
  }

  object solution extends common.Solution[Parsed]("2022", "11") {
    override def parse(input: String): Parsed = input.split("\n\n").map(Monkey.parse).toList

    def gcd(a: Long, b: Long): Long =
      if (b == 0) a else gcd(b, a % b)

    def lcm(a: Long, b: Long): Long = a * b / gcd(a, b)

    override def part1(parsed: Parsed): Any = {
      val turnAmount = 20
      val boring = (prev: WorryLevel) => WorryLevel(prev.value / 3)
      dontWorry(parsed, turnAmount, boring)
    }

    override def part2(parsed: Parsed): Any = {
      val turnAmount = 10000
      val lcmWorries = parsed.map(_.action.divider).foldLeft(1L)((a, b) => lcm(a, b))
      val boring = (prev: WorryLevel) => WorryLevel(prev.value % lcmWorries)
      dontWorry(parsed, turnAmount, boring)
    }

  }
}

