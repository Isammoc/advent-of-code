package aoc2022

import common.grid.Point

import scala.annotation.tailrec

object day08 extends common.AocApp(Day08Solution.solution)

object Day08Solution {
  type Tree = Int

  object Grid {
    def from(str: String): Grid =
      Grid(
        (for {
          (line, y) <- str.split("\n").zipWithIndex
          (tree, x) <- line.map(_.toString.toInt).zipWithIndex
        } yield Point(x, y) -> tree).toMap
      )
  }

  final case class Grid(trees: Map[Point, Tree]) {
    val minX: Int = 0
    val minY: Int = 0
    lazy val maxX: Int = trees.keys.map(_.x).max
    lazy val maxY: Int = trees.keys.map(_.y).max

    def isVisibleFromBorder(pos: Point): Boolean = {
      val height = trees(pos)

      @tailrec
      def loop(current: Point, dir: Point): Boolean = {
        val newPos = current + dir
        trees.get(newPos) match {
          case None => true
          case Some(t) if t >= height => false
          case _ => loop(newPos, dir)
        }
      }

      Point.OrthogonalDirections.exists(loop(pos, _))
    }

    def scenicScore(pos: Point): Int = {
      val height = trees(pos)

      @tailrec
      def loop(current: Point, dir: Point, found: Int): Int = {
        val newPos = current + dir
        trees.get(newPos) match {
          case None => found
          case Some(t) if t >= height => found + 1
          case _ => loop(newPos, dir, found + 1)
        }
      }

      Point.OrthogonalDirections.map(dir => loop(pos, dir, 0)).product
    }

  }

  type Parsed = Grid

  object solution extends common.Solution[Parsed]("2022", "08") {
    override def parse(input: String): Parsed = Grid.from(input)

    override def part1(parsed: Parsed): Any = (for {
      x <- parsed.minX to parsed.maxX
      y <- parsed.minY to parsed.maxY
    } yield Point(x, y)).count(parsed.isVisibleFromBorder)

    override def part2(parsed: Parsed): Any = (for {
      x <- parsed.minX to parsed.maxX
      y <- parsed.minY to parsed.maxY
    } yield parsed.scenicScore(Point(x, y))).max
  }
}
