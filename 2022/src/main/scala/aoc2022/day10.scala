package aoc2022

object day10 extends common.AocApp(Day10Solution.solution)

object Day10Solution {

  object VideoSystem {
    def from(instr: List[Instruction]): LazyList[VideoSystem] =
      LazyList.unfold((instr, VideoSystem(1, 0))) {
        case (instr :: remain, currentSystem) =>
          val nextSystem = currentSystem.next(instr)
          println(nextSystem)
          Some(nextSystem -> (remain, nextSystem))
        case (_, system) => Some(system -> (Nil, system))
      }
  }

  final case class VideoSystem(x: Long, cycle: Int) {
    def next(instr: Instruction): VideoSystem = instr match {
      case Instruction.AddX(value) => VideoSystem(x + value, cycle + 2)
      case Instruction.NoOp => VideoSystem(x, cycle + 1)
    }
  }

  sealed trait Instruction

  object Instruction {
    final case class AddX(value: Long) extends Instruction

    case object NoOp extends Instruction

    private val AddxR = "addx\\s+(-?\\d+)".r
    private val NoopR = "noop".r

    def from(str: String): Instruction = str match {
      case NoopR() => NoOp
      case AddxR(v) => AddX(v.toLong)
    }
  }

  type Parsed = List[Instruction]

  object solution extends common.Solution[Parsed]("2022", "10") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(Instruction.from)

    override def part1(parsed: Parsed): Any = {
      val importantCycles = List(20, 60, 100, 140, 180, 220)
      List.unfold((VideoSystem.from(parsed), importantCycles)) {
        case (cycles, currentCycle :: remain) =>
          val strength = cycles.takeWhile(_.cycle < currentCycle).last.x * currentCycle
          val nextCycles = cycles.dropWhile(_.cycle < currentCycle)
          Some(strength -> (nextCycles, remain))
        case _ => None
      }.sum
    }

    override def part2(parsed: Parsed): Any = {
      List.unfold((VideoSystem(1, -1) #:: VideoSystem.from(parsed), 0)) {
        case (_, cycle) if cycle >= 40 * 6 => None
        case (videoSystems, currentCycle) =>
          val before = videoSystems.takeWhile(_.cycle <= currentCycle)
          val middleSprite = before.last.x
          val nextCycles = videoSystems.drop(before.size - 1)
          val pixel = if ((middleSprite - (currentCycle % 40)).abs <= 1) '#' else '.'
          Some(pixel -> (nextCycles, currentCycle + 1))
      }.grouped(40).map(_.mkString).mkString("\n")
    }
  }
}

