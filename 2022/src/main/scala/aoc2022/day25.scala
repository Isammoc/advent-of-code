package aoc2022

import scala.annotation.tailrec

object day25 extends common.AocApp(Day25Solution.solution)

object Day25Solution {
  object Snafu {
    def toLong(str: String): Long = {
      @tailrec
      def loop(remain: List[Char], result: Long): Long = remain match {
        case Nil => result
        case '2' :: t => loop(t, result * 5 + 2)
        case '1' :: t => loop(t, result * 5 + 1)
        case '0' :: t => loop(t, result * 5)
        case '-' :: t => loop(t, result * 5 - 1)
        case '=' :: t => loop(t, result * 5 - 2)
      }

      loop(str.toList, 0)
    }

    def fromLong(value: Long): String = {
      @tailrec
      def loop(remain: Long, result: List[Char]): String =
        if (remain == 0) result.mkString
        else {
          remain % 5 match {
            case 0 => loop(remain / 5, '0' :: result)
            case 1 => loop(remain / 5, '1' :: result)
            case 2 => loop(remain / 5, '2' :: result)
            case 3 => loop(remain / 5 + 1, '=' :: result)
            case 4 => loop(remain / 5 + 1, '-' :: result)
          }
        }

      loop(value, Nil)
    }
  }

  object solution extends common.Solution[List[Long]]("2022", "25") {
    override def parse(input: String): List[Long] = input.split("\n").map(Snafu.toLong).toList

    override def part1(parsed: List[Long]): Any = Snafu.fromLong(parsed.sum)

    override def part2(parsed: List[Long]): Any = "NOT IMPLEMENTED"
  }
}

