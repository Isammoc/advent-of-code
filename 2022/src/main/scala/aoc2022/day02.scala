package aoc2022

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  sealed abstract class Result(val score: Int)

  case object Lose extends Result(0)
  case object Draw extends Result(3)
  case object Win extends Result(6)

  object Choice {
    val All = List(Rock, Paper, Scissors)
  }
  sealed trait Choice {
    def resultAgainst(other: Choice): Result
    def score: Int
  }
  case object Rock extends Choice {
    override def resultAgainst(other: Choice): Result = other match {
      case Paper => Lose
      case Rock => Draw
      case Scissors => Win
    }
    val score = 1
  }
  case object Paper extends Choice {
    override def resultAgainst(other: Choice): Result = other match {
      case Rock => Win
      case Paper => Draw
      case Scissors => Lose
    }
    val score = 2
  }
  case object Scissors extends Choice {
    override def resultAgainst(other: Choice): Result = other match {
      case Rock => Lose
      case Paper => Win
      case Scissors => Draw
    }
    val score = 3
  }

  val MovementScore = Map(
    'X' -> 1, // Rock
    'Y' -> 2, // Paper
    'Z' -> 3, // Scissors
  )

  val Score = Map(
    'X' -> Map( // Rock
      'A' -> 3, // Rock
      'B' -> 0, // Paper
      'C' -> 6, // Scissors
    ),
    'Y' -> Map( // Paper
      'A' -> 6, // Rock
      'B' -> 3, // Paper
      'C' -> 0, // Scissors
    ),
    'Z' -> Map( // Scissors
      'A' -> 0, // Rock
      'B' -> 6, // Paper
      'C' -> 3, // Scissors
    ),
  )


  val ScoreByResult = Map(

  )
  type Parsed = List[(Char, Char)]

  object solution extends common.Solution[Parsed]("2022", "02") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(s => s(0) -> s(2))

    def opponent(c: Char): Choice = c match {
      case 'A' => Rock
      case 'B' => Paper
      case 'C' => Scissors
    }

    override def part1(parsed: Parsed): Any = {
      def me(c: Char): Choice = c match {
        case 'X' => Rock
        case 'Y' => Paper
        case 'Z' => Scissors
      }
      parsed.map { case (o, m) =>
      me(m).score + me(m).resultAgainst(opponent(o)).score
      }.sum
    }

    override def part2(parsed: Parsed): Any = {
      def wantedResult(c: Char) = c match {
        case 'X' => Lose
        case 'Y' => Draw
        case 'Z' => Win
      }

      parsed.map { case (o, r) =>
        val opp = opponent(o)
        val result = wantedResult(r)

        val me = Choice.All.find(_.resultAgainst(opp) == result).get
        me.score + result.score
      }.sum
    }
  }
}

