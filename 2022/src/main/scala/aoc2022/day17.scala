package aoc2022

import common.grid.Point

import scala.annotation.tailrec

object day17 extends common.AocApp(Day17Solution.solution)

object Day17Solution {
  type Shape = Set[Point]

  object Shapes {
    private val Horizontal = Set(Point(0, 0), Point(1, 0), Point(2, 0), Point(3, 0))
    private val Plus = Set(Point(1, 0), Point(0, 1), Point(1, 1), Point(2, 1), Point(1, 2))
    private val Corner = Set(Point(0, 0), Point(1, 0), Point(2, 0), Point(2, 1), Point(2, 2))
    private val Vertical = Set(Point(0, 0), Point(0, 1), Point(0, 2), Point(0, 3))
    private val Square = Set(Point(0, 0), Point(0, 1), Point(1, 0), Point(1, 1))

    val FallingShapes: List[Shape] = List(Horizontal, Plus, Corner, Vertical, Square)
  }

  final case class Rock(shape: Shape, pos: Point) {
    def current: Shape = for {
      p <- shape
    } yield p + pos

    def go(direction: Point): Rock = this.copy(pos = pos + direction)
  }


  final case class Chamber(fixed: Set[Point]) {
    def isColliding(rock: Rock): Boolean = rock.current.exists(rockPoint => rockPoint.y <= 0 || rockPoint.x < 0 || rockPoint.x >= 7 || fixed.contains(rockPoint))

    def +(rock: Rock): Chamber = Chamber(fixed ++ rock.current)

    def starting: Point = Point(2, fixed.map(_.y).maxOption.getOrElse(0) + 4)

    override def toString: String =
      (for {
        y <- fixed.map(_.y).maxOption.getOrElse(0) until 0 by -1
      } yield (for {
        x <- -1 to 7
      } yield {
        val pos = Point(x, y)
        if (x == -1 || x == 7) '|' else if (fixed.contains(pos)) '#' else '.'
      }).mkString).mkString("\n") + "\n+-------+"

    def reduce: (Int, Chamber) = if (starting.y <= 50) (0, this) else {
      val diff = starting.y - 30
      diff -> Chamber(
        for {
          Point(x, y) <- fixed if y > diff
        } yield Point(x, y - diff))
    }
  }

  object Chamber {
    val Initial = Chamber(Set.empty)
  }

  final case class State(winds: List[Point], shapes: List[Shape], rock: Option[Rock], chamber: Chamber) {
    @tailrec
    def turn(initialWinds: List[Point], initialShapes: List[Shape]): State = winds match {
      case Nil => this.copy(winds = initialWinds).turn(initialWinds, initialShapes)
      case wind :: otherWinds =>
        rock match {
          case None =>
            shapes match {
              case Nil => this.copy(shapes = initialShapes).turn(initialWinds, initialShapes)
              case shape :: otherShapes => this.copy(shapes = otherShapes, rock = Some(Rock(shape, chamber.starting))).turn(initialWinds, initialShapes)
            }
          case Some(rock) =>
            val windRock = if (chamber.isColliding(rock.go(wind))) rock else rock.go(wind)
            if (chamber.isColliding(windRock.go(Point.Up))) this.copy(chamber = chamber + windRock, rock = None, winds = otherWinds)
            else this.copy(rock = Some(windRock.go(Point.Up)), winds = otherWinds)
        }
    }

    def isRockFixed: Boolean = rock.isEmpty

    def reduce: (Int, State) = {
      val (diff, newChamber) = chamber.reduce
      diff -> this.copy(chamber = newChamber)
    }
  }

  object State {
    val Initial: State = State(Nil, Nil, None, Chamber.Initial)
  }

  type Parsed = List[Point]

  object solution extends common.Solution[Parsed]("2022", "17") {
    override def parse(input: String): Parsed = input.toList.map {
      case '<' => Point.Left
      case '>' => Point.Right
    }

    private def solve(parsed: Parsed, targetRocks: Long) = {
      @tailrec
      def loop(state: State, rocksCount: Long, height: Long, visited: Map[State, (Long, Long)]): Long = {
        if (rocksCount >= targetRocks) height + state.chamber.fixed.map(_.y).max
        else {
          val nextState = state.turn(parsed, Shapes.FallingShapes)
          if (nextState.isRockFixed) {
            val newRocksCount = rocksCount + 1
            val (diff, newState) = nextState.reduce
            val newHeight = height + diff
            if (diff == 0 || !visited.contains(newState)) {
              loop(newState, newRocksCount, newHeight, visited + (newState -> (newHeight, newRocksCount)))
            } else {
              val (similarHeight, similarRocksCount) = visited(newState)

              val heightByZap = newHeight - similarHeight
              val rocksByZap = newRocksCount - similarRocksCount

              val zappedBlocks = (targetRocks - similarRocksCount) / rocksByZap

              val newlyHeight = heightByZap * zappedBlocks + similarHeight
              val newlyRocksCount = rocksByZap * zappedBlocks + similarRocksCount

              loop(newState, newlyRocksCount, newlyHeight, Map.empty)
            }
          } else {
            loop(nextState, rocksCount, height, visited)
          }
        }
      }

      loop(State.Initial, 0, 0, Map.empty)
    }

    override def part1(parsed: Parsed): Any = solve(parsed, 2022)

    override def part2(parsed: Parsed): Any = solve(parsed, 1000000000000L)
  }
}

