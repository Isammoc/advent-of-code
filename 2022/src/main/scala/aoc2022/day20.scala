package aoc2022

import common.util.Repeat

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day20 extends common.AocApp(Day20Solution.solution)

object Day20Solution {
  object File {
    def parse(input: String): File = File(Queue.from(input.split("\n").map(_.toInt).zipWithIndex.map { case (v, pos) => Node(pos, v) }))
  }

  final case class Node(pos: Int, value: Long)

  final case class File(queue: Queue[Node]) {
    private lazy val M = queue.size

    private def modulo(n: Long): Long = ((n % M) + M) % M

    private def place(n: Int): File = {
      var newQueue = queue
      while (newQueue.head.pos != n) {
        val h +: other = newQueue
        newQueue = other :+ h
      }
      File(newQueue)
    }

    private def turn(n: Long): File = {
      @tailrec
      def turn(n: Long, q: Queue[Node]): Queue[Node] =
        if (n <= 0) q
        else turn(n - 1, q.tail :+ q.head)

      File(turn(modulo(n), queue))
    }

    private def mix(n: Int): File = {
      val init = this.place(n)
      init.tail.turn(init.head.value) :+ init.head
    }

    def mixAll: File = queue.indices.foldLeft(this)((current, n) => current.mix(n))

    private def findZero: File = {
      var newQueue = queue
      while (newQueue.head.value != 0) {
        val h +: other = newQueue
        newQueue = other :+ h
      }
      File(newQueue)
    }

    def grove: Long = {
      val a = findZero.turn(1000).head.value
      val b = findZero.turn(2000).head.value
      val c = findZero.turn(3000).head.value

      a + b + c
    }

    def tail: File = File(queue.tail)

    private def :+(n: Node): File = File(queue :+ n)

    def head: Node = queue.head
  }

  type Parsed = File

  object solution extends common.Solution[Parsed]("2022", "20") {
    override def parse(input: String): Parsed = File.parse(input.trim)

    override def part1(parsed: Parsed): Any = parsed.mixAll.grove

    override def part2(parsed: Parsed): Any = {
      Repeat(10)(File(for {
        Node(pos, value) <- parsed.queue
      } yield Node(pos, value * 811589153)))(_.mixAll).grove
    }
  }
}

