package aoc2022

import scala.annotation.tailrec

object day19 extends common.AocApp(Day19Solution.solution)

object Day19Solution {
  object Inventory {
    val Zero: Inventory = Inventory(0, 0, 0, 0)
    val InitialRobot: Inventory = Inventory(1, 0, 0, 0)
  }

  final case class Inventory(ore: Int, clay: Int, obsidian: Int, geode: Int) {
    private def opposite: Inventory = Inventory(-ore, -clay, -obsidian, -geode)

    def +(that: Inventory): Inventory = Inventory(ore + that.ore, clay + that.clay, obsidian + that.obsidian, geode + that.geode)

    def -(that: Inventory): Inventory = this + that.opposite

    def *(size: Int): Inventory = Inventory(size * ore, size * clay, size * obsidian, size * geode)

    def max(that: Inventory): Inventory = Inventory(math.max(ore, that.ore), math.max(clay, that.clay), math.max(obsidian, that.obsidian), math.max(geode, that.geode))

    def isPositive: Boolean = ore >= 0 && clay >= 0 && obsidian >= 0 && geode >= 0
  }

  object Blueprint {
    private val BlueprintR = "Blueprint (\\d+): Each ore robot costs (\\d+) ore. Each clay robot costs (\\d+) ore. Each obsidian robot costs (\\d+) ore and (\\d+) clay. Each geode robot costs (\\d+) ore and (\\d+) obsidian.".r

    def parse(str: String): Blueprint = str match {
      case BlueprintR(id, oreRobotOre, clayRobotOre, obsidianRobotOre, obsidianRobotClay, geodeRobotOre, geodeRobotObsidian) =>
        Blueprint(
          id = id.toInt,
          oreRobot = Inventory.Zero.copy(ore = oreRobotOre.toInt),
          clayRobot = Inventory.Zero.copy(ore = clayRobotOre.toInt),
          obsidianRobot = Inventory.Zero.copy(ore = obsidianRobotOre.toInt, clay = obsidianRobotClay.toInt),
          geodeRobot = Inventory.Zero.copy(ore = geodeRobotOre.toInt, obsidian = geodeRobotObsidian.toInt))
    }
  }

  final case class Blueprint(id: Int, oreRobot: Inventory, clayRobot: Inventory, obsidianRobot: Inventory, geodeRobot: Inventory) {
    lazy val max: Inventory = oreRobot.max(clayRobot).max(obsidianRobot).max(geodeRobot)
  }

  type Parsed = List[Blueprint]

  final case class State(remaining: Int, robots: Inventory, inv: Inventory) {
    private def geodeChild(blueprint: Blueprint): Option[State] = if (robots.obsidian > 0) {
      var current = this
      while (!(current.inv - blueprint.geodeRobot).isPositive) {
        current = State(current.remaining - 1, robots, current.inv + robots)
      }
      Some(State(current.remaining - 1, robots.copy(geode = robots.geode + 1), current.inv + robots - blueprint.geodeRobot))
    } else None

    private def obsidianChild(blueprint: Blueprint): Option[State] = if (robots.clay > 0 && robots.obsidian < blueprint.max.obsidian && inv.obsidian < blueprint.max.obsidian + 2) {
      var current = this
      while (!(current.inv - blueprint.obsidianRobot).isPositive) {
        current = State(current.remaining - 1, robots, current.inv + robots)
      }
      Some(State(current.remaining - 1, robots.copy(obsidian = robots.obsidian + 1), current.inv + robots - blueprint.obsidianRobot))
    } else None

    private def clayChild(blueprint: Blueprint): Option[State] = if (robots.clay < blueprint.max.clay && inv.clay < blueprint.max.clay + 2) {
      var current = this
      while (!(current.inv - blueprint.clayRobot).isPositive) {
        current = State(current.remaining - 1, robots, current.inv + robots)
      }
      Some(State(current.remaining - 1, robots.copy(clay = robots.clay + 1), current.inv + robots - blueprint.clayRobot))
    } else None

    private def oreChild(blueprint: Blueprint): Option[State] = if (robots.ore < blueprint.max.ore && inv.ore < blueprint.max.ore + 2) {
      var current = this
      while (!(current.inv - blueprint.oreRobot).isPositive) {
        current = State(current.remaining - 1, robots, current.inv + robots)
      }
      Some(State(current.remaining - 1, robots.copy(ore = robots.ore + 1), current.inv + robots - blueprint.oreRobot))
    } else None

    def children(blueprint: Blueprint): Seq[State] =
      State(0, robots, inv + robots * remaining) :: List(geodeChild(blueprint), obsidianChild(blueprint), clayChild(blueprint), oreChild(blueprint)).flatten.filter(_.remaining >= 0)
  }

  object solution extends common.Solution[Parsed]("2022", "19") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(Blueprint.parse)

    private def maxGeode(blueprint: Blueprint, time: Int): Int = {
      @tailrec
      def loop(toVisit: List[State], result: Int): Int = toVisit match {
        case State(0, _, inventory) +: other => loop(other, math.max(result, inventory.geode))
        case state :: other => loop(other.prependedAll(state.children(blueprint)), result)
        case _ => result
      }

      loop(List(State(time, Inventory.InitialRobot, Inventory.Zero)), 0)
    }

    override def part1(parsed: Parsed): Any = parsed.map { blueprint =>
      maxGeode(blueprint, 24) * blueprint.id
    }.sum

    override def part2(parsed: Parsed): Any = parsed.take(3).map { blueprint =>
      maxGeode(blueprint, 32)
    }.product
  }
}

