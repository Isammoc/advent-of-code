package aoc2022

import scala.annotation.tailrec

object day15 extends common.AocApp(Day15Solution.solution)

object Day15Solution {
  final case class Pos(x: Long, y: Long) {
    def distance(other: Pos): Long = (x - other.x).abs + (y - other.y).abs
  }

  object Sensor {
    private val SensorR = "Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)".r

    def parse(str: String): Sensor = str match {
      case SensorR(posX, posY, beaconX, beaconY) => Sensor(Pos(posX.toLong, posY.toLong), Pos(beaconX.toLong, beaconY.toLong))
    }
  }

  final case class Sensor(pos: Pos, closestBeacon: Pos) {
    lazy val distanceToBeacon: Long = pos.distance(closestBeacon)
  }

  type Parsed = List[Sensor]

  object solution extends common.Solution[Parsed]("2022", "15") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(Sensor.parse)

    def part1WithY(parsed: Parsed, y: Long): Long = {

      @tailrec
      def loop(coveredRanges: List[(Long, Long)], count: Long): Long = coveredRanges match {
        case Nil => count
        case (start, x1) :: (x2, end) :: other if x2 <= x1 => loop((start, math.max(x1, end)) :: other, count)
        case (start, end) :: other => loop(other, count + end - start + 1)
      }

      loop(coveredRangesForY(parsed, y), 0) - parsed.map(_.closestBeacon).toSet.count(_.y == y)
    }


    def part2WithMax(parsed: Parsed, max: Long): Long = {
      def rangesFor(y: Long): List[(Long, Long)] = {
        @tailrec
        def loop(coveredRanges: List[(Long, Long)], reverse: List[(Long, Long)]): List[(Long, Long)] = {
          coveredRanges match {
            case Nil => reverse.reverse
            case (start, x1) :: (x2, end) :: other if x2 <= x1 => loop((start, math.max(x1, end)) :: other, reverse)
            case (start, end) :: other => loop(other, (start, end) :: reverse)
          }
        }

        loop(coveredRangesForY(parsed, y), Nil)
      }

      val (y, List((_, x), (_, _))) = LazyList.iterate(0L)(_ + 1).takeWhile(_ <= max).map(y => y -> rangesFor(y)).dropWhile(_._2.sizeIs < 2).head

      (x + 1) * 4000000 + y
    }

    private def coveredRangesForY(parsed: Parsed, y: Long) = {
      (for {
        sensor <- parsed
        d = sensor.distanceToBeacon - (sensor.pos.y - y).abs
        if d >= 0
      } yield (sensor.pos.x - d) -> (sensor.pos.x + d)).sortBy(_._1)
    }

    override def part1(parsed: Parsed): Any = part1WithY(parsed, 2000000)

    override def part2(parsed: Parsed): Any = part2WithMax(parsed, 4000000)
  }
}

