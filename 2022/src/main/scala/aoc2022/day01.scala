package aoc2022

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed = List[List[Int]]

  object solution extends common.Solution[Parsed]("2022", "01") {
    override def parse(input: String): Parsed = input.split("\n\n").toList.map(_.split("\n").map(_.toInt).toList)

    override def part1(parsed: Parsed): Any = parsed.map(_.sum).max

    override def part2(parsed: Parsed): Any = parsed.map(_.sum).sorted.reverse.take(3).sum
  }
}

