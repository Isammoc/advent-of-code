package aoc2022

import common.grid.Point

import scala.annotation.tailrec

object day14 extends common.AocApp(Day14Solution.solution)

object Day14Solution {
  val AllDirections: List[Point] = List(Point.Down, Point.Down + Point.Left, Point.Down + Point.Right)

  object Pos {
    def parse(str: String): Point = {
      val Array(x, y) = str.split(",")
      Point(x.toInt, y.toInt)
    }
  }

  type Parsed = Set[Point]

  object solution extends common.Solution[Parsed]("2022", "14") {
    override def parse(input: String): Parsed = (for {
      line <- input.split("\n").map(_.split(" -> ").map(Pos.parse).toList)
      (start, end) <- line.zip(line.tail)
      pos <- for {
        x <- math.min(start.x, end.x) to math.max(start.x, end.x)
        y <- math.min(start.y, end.y) to math.max(start.y, end.y)
      } yield Point(x, y)
    } yield pos).toSet

    override def part1(parsed: Parsed): Any = {
      val abyss = parsed.map(_.y).max

      @tailrec
      def loop(path: List[Point], blocked: Set[Point], count: Int): Int = path match {
        case Point(_, y) :: _ if y > abyss => count
        case current :: t =>
          AllDirections.map(current + _).find(p => !blocked.contains(p)) match {
            case None => loop(t, blocked + current, count + 1)
            case Some(p) => loop(p :: path, blocked, count)
          }
      }

      loop(List(Point(500, 0)), parsed, 0)
    }

    override def part2(parsed: Parsed): Any = {
      val abyss = parsed.map(_.y).max + 1

      @tailrec
      def loop(path: List[Point], blocked: Set[Point], count: Int): Int = path match {
        case Nil => count
        case current :: t =>
          AllDirections.map(current + _).find(p => !blocked.contains(p) && p.y <= abyss) match {
            case None => loop(t, blocked + current, count + 1)
            case Some(p) => loop(p :: path, blocked, count)
          }
      }

      loop(List(Point(500, 0)), parsed, 0)
    }
  }
}

