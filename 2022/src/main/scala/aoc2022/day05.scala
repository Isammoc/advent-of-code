package aoc2022

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  object Dock extends (String => Dock) {
    def apply(str: String): Dock = Dock(
      str
        .split("\n")
        .toList
        .map(_.toList)
        .transpose
        .tail
        .zipWithIndex
        .filter { case (_, i) => i % 4 == 0 }
        .map { case (stack, _) =>
          val i :: value = stack.filter(_ != ' ').reverse
          s"$i".toInt -> value.reverse
        }
        .toMap
    )
  }

  final case class Dock(stacks: Map[Int, List[Char]]) {
    def move(move: Move): Dock = move match {
      case Move(count, from, to) =>
        val (toMove, stay) = stacks(from).splitAt(count)
        Dock(stacks + (from -> stay) + (to -> (toMove.reverse ++ stacks(to))))
    }

    def move2(move: Move): Dock = move match {
      case Move(count, from, to) =>
        val (toMove, stay) = stacks(from).splitAt(count)
        Dock(stacks + (from -> stay) + (to -> (toMove ++ stacks(to))))
    }
  }

  object Move extends (String => Move) {
    private val MoveR = "move (\\d+) from (\\d+) to (\\d+)".r

    def apply(str: String): Move = str match {
      case MoveR(count, from, to) => Move(count.toInt, from.toInt, to.toInt)
    }
  }

  final case class Move(count: Int, from: Int, to: Int)

  type Parsed = (Dock, List[Move])

  object solution extends common.Solution[Parsed]("2022", "05") {
    override def parse(input: String): Parsed = {
      val Array(dock, moves) = input.split("\n\n")
      Dock(dock) -> moves.split("\n").toList.map(Move)
    }

    override def part1(parsed: Parsed): Any = (parsed match {
      case (initial, moves) => moves.foldLeft(initial)(_ move _)
    }).stacks.toList.sortBy(_._1).map(_._2.headOption.mkString).mkString

    override def part2(parsed: Parsed): Any = (parsed match {
      case (initial, moves) => moves.foldLeft(initial)(_ move2 _)
    }).stacks.toList.sortBy(_._1).map(_._2.headOption.mkString).mkString
  }
}

