package aoc2022

import aoc2022.Day13Solution.Value.{ListValue, SimpleValue}

import scala.annotation.tailrec

object day13 extends common.AocApp(Day13Solution.solution)

object Day13Solution {
  sealed trait Value extends Ordered[Value] {
    @tailrec
    private def compareList(left: List[Value], right: List[Value]): Int = (left, right) match {
      case (Nil, Nil) => 0
      case (Nil, _) => -1
      case (_, Nil) => 1
      case (lh :: ltail, rh :: rtail) =>
        val res = compare(lh, rh)
        if (res == 0) {
          compareList(ltail, rtail)
        } else res
    }

    @tailrec
    private def compare(left: Value, right: Value): Int = (left, right) match {
      case (SimpleValue(l), SimpleValue(r)) => l.compare(r)
      case (s: SimpleValue, list: ListValue) => compare(ListValue(List(s)), list)
      case (list: ListValue, s: SimpleValue) => compare(list, ListValue(List(s)))
      case (ListValue(l), ListValue(r)) => compareList(l, r)
    }

    def compare(that: Value): Int = compare(this, that)
  }

  object Value {
    object SimpleValue {
      def parse(remain: List[Char]): (List[Char], Value) = {
        remain.dropWhile(c => '0' <= c && c <= '9') -> SimpleValue(remain.takeWhile(c => '0' <= c && c <= '9').mkString.toInt)
      }
    }

    final case class SimpleValue(value: Int) extends Value

    object ListValue {
      def parse(remain: List[Char]): (List[Char], Value) = {
        @tailrec
        def loop(remain: List[Char], reverseCurrent: List[Value]): (List[Char], Value) = remain match {
          case ']' :: other => other -> ListValue(reverseCurrent.reverse)
          case '[' :: _ =>
            val (nextRemain, readValue) = ListValue.parse(remain)
            loop(nextRemain, readValue :: reverseCurrent)
          case ',' :: other => loop(other, reverseCurrent)
          case _ =>
            val (nextRemain, readValue) = SimpleValue.parse(remain)
            loop(nextRemain, readValue :: reverseCurrent)
        }

        loop(remain.drop(1), Nil)
      }
    }

    final case class ListValue(value: List[Value]) extends Value

    def parse(line: String): Value = ListValue.parse(line.toList)._2
  }

  object Pair {
    def parse(str: String, i: Int): Pair = {
      val Array(left, right) = str.split("\n")
      Pair(i + 1, Value.parse(left), Value.parse(right))
    }

  }

  final case class Pair(i: Int, left: Value, right: Value)

  type Parsed = List[Pair]

  object solution extends common.Solution[Parsed]("2022", "13") {
    override def parse(input: String): Parsed = input.split("\n\n").zipWithIndex.map((Pair.parse _).tupled).toList

    override def part1(parsed: Parsed): Any =
      parsed.filter(pair => pair.left < pair.right).map(_.i).sum

    override def part2(parsed: Parsed): Any = {
      val allPackets = for {
        Pair(_, left, right) <- parsed
        l <- List(left, right)
      } yield l

      val packetTwo = ListValue(List(ListValue(List(SimpleValue(2)))))
      val packetSix = ListValue(List(ListValue(List(SimpleValue(6)))))

      val sorted = (packetTwo :: packetSix :: allPackets).sorted.zipWithIndex

      val packetTwoIndex = sorted.find(_._1 == packetTwo).get._2 + 1
      val packetSixIndex = sorted.find(_._1 == packetSix).get._2 + 1

      packetTwoIndex * packetSixIndex
    }
  }
}

