package aoc2022

import common.grid.Point

import scala.annotation.tailrec

object day09 extends common.AocApp(Day09Solution.solution)

object Day09Solution {

  object Direction {
    def parse(str: String): Point = str match {
      case "R" => Point.Right
      case "U" => Point.Up
      case "D" => Point.Down
      case "L" => Point.Left
    }
  }

  implicit class RichPoint(thiz: Point) {
    def follow(head: Point): Point =
      if ((head.x - thiz.x).abs <= 1 && (head.y - thiz.y).abs <= 1) thiz
      else {
        def delta(mine: Int, other: Int) = if (mine == other) 0 else if (mine < other) 1 else -1

        Point(thiz.x + delta(thiz.x, head.x), thiz.y + delta(thiz.y, head.y))
      }
  }

  object Rope {
    def fromSize(n: Int): Rope = Rope(List.fill(n)(Point(0, 0)))
  }

  final case class Rope(knots: List[Point]) {
    def go(direction: Point): Rope = {
      def loop(current: Point, others: List[Point]): List[Point] = others match {
        case Nil => List(current)
        case h :: t =>
          current :: loop(h.follow(current), t)
      }

      Rope(loop(knots.head + direction, knots.tail))
    }
  }

  type Parsed = LazyList[Point]

  object solution extends common.Solution[Parsed]("2022", "09") {
    override def parse(input: String): Parsed = {
      val LineR = "(.) (\\d+)".r
      LazyList.from(input.split("\n")).flatMap {
        case LineR(dir, count) => LazyList.fill(count.toInt)(Direction.parse(dir))
      }
    }

    def countTailVisit(parsed: Parsed, size: Int): Int = {
      @tailrec
      def loop(remain: LazyList[Point], rope: Rope, visited: Set[Point]): Int = remain match {
        case direction #:: other =>
          val newRope = rope.go(direction)
          loop(other, newRope, visited + newRope.knots.last)
        case _ => visited.size
      }

      loop(parsed, Rope.fromSize(size), Set(Point.Origin))
    }

    override def part1(parsed: Parsed): Any = countTailVisit(parsed, 2)

    override def part2(parsed: Parsed): Any = countTailVisit(parsed, 10)
  }
}
