package aoc2022

import common.priorityqueue.PriorityQueue

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day16 extends common.AocApp(Day16Solution.solution)

object Day16Solution {
  final case class ValveId(id: String) extends AnyVal

  sealed trait Character {
    def isMoving: Boolean

    def valve: ValveId
  }

  object Character {
    final case class At(valve: ValveId) extends Character {
      override val isMoving: Boolean = false
    }

    final case class To(valve: ValveId, remainingTime: Int) extends Character {
      override val isMoving: Boolean = true
    }

    def apply(valveId: ValveId, remainingTime: Int): Character = if (remainingTime <= 0) At(valveId) else To(valveId, remainingTime)

    val Initial: Character = At(ValveId("AA"))
  }


  final case class CompleteState(valveRates: Map[ValveId, Long], currentRate: Long, characters: List[Character], paths: Map[(ValveId, ValveId), Int], remainingTime: Int, totalPressure: Long) {
    private def openValves(valves: Set[ValveId]): CompleteState = this.copy(valveRates -- valves, currentRate + valves.map(valveRates).sum)

    private def withCharacters(characters: List[Character]): CompleteState = this.copy(characters = characters)

    private def turn: CompleteState = this.copy(remainingTime = remainingTime - 1, totalPressure = totalPressure + currentRate)

    private def addCharacter(character: Character): CompleteState = this.copy(characters = character :: characters)

    private def targets = valveRates.keySet -- characters.map(_.valve)

    def children: List[CompleteState] =
      if (characters.isEmpty) {
        if (remainingTime <= 0) Nil
        else List(this.turn)
      } else {
        val (newOpen: Set[ValveId], follow: List[Character], start: List[ValveId]) = characters.foldLeft((Set.empty[ValveId], List.empty[Character], List.empty[ValveId])) {
          case ((newOpen, follow, start), c@Character.At(v)) if valveRates.keySet.contains(v) => (newOpen + v, c :: follow, start)
          case ((newOpen, follow, start), Character.At(v)) => (newOpen, follow, v :: start)
          case ((newOpen, follow, start), Character.To(t, r)) => (newOpen, Character(t, r - 1) :: follow, start)
        }
        start.foldLeft(List(this.turn.openValves(newOpen).withCharacters(follow))) { (currentList, start) =>
          currentList.flatMap { current =>
            current :: current.targets.toList.map(target => Character(target, paths(start -> target) - 1)).map(current.addCharacter)
          }
        }
      }

    def simulateEnd: Long = totalPressure + currentRate * remainingTime

    override def toString: String = {
      val currentMinute = 30 - remainingTime + 1
      s"""== Minute $currentMinute
         |remaining valves: ${valveRates.keys.toList.map(_.id).sorted.mkString(", ")}
         |current rate: $currentRate
         |characters: ${characters.mkString(", ")}
         |""".stripMargin
    }
  }


  type Parsed = (Map[ValveId, Long], Map[ValveId, List[ValveId]])

  object solution extends common.Solution[Parsed]("2022", "16") {
    override def parse(input: String): Parsed = {
      val LineR = "Valve (.+) has flow rate=(\\d+); tunnels? leads? to valves? (.+)".r
      val lines = input.split("\n").map {
        case LineR(id, rate, leads) => (ValveId(id), rate.toLong, leads.split(", ").map(ValveId).toList)
      }

      (lines.map(l => l._1 -> l._2).toMap, lines.map(l => l._1 -> l._3).toMap)
    }

    private def computeTimeBetween(leads: Map[ValveId, List[ValveId]]): Map[(ValveId, ValveId), Int] = {
      def from(f: ValveId): Map[(ValveId, ValveId), Int] = {
        @tailrec
        def loop(toVisit: Queue[(ValveId, Int)], result: Map[(ValveId, ValveId), Int]): Map[(ValveId, ValveId), Int] = toVisit match {
          case (to, time) +: other if time < result(f -> to) =>
            loop(other.enqueueAll(leads(to).map(_ -> (time + 1))), result + ((f -> to) -> time))
          case _ +: other => loop(other, result)
          case _ => result
        }

        loop(Queue((f, 0)), leads.keySet.map(f -> _ -> Int.MaxValue).toMap)
      }

      leads.keySet.map(from).fold(Map.empty)(_ ++ _)
    }


    private def solve(parsed: (Map[ValveId, Long], Map[ValveId, List[ValveId]]), characters: List[Character], time: Int) = {
      val (rates, leads) = parsed
      val interestingRates = rates.filter(_._2 > 0)
      val maxRates = rates.values.sum
      val paths = computeTimeBetween(leads)

      val initial = CompleteState(interestingRates, 0, characters, paths, time, 0)

      def heuristic(current: CompleteState): Long = current.totalPressure + current.remainingTime * maxRates

      implicit val stateOrdering: Ordering[CompleteState] = Ordering.by(heuristic).reverse

      @tailrec
      def loop(queue: PriorityQueue[CompleteState], maxSeen: Long): Long = queue match {
        case PriorityQueue(current, other) if heuristic(current) <= maxSeen => loop(other, maxSeen)
        case PriorityQueue(current, other) => loop(other.+(current.children), math.max(maxSeen, current.simulateEnd))
        case _ => maxSeen
      }

      loop(PriorityQueue.empty[CompleteState].+(initial), 0)
    }

    override def part1(parsed: Parsed): Any = solve(parsed, List(Character.Initial), 30)

    override def part2(parsed: Parsed): Any = solve(parsed, List(Character.Initial, Character.Initial), 26)
  }
}


