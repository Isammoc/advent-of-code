package aoc2022

import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day24 extends common.AocApp(Day24Solution.solution)

object Day24Solution {
  val AllActions: List[Point] = Point.Origin :: Point.OrthogonalDirections

  implicit class RichModuloInt(val value: Int) extends AnyVal {
    def mod(M: Int): Int = ((value % M) + M) % M
  }

  final case class LinearBlizzard(pos: Int, direction: Int) {
    def at(time: Int, limit: Int): Int = ((pos + direction * (time mod limit) - 1) mod limit) + 1
  }

  object Valley {
    def parse(str: String): Valley = {
      val positioned = for {
        (line, y) <- str.split("\n").zipWithIndex.toList
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c

      val height = positioned.map(_._1.y).max - 1
      val width = positioned.map(_._1.x).max - 1

      val start = positioned.find { case (p, c) => p.y == 0 && c == '.' }.get._1
      val end = positioned.find { case (p, c) => p.y == (height + 1) && c == '.' }.get._1

      val vertical = positioned.collect {
        case (p, 'v') => p -> 1
        case (p, '^') => p -> -1
      }.groupMap(_._1.x) { case (Point(_, y), dir) => LinearBlizzard(y, dir) }

      val horizontal = positioned.collect {
        case (p, '>') => p -> 1
        case (p, '<') => p -> -1
      }.groupMap(_._1.y) { case (Point(x, _), dir) => LinearBlizzard(x, dir) }

      Valley(vertical, horizontal, width, height, start, end)
    }
  }

  final case class Valley(vertical: Map[Int, List[LinearBlizzard]], horizontal: Map[Int, List[LinearBlizzard]], width: Int, height: Int, start: Point, end: Point) {
    def isIllegal(state: State): Boolean =
      ((state.pos != start && state.pos != end) && (state.pos.x <= 0 || width < state.pos.x || state.pos.y <= 0 || height < state.pos.y)) ||
        vertical.get(state.pos.x).exists(_.exists(_.at(state.time, height) == state.pos.y)) ||
        horizontal.get(state.pos.y).exists(_.exists(_.at(state.time, width) == state.pos.x))

    def reverse: Valley = this.copy(start = end, end = start)
  }

  final case class State(pos: Point, time: Int) {
    def +(action: Point): State = State(pos + action, time + 1)
  }

  object solution extends common.Solution[Valley]("2022", "24") {
    override def parse(input: String): Valley = Valley.parse(input)

    private def solve(initial: State, valley: Valley): State = {
      @tailrec
      def loop(toVisit: Queue[State], visited: Set[State]): State = toVisit match {
        case (s@State(p, _)) +: _ if p == valley.end => s
        case s +: other if visited.contains(s) => loop(other, visited)
        case illegal +: other if valley.isIllegal(illegal) => loop(other, visited + illegal)
        case current +: other => loop(other :++ AllActions.map(current + _), visited + current)
      }

      loop(Queue(initial), Set.empty)
    }

    override def part1(valley: Valley): Any = solve(State(valley.start, 0), valley).time

    override def part2(valley: Valley): Any = {
      val go = solve(State(valley.start, 0), valley)
      val back = solve(go, valley.reverse)
      val goSecond = solve(back, valley)

      goSecond.time
    }
  }
}
