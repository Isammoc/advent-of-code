package aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day18 extends common.AocApp(Day18Solution.solution)

object Day18Solution {
  object Point {
    val AllDirections: List[Point] = List(
      Point(1, 0, 0),
      Point(-1, 0, 0),
      Point(0, 1, 0),
      Point(0, -1, 0),
      Point(0, 0, 1),
      Point(0, 0, -1),
    )
  }

  final case class Point(x: Int, y: Int, z: Int) {
    def go(dir: Point): Point = Point(x + dir.x, y + dir.y, z + dir.z)
  }

  type Parsed = Set[Point]

  object solution extends common.Solution[Parsed]("2022", "18") {
    override def parse(input: String): Parsed = input.split("\n").map { line =>
      val Array(x, y, z) = line.split(",").map(_.toInt)
      Point(x, y, z)
    }.toSet

    override def part1(parsed: Parsed): Any = parsed.toList.flatMap(cube => Point.AllDirections.map(cube.go)).count(face => !parsed.contains(face))

    override def part2(parsed: Parsed): Any = {
      val minValue = (parsed.map(_.x) ++ parsed.map(_.y) ++ parsed.map(_.z)).min - 2
      val maxValue = (parsed.map(_.x) ++ parsed.map(_.y) ++ parsed.map(_.z)).max + 2

      def isOutsideScope(point: Point): Boolean =
        point.x < minValue || maxValue < point.x ||
          point.y < minValue || maxValue < point.y ||
          point.z < minValue || maxValue < point.z

      @tailrec
      def loop(toVisit: Queue[Point], visited: Set[Point], result: Int): Int = toVisit match {
        case p +: other if isOutsideScope(p) => loop(other, visited, result)
        case p +: other if visited.contains(p) => loop(other, visited, result)
        case p +: other if parsed.contains(p) => loop(other, visited, result)
        case p +: other =>
          val around = Point.AllDirections.map(p.go)
          val newResult = result + around.count(parsed.contains)
          loop(other.enqueueAll(around), visited + p, newResult)
        case _ => result
      }

      loop(Queue(Point(minValue, minValue, minValue)), Set.empty, 0)
    }
  }
}

