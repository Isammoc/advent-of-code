package aoc2022

import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day12 extends common.AocApp(Day12Solution.solution)

object Day12Solution {

  type Parsed = (Map[Point, Char], Point, Point)

  object solution extends common.Solution[Parsed]("2022", "12") {
    override def parse(input: String): Parsed = {
      val elevations = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c).toMap

      val start = elevations.find(_._2 == 'S').get._1
      val end = elevations.find(_._2 == 'E').get._1

      (elevations + (start -> 'a') + (end -> 'z'), start, end)
    }

    def dijkstra(elevations: Map[Point, Char], end: Point, startingPoints: Iterable[Point]): Int = {
      @tailrec
      def loop(toVisit: Queue[(Int, Char, Point)], visited: Set[Point]): Int = toVisit match {
        case (steps, _, current) +: _ if current == end => steps
        case (_, _, current) +: other if visited.contains(current) => loop(other, visited)
        case (steps, high, current) +: other =>
          val toEnqueue = for {
            dir <- Point.OrthogonalDirections
            next = current + dir
            elevation <- elevations.get(next) if elevation <= high + 1
          } yield (steps + 1, elevation, next)

          loop(other.enqueueAll(toEnqueue), visited + current)
      }

      loop(Queue.from(startingPoints.map((0, 'a', _))), Set.empty)
    }

    override def part1(parsed: Parsed): Any = parsed match {
      case (elevations, start, end) =>
        dijkstra(elevations, end, List(start))
    }

    override def part2(parsed: Parsed): Any = parsed match {
      case (elevations, _, end) =>
        dijkstra(elevations, end, elevations.filter(_._2 == 'a').keys)
    }
  }
}
