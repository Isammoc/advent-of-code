package aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day07 extends common.AocApp(Day07Solution.solution)

object Day07Solution {
  object FS {
    private val FileR = "(\\d+)\\s+(.+)".r
    private val NewDirR = "dir\\s+(.*)".r
    private val CdUpR = "\\$\\s+cd\\s+\\.\\.".r
    private val CdInR = "\\$\\s+cd\\s+(.+)".r
    private val CdRoot = "\\$\\s+cd\\s+/".r
    private val LsR = "\\$\\s+ls".r

    private def buildFS(currentDirectory: List[String], contents: Map[List[String], List[FS]]): FS =
      Directory(currentDirectory.head, contents.getOrElse(currentDirectory, Nil).map {
        case NewDir(name) => buildFS(name :: currentDirectory, contents)
        case file => file
      })

    def fromString(str: String): _root_.aoc2022.Day07Solution.FS = {
      @tailrec
      def loop(lines: List[String], pwd: List[String], contents: Map[List[String], List[FS]]): FS = lines match {
        case Nil => buildFS(List("/"), contents)
        case LsR() :: t => loop(t, pwd, contents)
        case CdRoot() :: t => loop(t, "/" :: Nil, contents)
        case CdUpR() :: t => loop(t, pwd.tail, contents)
        case CdInR(dirName) :: t => loop(t, dirName :: pwd, contents)
        case FileR(size, name) :: t => loop(t, pwd, contents.updatedWith(pwd)(current => Some(File(name, size.toLong) :: current.getOrElse(List.empty))))
        case NewDirR(name) :: t => loop(t, pwd, contents.updatedWith(pwd)(current => Some(NewDir(name) :: current.getOrElse(List.empty))))
      }

      loop(str.split("\n").toList, Nil, Map.empty)
    }
  }

  sealed trait FS {
    def size: Long
  }

  case class File(name: String, size: Long) extends FS

  case class Directory(name: String, content: Seq[FS]) extends FS {
    lazy val size: Long = content.map(_.size).sum
  }

  case class NewDir(name: String) extends FS {
    val size: Long = 0
  }

  type Parsed = FS

  object solution extends common.Solution[Parsed]("2022", "07") {
    override def parse(input: String): Parsed = FS.fromString(input)

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(toVisit: Queue[FS], current: Long): Long = toVisit match {
        case (dir: Directory) +: other if dir.size <= 100000 => loop(other ++ dir.content, current + dir.size)
        case (dir: Directory) +: other => loop(other ++ dir.content, current)
        case _ +: other => loop(other, current)
        case _ => current
      }

      loop(Queue(parsed), 0)
    }

    override def part2(parsed: Parsed): Any = {
      val total = 70000000L
      val needed = 30000000L
      val used = parsed.size
      val wanted = needed - (total - used)

      @tailrec
      def loop(toVisit: Queue[FS], found: List[Long]): Long = toVisit match {
        case (dir: Directory) +: other => loop(other ++ dir.content, dir.size :: found)
        case _ +: other => loop(other, found)
        case _ => found.filter(_ >= wanted).min
      }

      loop(Queue(parsed), Nil)
    }
  }
}

