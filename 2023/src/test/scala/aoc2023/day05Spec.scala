package aoc2023

class day05Spec extends common.AocSpec {
  import Day05Solution._

  val sampleInput: String =
    """seeds: 79 14 55 13
      |
      |seed-to-soil map:
      |50 98 2
      |52 50 48
      |
      |soil-to-fertilizer map:
      |0 15 37
      |37 52 2
      |39 0 15
      |
      |fertilizer-to-water map:
      |49 53 8
      |0 11 42
      |42 0 7
      |57 7 4
      |
      |water-to-light map:
      |88 18 7
      |18 25 70
      |
      |light-to-temperature map:
      |45 77 23
      |81 45 19
      |68 64 13
      |
      |temperature-to-humidity map:
      |0 69 1
      |1 0 69
      |
      |humidity-to-location map:
      |60 56 37
      |56 93 4
      |""".stripMargin

  "day05 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 35
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 46
      }
    }
  }

  "Interval" should {
    val reference = Interval(3, 7)
    "fully before" in {
      val before = Interval(1, 2)
      reference.split(before) shouldBe List(before)
    }
    "before and in" in {
      val beforeIn = Interval(1,5)
      reference.split(beforeIn) shouldBe List(Interval(1,2), Interval(3,5))
    }
    "before and after" in {
      val beforeAfter = Interval(1,9)
      reference.split(beforeAfter) shouldBe List(Interval(1,2), Interval(3,7), Interval(8,9))
    }
    "fully in" in {
      val in = Interval(4,6)
      reference.split(in) shouldBe(List(in))
    }
    "in and after" in {
      val inAfter = Interval(4,9)
      reference.split(inAfter) shouldBe List(Interval(4,7), Interval(8,9))
    }
    "fully after" in {
      val after = Interval(8,9)
      reference.split(after) shouldBe List(after)
    }
  }
}

