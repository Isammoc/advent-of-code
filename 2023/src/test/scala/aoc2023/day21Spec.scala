package aoc2023

class day21Spec extends common.AocSpec {
  import Day21Solution._

  val sampleInput: String =
    """...........
      |.....###.#.
      |.###.##..#.
      |..#.#...#..
      |....#.#....
      |.##..S####.
      |.##..#...#.
      |.......##..
      |.##.#.####.
      |.##..##.##.
      |...........
      |""".stripMargin

  "day21 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample, 6) shouldEqual 16
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }
  }
}
