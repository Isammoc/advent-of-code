package aoc2023

class day17Spec extends common.AocSpec {
  import Day17Solution._

  val sampleInput: String =
    """2413432311323
      |3215453535623
      |3255245654254
      |3446585845452
      |4546657867536
      |1438598798454
      |4457876987766
      |3637877979653
      |4654967986887
      |4564679986453
      |1224686865563
      |2546548887735
      |4322674655533
      |""".stripMargin

  "day17 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 102
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 94
      }
    }
  }
}
