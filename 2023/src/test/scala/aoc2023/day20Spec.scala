package aoc2023

class day20Spec extends common.AocSpec {
  import Day20Solution._

  val sampleInput1: String =
    """broadcaster -> a, b, c
      |%a -> b
      |%b -> c
      |%c -> inv
      |&inv -> a
      |""".stripMargin

  val sampleInput: String =
    """broadcaster -> a
      |%a -> inv, con
      |&inv -> b
      |%b -> con
      |&con -> output
      |""".stripMargin

  "day20 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example1" in {
        part1(parse(sampleInput1)) shouldEqual 32_000_000L
      }
      "example" in {
        part1(sample) shouldEqual 11_687_500L
      }
    }
  }
}
