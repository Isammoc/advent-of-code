package aoc2023

class day08Spec extends common.AocSpec {
  import Day08Solution._

  val sampleInput: String =
    """LLR
      |
      |AAA = (BBB, BBB)
      |BBB = (AAA, ZZZ)
      |ZZZ = (ZZZ, ZZZ)
      |""".stripMargin

  "day08 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6
      }
    }

    "part2" should {
      val sample = parse("""LR
                           |
                           |11A = (11B, XXX)
                           |11B = (XXX, 11Z)
                           |11Z = (11B, XXX)
                           |22A = (22B, XXX)
                           |22B = (22C, 22C)
                           |22C = (22Z, 22Z)
                           |22Z = (22B, 22B)
                           |XXX = (XXX, XXX)
                           |""".stripMargin)
      "example" in {
        part2(sample) shouldEqual 6
      }
    }
  }
}

