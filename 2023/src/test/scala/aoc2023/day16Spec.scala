package aoc2023

class day16Spec extends common.AocSpec {
  import Day16Solution._

  val sampleInput: String =
    """.|...\....
      ||.-.\.....
      |.....|-...
      |........|.
      |..........
      |.........\
      |..../.\\..
      |.-.-/..|..
      |.|....-|.\
      |..//.|....
      |""".stripMargin

  "day16 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 46
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 51
      }
    }
  }
}
