package aoc2023

class day18Spec extends common.AocSpec {
  import Day18Solution._

  val sampleInput: String =
    """R 6 (#70c710)
      |D 5 (#0dc571)
      |L 2 (#5713f0)
      |D 2 (#d2c081)
      |R 2 (#59c680)
      |D 2 (#411b91)
      |L 5 (#8ceee2)
      |U 2 (#caa173)
      |L 1 (#1b58a2)
      |U 2 (#caa171)
      |R 2 (#7807d2)
      |U 3 (#a77fa3)
      |L 2 (#015232)
      |U 2 (#7a21e3)
      |""".stripMargin

  "day18 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 62
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual BigInt("952408144115")
      }
    }
  }
}
