package aoc2023

class day24Spec extends common.AocSpec {
  import Day24Solution._

  val sampleInput: String =
    """19, 13, 30 @ -2,  1, -2
      |18, 19, 22 @ -1, -1, -2
      |20, 25, 34 @ -2, -2, -4
      |12, 31, 28 @ -1, -2, -1
      |20, 19, 15 @  1, -5, -3
      |""".stripMargin

  "day24 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1Spec(sample, 7, 27) shouldEqual 2
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }
  }
}

