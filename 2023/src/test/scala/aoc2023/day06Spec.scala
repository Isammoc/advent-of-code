package aoc2023

class day06Spec extends common.AocSpec {
  import Day06Solution._

  val sampleInput: String =
    """Time:      7  15   30
      |Distance:  9  40  200
      |""".stripMargin

  "day06 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 288
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 71503
      }
    }
  }
}
