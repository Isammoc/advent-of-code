package aoc2023

class day01Spec extends common.AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """1abc2
      |pqr3stu8vwx
      |a1b2c3d4e5f
      |treb7uchet
      |""".stripMargin

  "day01 2023" can {
    import solution._


    "part1" should {
      val sample = parse(sampleInput)
      "example" in {
        part1(sample) shouldEqual 142
      }
    }

    "part2" should {
      val sample = parse(
        """two1nine
          |eightwothree
          |abcone2threexyz
          |xtwone3four
          |4nineeightseven2
          |zoneight234
          |7pqrstsixteen""".stripMargin)
      "example" in {
        part2(sample) shouldEqual 281
      }
    }
  }
}

