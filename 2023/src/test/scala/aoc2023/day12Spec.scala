package aoc2023

class day12Spec extends common.AocSpec {
  import Day12Solution._

  val sampleInput: String =
    """???.### 1,1,3
      |.??..??...?##. 1,1,3
      |?#?#?#?#?#?#?#? 1,3,1,6
      |????.#...#... 4,1,1
      |????.######..#####. 1,6,5
      |?###???????? 3,2,1
      |""".stripMargin

  "day12 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 21L
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 525152L
      }
    }
  }
}
