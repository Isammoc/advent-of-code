package aoc2023

class day22Spec extends common.AocSpec {
  import Day22Solution._

  val sampleInput: String =
    """1,0,1~1,2,1
      |0,0,2~2,0,2
      |0,2,3~2,2,3
      |0,0,4~0,2,4
      |2,0,5~2,2,5
      |0,1,6~2,1,6
      |1,1,8~1,1,9
      |""".stripMargin

  "day22 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 5
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 7
      }
    }
  }
}
