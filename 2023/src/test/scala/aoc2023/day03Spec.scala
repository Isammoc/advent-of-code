package aoc2023

class day03Spec extends common.AocSpec {
  import Day03Solution._

  val sampleInput: String =
    """467..114..
      |...*......
      |..35..633.
      |......#...
      |617*......
      |.....+.58.
      |..592.....
      |......755.
      |...$.*....
      |.664.598..
      |""".stripMargin

  "day03 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 4361
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 467835
      }
    }
  }
}

