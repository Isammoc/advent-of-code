package aoc2023

class day10Spec extends common.AocSpec {
  import Day10Solution._

  val simpleInput: String =
    """.....
      |.S-7.
      |.|.|.
      |.L-J.
      |.....
      |""".stripMargin
  val sampleInput: String =
    """..F7.
      |.FJ|.
      |SJ.L7
      ||F--J
      |LJ...
      |""".stripMargin

  "day10 2023" can {
    import solution._

    val simple = parse(simpleInput)
    val sample = parse(sampleInput)

    "part1" should {
      "simple" in {
        part1(simple) shouldEqual 4
      }
      "example" in {
        part1(sample) shouldEqual 8
      }
    }

    "part2" should {
      "first" in {
        val sample =
          """...........
            |.S-------7.
            |.|F-----7|.
            |.||.....||.
            |.||.....||.
            |.|L-7.F-J|.
            |.|..|.|..|.
            |.L--J.L--J.
            |...........
            |""".stripMargin
        val parsed = parse(sample)
        part2(parsed) shouldEqual 4
      }

      "second" in {
        val sample =
          """.F----7F7F7F7F-7....
            |.|F--7||||||||FJ....
            |.||.FJ||||||||L7....
            |FJL7L7LJLJ||LJ.L-7..
            |L--J.L7...LJS7F-7L7.
            |....F-J..F7FJ|L7L7L7
            |....L7.F7||L7|.L7L7|
            |.....|FJLJ|FJ|F7|.LJ
            |....FJL-7.||.||||...
            |....L---J.LJ.LJLJ...
            |""".stripMargin
        val parsed = parse(sample)
        part2(parsed) shouldEqual 8
      }

      "third" in {
        val sample =
          """FF7FSF7F7F7F7F7F---7
            |L|LJ||||||||||||F--J
            |FL-7LJLJ||||||LJL-77
            |F--JF--7||LJLJ7F7FJ-
            |L---JF-JLJ.||-FJLJJ7
            ||F|F-JF---7F7-L7L|7|
            ||FFJF7L7F-JF7|JL---7
            |7-L-JL7||F7|L7F-7F7|
            |L.L7LFJ|||||FJL7||LJ
            |L7JLJL-JLJLJL--JLJ.L
            |""".stripMargin
        val parsed = parse(sample)
        part2(parsed) shouldEqual 10
      }
    }
  }
}
