package aoc2023

class day15Spec extends common.AocSpec {
  import Day15Solution._

  val sampleInput: String =
    """rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7""".stripMargin

  "day15 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 1320
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 145
      }
    }
  }
}
