package aoc2023

class day11Spec extends common.AocSpec {
  import Day11Solution._

  val sampleInput: String =
    """...#......
      |.......#..
      |#.........
      |..........
      |......#...
      |.#........
      |.........#
      |..........
      |.......#..
      |#...#.....
      |""".stripMargin

  "day11 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "zero" in {
        part1(parse("""#""")) shouldEqual 0
      }

      "2" in {
        part1(parse("""#.
                      |.#""".stripMargin)) shouldEqual 2
      }

      "4" in {
        part1(parse("""#..
                      |..#""".stripMargin)) shouldEqual 4
      }

      "6" in {
        part1(parse("""#..
                      |...
                      |..#""".stripMargin)) shouldEqual 6
      }

      "example" in {
        part1(sample) shouldEqual 374
      }
    }

    "part2" should {
      "example" in {
        total(sample, 10) shouldEqual 1030
        total(sample, 100) shouldEqual 8410
      }
    }
  }
}
