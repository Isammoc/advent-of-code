package aoc2023

class day09Spec extends common.AocSpec {
  import Day09Solution._

  val sampleInput: String =
    """0 3 6 9 12 15
      |1 3 6 10 15 21
      |10 13 16 21 30 45
      |""".stripMargin

  "day09 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 114
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 2
      }
    }
  }
}

