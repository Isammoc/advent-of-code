package aoc2023

class day14Spec extends common.AocSpec {
  import Day14Solution._

  val sampleInput: String =
    """O....#....
      |O.OO#....#
      |.....##...
      |OO.#O....O
      |.O.....O#.
      |O.#..O.#.#
      |..O..#O..O
      |.......O..
      |#....###..
      |#OO..#....
      |""".stripMargin

  "day14 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 136
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 64
      }
    }
  }
}
