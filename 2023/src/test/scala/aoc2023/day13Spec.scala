package aoc2023

class day13Spec extends common.AocSpec {
  import Day13Solution._

  val sampleInput: String =
    """#.##..##.
      |..#.##.#.
      |##......#
      |##......#
      |..#.##.#.
      |..##..##.
      |#.#.##.#.
      |
      |#...##..#
      |#....#..#
      |..##..###
      |#####.##.
      |#####.##.
      |..##..###
      |#....#..#
      |""".stripMargin

  "day13 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 405
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 400
      }
    }
  }
}
