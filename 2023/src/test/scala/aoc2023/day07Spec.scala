package aoc2023

class day07Spec extends common.AocSpec {
  import Day07Solution._

  val sampleInput: String =
    """32T3K 765
      |T55J5 684
      |KK677 28
      |KTJJT 220
      |QQQJA 483
      |""".stripMargin

  "day07 2023" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 6440
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 5905
      }
    }
  }
}
