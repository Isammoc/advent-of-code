package aoc2023

import scala.annotation.tailrec

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed = List[String]

  private val NumeralDigits = Set('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

  object solution extends common.Solution[Parsed]("2023", "01") {
    override def parse(input: String): Parsed = input.split("\n").toList

    override def part1(parsed: Parsed): Any = {
      parsed.map{line =>
        val digits = line.filter(NumeralDigits)
        (digits.head - '0') * 10 + (digits.last - '0')
      }.sum
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(remain: List[Char], found: List[Int]): Int = remain match {
        case Nil => found.head + found.last * 10
        case '0' :: t => loop(t, 0 :: found)
        case '1' :: t => loop(t, 1 :: found)
        case '2' :: t => loop(t, 2 :: found)
        case '3' :: t => loop(t, 3 :: found)
        case '4' :: t => loop(t, 4 :: found)
        case '5' :: t => loop(t, 5 :: found)
        case '6' :: t => loop(t, 6 :: found)
        case '7' :: t => loop(t, 7 :: found)
        case '8' :: t => loop(t, 8 :: found)
        case '9' :: t => loop(t, 9 :: found)
        case 'o' :: 'n' :: 'e' :: _ => loop(remain.tail, 1 :: found)
        case 't' :: 'w' :: 'o' :: _ => loop(remain.tail, 2 :: found)
        case 't' :: 'h' :: 'r' :: 'e' :: 'e' :: _ => loop(remain.tail, 3 :: found)
        case 'f' :: 'o' :: 'u' :: 'r' :: _ => loop(remain.tail, 4 :: found)
        case 'f' :: 'i' :: 'v' :: 'e' :: _ => loop(remain.tail, 5 :: found)
        case 's' :: 'i' :: 'x' :: _ => loop(remain.tail, 6 :: found)
        case 's' :: 'e' :: 'v' :: 'e' :: 'n' :: _ => loop(remain.tail, 7 :: found)
        case 'e' :: 'i' :: 'g' :: 'h' :: 't' :: _ => loop(remain.tail, 8 :: found)
        case 'n' :: 'i' :: 'n' :: 'e' :: _ => loop(remain.tail, 9 :: found)
        case _ :: t => loop(t, found)
      }

      parsed.map(line =>
        loop(line.toList, Nil)
      ).sum
    }
  }
}

