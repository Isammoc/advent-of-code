package aoc2023

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  type Parsed = Map[Int, List[List[(String, Int)]]]

  object solution extends common.Solution[Parsed]("2023", "02") {
    private val GameIDR = "Game (\\d+): (.*)".r
    override def parse(input: String): Parsed = input.split("\n").toList.map {
      case GameIDR(id, game) => id.toInt -> game.split("; ").toList
        .map(_.split(", ").toList.map { nc =>
      val Array(n, c) = nc.split(" ")
        c -> n.toInt
    })}.toMap

    override def part1(parsed: Parsed): Any = parsed.view.filter {
      case (_, values) => values.flatten.forall {
        case ("red", i) => i <= 12
        case ("green", i) => i <= 13
        case ("blue", i) => i <= 14
      }
    }.keys.sum

    override def part2(parsed: Parsed): Any = parsed.values.map(_.flatten.groupMapReduce(_._1)(_._2)(_.max(_)).values.product).sum
  }
}

