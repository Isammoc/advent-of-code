package aoc2023

import common.grid.Point

import scala.annotation.tailrec

object day22 extends common.AocApp(Day22Solution.solution)

object Day22Solution {
  type Parsed = (List[Brick], Map[Brick, Set[Brick]], Map[Brick, Set[Brick]])

  private object Point3 {
    private val Point3R = "(\\d+),(\\d+),(\\d+)".r
    def parse(str: String): Point3 = str match {
      case Point3R(x, y, z) => Point3(x.toInt, y.toInt, z.toInt)
    }
  }
  final case class Point3(x: Int, y: Int, z: Int)

  private object Brick {
    private val BrickR = "(.+)~(.+)".r
    def parse(str: String): Brick = str match {
      case BrickR(one, other) => Brick(Point3.parse(one), Point3.parse(other))
    }
  }
  final case class Brick(one: Point3, other: Point3) {
    def planPoints: List[Point] = for {
      x <- (math.min(one.x, other.x) to math.max(one.x, other.x)).toList
      y <- math.min(one.y, other.y) to math.max(one.y, other.y)
    } yield Point(x, y)

    def height: Int = math.max(one.z, other.z) - math.min(one.z, other.z) + 1
  }

  object solution extends common.Solution[Parsed]("2023", "22") {
    override def parse(input: String): Parsed = {
      val parsed = input.split("\n").toList.map(Brick.parse)

      @tailrec
      def loop(
          remain: List[Brick],
          bricksFromTop: Map[Point, Brick],
          maxHeightByBrick: Map[Brick, Int],
          touchBelow: List[(Brick, Brick)]
      ): (List[Brick], Map[Brick, Set[Brick]], Map[Brick, Set[Brick]]) = remain match {
        case current :: otherBricks =>
          val bricksBelow = current.planPoints.flatMap(bricksFromTop.get).toSet
          val currentHeightGround = bricksBelow.map(maxHeightByBrick).maxOption.getOrElse(0)
          val currentHeight = currentHeightGround + current.height
          val belowTouching = bricksBelow.filter(brick => maxHeightByBrick(brick) == currentHeightGround)

          loop(
            otherBricks,
            bricksFromTop ++ current.planPoints.map(_ -> current),
            maxHeightByBrick + (current -> currentHeight),
            touchBelow ++ belowTouching.map(current -> _)
          )

        case Nil =>
          val below: Map[Brick, Set[Brick]] = touchBelow.groupMapReduce(_._1)(pair => Set(pair._2))(_ ++ _)
          val above: Map[Brick, Set[Brick]] = touchBelow.groupMapReduce(_._2)(pair => Set(pair._1))(_ ++ _)
          (parsed, below, above)
      }

      loop(parsed.sortBy(_.one.z), Map.empty, Map.empty, Nil)
    }

    override def part1(parsed: Parsed): Any = {
      val (bricks, below, above) = parsed
      bricks.count { brick =>
        above.getOrElse(brick, Set.empty).count(supported => below.getOrElse(supported, Set.empty).size == 1) == 0
      }
    }

    override def part2(parsed: Parsed): Any = {
      val (bricks, below, above) = parsed

      @tailrec
      def innerLoop(fall: Set[Brick]): Int = {
        val newFall = (fall.flatMap(above.getOrElse(_, Set.empty)) -- fall).filter(mayfall => (below(mayfall) -- fall).isEmpty)
        if (newFall.isEmpty) fall.size
        else innerLoop(fall ++ newFall)
      }
      bricks.map { brick =>
        innerLoop(Set(brick)) - 1
      }.sum
    }
  }
}
