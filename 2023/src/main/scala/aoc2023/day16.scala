package aoc2023

import common.grid.Point

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day16 extends common.AocApp(Day16Solution.solution)

object Day16Solution {
  type Parsed = Contraption

  object Contraption {
    val Empty: Char = '.'
    val RightUpMirror: Char = '/'
    val RightDownMirror: Char = '\\'
    val VerticalSplitter: Char = '|'
    val HorizontalSplitter: Char = '-'
  }
  final case class Contraption(size: Point, grid: Map[Point, Char])

  object Node {
    val Start: Node = Node(Point(0, 0), Point.Right)
  }
  final case class Node(pos: Point, dir: Point) {
    def isValid(implicit contraption: Contraption): Boolean =
      0 <= pos.x && pos.x <= contraption.size.x &&
        0 <= pos.y && pos.y <= contraption.size.y

    def moveForward: Node = Node(pos + dir, dir)

    def turnBy(c: Char): List[Node] = {
      import Contraption._
      import Point._
      val newDirs = (dir, c) match {
        case (dir, Empty) => List(dir)

        case (Up, RightUpMirror)    => List(Right)
        case (Right, RightUpMirror) => List(Up)
        case (Down, RightUpMirror)  => List(Left)
        case (Left, RightUpMirror)  => List(Down)

        case (Up, RightDownMirror)    => List(Left)
        case (Right, RightDownMirror) => List(Down)
        case (Down, RightDownMirror)  => List(Right)
        case (Left, RightDownMirror)  => List(Up)

        case (Up, VerticalSplitter)    => List(Up)
        case (Right, VerticalSplitter) => List(Up, Down)
        case (Down, VerticalSplitter)  => List(Down)
        case (Left, VerticalSplitter)  => List(Up, Down)

        case (Up, HorizontalSplitter)    => List(Right, Left)
        case (Right, HorizontalSplitter) => List(Right)
        case (Down, HorizontalSplitter)  => List(Right, Left)
        case (Left, HorizontalSplitter)  => List(Left)
      }
      newDirs.map(Node(pos, _))
    }

    def next(implicit contraption: Contraption): List[Node] =
      turnBy(contraption.grid(pos)).map(_.moveForward).filter(_.isValid)
  }

  object solution extends common.Solution[Parsed]("2023", "16") {
    override def parse(input: String): Parsed = {
      val grid = (for {
        (line, y) <- input.split("\\n").zipWithIndex
        (c, x) <- line.toList.zipWithIndex
      } yield Point(x, y) -> c).toMap

      Contraption(grid.keys.maxBy(p => (p.x, p.y)), grid)
    }

    def energy(start: Node)(implicit contraption: Contraption): Int = {
      @tailrec
      def loop(toVisit: Queue[Node], visited: Set[Node]): Int = toVisit match {
        case current +: other if visited(current) => loop(other, visited)
        case current +: other                     => loop(other ++ current.next, visited + current)
        case _                                    => visited.map(_.pos).size
      }
      loop(Queue(start), Set.empty)
    }

    override def part1(parsed: Parsed): Any = energy(Node.Start)(parsed)

    override def part2(parsed: Parsed): Any = {
      import Point._
      ((for {
        y <- 0 to parsed.size.y
        (x, dir) <- List((0, Right), (parsed.size.x, Left))
      } yield Node(Point(x, y), dir)) ++ (for {
        x <- 0 to parsed.size.x
        (y, dir) <- List((0, Down), (parsed.size.y, Up))
      } yield Node(Point(x, y), dir))).map(energy(_)(parsed)).max
    }
  }
}
