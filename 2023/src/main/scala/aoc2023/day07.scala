package aoc2023

import scala.math.Ordering.Implicits._

object day07 extends common.AocApp(Day07Solution.solution)

object Day07Solution {
  type Parsed = List[Hand]
  val Order = List('A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2').zipWithIndex.toMap
  val Order2 = List('A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J').zipWithIndex.toMap

  final case class Hand(values: String, bid: Int) {
    def strength: String = values.groupMapReduce(identity)(_ => 1)(_ + _).values.toList.sorted.reverse.mkString
    def strength2: String = {
      val (jokers, others) = values.partition(_ == 'J')

      {
        val tmp = (0 :: others.groupMapReduce(identity)(_ => 1)(_ + _).values.toList).sorted.reverse
        (tmp.head + jokers.length) :: tmp.tail
      }.mkString
    }
  }

  object solution extends common.Solution[Parsed]("2023", "07") {
    override def parse(input: String): Parsed = for {
      line <- input.split("\n").toList
    } yield {
      val Array(values, bidStr) = line.split("\\s+")
      Hand(values = values, bid = bidStr.toInt)
    }

    def common(first: Hand => String, second: Map[Char, Int], parsed: Parsed): Int =
      parsed
        .sorted(Ordering.by[Hand, String](first).orElse(Ordering.by[Hand, Seq[Int]](_.values.map(second)).reverse))
        .zipWithIndex
        .map { case (Hand(_, bid), i) =>
          bid * (i + 1)
        }
        .sum

    override def part1(parsed: Parsed): Any =
      common(_.strength, Order, parsed)

    override def part2(parsed: Parsed): Any =
      common(_.strength2, Order2, parsed)
  }
}
