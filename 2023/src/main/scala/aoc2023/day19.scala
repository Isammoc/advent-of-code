package aoc2023

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day19 extends common.AocApp(Day19Solution.solution)

object Day19Solution {
  type Parsed = (Map[String, Workflow], List[Part])

  private object Instruction {
    private val CommonInstructionR = "(.)(.)(\\d+):(.+)".r

    def parse(str: String): Instruction = str match {
      case CommonInstructionR(cat, filter, value, dest) => CommonInstruction(cat, filter, value.toInt, dest)
      case other                                        => DefaultInstruction(other)
    }
  }
  sealed trait Instruction {
    def take(part: Part): Option[String]
    def split(part: IntervalPart): ((String, Option[IntervalPart]), Option[IntervalPart])
  }

  private final case class DefaultInstruction(workflow: String) extends Instruction {
    override def take(part: Part): Option[String] = Some(workflow)

    override def split(part: IntervalPart): ((String, Option[IntervalPart]), Option[IntervalPart]) = ((workflow, Some(part)), None)
  }
  private final case class CommonInstruction(extractor: String, comparator: String, value: Int, workflow: String) extends Instruction {
    private val extractInt: Part => Int = extractor match {
      case "a" => _.a
      case "x" => _.x
      case "m" => _.m
      case "s" => _.s
    }

    private val compare: Int => Boolean = comparator match {
      case "<" => _ < value
      case ">" => _ > value
    }

    override def take(part: Part): Option[String] = Option.when(compare(extractInt(part)))(workflow)

    private def withCat(part: IntervalPart, newInterval: Interval): IntervalPart = extractor match {
      case "a" => part.copy(a = newInterval)
      case "x" => part.copy(x = newInterval)
      case "m" => part.copy(m = newInterval)
      case "s" => part.copy(s = newInterval)
    }

    override def split(part: IntervalPart): ((String, Option[IntervalPart]), Option[IntervalPart]) = {
      val ofInterest = extractor match {
        case "a" => part.a
        case "x" => part.x
        case "m" => part.m
        case "s" => part.s
      }

      val (before, after) = comparator match {
        case "<" if ofInterest.b < value => (Some(ofInterest), None)
        case "<" if ofInterest.a < value => (Some(Interval(ofInterest.a, value - 1)), Some(Interval(value, ofInterest.b)))
        case "<"                         => (None, Some(ofInterest))

        case ">" if ofInterest.a > value => (Some(ofInterest), None)
        case ">" if ofInterest.b > value => (Some(Interval(value + 1, ofInterest.b)), Some(Interval(ofInterest.a, value)))
        case ">"                         => (None, Some(ofInterest))
      }

      ((workflow, before.map(withCat(part, _))), after.map(withCat(part, _)))
    }
  }

  private object Workflow {
    private val WorkflowR = "(.+)\\{(.+)}".r
    def parse(str: String): Workflow = str match {
      case WorkflowR(name, instructions) => Workflow(name, instructions.split(",").toList.map(Instruction.parse))
    }
  }
  final case class Workflow(name: String, instructions: List[Instruction]) {
    def take(part: Part): String = instructions.collectFirst(((c: Instruction) => c.take(part)).unlift).get

    def split(part: IntervalPart): List[(String, IntervalPart)] =
      List
        .unfold((Option(part), instructions)) {
          case (Some(part), instr :: other) =>
            val ((workflow, toWorkflow), toContinue) = instr.split(part)

            Some((toWorkflow.map(workflow -> _), (toContinue, other)))

          case _ => None
        }
        .flatten
  }

  object Part {
    private val PartR = "\\{x=(\\d+),m=(\\d+),a=(\\d+),s=(\\d+)}".r
    def parse(str: String): Part = str match {
      case PartR(x, m, a, s) => Part(x.toInt, m.toInt, a.toInt, s.toInt)
    }
  }
  final case class Part(x: Int, m: Int, a: Int, s: Int) {
    def rating: Long = x + m + a + s
  }

  object Interval {
    val All: Interval = Interval(1, 4000)
  }
  final case class Interval(a: Long, b: Long) {
    def size: Long = b - a + 1
  }
  private object IntervalPart {
    val All: IntervalPart = IntervalPart(Interval.All, Interval.All, Interval.All, Interval.All)
  }
  final case class IntervalPart(x: Interval, m: Interval, a: Interval, s: Interval) {
    def size: Long = x.size * m.size * a.size * s.size
  }

  object solution extends common.Solution[Parsed]("2023", "19") {
    override def parse(input: String): Parsed = {
      val Array(workflows, parts) = input.split("\n\n")

      (
        workflows.split("\n").map(Workflow.parse).map(w => w.name -> w).toMap,
        parts.split("\n").toList.map(Part.parse)
      )
    }

    // 348378
    override def part1(parsed: Parsed): Any = {
      val (workflows, parts) = parsed

      @tailrec
      def isAccepted(current: String, part: Part): Boolean = workflows(current).take(part) match {
        case "A"  => true
        case "R"  => false
        case name => isAccepted(name, part)
      }

      @tailrec
      def loop(remain: List[Part], value: Long): Long = remain match {
        case part :: t => loop(t, value + (if (isAccepted("in", part)) part.rating else 0))
        case _         => value
      }

      loop(parts, 0)
    }

    final case class Node(workflow: String, part: IntervalPart)

    override def part2(parsed: Parsed): Any = {
      val (workflows, _) = parsed

      @tailrec
      def loop(toVisit: Queue[Node], visited: Set[Node], size: Long): Long = toVisit match {
        case current +: other if visited.contains(current) => loop(other, visited, size)
        case Node("A", part) +: other                      => loop(other, visited, size + part.size)
        case Node("R", _) +: other                         => loop(other, visited, size)
        case Node(name, part) +: other =>
          loop(other ++ workflows(name).split(part).map(Node.tupled), visited + Node(name, part), size)
        case _ => size
      }

      loop(Queue(Node("in", IntervalPart.All)), Set.empty, 0)
    }
  }
}
