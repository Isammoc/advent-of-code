package aoc2023

import scala.annotation.tailrec

object day24 extends common.AocApp(Day24Solution.solution)

object Day24Solution {
  type Parsed = List[Hailstone]

  object Point3 {
    private val Point3R = "(-?\\d+),\\s+(-?\\d+),\\s+(-?\\d+)".r
    def parse(str: String): Point3 = str match {
      case Point3R(x, y, z) => Point3(BigInt(x), BigInt(y), BigInt(z))
    }
  }
  final case class Point3(x: BigInt, y: BigInt, z: BigInt)

  private object Hailstone {
    private val HailstoneR = "(.+)\\s+@\\s+(.*)".r
    def parse(str: String): Hailstone = str match {
      case HailstoneR(pos, vel) => Hailstone(Point3.parse(pos), Point3.parse(vel))
    }

    def intersection(one: Hailstone, other: Hailstone, min: BigDecimal, max: BigDecimal): Boolean = {
      val a = BigDecimal(one.velocity.y) / BigDecimal(one.velocity.x)
      val b = BigDecimal(other.velocity.y) / BigDecimal(other.velocity.x)
      val c = BigDecimal(one.pos.y) - a * BigDecimal(one.pos.x)
      val d = BigDecimal(other.pos.y) - b * BigDecimal(other.pos.x)

      if (a == b) false
      else {
        val x = (d - c) / (a - b)
        val y = a * x + c

        val kone = (y - BigDecimal(one.pos.y)) / BigDecimal(one.velocity.y)
        val kother = (y - BigDecimal(other.pos.y)) / BigDecimal(other.velocity.y)

        min <= x && x <= max && min <= y && y <= max && (kone > 0 && kother > 0)
      }
    }
  }
  final case class Hailstone(pos: Point3, velocity: Point3)

  object solution extends common.Solution[Parsed]("2023", "24") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(Hailstone.parse)

    def part1Spec(parsed: Parsed, min: BigInt, max: BigInt): Int = {
      @tailrec
      def loop(remain: List[Hailstone], result: Int): Int = remain match {
        case one :: other =>
          loop(other, result + other.count(b => Hailstone.intersection(one, b, BigDecimal(min), BigDecimal(max))))
        case _ => result
      }

      loop(parsed, 0)
    }

    override def part1(parsed: Parsed): Any = part1Spec(parsed, BigInt("200000000000000"), BigInt("400000000000000"))

    override def part2(parsed: Parsed): Any = "NOT IMPLEMENTED"
  }
}
