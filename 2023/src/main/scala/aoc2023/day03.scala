package aoc2023

import common.grid.Point

import scala.annotation.tailrec

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {
  type Parsed = List[(Symbol, List[Int])]

  sealed trait Part {
    val isSymbol: Boolean = false
  }
  case class Symbol(c: Char) extends Part {
    override val isSymbol: Boolean = true
  }
  case class Digit(v: Int) extends Part

  object solution extends common.Solution[Parsed]("2023", "03") {
    override def parse(input: String): Parsed = {
      val parsed = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex if c != '.'
      } yield Point(x, y) -> (if ('0' <= c && c <= '9') Digit(c - '0') else Symbol(c))).toMap

      val maxX = parsed.keys.map(_.x).max
      val maxY = parsed.keys.map(_.y).max

      @tailrec
      def loop(x: Int, y: Int, result: List[(Point, Int)]): List[(Symbol, List[Int])] =
        if (y > maxY) result.groupMap(_._1)(_._2).toList.map { case (p, values) => (parsed(p), values) }.collect { case (s: Symbol, v) =>
          (s, v)
        }
        else if (x > maxX) loop(0, y + 1, result)
        else {
          val p = Point(x, y)
          parsed.get(p) match {
            case Some(Digit(_)) =>
              val digits = LazyList
                .from(0)
                .map(dx => parsed.get(Point(x + dx, y)))
                .takeWhile {
                  case Some(_: Digit) => true
                  case _              => false
                }
                .collect { case Some(Digit(v)) =>
                  v
                }
              val length = digits.size
              val number = digits.foldLeft(0)(_ * 10 + _)
              val toConsider = ((-1 to length).flatMap(dx => List(Point(x + dx, y - 1), Point(x + dx, y + 1))) :+ Point(x - 1, y) :+ Point(x + length, y))
                .map(p => (p, parsed.get(p)))
                .collectFirst { case (p, Some(_: Symbol)) =>
                  p
                }

              toConsider match {
                case Some(p) => loop(x + length + 1, y, (p, number) :: result)
                case _       => loop(x + length + 1, y, result)
              }

            case _ => loop(x + 1, y, result)
          }
        }

      loop(0, 0, Nil)
    }

    override def part1(parsed: Parsed): Any = parsed.flatMap(_._2).sum

    override def part2(parsed: Parsed): Any = (for {
      (s, values) <- parsed if s == Symbol('*') && values.sizeIs == 2
    } yield values.product).sum
  }
}
