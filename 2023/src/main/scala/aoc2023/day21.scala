package aoc2023

import common.algo.graph.Dijkstra
import common.grid.Point

object day21 extends common.AocApp(Day21Solution.solution)

object Day21Solution {
  type Parsed = Grid

  final case class Grid(start: Point, walls: Set[Point])

  object solution extends common.Solution[Parsed]("2023", "21") {
    override def parse(input: String): Parsed = {
      val walls = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.toList.zipWithIndex if c == '#'
      } yield Point(x, y)).toSet
      val start = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.toList.zipWithIndex if c == 'S'
      } yield Point(x, y)).head
      Grid(start, walls)
    }

    override def part1(parsed: Parsed): Any = part1(parsed, 64)

    def part1(parsed: Parsed, steps: Int): Any =
      Dijkstra
        .simpleDistance(parsed.start) { node =>
          Point.OrthogonalDirections
            .map(node + _)
            .filterNot(parsed.walls)
            .filter(_.manhattan(parsed.start) < 65)
        }
        .values
        .count(dist => dist <= steps && dist % 2 == steps % 2)

    override def part2(parsed: Parsed): Any = "NOT IMPLEMENTED"
  }
}
