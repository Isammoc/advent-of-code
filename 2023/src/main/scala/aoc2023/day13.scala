package aoc2023

import scala.annotation.tailrec

object day13 extends common.AocApp(Day13Solution.solution)

object Day13Solution {
  type Parsed = List[List[List[Char]]]

  object solution extends common.Solution[Parsed]("2023", "13") {
    override def parse(input: String): Parsed = input.split("\n\n").map(_.split("\n").map(_.toList).toList).toList

    def rowBeforeReflection(input: List[List[Char]]): Int = {
      @tailrec
      def loop(left: List[List[Char]], right: List[List[Char]]): Int =
        if (right.isEmpty) 0
        else if (left.zip(right).forall { case (a, b) => a == b }) left.size
        else loop(right.head :: left, right.tail)

      loop(List(input.head), input.tail)
    }

    def rowSmudge(input: List[List[Char]]): Int = {
      @tailrec
      def loop(left: List[List[Char]], right: List[List[Char]]): Int =
        if (right.isEmpty) 0
        else if (left.flatten.zip(right.flatten).count { case (a, b) => a != b } == 1) left.size
        else loop(right.head :: left, right.tail)

      loop(List(input.head), input.tail)
    }

    override def part1(parsed: Parsed): Any = parsed.map { field =>
      rowBeforeReflection(field) * 100 + rowBeforeReflection(field.transpose)
    }.sum

    override def part2(parsed: Parsed): Any = parsed.map { field =>
      rowSmudge(field) * 100 + rowSmudge(field.transpose)
    }.sum
  }
}
