package aoc2023

object day15 extends common.AocApp(Day15Solution.solution)

object Day15Solution {
  type Parsed = String

  private def hash(input: String): Int = input.foldLeft(0)((acc, c) => (acc + c) * 17 % 256)

  final case class Lens(label: String, focal: Int)
  final case class Box(lenses: List[Lens]) {
    def remove(label: String): Box = Box(lenses.filterNot(_.label == label))
    def add(lens: Lens): Box =
      if (lenses.exists(_.label == lens.label)) {
        Box(lenses.map {
          case Lens(label, _) if label == lens.label => lens
          case other                                 => other
        })
      } else Box(lenses :+ lens)
    def value: Long = lenses.zipWithIndex.map { case (lens, index) =>
      lens.focal * (index + 1L)
    }.sum
  }

  object Laser {
    val Empty: Laser = Laser(Map.empty)
  }
  final case class Laser(boxes: Map[Int, Box]) {
    def update(label: String)(f: Box => Box): Laser = Laser(boxes + (hash(label) -> f(boxes.getOrElse(hash(label), Box(Nil)))))
    def remove(label: String): Laser = update(label)(_.remove(label))
    def add(lens: Lens): Laser = update(lens.label)(_.add(lens))
    def value: Long = boxes.map { case (index, box) => (index + 1L) * box.value }.sum
  }

  object solution extends common.Solution[Parsed]("2023", "15") {
    override def parse(input: String): Parsed = input
    val AddR = "(.*)=(\\d+)".r
    val RemoveR = "(.*)-".r

    override def part1(parsed: Parsed): Any = parsed.split(",").map(hash).sum

    override def part2(parsed: Parsed): Any =
      parsed
        .split(",")
        .foldLeft(Laser.Empty) {
          case (acc, AddR(label, value)) => acc.add(Lens(label, value.toInt))
          case (acc, RemoveR(label))     => acc.remove(label)
        }
        .value
  }
}
