package aoc2023

import common.algo.graph.Dijkstra
import common.grid.Point

object day10 extends common.AocApp(Day10Solution.solution)

object Day10Solution {
  object Parsed {
    private def charToDirections(c: Char): Seq[Point] = c match {
      case '|' => List(Point.Up, Point.Down)
      case '-' => List(Point.Right, Point.Left)
      case 'L' => List(Point.Up, Point.Right)
      case 'J' => List(Point.Up, Point.Left)
      case '7' => List(Point.Down, Point.Left)
      case 'F' => List(Point.Down, Point.Right)
      case 'S' => Point.OrthogonalDirections
    }
  }
  final case class Parsed(start: Point, pipes: Map[Point, Char]) {
    import Parsed._

    private def potentialNeighbors(point: Point): Seq[Point] = pipes.get(point).fold(Seq.empty[Point])(c => charToDirections(c).map(_ + point))

    def neighbors(point: Point): Seq[Point] = potentialNeighbors(point).filter(potential => potentialNeighbors(potential).contains(point))

    private val dijkstra = Dijkstra.simpleDistance(start)(neighbors)

    def part1: Long = dijkstra.values.max

    def part2: Long = {
      val toNorth = dijkstra.keySet.collect {
        case p if Set('|', 'L', 'J').contains(pipes(p))                                         => p
        case p if pipes.get(p).contains('S') && pipes.get(p + Point.Up).exists(Set('|', '7', 'F')) => p
      }

      val minX = pipes.keys.map(_.x).min
      val maxX = pipes.keys.map(_.x).max
      val minY = pipes.keys.map(_.y).min
      val maxY = pipes.keys.map(_.y).max

      (for {
        y <- (minY + 1) until maxY
        x <- (minX + 1) until maxX
        p = Point(x, y) if !dijkstra.keySet.contains(p)
      } yield p).count { point =>
        (for {
          x <- -1 to point.x
        } yield Point(x, point.y)).count(toNorth) % 2 == 1
      }
    }
  }


  object solution extends common.Solution[Parsed]("2023", "10") {
    override def parse(input: String): Parsed = {
      val ground = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex if c != '.'
      } yield Point(x, y) -> c).toMap
      Parsed(ground.find(_._2 == 'S').get._1, ground)
    }

    override def part1(parsed: Parsed): Any = parsed.part1

    override def part2(parsed: Parsed): Any = parsed.part2
  }
}
