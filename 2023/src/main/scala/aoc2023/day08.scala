package aoc2023

import common.algo.integers.LCM

import scala.annotation.tailrec

object day08 extends common.AocApp(Day08Solution.solution)

object Day08Solution {
  type Parsed = (String, Map[String, (String, String)])

  val LineR = "(.+) = \\((.+), (.+)\\)".r
  final case class Step(where: String, count: 0)

  object solution extends common.Solution[Parsed]("2023", "08") {
    override def parse(input: String): Parsed = {
      val Array(directions, nodes) = input.split("\n\n")
      (
        directions,
        nodes
          .split("\n")
          .map { case LineR(name, left, right) =>
            name -> (left, right)
          }
          .toMap
      )
    }

    override def part1(parsed: Parsed): Any = {
      val (directions, nodes) = parsed

      @tailrec
      def loop(where: String, directions: LazyList[Char], count: Int): Int =
        if (where == "ZZZ") count
        else {
          directions.head match {
            case 'R' => loop(nodes(where)._2, directions.tail, count + 1)
            case 'L' => loop(nodes(where)._1, directions.tail, count + 1)
          }
        }

      loop("AAA", LazyList.continually(directions).flatten, 0)
    }

    override def part2(parsed: Parsed): Any = {
      val (directions, nodes) = parsed

      @tailrec
      def forOne(where: String, directions: LazyList[Char], count: Int): Int =
        if (where.endsWith("Z")) count
        else {
          directions.head match {
            case 'R' => forOne(nodes(where)._2, directions.tail, count + 1)
            case 'L' => forOne(nodes(where)._1, directions.tail, count + 1)
          }
        }

      LCM(nodes.keySet.filter(_.endsWith("A")).map(forOne(_, LazyList.continually(directions).flatten, 0)).map(BigInt.apply).toSeq)
    }
  }
}
