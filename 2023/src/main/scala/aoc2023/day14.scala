package aoc2023

import common.grid.Point

import scala.annotation.tailrec

object day14 extends common.AocApp(Day14Solution.solution)

object Day14Solution {
  type Parsed = (Platform, Set[Point])

  final case class Platform(maxed: Point, squared: Set[Point]) {
    def tilt(rounded: Set[Point], direction: Point): Set[Point] =
      rounded.map { round =>
        val c = LazyList.iterate(round)(_ + direction).takeWhile(p => !squared(p)).count(p => !rounded(p))
        round + direction * c
      }

    def cycle(rounded: Set[Point]): Set[Point] = Point.OrthogonalDirections.foldLeft(rounded)(tilt)

    def load(rounded: Set[Point]): Int =
      (for {
        y <- 0 to maxed.y
        x <- 0 to maxed.x
        p = Point(x, y)
        if rounded.contains(p)
      } yield maxed.y - y + 1).sum
  }

  object solution extends common.Solution[Parsed]("2023", "14") {
    override def parse(input: String): Parsed = {
      val map = (for {
        (line, y) <- input.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield c -> Point(x, y)).groupMapReduce(_._1)(tu => Set(tu._2))(_ ++ _)
      val rounded = map('O')
      val maxed = Point(rounded.map(_.x).max, rounded.map(_.y).max)
      (
        Platform(
          maxed,
          map('#') ++ (for {
            y <- 0 to maxed.y
            x <- List(-1, maxed.x + 1)
          } yield Point(x, y)) ++
            (for {
              x <- 0 to maxed.x
              y <- List(-1, maxed.y + 1)
            } yield Point(x, y))
        ),
        rounded
      )
    }

    // 113456
    override def part1(parsed: Parsed): Any = {
      val (platform, rounded) = parsed

      platform.load(platform.tilt(rounded, Point.Up))
    }

    private val TotalCycles = 1_000_000_000L

    override def part2(parsed: Parsed): Any = {
      val (platform, rounded) = parsed

      @tailrec
      def loop(current: Set[Point], count: Long, visited: Map[Set[Point], Long]): Set[Point] = visited.get(current) match {
        case Some(value) =>
          val cycleLength = count - value
          val remain = (TotalCycles - value) % cycleLength
          visited.find(_._2 == value + remain).get._1
        case _ => loop(platform.cycle(current), count + 1, visited + (current -> count))
      }

      platform.load(loop(rounded, 0, Map.empty))
    }
  }
}
