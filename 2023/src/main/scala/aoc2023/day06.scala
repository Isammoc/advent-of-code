package aoc2023

import scala.annotation.tailrec

object day06 extends common.AocApp(Day06Solution.solution)

object Day06Solution {
  type Parsed = String

  implicit class RichBigInt(val number: BigInt) extends AnyVal {
    def sqrt: BigInt = {
      def next(n: BigInt, i: BigInt): BigInt = (n + i / n) >> 1

      val one = BigInt(1)
      val n = one
      val n1 = next(n, number)

      @tailrec
      def sqrtHelper(n: BigInt, n1: BigInt): BigInt = if ((n1 - n).abs <= one) List(n1, n).max else sqrtHelper(n1, next(n1, number))

      sqrtHelper(n, n1)
    }
  }

  object solution extends common.Solution[Parsed]("2023", "06") {
    override def parse(input: String): Parsed = input

    private def forOne(time: BigInt, distance: BigInt): Long = {
      val min = (time - (time * time - 4 * distance).sqrt) / 2
      val max = (time + (time * time - 4 * distance).sqrt) / 2

      val realMin = List(-1, 0, 1, 2).map(i => min + i).find(x => x * (time - x) > distance).get.toLong
      val realMax = List(2, 1, 0, -1).map(i => max + i).find(x => x * (time - x) > distance).get.toLong

      realMax - realMin + 1
    }

    override def part1(parsed: Parsed): Any = {
      val Array(times, distances) = parsed.split("\n").map(_.split("\\s+").toList.tail.map(_.toInt))

      times.zip(distances).map { case (time, distance) => forOne(time, distance) }.product
    }

    override def part2(parsed: Parsed): Any = {
      val Array(time, distance) = parsed.split("\n").map(line => BigInt(line.split("\\s+").drop(1).mkString))
      forOne(time, distance)
    }
  }
}
