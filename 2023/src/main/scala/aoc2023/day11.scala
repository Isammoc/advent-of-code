package aoc2023

import common.grid.Point

import scala.annotation.tailrec

object day11 extends common.AocApp(Day11Solution.solution)

object Day11Solution {
  type Parsed = Set[Point]

  object solution extends common.Solution[Parsed]("2023", "11") {
    override def parse(input: String): Parsed = (for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex if c == '#'
    } yield Point(x, y)).toSet

    def total(parsed: Parsed, times: Long): Long = {
      val xs = parsed.map(_.x)
      val additionalX = (0 to xs.max).toSet -- xs
      val ys = parsed.map(_.y)
      val additionalY = (0 to ys.max).toSet -- ys

      def distance(a: Point, b: Point) =
        a.manhattan(b) + (times - 1) * ((math.min(a.x, b.x) to math.max(a.x, b.x)).count(additionalX) + (math.min(a.y, b.y) to math.max(a.y, b.y))
          .count(additionalY))

      @tailrec
      def loop(remain: List[Point], current: Long): Long = remain match {
        case a :: others =>
          loop(others, current + others.map(distance(a, _)).sum)
        case _ => current
      }
      loop(parsed.toList, 0)
    }

    override def part1(parsed: Parsed): Any = total(parsed, 2)

    override def part2(parsed: Parsed): Any = total(parsed, 1_000_000)
  }
}
