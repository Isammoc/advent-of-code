package aoc2023

import common.grid.Point
import common.grid.Point._

import scala.annotation.tailrec

object day23 extends common.AocApp(Day23Solution.solution)

object Day23Solution {
  type Parsed = Grid

  object Grid {
    def parse(str: String): Grid = Grid(
      (for {
        (line, y) <- str.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> c).toMap
    )
  }
  final case class Grid(grid: Map[Point, Char]) {
    private val miny = grid.keySet.map(_.y).min
    private val maxy = grid.keySet.map(_.y).max

    private def findWithY(y: Int): Point = grid.filter { case (start, c) => start.y == y && c != '#' }.keySet.head

    lazy val start: Point = findWithY(miny)
    lazy val end: Point = findWithY(maxy)
  }

  final case class Node(current: Point, visited: Set[Point])
  private final case class WeightNode(current: Point, visited: Set[Point], weight: Int)

  final case class Graph(neighbors: Map[Point, Map[Point, Int]]) {
    def reduce: Graph = {
      @tailrec
      def loop(current: Map[Point, Map[Point, Int]]): Graph =
        current.collectFirst { case (middle, edges) if edges.size == 2 => middle -> edges } match {
          case Some((middle, edges)) =>
            val newWeight = edges.values.sum
            val List(one, other) = edges.keys.toList
            loop(current - middle + (one -> (current(one) - middle + (other -> newWeight))) + (other -> (current(other) - middle + (one -> newWeight))))
          case None => Graph(current)
        }
      loop(this.neighbors)
    }
  }

  object solution extends common.Solution[Parsed]("2023", "23") {
    override def parse(input: String): Parsed = Grid.parse(input)

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(toVisit: List[Node], result: Int): Int = toVisit match {
        case Node(current, visited) :: other if current == parsed.end => loop(other, math.max(visited.size, result))
        case Node(current, visited) :: other =>
          val possible = Point.OrthogonalDirections.map(dir => (dir, current + dir)).collect {
            case (_, p) if parsed.grid.get(p).contains('.')     => p
            case (Up, p) if parsed.grid.get(p).contains('^')    => p
            case (Right, p) if parsed.grid.get(p).contains('>') => p
            case (Down, p) if parsed.grid.get(p).contains('v')  => p
            case (Left, p) if parsed.grid.get(p).contains('<')  => p
          }
          loop((possible.toSet -- visited).map(Node(_, visited + current)) ++: other, result)
        case _ => result
      }

      loop(List(Node(parsed.start, Set.empty)), 0)
    }

    override def part2(parsed: Parsed): Any = {
      val paths = parsed.grid.collect {
        case (p, c) if c != '#' => p
      }.toSet

      val graph = Graph((for {
        one <- paths.toList
        other <- OrthogonalDirections.map(one + _) if paths(other)
      } yield (one, other, 1)).groupMapReduce(_._1) { case (_, other, 1) => Map(other -> 1) }(_ ++ _)).reduce

      @tailrec
      def loop(toVisit: List[WeightNode], result: Int): Int = toVisit match {
        case WeightNode(current, _, weight) :: other if current == parsed.end => loop(other, math.max(weight, result))
        case WeightNode(current, visited, weight) :: other =>
          val possibles = graph.neighbors(current)
          loop(
            possibles
              .filterNot { case (p, _) => visited.contains(p) }
              .map { case (p, w) => WeightNode(p, visited + current, weight + w) } ++: other,
            result
          )
        case _ => result
      }

      loop(List(WeightNode(parsed.start, Set.empty, 0)), 0)
    }
  }
}
