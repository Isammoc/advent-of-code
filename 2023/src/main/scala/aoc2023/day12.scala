package aoc2023

import scala.collection.mutable

object day12 extends common.AocApp(Day12Solution.solution)

object Day12Solution {
  type Parsed = List[Node]
  def memoize[I, O](f: I => O): I => O = {
    val memory = new mutable.HashMap[I, O]()
    key => memory.getOrElseUpdate(key, f(key))
  }

  object Node {
    val End = Node(Nil, Nil)
  }
  final case class Node(remain: List[Char], want: List[Int]) {
    override def toString: String = s"{${remain.mkString("")} ${want.mkString(",")}}"
    def unfold: Node = Node(remain ++: '?' +: remain ++: '?' +: remain ++: '?' +: remain ++: '?' +: remain, want ++ want ++ want ++ want ++ want)
  }

  object solution extends common.Solution[Parsed]("2023", "12") {
    override def parse(input: String): Parsed = input
      .split("\n")
      .map { line =>
        val Array(remain, want) = line.split("\\s+")
        Node(remain.toList, want.split(",").map(_.toInt).toList)
      }
      .toList

    lazy val calc: Node => Long = memoize {
      case Node.End                 => 1L
      case Node('.' :: other, want) => calc(Node(other, want))
      case Node('?' :: other, want) => calc(Node(other, want)) + calc(Node('#' :: other, want))
      case Node(remain, i :: want) if remain.size >= i && remain.take(i).forall(Set('#', '?')) =>
        val after = remain.drop(i)
        if (after.isEmpty)
          calc(Node(Nil, want))
        else if (Set('.', '?').contains(after.head))
          calc(Node(after.tail, want))
        else 0L
      case _ => 0L
    }

    override def part1(parsed: Parsed): Any = parsed.map(calc).sum

    override def part2(parsed: Parsed): Any = parsed.map(_.unfold).map(calc).sum
  }
}
