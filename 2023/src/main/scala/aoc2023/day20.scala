package aoc2023

import common.algo.integers.LCM
import common.util.Repeat

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day20 extends common.AocApp(Day20Solution.solution)

object Day20Solution {
  type Parsed = System

  private object Pulse {
    object Low extends Pulse {
      override val inverse: Pulse = High
    }
    object High extends Pulse {
      override val inverse: Pulse = Low
    }
  }
  sealed trait Pulse {
    def inverse: Pulse
  }

  private object Signal {
    val Silence: List[Signal] = Nil
  }
  final case class Signal(origin: String, pulse: Pulse, destination: String)

  private object Module {
    private val FlipFlopR = "%(.+) -> (.+)".r
    private val ConjunctionR = "&(.+) -> (.+)".r
    private val BroadcastR = "(.+) -> (.+)".r
    def parse(str: String): Module = str match {
      case FlipFlopR(name, targets)    => FlipFlop(name, on = false, targets.split(", ").toList)
      case ConjunctionR(name, targets) => Conjunction(name, Map.empty, targets.split(", ").toList)
      case BroadcastR(name, targets)   => Broadcast(name, targets.split(", ").toList)
    }
  }
  sealed abstract class Module(val name: String, val targets: List[String]) {
    def send(pulse: Pulse): List[Signal] = targets.map(Signal(name, pulse, _))
    def handle(signal: Signal, turn: Long): (Module, List[Signal])
    def withInputs(inputs: List[String]): Module = this
  }
  private final case class Broadcast(override val name: String, override val targets: List[String]) extends Module(name, targets) {
    override def handle(signal: Signal, turn: Long): (Module, List[Signal]) = (this, send(signal.pulse))
  }
  private final case class FlipFlop(override val name: String, on: Boolean, override val targets: List[String]) extends Module(name, targets) {
    private def flip: FlipFlop =
      this.copy(on = !on)
    override def handle(signal: Signal, turn: Long): (Module, List[Signal]) = signal.pulse match {
      case Pulse.High => (this, Signal.Silence)
      case Pulse.Low  => (this.flip, send(if (on) Pulse.Low else Pulse.High))
    }
  }
  private final case class Conjunction(override val name: String, memory: Map[String, Pulse], override val targets: List[String])
      extends Module(name, targets) {
    override def handle(signal: Signal, turn: Long): (Module, List[Signal]) = {
      val newMemory = memory + (signal.origin -> signal.pulse)
      (this.copy(memory = newMemory), send(if (newMemory.values.forall(_ == Pulse.High)) Pulse.Low else Pulse.High))
    }

    override def withInputs(inputs: List[String]): Module = this.copy(memory = inputs.map(_ -> Pulse.Low).toMap)
  }
  private final case class AnalyseConjunction(
      override val name: String,
      memory: Map[String, Pulse],
      lastHigh: Map[String, Long],
      turns: Map[String, Long],
      override val targets: List[String]
  ) extends Module(name, targets) {
    override def handle(signal: Signal, turn: Long): (Module, List[Signal]) = {
      val newMemory = memory + (signal.origin -> signal.pulse)
      if (memory(signal.origin) != signal.pulse && signal.pulse == Pulse.High) {
        (
          this.copy(
            memory = newMemory,
            lastHigh = lastHigh + (signal.origin -> turn),
            turns = turns + (signal.origin -> (turn - lastHigh.getOrElse(signal.origin, 0L)))
          ),
          send(if (newMemory.values.forall(_ == Pulse.High)) Pulse.Low else Pulse.High)
        )
      } else {
        (this.copy(memory = newMemory), send(if (newMemory.values.forall(_ == Pulse.High)) Pulse.Low else Pulse.High))
      }
    }

    override def withInputs(inputs: List[String]): Module = this.copy(memory = inputs.map(_ -> Pulse.Low).toMap)
  }

  object System {
    def parse(str: String): System = {
      val allModules = str.split("\n").toList.map(Module.parse)

      val inputsByTarget = (for {
        module <- allModules
        input = module.name
        target <- module.targets
      } yield target -> input).groupMap(_._1)(_._2)

      System((for {
        module <- allModules
      } yield module.name -> module.withInputs(inputsByTarget.getOrElse(module.name, Nil))).toMap)
    }
  }
  final case class System(modules: Map[String, Module]) {
    def button: (System, Long, Long) = {
      @tailrec
      def loop(modules: Map[String, Module], toVisit: Queue[Signal], low: Long, high: Long): (System, Long, Long) = toVisit match {
        case s +: other if modules.contains(s.destination) =>
          val (newModule, nextSignals) = modules(s.destination).handle(s, 0)
          loop(
            modules + (s.destination -> newModule),
            other ++ nextSignals,
            low + (if (s.pulse == Pulse.Low) 1 else 0),
            high + (if (s.pulse == Pulse.High) 1 else 0)
          )
        case s +: other =>
          loop(
            modules,
            other,
            low + (if (s.pulse == Pulse.Low) 1 else 0),
            high + (if (s.pulse == Pulse.High) 1 else 0)
          )
        case _ => (System(modules), low, high)
      }

      loop(modules, Queue(Signal("button", Pulse.Low, "broadcaster")), 0, 0)
    }

    def button2(turn: Long): Option[System] = {
      @tailrec
      def loop(modules: Map[String, Module], toVisit: Queue[Signal]): Option[System] = toVisit match {
        case Signal(_, Pulse.Low, "rx") +: _ => None
        case s +: other if modules.contains(s.destination) =>
          val (newModule, nextSignals) = modules(s.destination).handle(s, turn)
          loop(
            modules + (s.destination -> newModule),
            other ++ nextSignals
          )
        case _ +: other =>
          loop(
            modules,
            other
          )
        case _ => Some(System(modules))
      }

      loop(modules, Queue(Signal("button", Pulse.Low, "broadcaster")))
    }

  }

  object solution extends common.Solution[Parsed]("2023", "20") {
    override def parse(input: String): Parsed = System.parse(input)

    override def part1(parsed: Parsed): Any = {
      val (_, low, high) = Repeat(1000)((parsed, 0L, 0L)) { case (before, low, high) =>
        val (after, newLow, newHigh) = before.button
        (after, low + newLow, high + newHigh)
      }
      low * high
    }

    // 241528184647003
    override def part2(parsed: Parsed): Any = {
      val conjunctionName = parsed.modules.find(_._2.targets.contains("rx")).get._1

      @tailrec
      def loop(current: System, count: Long): BigInt =
        if (count <= 0) {
          current.modules(conjunctionName) match {
            case AnalyseConjunction(_, _, _, turns, _) =>
              LCM(turns.values.toList.map(BigInt.apply))
          }
        } else {
          current.button2(count) match {
            case Some(next) =>
              loop(next, count - 1)
          }
        }

      val analyseSystem = parsed.modules(conjunctionName) match {
        case Conjunction(name, memory, targets) =>
          System(parsed.modules + (conjunctionName -> AnalyseConjunction(name, memory, Map.empty, Map.empty, targets)))
      }

      loop(analyseSystem, 10000)
    }
  }
}
