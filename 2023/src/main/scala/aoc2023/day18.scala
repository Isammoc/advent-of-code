package aoc2023

import scala.annotation.tailrec
import scala.util.matching.Regex

object day18 extends common.AocApp(Day18Solution.solution)

object Day18Solution {
  type Parsed = Iterable[PlanLine]

  object Point {
    val Up: Point = Point(0, -1)
    val Right: Point = Point(1, 0)
    val Down: Point = Point(0, 1)
    val Left: Point = Point(-1, 0)
  }
  final case class Point(x: BigInt, y: BigInt) {
    def +(that: Point): Point = Point(x + that.x, y + that.y)
    def *(size: BigInt): Point = Point(x * size, y * size)
  }

  private object PlanLine {
    private def parseDir(dir: String): Point = dir match {
      case "U" => Point.Up
      case "R" => Point.Right
      case "D" => Point.Down
      case "L" => Point.Left
    }

    private val PlanLineR: Regex = "([RDLU])\\s+(\\d+)\\s+\\((.+)\\)".r

    def parse(line: String): PlanLine = line match {
      case PlanLineR(dir, length, color) =>
        PlanLine(parseDir(dir), length.toInt, color)
    }
  }

  final case class PlanLine(dir: Point, length: Int, color: String) {
    val InnerR = "#(.....)(.)".r
    def otherDir: Point = color match {
      case InnerR(_, "0") => Point.Right
      case InnerR(_, "1") => Point.Down
      case InnerR(_, "2") => Point.Left
      case InnerR(_, "3") => Point.Up
    }

    def otherLength: BigInt = color match {
      case InnerR(l, _) => BigInt(l, 16)
    }
  }

  final case class Grid(walls: Set[Point]) {
    val minX: BigInt = walls.map(_.x).min
    val maxX: BigInt = walls.map(_.x).max
    val minY: BigInt = walls.map(_.y).min
    val maxY: BigInt = walls.map(_.y).max
  }

  final case class Corners(corners: Set[Point]) {
    val minX: BigInt = corners.map(_.x).min
    val maxX: BigInt = corners.map(_.x).max
    val minY: BigInt = corners.map(_.y).min
    val maxY: BigInt = corners.map(_.y).max

    def computeContent: BigInt = {
      @tailrec
      def loop(toVisit: List[Point], currentInside: List[Interval], height: BigInt, size: BigInt): BigInt =
        if (toVisit.isEmpty) size + currentInside.map(_.size).sum
        else {
          val newHeight = toVisit.head.y
          val (toConsider, after) = toVisit.span(_.y == newHeight)
          val newWalls = toConsider.grouped(2).map { case a :: b :: Nil => Interval(a.x, b.x) }.toList
          val (newInside, fence) = Interval.change(currentInside, newWalls)
          val addedSize = (newHeight - height - 1) * currentInside.map(_.size).sum + fence
          loop(
            after,
            newInside,
            newHeight,
            size + addedSize
          )
        }

      loop(corners.toList.sortBy(p => (p.y, p.x)), Nil, minY - 1, 0)
    }
  }

  object Interval {
    def change(before: List[Interval], change: List[Interval]): (List[Interval], BigInt) = {
      @tailrec
      def loop(before: List[Interval], change: List[Interval], reverse: List[Interval], fence: BigInt): (List[Interval], BigInt) = (before, change) match {
        case (Nil, change) => (reverse.reverse ++ change, fence + change.map(_.size).sum)
        case (before, Nil) => (reverse.reverse ++ before, fence + before.map(_.size).sum)
        case (Interval(a, b) :: otherBefore, Interval(c, d) :: otherChange) =>
          if (c < a) {
            if (d < a) loop(before, otherChange, Interval(c, d) :: reverse, fence + d - c + 1)
            else if (d == a) loop(Interval(c, b) :: otherBefore, otherChange, reverse, fence)
            else ???
          } else if (c == a) {
            if (d < b) loop(Interval(d, b) :: otherBefore, otherChange, reverse, fence + d - c)
            else if (d == b) loop(otherBefore, otherChange, reverse, fence + d - c + 1)
            else ???
          } else if (c < b) {
            if (d < b) loop(Interval(d, b) :: otherBefore, otherChange, Interval(a, c) :: reverse, fence + d - a)
            else if (d == b) loop(otherBefore, otherChange, Interval(a, c) :: reverse, fence + b - a + 1)
            else ???
          } else if (c == b) {
            if (otherBefore.headOption.exists(_.a == d)) {
              loop(Interval(a, otherBefore.head.b) :: otherBefore.tail, otherChange, reverse, fence)
            } else loop(Interval(a, d) :: otherBefore, otherChange, reverse, fence)
          } else // c > b
            loop(otherBefore, change, Interval(a, b) :: reverse, fence + b - a + 1)
      }

      loop(before, change, Nil, 0)
    }
  }
  final case class Interval(a: BigInt, b: BigInt) {
    def size: BigInt = b - a + 1
  }

  object solution extends common.Solution[Parsed]("2023", "18") {
    override def parse(input: String): Parsed = input.split("\n").map(PlanLine.parse)

    override def part1(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Point, remain: List[PlanLine], corners: Set[Point]): Corners = remain match {
        case PlanLine(dir, length, _) :: t =>
          loop(current + dir * length, t, corners + current)
        case _ => Corners(corners)
      }

      val corners = loop(Point(0, 0), parsed.toList, Set.empty)
      corners.computeContent
    }

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Point, remain: List[PlanLine], corners: Set[Point]): Corners = remain match {
        case pl :: t =>
          loop(current + pl.otherDir * pl.otherLength, t, corners + current)
        case _ => Corners(corners)
      }

      val corners = loop(Point(0, 0), parsed.toList, Set.empty)
      corners.computeContent
    }
  }
}
