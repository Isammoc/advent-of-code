package aoc2023

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {
  type Parsed = List[Card]

  final case class Card(index: Int, win: Set[Int], numbers: List[Int]) {
    val winAmount: Int = numbers.count(win)
  }

  object solution extends common.Solution[Parsed]("2023", "04") {
    override def parse(input: String): Parsed = for {
      line <- input.split("\n").toList
    } yield {
      val Array(cardNumber, others) = line.split(":")
      val index = cardNumber.split("\\s+")(1).toInt
      val Array(left, right) = others.split("\\|")
      Card(index, left.split("\\s+").filter(_.nonEmpty).map(_.toInt).toSet, right.split("\\s+").filter(_.nonEmpty).map(_.toInt).toList)
    }

    override def part1(parsed: Parsed): Any =
      parsed.map { card =>
        if (card.winAmount == 0) 0 else math.pow(2, card.winAmount - 1).intValue
      }.sum

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(remain: Queue[Card], cards: Map[Int, Int]): Int = remain match {
        case current +: other =>
          val currentAmount = cards(current.index)
          loop(
            other,
            if (current.winAmount > 0)
              cards ++ (1 to current.winAmount).map(_ + current.index).map(i => i -> (cards(i) + currentAmount))
            else cards
          )
        case _ => cards.values.sum
      }
      loop(Queue.from(parsed), parsed.map(_.index -> 1).toMap)
    }
  }
}
