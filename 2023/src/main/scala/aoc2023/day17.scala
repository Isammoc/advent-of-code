package aoc2023

import common.algo.graph.Dijkstra
import common.grid.Point

object day17 extends common.AocApp(Day17Solution.solution)

object Day17Solution {
  import Point._
  type Parsed = Grid

  final case class Grid(max: Point, grid: Map[Point, Int]) {
    def isValid(p: Point): Boolean =
      0 <= p.x && p.x <= max.x &&
        0 <= p.y && p.y <= max.y
  }

  object solution extends common.Solution[Parsed]("2023", "17") {
    override def parse(input: String): Parsed = {
      val grid = (
        for {
          (line, y) <- input.split("\n").zipWithIndex
          (c, x) <- line.zipWithIndex
        } yield Point(x, y) -> (c - '0')
      ).toMap

      Grid(grid.keys.maxBy(p => (p.x, p.y)), grid)
    }

    final case class Node(pos: Point, dir: Point, length: Int) {
      def neighbors(grid: Grid): List[(Node, Int)] =
        List(
          Node(pos + dir, dir, length + 1),
          Node(pos + dir.rotateLeft, dir.rotateLeft, 1),
          Node(pos + dir.rotateRight, dir.rotateRight, 1)
        ).filter(n => grid.isValid(n.pos) && n.length <= 3).map(n => n -> grid.grid(n.pos))

      def neighbors2(grid: Grid): List[(Node, Int)] =
        (if (length == 0) // start
           OrthogonalDirections.map(dir => Node(pos + dir, dir, 1))
         else if (length < 4) List(Node(pos + dir, dir, length + 1))
         else
           List(
             Node(pos + dir, dir, length + 1),
             Node(pos + dir.rotateLeft, dir.rotateLeft, 1),
             Node(pos + dir.rotateRight, dir.rotateRight, 1)
           )).filter(n => grid.isValid(n.pos) && n.length <= 10).map(n => n -> grid.grid(n.pos))
    }

    override def part1(parsed: Parsed): Any = {
      val start = Node(Point(0, 0), Up, 0)
      Dijkstra.reach[Int, Node](start, (n: Node) => n.neighbors(parsed), (n: Node) => n.pos == parsed.max)
    }

    override def part2(parsed: Parsed): Any = {
      val start = Node(Point(0, 0), Up, 0)
      Dijkstra.reach[Int, Node](start, (n: Node) => n.neighbors2(parsed), (n: Node) => n.pos == parsed.max)
    }
  }
}
