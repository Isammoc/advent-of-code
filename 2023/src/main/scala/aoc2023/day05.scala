package aoc2023

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  type Parsed = (List[Long], List[List[Mapper]])

  final case class Mapper(source: Interval, destinationStart: Long) {
    def map(left: Long): Option[Long] = Option.when(source.start <= left && left <= source.end)(destinationStart + left - source.start)
    def map(original: Interval): Option[Interval] = for {
      start <- map(original.start)
      end <- map(original.end)
    } yield Interval(start, end)
  }

  object Interval {
    def fromStartAndRange(start: Long, range: Long): Interval = Interval(start, start + range - 1)
  }
  final case class Interval(start: Long, end: Long) {
    def split(that: Interval): List[Interval] = {
      val before = if (that.start < this.start) List(Interval(that.start, math.min(that.end, this.start - 1))) else Nil
      val in = if (that.start < this.end && this.start < that.end) List(Interval(math.max(this.start, that.start), math.min(this.end, that.end))) else Nil
      val after = if (this.end < that.end) List(Interval(math.max(this.end + 1, that.start), that.end)) else Nil
      List(before, in, after).flatten
    }

    override def toString: String = s"$start-$end"
  }

  object solution extends common.Solution[Parsed]("2023", "05") {
    override def parse(input: String): Parsed = {
      val seedsPart :: mapperParts = input.split("\n\n").toList

      val seeds = seedsPart.split("\\s+").toList.tail.map(_.toLong)
      val mappers = mapperParts.map { mapperPart =>
        mapperPart
          .split("\n")
          .drop(1)
          .map { line =>
            val Array(destination, source, range) = line.split("\\s+").map(_.toLong)
            Mapper(Interval.fromStartAndRange(source, range), destination)
          }
          .toList
      }
      (seeds, mappers)
    }

    override def part1(parsed: Parsed): Any = {
      val (seeds, mapperList) = parsed

      seeds.map { seed =>
        mapperList.foldLeft(seed) { case (before, todo) =>
          todo.collectFirst({ (mapper: Mapper) => mapper.map(before) }.unlift).getOrElse(before)
        }
      }.min
    }

    override def part2(parsed: Parsed): Any = {
      val (seeds, mapperList) = parsed
      val intervalSeeds = seeds.grouped(2).toList.map { case List(start, range) =>
        Interval.fromStartAndRange(start, range)
      }

      mapperList.foldLeft(intervalSeeds) {
        case (before, todo) =>
          val toConsider = todo.foldLeft(before){ case (one, mapper) => one.flatMap(mapper.source.split)}
          toConsider.map { interval =>
            todo.collectFirst{{(mapper: Mapper) => mapper.map(interval)}.unlift}.getOrElse(interval)
          }
      }.map(_.start).min
    }
  }
}
