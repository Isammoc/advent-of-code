package aoc2023

import scala.annotation.tailrec

object day09 extends common.AocApp(Day09Solution.solution)

object Day09Solution {
  type Parsed = List[List[Long]]

  object solution extends common.Solution[Parsed]("2023", "09") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.split("\\s+").map(_.toLong).toList)

    @tailrec
    private def popStack(stack: List[List[Long]]): Long = stack match {
      case a :: b :: t        => popStack(((b.head + a.head) :: b) :: t)
      case (result :: _) :: _ => result
    }

    @tailrec
    private def pushStack(stack: List[List[Long]]): Long = stack match {
      case top :: others if top.forall(_ == 0) => popStack((0L :: top) :: others)
      case top :: _                            => pushStack(top.zip(top.tail).map { case (a, b) => a - b } :: stack)
    }

    override def part1(parsed: Parsed): Any = parsed.map(history => pushStack(history.reverse :: Nil)).sum

    override def part2(parsed: Parsed): Any = parsed.map(history => pushStack(history :: Nil)).sum
  }
}
