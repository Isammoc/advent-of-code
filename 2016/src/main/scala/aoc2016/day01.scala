package aoc2016

import common.grid.Point

import scala.annotation.tailrec

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  sealed trait Instruction {
    def steps: Int
  }

  object Instruction extends (String => Instruction) {
    private val LeftR = "L(\\d+)".r
    private val RightR = "R(\\d+)".r

    case class Left(steps: Int) extends Instruction

    case class Right(steps: Int) extends Instruction

    case class Forward(steps: Int) extends Instruction

    def apply(str: String): Instruction = {
      println(str)
      str match {
        case LeftR(steps) => Left(steps.toInt)
        case RightR(steps) => Right(steps.toInt)
      }
    }
  }

  object Person {
    val Initial: Person = Person(Point.Up, Point.Origin)
  }

  case class Person(direction: Point, position: Point) {
    def instr(instruction: Instruction): Person = this.instrStep(instruction) match {
      case (Instruction.Forward(0), result) => result
      case (instr, intermediate) => intermediate.instr(instr)
    }

    def instrStep(instruction: Instruction): (Instruction, Person) = {
      val nextDirection = instruction match {
        case Instruction.Left(_) => direction.rotateLeft
        case Instruction.Right(_) => direction.rotateRight
        case Instruction.Forward(_) => direction
      }
      (Instruction.Forward(instruction.steps - 1), Person(nextDirection, position + nextDirection))
    }
  }

  type Parsed = List[Instruction]

  object solution extends common.Solution[Parsed]("2016", "01") {
    override def parse(input: String): Parsed = input.split(", ").toList.map(Instruction)

    override def part1(parsed: Parsed): Any = parsed.foldLeft(Person.Initial)(_.instr(_)).position.manhattan(Point.Origin)

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(current: Person, todo: Parsed, visited: Set[Point]): Int = todo match {
        case Instruction.Forward(0) :: t => loop(current, t, visited)
        case instr :: t =>
          val (nextInstr, nextCurrent) = current.instrStep(instr)
          if (visited.contains(nextCurrent.position)) {
            nextCurrent.position.manhattan(Point.Origin)
          } else {
            loop(nextCurrent, nextInstr :: t, visited + nextCurrent.position)
          }
      }

      loop(Person.Initial, parsed, Set(Person.Initial.position))
    }
  }
}
