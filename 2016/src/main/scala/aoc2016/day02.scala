package aoc2016

import common.grid.Point

import scala.annotation.tailrec

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  object Direction extends (Char => Point) {
    override def apply(c: Char): Point = c match {
      case 'U' => Point.Up
      case 'D' => Point.Down
      case 'L' => Point.Left
      case 'R' => Point.Right
    }
  }

  object Pad {
    def from(str: String): Pad = {
      Pad((for {
        (line, y) <- str.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex if c != ' '
      } yield c -> Point(x, y)).toMap)
    }
  }

  case class Pad(digitToPosition: Map[Char, Point]) {
    val positionToDigit: Map[Point, Char] = digitToPosition.map { case (a, b) => b -> a }

    def go(start: Char, direction: Point): Char = positionToDigit.getOrElse(digitToPosition(start) + direction, start)

    def go(start: Char, directions: List[Point]): Char = directions.foldLeft(start) { case (c, dir) => go(c, dir) }
  }

  type Parsed = List[List[Point]]

  object solution extends common.Solution[Parsed]("2016", "02") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.toList.map(Direction))

    private def solve(pad: Pad, parsed: Parsed) = {
      @tailrec
      def loop(todo: Parsed, current: Char, done: List[Char]): String = todo match {
        case Nil => done.reverse.mkString
        case h :: t =>
          val next = pad.go(current, h)
          loop(t, next, next :: done)
      }

      loop(parsed, '5', Nil)
    }

    override def part1(parsed: Parsed): Any = {
      val pad = Pad.from(
        """123
          |456
          |789""".stripMargin)
      solve(pad, parsed)
    }

    override def part2(parsed: Parsed): Any = {
      val pad = Pad.from(
        """  1
          | 234
          |56789
          | ABC
          |  D""".stripMargin)
      solve(pad, parsed)
    }
  }
}

