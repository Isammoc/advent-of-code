package aoc2016

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {
  type Parsed = List[List[Int]]

  object solution extends common.Solution[Parsed]("2016", "03") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.trim.split("\\s+").toList.map(_.toInt))

    def trianglePossible(values: List[Int]): Boolean = {
      val sorted = values.sorted
      sorted(0) + sorted(1) > sorted(2)
    }

    override def part1(parsed: Parsed): Any = parsed.count(trianglePossible)

    override def part2(parsed: Parsed): Any = parsed.transpose.map {
      _.grouped(3).count(trianglePossible)
    }.sum
  }
}

