package aoc2016

import java.security.MessageDigest
import scala.annotation.tailrec

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  def md5(str: String): String = MessageDigest.getInstance("MD5").digest(str.getBytes).map("%02X".format(_)).mkString

  type Parsed = String

  object solution extends common.Solution[Parsed]("2016", "05") {
    override def parse(input: String): Parsed = input

    override def part1(parsed: Parsed): Any =
      LazyList
        .iterate(0L)(_ + 1)
        .map(parsed + _.toString)
        .map(md5)
        .filter(_.startsWith("00000"))
        .tapEach(println(_))
        .map(_ (5))
        .take(8)
        .mkString
        .toLowerCase

    override def part2(parsed: Parsed): Any = {
      @tailrec
      def loop(password: String, current: Long): String = {
        val hash = md5(parsed + current.toString)
        if (hash.startsWith("00000")) {
          if ('0' <= hash(5) && hash(5) <= '7') {
            val pos = (hash(5) - '0').toInt
            if (password(pos) == '_') {
              val newPassword = password.substring(0, pos) + hash(6) + password.substring(pos + 1)
              println(newPassword)
              if (newPassword.contains('_')) loop(newPassword, current + 1)
              else newPassword
            } else loop(password, current + 1)
          } else {
            loop(password, current + 1)
          }
        } else loop(password, current + 1)
      }

      loop("________", 0).toLowerCase
    }
  }
}

