package aoc2016

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {
  object Room extends (String => Room) {
    private val RoomR = "([a-z-]+)-(\\d+)\\[([a-z]+)]".r

    def apply(str: String): Room = str match {
      case RoomR(name, sectorId, checksum) => Room(name, sectorId.toInt, checksum)
    }
  }

  final case class Room(name: String, sectorId: Int, checksum: String) {
    def isReal: Boolean =
      name
        .toList
        .filter(_ != '-')
        .groupMapReduce(identity)(_ => 1)(_ + _)
        .toList
        .map { case (c, size) => (-size) -> c }
        .sorted
        .take(5)
        .map(_._2)
        .mkString == checksum

    private def shift(c: Char): Char =
      if (c == '-') ' '
      else ((c - 'a' + sectorId) % 26 + 'a').toChar

    def decryptedName: String = name.map(shift)
  }

  type Parsed = List[Room]

  object solution extends common.Solution[Parsed]("2016", "04") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(Room)

    override def part1(parsed: Parsed): Any = parsed.filter(_.isReal).map(_.sectorId).sum

    override def part2(parsed: Parsed): Any = parsed.filter(_.isReal).find(_.decryptedName.contains("northpole")).get.sectorId
  }
}

