package aoc2016

class day02Spec extends common.AocSpec {

  import Day02Solution._

  val sampleInput: String =
    """ULL
      |RRDDD
      |LURDL
      |UUUUD""".stripMargin

  "day02 2016" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "1985"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "5DB3"
      }
    }
  }
}

