package aoc2016

class day05Spec extends common.AocSpec {

  import Day05Solution._

  val sampleInput: String =
    """abc""".stripMargin

  "day05 2016" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "18f47a30"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "05ace8e3"
      }
    }
  }
}

