package aoc2016

class day03Spec extends common.AocSpec {

  import Day03Solution._

  val sampleInput: String =
    """5 10 25
      |10 10 10
      |10 10 10
      |""".stripMargin

  "day03 2016" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 2
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 2
      }
    }
  }
}

