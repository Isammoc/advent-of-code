package aoc2016

class day01Spec extends common.AocSpec {

  import Day01Solution._

  "day01 2016" can {
    import solution._

    "parse" should {
      "sample" in {
        parse("R1") shouldBe List(Instruction.Right(1))
      }
    }

    "part1" should {
      "example" in {
        part1(parse("R2, L3")) shouldEqual 5
        part1(parse("R2, R2, R2")) shouldEqual 2
        part1(parse("R5, L5, R5, R3")) shouldEqual 12
      }
    }

    "part2" should {
      "example" in {
        part2(parse("R8, R4, R4, R8")) shouldEqual 4
      }
    }
  }
}

