package aoc2016

class day04Spec extends common.AocSpec {
  import Day04Solution._

  val sampleInput: String =
    """aaaaa-bbb-z-y-x-123[abxyz]
      |a-b-c-d-e-f-g-h-987[abcde]
      |not-a-real-room-404[oarel]
      |totally-real-room-200[decoy]
      |""".stripMargin

  "day04 2016" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 1514
      }
    }

    "part2" should {
      "example" in {
        parse("qzmt-zixmtkozy-ivhz-343[aaa]").head.decryptedName shouldEqual "very encrypted name"
      }
    }
  }
}

