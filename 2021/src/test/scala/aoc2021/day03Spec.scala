package aoc2021
import common.AocSpec

class day03Spec extends AocSpec {
  import Day03Solution._

  val sampleInput: String =
    """00100
      |11110
      |10110
      |10111
      |10101
      |01111
      |00111
      |11100
      |10000
      |11001
      |00010
      |01010""".stripMargin

  "day03 2021" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 198
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 230
      }
    }
  }
}
