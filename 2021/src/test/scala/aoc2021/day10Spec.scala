package aoc2021
import common.AocSpec

class day10Spec extends AocSpec {
  import Day10Solution._

  val sampleInput: String =
    """[({(<(())[]>[[{[]{<()<>>
      |[(()[<>])]({[<{<<[]>>(
      |{([(<{}[<>[]}>{[]{[(<()>
      |(((({<>}<{<{<>}{[]{[]{}
      |[[<[([]))<([[{}[[()]]]
      |[{[{({}]{}}([{[{{{}}([]
      |{<[[]]>}<{[{[{[]{()[[[]
      |[<(<(<(<{}))><([]([]()
      |<{([([[(<>()){}]>(<<{{
      |<{([{{}}[<[[[<>{}]]]>[]]""".stripMargin

  "day10 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 26397
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 288957
      }
    }
  }
}
