package aoc2021
import common.AocSpec

class day06Spec extends AocSpec {
  import Day06Solution._

  val sampleInput: String =
    """3,4,3,1,2""".stripMargin

  "day06 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 5934
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual BigInt("26984457539")
      }
    }
  }
}
