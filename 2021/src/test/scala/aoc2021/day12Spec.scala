package aoc2021
import common.AocSpec

class day12Spec extends AocSpec {
  import Day12Solution._

  "day12 2021" can {
    import solution._
    val sample1 = parse("""start-A
                          |start-b
                          |A-c
                          |A-b
                          |b-d
                          |A-end
                          |b-end""".stripMargin)

    val sample2 = parse("""dc-end
                          |HN-start
                          |start-kj
                          |dc-start
                          |dc-HN
                          |LN-dc
                          |HN-end
                          |kj-sa
                          |kj-HN
                          |kj-dc""".stripMargin)

    val sample3 = parse("""fs-end
                          |he-DX
                          |fs-he
                          |start-DX
                          |pj-DX
                          |end-zg
                          |zg-sl
                          |zg-pj
                          |pj-he
                          |RW-he
                          |fs-DX
                          |pj-RW
                          |zg-RW
                          |start-pj
                          |he-WI
                          |zg-he
                          |pj-fs
                          |start-RW""".stripMargin)
    "part1" should {
      "example" in {
        part1(sample1) shouldEqual 10
        part1(sample2) shouldEqual 19
        part1(sample3) shouldEqual 226
      }
    }

    "part2" should {
      "example" in {
        part2(sample1) shouldEqual 36
        part2(sample2) shouldEqual 103
        part2(sample3) shouldEqual 3509
      }
    }
  }
}
