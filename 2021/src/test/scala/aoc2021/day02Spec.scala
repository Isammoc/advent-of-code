package aoc2021

class day02Spec extends common.AocSpec {
  import Day02Solution._

  val sampleInput: String =
    """forward 5
      |down 5
      |forward 8
      |up 3
      |down 8
      |forward 2""".stripMargin

  "day02 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 150
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 900
      }
    }
  }
}
