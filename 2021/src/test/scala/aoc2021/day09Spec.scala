package aoc2021
import common.AocSpec

class day09Spec extends AocSpec {
  import Day09Solution._
  val sampleInput: String =
    """2199943210
      |3987894921
      |9856789892
      |8767896789
      |9899965678""".stripMargin

  "day09 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 15
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1134
      }
    }
  }
}
