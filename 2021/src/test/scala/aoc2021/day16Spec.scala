package aoc2021
import common.AocSpec

class day16Spec extends AocSpec {
  import Day16Solution._

  "day16 2021" can {
    "charToBinary" in {
      import Binary._
      Binary.from('0') shouldBe Seq(Zero, Zero, Zero, Zero)
      Binary.from('1') shouldBe Seq(Zero, Zero, Zero, One)
      Binary.from('2') shouldBe Seq(Zero, Zero, One, Zero)
      Binary.from('3') shouldBe Seq(Zero, Zero, One, One)
      Binary.from('4') shouldBe Seq(Zero, One, Zero, Zero)
      Binary.from('5') shouldBe Seq(Zero, One, Zero, One)
      Binary.from('6') shouldBe Seq(Zero, One, One, Zero)
      Binary.from('7') shouldBe Seq(Zero, One, One, One)
      Binary.from('8') shouldBe Seq(One, Zero, Zero, Zero)
      Binary.from('9') shouldBe Seq(One, Zero, Zero, One)
      Binary.from('A') shouldBe Seq(One, Zero, One, Zero)
      Binary.from('B') shouldBe Seq(One, Zero, One, One)
      Binary.from('C') shouldBe Seq(One, One, Zero, Zero)
      Binary.from('D') shouldBe Seq(One, One, Zero, One)
      Binary.from('E') shouldBe Seq(One, One, One, Zero)
      Binary.from('F') shouldBe Seq(One, One, One, One)
    }

    "parse" in {
      Packet.parse("D2FE28") shouldBe Packet.Literal(6, 2021)
      Packet.parse("38006F45291200") shouldBe Packet.Operation(1, 6, List(Packet.Literal(6, 10), Packet.Literal(2, 20)))
      Packet.parse("EE00D40C823060") shouldBe Packet.Operation(7, 3, List(Packet.Literal(2, 1), Packet.Literal(4, 2), Packet.Literal(1, 3)))
    }

    "part1" should {
      val part1 = (solution.parse _).andThen(solution.part1)
      "example" in {
        part1("8A004A801A8002F478") shouldBe 16
        part1("620080001611562C8802118E34") shouldBe 12
        part1("C0015000016115A2E0802F182340") shouldBe 23
        part1("A0016C880162017C3686B18A3D4780") shouldBe 31
      }
    }

    "part2" should {
      val part2 = (solution.parse _).andThen(solution.part2)
      "example" in {
        part2("C200B40A82") shouldBe 3
        part2("04005AC33890") shouldBe 54
        part2("880086C3E88112") shouldBe 7
        part2("CE00C43D881120") shouldBe 9
        part2("D8005AC2A8F0") shouldBe 1
        part2("F600BC2D8F") shouldBe 0
        part2("9C005AC2F8F0") shouldBe 0
        part2("9C0141080250320F1802104A08") shouldBe 1
      }
    }
  }
}
