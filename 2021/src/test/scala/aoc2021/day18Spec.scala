package aoc2021
import common.AocSpec

class day18Spec extends AocSpec {
  import Day18Solution._

  "day18 2021" can {

    "SnailFishNumber" should {
      "parse" in {
        def parseTest(input: String) = SnailFishNumber.parse(input).toString
        parseTest("[1,2]") shouldBe "[1,2]"
        parseTest("[[1,2],3]") shouldBe "[[1,2],3]"
        parseTest("[9,[8,7]]") shouldBe "[9,[8,7]]"
        parseTest("[[1,9],[8,5]]") shouldBe "[[1,9],[8,5]]"
        parseTest("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]") shouldBe "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"
        parseTest("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]") shouldBe "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"
        parseTest("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]") shouldBe "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]"
      }

      "explode" in {
        def explodeTest(input: String) = SnailFishNumber.parse(input).explode.toString
        explodeTest("[[[[[9,8],1],2],3],4]") shouldBe "[[[[0,9],2],3],4]"
        explodeTest("[7,[6,[5,[4,[3,2]]]]]") shouldBe "[7,[6,[5,[7,0]]]]"
        explodeTest("[[6,[5,[4,[3,2]]]],1]") shouldBe "[[6,[5,[7,0]]],3]"
        explodeTest("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]") shouldBe "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
        explodeTest("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]") shouldBe "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"
      }

      "split" in {
        SnailFishNumber.parse("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]").explode.split.toString shouldBe "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"
      }

      "reduce" in {
        SnailFishNumber.parse("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]").reduce.toString shouldBe "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
      }

      "add" in {
        SnailFishNumber
          .plus(SnailFishNumber.parse("[[[[4,3],4],4],[7,[[8,4],9]]]"), SnailFishNumber.parse("[1,1]"))
          .toString shouldBe "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
      }

      "addAll" in {
        def addAllTest(str: String) = {
          str.split("\n").toList.map(SnailFishNumber.parse).reduceLeft((a, b) => SnailFishNumber.plus(a, b)).toString
        }

        addAllTest("""[1,1]
                     |[2,2]
                     |[3,3]
                     |[4,4]""".stripMargin) shouldBe "[[[[1,1],[2,2]],[3,3]],[4,4]]"

        addAllTest("""[1,1]
                     |[2,2]
                     |[3,3]
                     |[4,4]
                     |[5,5]""".stripMargin) shouldBe "[[[[3,0],[5,3]],[4,4]],[5,5]]"

        addAllTest("""[1,1]
                     |[2,2]
                     |[3,3]
                     |[4,4]
                     |[5,5]
                     |[6,6]""".stripMargin) shouldBe "[[[[5,0],[7,4]],[5,5]],[6,6]]"

        addAllTest("""[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
                     |[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
                     |[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
                     |[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
                     |[7,[5,[[3,8],[1,4]]]]
                     |[[2,[2,2]],[8,[8,1]]]
                     |[2,9]
                     |[1,[[[9,3],9],[[9,0],[0,7]]]]
                     |[[[5,[7,4]],7],1]
                     |[[[[4,2],2],6],[8,7]]""".stripMargin) shouldBe "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
      }

      "magnitude" in {
        def magnitudeTest(input: String) = SnailFishNumber.parse(input).magnitude
        magnitudeTest("[9,1]") shouldBe 29
        magnitudeTest("[1,9]") shouldBe 21
        magnitudeTest("[[9,1],[1,9]]") shouldBe 129

        magnitudeTest("[[1,2],[[3,4],5]]") shouldBe 143
        magnitudeTest("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]") shouldBe 1384
        magnitudeTest("[[[[1,1],[2,2]],[3,3]],[4,4]]") shouldBe 445
        magnitudeTest("[[[[3,0],[5,3]],[4,4]],[5,5]]") shouldBe 791
        magnitudeTest("[[[[5,0],[7,4]],[5,5]],[6,6]]") shouldBe 1137
        magnitudeTest("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]") shouldBe 3488
      }
    }

    val sampleInput =
      """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
        |[[[5,[2,8]],4],[5,[[9,9],0]]]
        |[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
        |[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
        |[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
        |[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
        |[[[[5,4],[7,7]],8],[[8,3],8]]
        |[[9,3],[[9,9],[6,[4,9]]]]
        |[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
        |[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]""".stripMargin

    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 4140
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 3993
      }
    }
  }
}
