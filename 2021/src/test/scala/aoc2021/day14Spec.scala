package aoc2021
import common.AocSpec

class day14Spec extends AocSpec {
  import Day14Solution._

  val sampleInput: String =
    """NNCB
      |
      |CH -> B
      |HH -> N
      |CB -> H
      |NH -> C
      |HB -> C
      |HC -> B
      |HN -> C
      |NN -> C
      |BH -> H
      |NC -> B
      |NB -> B
      |BN -> B
      |BB -> N
      |BC -> B
      |CC -> N
      |CN -> C""".stripMargin

  "polymere" should {

    val polymere = Polymere.parse(sampleInput)
    val chain = LazyList.iterate(polymere)(_.next)

    "step 1" in {
      chain.drop(1).head.pairs shouldBe Polymere.parsePairs("NCNBCHB")
    }
    "step 2" in {
      chain.drop(2).head.pairs shouldBe Polymere.parsePairs("NBCCNBBBCBHCB")
    }
    "step 3" in {
      chain.drop(3).head.pairs shouldBe Polymere.parsePairs("NBBBCNCCNBBNBNBBCHBHHBCHB")
    }
    "step 4" in {
      chain.drop(4).head.pairs shouldBe Polymere.parsePairs("NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB")
      chain.drop(4).head.size shouldBe 49
    }
    "step 5" in {
      chain.drop(5).head.size shouldBe 97
    }
    "step 10" in {
      chain.drop(10).head.size shouldBe 3073
    }
  }

  "day14 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 1588
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual BigInt("2188189693529")
      }
    }
  }
}
