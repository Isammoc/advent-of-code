package aoc2021
import common.AocSpec
import common.algo.integers.Dichotomy
import common.grid.Point

class day17Spec extends AocSpec {
  import Day17Solution._
  val sampleInput: String =
    """target area: x=20..30, y=-10..-5""".stripMargin

  "day17 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 45
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 112
      }
      val ta = sample

      "all initial velocities" in {
        val wanted = Set(
          Point(30, -10),
          Point(29, -10),
          Point(28, -10),
          Point(27, -10),
          Point(26, -10),
          Point(25, -10),
          Point(24, -10),
          Point(23, -10),
          Point(22, -10),
          Point(21, -10),
          Point(20, -10),
          Point(25, -9),
          Point(21, -9),
          Point(29, -9),
          Point(28, -9),
          Point(23, -9),
          Point(26, -9),
          Point(22, -9),
          Point(20, -9),
          Point(27, -9),
          Point(30, -9),
          Point(24, -9),
          Point(29, -8),
          Point(20, -8),
          Point(28, -8),
          Point(26, -8),
          Point(25, -8),
          Point(22, -8),
          Point(23, -8),
          Point(30, -8),
          Point(24, -8),
          Point(21, -8),
          Point(27, -8),
          Point(21, -7),
          Point(27, -7),
          Point(25, -7),
          Point(28, -7),
          Point(20, -7),
          Point(23, -7),
          Point(26, -7),
          Point(29, -7),
          Point(30, -7),
          Point(22, -7),
          Point(24, -7),
          Point(29, -6),
          Point(22, -6),
          Point(26, -6),
          Point(30, -6),
          Point(21, -6),
          Point(25, -6),
          Point(24, -6),
          Point(27, -6),
          Point(28, -6),
          Point(23, -6),
          Point(20, -6),
          Point(27, -5),
          Point(24, -5),
          Point(25, -5),
          Point(20, -5),
          Point(22, -5),
          Point(26, -5),
          Point(23, -5),
          Point(28, -5),
          Point(21, -5),
          Point(29, -5),
          Point(30, -5),
          Point(14, -4),
          Point(15, -4),
          Point(13, -4),
          Point(11, -4),
          Point(12, -4),
          Point(14, -3),
          Point(15, -3),
          Point(12, -3),
          Point(11, -3),
          Point(13, -3),
          Point(11, -2),
          Point(13, -2),
          Point(9, -2),
          Point(15, -2),
          Point(12, -2),
          Point(10, -2),
          Point(14, -2),
          Point(8, -2),
          Point(7, -1),
          Point(8, -1),
          Point(9, -1),
          Point(10, -1),
          Point(11, -1),
          Point(9, 0),
          Point(8, 0),
          Point(6, 0),
          Point(7, 0),
          Point(6, 1),
          Point(7, 1),
          Point(8, 1),
          Point(6, 2),
          Point(7, 2),
          Point(6, 3),
          Point(7, 3),
          Point(6, 4),
          Point(7, 4),
          Point(6, 5),
          Point(7, 5),
          Point(7, 6),
          Point(6, 6),
          Point(6, 7),
          Point(7, 7),
          Point(6, 8),
          Point(7, 8),
          Point(7, 9),
          Point(6, 9)
        )

        val diff = ta.allPossible.diff(wanted)

        diff shouldBe Set.empty
      }

      "findXsForSteps" in {
        ta.findXsForSteps(8) shouldBe Set(6, 7)
      }

      "Simple example" in {
        ta.findStepsForVerticalVelocity(-10) shouldBe List(1)
        ta.findStepsForVerticalVelocity(-4) shouldBe List(2)
        ta.findStepsForVerticalVelocity(-2) shouldBe List(3, 2)
      }

      "finalHorizontalPosition" in {
        ta.finalHorizontalPosition(1, 30) shouldBe 30
      }

      "Dichotomy" in {
        val minX: Int => Boolean = currentVelocity => ta.finalHorizontalPosition(1, currentVelocity) >= ta.minX
        minX(10) shouldBe false
        minX(30) shouldBe true
        Dichotomy(0, ta.maxX * 2, minX) shouldBe 20
        Dichotomy(0, ta.maxX * 2, currentVelocity => ta.finalHorizontalPosition(1, currentVelocity) >= ta.maxX) shouldBe 30
      }
    }
  }
}
