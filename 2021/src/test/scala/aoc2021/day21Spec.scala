package aoc2021

class day21Spec extends common.AocSpec {
  import Day21Solution._

  val sampleInput: String =
    """Player 1 starting position: 4
      |Player 2 starting position: 8
      |""".stripMargin

  "day21 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 739785
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual BigInt("444356092776315")
      }
    }
  }
}
