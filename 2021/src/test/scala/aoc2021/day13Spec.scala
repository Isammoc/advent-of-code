package aoc2021
import common.AocSpec

class day13Spec extends AocSpec {
  import Day13Solution._

  val sampleInput: String =
    """6,10
      |0,14
      |9,10
      |0,3
      |10,4
      |4,11
      |6,0
      |6,12
      |4,1
      |0,13
      |10,12
      |3,4
      |3,0
      |8,4
      |1,10
      |2,14
      |8,10
      |9,0
      |
      |fold along y=7
      |fold along x=5""".stripMargin
  "day13 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 17
      }
    }

    "part2" should {
      "example" in {
        part2(sample).trim shouldEqual """#####
                                         |#   #
                                         |#   #
                                         |#   #
                                         |#####""".stripMargin
      }
    }
  }
}
