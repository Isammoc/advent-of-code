package aoc2021
import common.AocSpec

class day05Spec extends AocSpec {
  import Day05Solution._

  val sampleInput: String =
    """0,9 -> 5,9
      |8,0 -> 0,8
      |9,4 -> 3,4
      |2,2 -> 2,1
      |7,0 -> 7,4
      |6,4 -> 2,0
      |0,9 -> 2,9
      |3,4 -> 1,4
      |0,0 -> 8,8
      |5,5 -> 8,2""".stripMargin

  "day05 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 5
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 12
      }
    }
  }
}
