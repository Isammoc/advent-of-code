package aoc2021
import common.AocSpec

class day07Spec extends AocSpec {
  import Day07Solution._

  val sampleInput: String =
    """16,1,2,0,4,2,7,1,2,14""".stripMargin

  "day07 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 37
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 168
      }
    }
  }
}
