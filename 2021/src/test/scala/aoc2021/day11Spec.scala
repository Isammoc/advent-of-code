package aoc2021
import common.AocSpec

class day11Spec extends AocSpec {
  import Day11Solution._

  val sampleInput: String =
    """5483143223
      |2745854711
      |5264556173
      |6141336146
      |6357385478
      |4167524645
      |2176841721
      |6882881134
      |4846848554
      |5283751526""".stripMargin

  "day11 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 1656
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 195
      }
    }
  }
}
