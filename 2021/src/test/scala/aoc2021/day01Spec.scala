package aoc2021

class day01Spec extends common.AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """199
      |200
      |208
      |210
      |200
      |207
      |240
      |269
      |260
      |263""".stripMargin

  "day01 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 7
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 5
      }
    }
  }
}
