package aoc2021
import common.AocSpec

class day15Spec extends AocSpec {
  import Day15Solution._

  val sampleInput: String =
    """1163751742
      |1381373672
      |2136511328
      |3694931569
      |7463417111
      |1319128137
      |1359912421
      |3125421639
      |1293138521
      |2311944581
      |""".stripMargin

  "day15 2021" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 40
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 315
      }
    }
  }
}
