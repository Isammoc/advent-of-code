package aoc2021

class day23Spec extends common.AocSpec {
  import Day23Solution._

  val sampleInput: String =
    """#############
      |#...........#
      |###B#C#B#D###
      |  #A#D#C#A#
      |  #########""".stripMargin

  "day23 2021" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 12521
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 44169
      }
    }
  }
}
