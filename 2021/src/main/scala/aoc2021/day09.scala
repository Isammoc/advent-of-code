package aoc2021

import common.grid.Point

import scala.annotation.tailrec

object day09 extends common.AocApp(Day09Solution.solution)

object Day09Solution {
  final case class HeightMap(values: Map[Point, Int]) {
    lazy val minX: Int = values.keys.map(_.x).min
    lazy val minY: Int = values.keys.map(_.y).min
    lazy val maxX: Int = values.keys.map(_.x).max
    lazy val maxY: Int = values.keys.map(_.y).max
    def apply: Point => Int = values.apply
    def neighborsValues(p: Point): List[Int] = neighbors(p).flatMap(values.get)
    def neighbors(p: Point): List[Point] = Point.OrthogonalDirections.map(p + _)
    def basin(from: Point): Set[Point] = {
      @tailrec
      def loop(toVisit: List[Point], result: Set[Point]): Set[Point] = toVisit match {
        case Nil                                               => result
        case current :: t if result.contains(current)          => loop(t, result)
        case current :: t if values.getOrElse(current, 9) == 9 => loop(t, result)
        case current :: _                                      => loop(neighbors(current) ++ toVisit, result + current)
      }
      loop(List(from), Set.empty)
    }

    def allPoints: List[Point] =
      (for {
        x <- minX to maxX
        y <- minY to maxY
      } yield Point(x, y)).toList

  }

  type Parsed = HeightMap
  object solution extends common.Solution[Parsed]("2021", "09") {

    def parse(input: String): Parsed = HeightMap((for {
      (line, y) <- input.split("\n").zipWithIndex
      (depth, x) <- line.zipWithIndex
    } yield Point(x, y) -> (depth - '0')).toMap)

    def part1(heightMap: Parsed): Int = (for {
      p <- heightMap.allPoints
      if heightMap.neighborsValues(p).forall(_ > heightMap.apply(p))
    } yield heightMap.apply(p) + 1).sum

    def part2(heightMap: Parsed): Int = {

      @tailrec
      def loop(toVisit: List[Point], basins: List[Int], visited: Set[Point]): Int = toVisit match {
        case Nil                                       => basins.sortBy(-_).take(3).product
        case current :: t if visited.contains(current) => loop(t, basins, visited)
        case current :: t =>
          val basin = heightMap.basin(current)
          loop(t, basin.size :: basins, visited ++ basin)
      }

      loop(heightMap.allPoints, Nil, Set.empty)
    }
  }
}
