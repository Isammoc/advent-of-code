package aoc2021

import common.grid.Point

object day20 extends common.AocApp(Day20Solution.solution)

object Day20Solution {
  object ImageEnhancer {

    def parseRules(str: String): Set[Int] = (for {
      (c, i) <- str.split("\n").mkString("").zipWithIndex
      if c == '#'
    } yield i).toSet

    def parseLitPixels(str: String): Set[Point] = (for {
      (line, y) <- str.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      if c == '#'
    } yield Point(x, y)).toSet

    def parse(str: String): ImageEnhancer = {
      val Array(rules, litPixels) = str.split("\n\n")
      ImageEnhancer(oddTurn = false, litPixels.split("\n").head.length, litPixels.split("\n").length, parseRules(rules), parseLitPixels(litPixels))
    }
  }
  final case class ImageEnhancer(oddTurn: Boolean, width: Int, height: Int, rules: Set[Int], litPixels: Set[Point]) {
    def next: ImageEnhancer = {
      val newLitPixels = for {
        y <- -1 until (height + 1)
        x <- -1 until (width + 1)
        toDecode = Integer.parseInt(
          (for {
            dy <- -1 to 1
            dx <- -1 to 1
          } yield get(Point(x + dx, y + dy))).mkString(""),
          2
        )
        if rules.contains(toDecode)
      } yield Point(x + 1, y + 1)

      ImageEnhancer(!oddTurn, width + 2, height + 2, rules, newLitPixels.toSet)
    }

    def get(p: Point): Char =
      if (p.y < 0 || height <= p.y || p.x < 0 || width <= p.x) {
        if (oddTurn && rules.contains(0))
          '1'
        else
          '0'
      } else if (litPixels.contains(p))
        '1'
      else
        '0'

    override def toString: String = {
      val rules = (0 until 512).map(i => if (this.rules.contains(i)) '#' else '.').mkString("")
      val image =
        (for {
          y <- 0 until height
        } yield (for {
          x <- 0 until width
          p = Point(x, y)
        } yield if (litPixels.contains(p)) '#' else '.').mkString("")).mkString("\n")

      s"$rules\n\n$image"
    }
  }

  type Parsed = ImageEnhancer
  object solution extends common.Solution[Parsed]("2021", "20") {
    override def parse(input: String): Parsed = ImageEnhancer.parse(input)

    override def part1(parsed: Parsed): Int = parsed.next.next.litPixels.size

    override def part2(parsed: Parsed): Int = LazyList.iterate(parsed)(_.next).drop(50).head.litPixels.size
  }
}
