package aoc2021

import aoc2021.Day11Solution.Step.nextWithFlashes
import common.grid.Point

import scala.annotation.tailrec

object day11 extends common.AocApp(Day11Solution.solution)

object Day11Solution {
  object Step {
    def parse(str: String): Step = {
      Step((for {
        (line, y) <- str.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> (c - '0')).toMap)
    }

    def nextWithFlashes(start: Step): (Step, Int) = {
      @tailrec
      def loop(current: Step, flashes: Int): (Step, Int) =
        current.octopuses.find { case (_, v) => v > 9 } match {
          case None => (current, flashes)
          case Some((p, _)) =>
            loop(current.increaseNeighboorNotAlreadyFlashing(p), flashes + 1)
        }

      loop(start.increaseAll, 0)
    }
  }

  final case class Step(octopuses: Map[Point, Int]) {
    def increaseAll: Step = Step(for {
      (p, v) <- octopuses
    } yield p -> (v + 1))

    def increaseNeighboorNotAlreadyFlashing(p: Point): Step = {
      val newlySet = for {
        y <- p.y - 1 to p.y + 1
        x <- p.x - 1 to p.x + 1
        n = Point(x, y) if octopuses.contains(n) && octopuses(n) > 0
      } yield n -> (octopuses(n) + 1)

      Step(octopuses ++ newlySet + (p -> 0))
    }
  }

  type Parsed = Step
  object solution extends common.Solution[Parsed]("2021", "11") {
    def parse(input: String): Parsed = Step.parse(input)

    def part1(input: Parsed): Int =
      LazyList.unfold(input)(step => Some(nextWithFlashes(step).swap)).take(100).sum

    def part2(input: Parsed): Int = LazyList.unfold(input)(step => Some(nextWithFlashes(step).swap)).zipWithIndex.find(_._1 == 100).get._2 + 1
  }
}
