package aoc2021

object day08 extends common.AocApp(Day08Solution.solution)

object Day08Solution {

  object Digit {
    def apply(str: String): Digit = new Digit(str.toSet)
  }
  final case class Digit(segments: Set[Char]) {
    def size: Int = segments.size
    def common(other: Digit): Int = segments.intersect(other.segments).size
    def contains(segment: Char): Boolean = segments.contains(segment)

    override def toString: String = segments.toList.sorted.mkString("")
  }
  object Line {
    def apply(str: String): Line = {
      val Array(analyse, display) = str.split("\\|").map(_.trim)
      Line(analyse.split(" ").map(Digit.apply).toSet, display.split(" ").map(Digit.apply).toList)
    }
  }
  final case class Line(analyse: Set[Digit], display: List[Digit]) {
    lazy val correspondences: Map[Digit, Int] = {
      val one = analyse.find(_.size == 2).get
      val four = analyse.find(_.size == 4).get
      val seven = analyse.find(_.size == 3).get
      val eight = analyse.find(_.size == 7).get

      val fiveSegments = analyse.filter(_.size == 5)
      val three = fiveSegments.find(t => one.common(t) == 2).get

      val segmentE = analyse.flatMap(_.segments).find(c => analyse.count(_.contains(c)) == 4).get

      val two = (fiveSegments - three).find(_.contains(segmentE)).get
      val five = (fiveSegments - three - two).head

      val sixSegments = analyse.filter(_.size == 6)

      val nine = sixSegments.find(!_.contains(segmentE)).get
      val six = (sixSegments - nine).find(_.common(one) == 1).get

      val zero = (sixSegments - nine - six).head

      Map(
        zero -> 0,
        one -> 1,
        two -> 2,
        three -> 3,
        four -> 4,
        five -> 5,
        six -> 6,
        seven -> 7,
        eight -> 8,
        nine -> 9
      )
    }

    lazy val valueDisplayed: Int = found.mkString("").toInt

    lazy val found: List[Int] = display.map(correspondences.apply)
  }

  type Parsed = Seq[Line]
  object solution extends common.Solution[Parsed]("2021", "08") {
    def parse(str: String): Parsed = str.split("\n").map(Line.apply).toList

    def part1(lines: Parsed): Int = {
      val possible = Set(1, 4, 7, 8)

      (for {
        line <- lines
        digit <- line.found
        if possible.contains(digit)
      } yield 1).sum
    }

    def part2(lines: Parsed): Int = (for {
      line <- lines
    } yield line.valueDisplayed).sum
  }
}
