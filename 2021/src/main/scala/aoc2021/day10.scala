package aoc2021

import scala.annotation.tailrec

object day10 extends common.AocApp(Day10Solution.solution)

object Day10Solution {
  type Parsed = Array[List[Char]]
  object solution extends common.Solution[Parsed]("2021", "10") {
    def parse(input: String): Parsed = input.split("\n").map(_.toList)

    def part1(input: Parsed): Int = {

      @tailrec
      def loop(remain: List[Char], stack: List[Char]): Int = (remain, stack) match {
        case (Nil, _)                   => 0
        case ('(' :: t, _)              => loop(t, ')' :: stack)
        case ('{' :: t, _)              => loop(t, '}' :: stack)
        case ('[' :: t, _)              => loop(t, ']' :: stack)
        case ('<' :: t, _)              => loop(t, '>' :: stack)
        case (a :: t, b :: s) if a == b => loop(t, s)

        case (')' :: _, _) => 3
        case (']' :: _, _) => 57
        case ('}' :: _, _) => 1197
        case ('>' :: _, _) => 25137
      }

      (for {
        line <- input
      } yield loop(line, Nil)).sum
    }

    def part2(input: Parsed): BigInt = {

      def score(stack: List[Char]): BigInt =
        stack
          .map {
            case ')' => 1
            case ']' => 2
            case '}' => 3
            case '>' => 4
          }
          .foldLeft(BigInt(0))((prev, current) => prev * 5 + current)

      @tailrec
      def loop(remain: List[Char], stack: List[Char]): BigInt = (remain, stack) match {
        case (Nil, _)                   => score(stack)
        case ('(' :: t, _)              => loop(t, ')' :: stack)
        case ('{' :: t, _)              => loop(t, '}' :: stack)
        case ('[' :: t, _)              => loop(t, ']' :: stack)
        case ('<' :: t, _)              => loop(t, '>' :: stack)
        case (a :: t, b :: s) if a == b => loop(t, s)
        case _                          => 0
      }

      val scores = (for {
        line <- input
      } yield loop(line, Nil)).filterNot(_ == 0)
      scores.sorted.drop(scores.length / 2).head
    }
  }
}
