package aoc2021

object day07 extends common.AocApp(Day07Solution.solution)

object Day07Solution {
  def median(values: List[Int]): Int = {
    val sorted = values.sorted
    if (values.size % 2 == 0) {
      (sorted.drop(values.size / 2).head + sorted.drop(values.size / 2 - 1).head) / 2
    } else {
      values.drop(values.size / 2).head
    }
  }

  type Parsed = List[Int]
  object solution extends common.Solution[Parsed]("2021", "07") {
    def parse(input: String): Parsed = input.split(",").map(_.toInt).toList

    def part1(crabs: Parsed): Int = {
      val crabsMedian = median(crabs)

      crabs.map(i => (i - crabsMedian).abs).sum
    }

    def part2(crabs: Parsed): Int = {
      def distance(ref: Int)(current: Int) = ((current - ref).abs * ((current - ref).abs + 1)) / 2

      (for {
        toConsider <- crabs.min to crabs.max
      } yield crabs.map(distance(toConsider)).sum).min
    }
  }
}
