package aoc2021

object day13 extends common.AocApp(Day13Solution.solution)

object Day13Solution {
  final case class Point(x: Int, y: Int) {
    def applyFold(fold: Fold): Point = fold.axis match {
      case "x" =>
        if (x > fold.value) Point(2 * fold.value - x, y)
        else this
      case "y" =>
        if (y > fold.value) Point(x, 2 * fold.value - y)
        else this
    }
  }
  final case class Fold(axis: String, value: Int)
  object Transparent {
    def parsePoint(str: String): Point = {
      val Array(x, y) = str.split(",")
      Point(x.toInt, y.toInt)
    }
    def parseFold(str: String): Fold = {
      val Array(axis, value) = str.split(" ").last.split("=")
      Fold(axis, value.toInt)
    }
    def parse(str: String): Transparent = {
      val Array(points, folds) = str.split("\n\n")
      Transparent(points.split("\n").map(parsePoint).toSet, folds.split("\n").map(parseFold).toList)
    }
  }
  final case class Transparent(points: Set[Point], folds: List[Fold]) {
    def next: Transparent = folds match {
      case Nil    => this
      case f :: t => Transparent(points.map(_.applyFold(f)), t)
    }
    def foldAll: Transparent = LazyList.iterate(this)(_.next).dropWhile(_.folds.nonEmpty).head

    override def toString: String = {
      val minX = points.map(_.x).min
      val maxX = points.map(_.x).max
      val minY = points.map(_.y).min
      val maxY = points.map(_.y).max

      (for {
        y <- minY to maxY
      } yield {
        (for {
          x <- minX to maxX
        } yield if (points.contains(Point(x, y))) "#" else " ").mkString("")
      }).mkString("\n")
    }
  }

  type Parsed = Transparent
  object solution extends common.Solution[Parsed]("2021", "13") {
    def parse(input: String): Parsed = Transparent.parse(input)

    def part1(input: Parsed): Int = input.next.points.size

    def part2(input: Parsed): String = input.foldAll.toString
  }
}
