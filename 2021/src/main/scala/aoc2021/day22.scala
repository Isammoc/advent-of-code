package aoc2021

object day22 extends common.AocApp(Day22Solution.solution)

object Day22Solution {
  final case class Point(x: BigInt, y: BigInt, z: BigInt)

  object Instruction {
    def parse(str: String): Instruction = {
      val Array(on, coordinates) = str.split(" ")
      Instruction(on == "on", Cuboid.parse(coordinates))
    }
  }
  final case class Instruction(on: Boolean, cuboid: Cuboid) {
    def intersection(cuboid: Cuboid): Option[Instruction] = for {
      inter <- cuboid.intersection(this.cuboid)
    } yield Instruction(on, inter)
  }

  object Segment {
    def parse(str: String): Segment = {
      val Array(min, max) = str.split("\\.\\.").map(BigInt.apply)
      Segment(min, max)
    }
  }
  final case class Segment(min: BigInt, max: BigInt) {
    def intersection(other: Segment): Option[Segment] = {
      val min = Seq(this.min, other.min).max
      val max = Seq(this.max, other.max).min
      if (min <= max) Some(Segment(min, max)) else None
    }
    def size: BigInt = (max - min) + 1
  }

  object Cuboid {
    def parse(str: String): Cuboid = {
      val Array(x, y, z) = str.split(",").map(_.split("=")(1))
      Cuboid(Segment.parse(x), Segment.parse(y), Segment.parse(z))
    }
  }
  final case class Cuboid(x: Segment, y: Segment, z: Segment) {
    def intersection(other: Cuboid): Option[Cuboid] = for {
      xSegment <- x.intersection(other.x)
      ySegment <- y.intersection(other.y)
      zSegment <- z.intersection(other.z)
    } yield Cuboid(xSegment, ySegment, zSegment)

    def size: BigInt = x.size * y.size * z.size
  }

  object World {
    val Origin: World = World(Nil, Nil)
  }
  case class World(on: List[Cuboid], off: List[Cuboid]) {
    def setOff(cuboid: Cuboid): World = {
      val newOn = for {
        oneOff <- off
        toOn <- oneOff.intersection(cuboid)
      } yield toOn

      val newOff = for {
        oneOn <- on
        toOff <- oneOn.intersection(cuboid)
      } yield toOff

      World(newOn ++ on, newOff ++ off)
    }

    def setOn(cuboid: Cuboid): World = {
      def worldOff = setOff(cuboid)
      World(cuboid :: worldOff.on, worldOff.off)
    }

    def applyInstruction(instruction: Instruction): World =
      if (instruction.on)
        setOn(instruction.cuboid)
      else
        setOff(instruction.cuboid)

    def size: BigInt = on.map(_.size).sum - off.map(_.size).sum
  }

  type Parsed = Iterable[Instruction]

  object solution extends common.Solution[Parsed]("2021", "22") {
    override def parse(input: String): Parsed =
      input.split("\n").map(Instruction.parse)

    override def part1(parsed: Parsed): Any =
      parsed.flatMap(_.intersection(Cuboid(Segment(-50, 50), Segment(-50, 50), Segment(-50, 50)))).foldLeft(World.Origin)(_.applyInstruction(_)).size

    override def part2(parsed: Parsed): Any = parsed.foldLeft(World.Origin)(_.applyInstruction(_)).size
  }
}
