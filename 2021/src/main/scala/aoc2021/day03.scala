package aoc2021

import scala.annotation.tailrec

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {
  type Parsed = List[String]
  object solution extends common.Solution[Parsed]("2021", "03") {
    def parse(input: String): Parsed = input.split("\n").toList

    def part1(parsed: Parsed): BigInt = {
      val transposed = parsed.map(_.toList).transpose
      val gamma = BigInt(
        (for {
          column <- transposed
        } yield {
          column.groupBy(identity).maxBy(_._2.size)._1
        }).mkString(""),
        2
      )

      val epsilon = BigInt(
        (for {
          column <- transposed
        } yield {
          column.groupBy(identity).minBy(_._2.size)._1
        }).mkString(""),
        2
      )

      gamma * epsilon
    }

    def part2(parsed: Parsed): BigInt = {
      @tailrec
      def oxygenLoop(remain: List[String], index: Int): BigInt =
        if (remain.size == 1) BigInt(remain.head, 2)
        else {
          val byBit = remain.groupBy(_.charAt(index))
          oxygenLoop(
            if (byBit('0').size == byBit('1').size) {
              byBit('1')
            } else if (byBit('0').size > byBit('1').size) {
              byBit('0')
            } else {
              byBit('1')
            },
            index + 1
          )
        }

      @tailrec
      def co2ScrubberLoop(remain: List[String], index: Int): BigInt =
        if (remain.size == 1) BigInt(remain.head, 2)
        else {
          val byBit = remain.groupBy(_.charAt(index))
          co2ScrubberLoop(
            if (byBit('0').size == byBit('1').size) {
              byBit('0')
            } else if (byBit('0').size > byBit('1').size) {
              byBit('1')
            } else {
              byBit('0')
            },
            index + 1
          )
        }

      oxygenLoop(parsed, 0) * co2ScrubberLoop(parsed, 0)
    }
  }
}
