package aoc2021

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  sealed trait Command
  final case class Forward(value: Int) extends Command
  final case class Down(value: Int) extends Command
  final case class Up(value: Int) extends Command

  final case class Point(x: BigInt, y: BigInt) {
    def execute(cmd: Command): Point = cmd match {
      case Forward(v) => this.copy(x = x + v)
      case Down(v)    => this.copy(y = y + v)
      case Up(v)      => this.copy(y = y - v)
    }
    def result: BigInt = x * y
  }

  final case class AimPoint(x: BigInt, y: BigInt, aim: BigInt) {
    def execute(cmd: Command): AimPoint = cmd match {
      case Forward(value) => this.copy(x = x + value, y = y + value * aim)
      case Down(value)    => this.copy(aim = aim + value)
      case Up(value)      => this.copy(aim = aim - value)
    }

    def result: BigInt = x * y
  }

  type Parsed = List[Command]

  object solution extends common.Solution[Parsed]("2021", "02") {
    def parse(input: String): Parsed = {
      val ForwardR = "forward (\\d+)".r
      val DownR = "down (\\d+)".r
      val UpR = "up (\\d+)".r

      input
        .split("\n")
        .map {
          case ForwardR(v) => Forward(v.toInt)
          case DownR(v)    => Down(v.toInt)
          case UpR(v)      => Up(v.toInt)
        }
        .toList
    }

    def part1(input: Parsed): BigInt = input.foldLeft(Point(0, 0))(_.execute(_)).result

    def part2(input: Parsed): BigInt = input.foldLeft(AimPoint(0, 0, 0))(_.execute(_)).result
  }
}
