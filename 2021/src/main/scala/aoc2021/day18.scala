package aoc2021

import scala.annotation.tailrec

object day18 extends common.AocApp(Day18Solution.solution)

object Day18Solution {
  sealed trait SnailFishNumber {
    def explode: SnailFishNumber = this.explode(0) match {
      case Left(result)          => result
      case Right((_, result, _)) => result
    }
    def explode(deep: Int): Either[SnailFishNumber, (Int, SnailFishNumber, Int)]
    def addLeft(value: Int): SnailFishNumber
    def addRight(value: Int): SnailFishNumber
    def split: SnailFishNumber
    def reduce: SnailFishNumber = SnailFishNumber.reduce(this)
    def magnitude: BigInt
  }

  object SnailFishNumber {
    final case class Single(value: Int) extends SnailFishNumber {
      override def explode(deep: Int): Either[SnailFishNumber, (Int, SnailFishNumber, Int)] = Left(this)
      override def addLeft(value: Int): SnailFishNumber = Single(this.value + value)
      override def addRight(value: Int): SnailFishNumber = Single(this.value + value)
      override def toString: String = value.toString

      override def split: SnailFishNumber = if (value >= 10) Pair(Single(value / 2), Single((value + 1) / 2)) else this

      override def magnitude: BigInt = value
    }
    final case class Pair(left: SnailFishNumber, right: SnailFishNumber) extends SnailFishNumber {
      override def explode(deep: Int): Either[SnailFishNumber, (Int, SnailFishNumber, Int)] =
        (left, right) match {
          case (Single(leftValue), Single(rightValue)) if deep > 3 =>
            Right((leftValue, Single(0), rightValue))
          case _ =>
            left.explode(deep + 1) match {
              case Left(newLeft) =>
                right.explode(deep + 1) match {
                  case Left(newRight) =>
                    Left(Pair(newLeft, newRight)) // no explosion here
                  case Right((leftValue, newRight, rightValue)) =>
                    Right((0, Pair(newLeft.addRight(leftValue), newRight), rightValue))
                }
              case Right((leftValue, newLeft, rightValue)) =>
                Right((leftValue, Pair(newLeft, right.addLeft(rightValue)), 0))
            }
        }
      override def addLeft(value: Int): SnailFishNumber = if (value == 0) this else Pair(left.addLeft(value), right)
      override def addRight(value: Int): SnailFishNumber = if (value == 0) this else Pair(left, right.addRight(value))

      override def toString: String = s"[$left,$right]"

      override def split: SnailFishNumber = {
        val newLeft = left.split
        if (newLeft == left) {
          Pair(newLeft, right.split)
        } else {
          Pair(newLeft, right)
        }
      }

      override def magnitude: BigInt = left.magnitude * 3 + right.magnitude * 2
    }

    def parseSingle(input: List[Char]): (List[Char], Single) = (input.tail, Single(input.head - '0'))

    def parsePair(input: List[Char]): (List[Char], Pair) = {
      val (afterLeft, left) = parse(input)
      assert(afterLeft.head == ',')
      val (afterRight, right) = parse(afterLeft.tail)
      assert(afterRight.head == ']')
      (afterRight.tail, Pair(left, right))
    }

    def parse(input: List[Char]): (List[Char], SnailFishNumber) = input match {
      case '[' :: t => parsePair(t)
      case _        => parseSingle(input)
    }

    def parse(str: String): SnailFishNumber = parse(str.toList)._2

    @tailrec
    def reduce(number: SnailFishNumber): SnailFishNumber = {
      val exploded = number.explode
      if (exploded == number) {
        val split = exploded.split
        if (split == exploded) {
          split
        } else {
          reduce(split)
        }
      } else {
        reduce(exploded)
      }
    }

    def plus(leftSide: SnailFishNumber, rightSide: SnailFishNumber): SnailFishNumber = Pair(leftSide, rightSide).reduce
  }

  type Parsed = Array[SnailFishNumber]
  object solution extends common.Solution[Parsed]("2021", "18") {
    override def parse(input: String): Parsed = input.split("\n").map(SnailFishNumber.parse)

    def part1(input: Parsed): BigInt = input.reduceLeft(SnailFishNumber.plus).magnitude

    def part2(input: Parsed): BigInt = {
      val list = input

      (for {
        a <- list
        b <- list
        if a != b
      } yield SnailFishNumber.plus(a, b).magnitude).max
    }
  }
}
