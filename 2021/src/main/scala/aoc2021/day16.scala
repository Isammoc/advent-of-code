package aoc2021

import scala.annotation.tailrec

object day16 extends common.AocApp(Day16Solution.solution)

object Day16Solution {

  object Binary {
    final case object One extends Binary
    final case object Zero extends Binary

    def toBigInt(lazyList: LazyList[Binary]): BigInt = {
      @tailrec
      def loop(current: BigInt, remain: LazyList[Binary]): BigInt = remain match {
        case LazyList() => current
        case Zero #:: t => loop(current * 2, t)
        case One #:: t  => loop(current * 2 + 1, t)
      }
      loop(0, lazyList)
    }

    def from(c: Char): Seq[Binary] = {
      val binaryString = BigInt(c.toString, 16).toString(2)
      ("0" * (4 - binaryString.length) + binaryString).map {
        case '0' => Zero
        case '1' => One
      }
    }
  }

  sealed trait Binary

  sealed trait Packet {
    def version: BigInt
    def versionSum: BigInt
    def value: BigInt
  }

  object Packet {
    final case class Operation(version: BigInt, typeId: BigInt, subPackets: List[Packet]) extends Packet {
      override def versionSum: BigInt = version + subPackets.map(_.versionSum).sum

      override def value: BigInt = typeId.toInt match {
        case 0 => subPackets.map(_.value).sum
        case 1 => subPackets.map(_.value).product
        case 2 => subPackets.map(_.value).min
        case 3 => subPackets.map(_.value).max
        case 5 =>
          val List(a, b) = subPackets
          if (a.value > b.value) 1 else 0
        case 6 =>
          val List(a, b) = subPackets
          if (a.value < b.value) 1 else 0
        case 7 =>
          val List(a, b) = subPackets
          if (a.value == b.value) 1 else 0
      }
    }

    object Operation {
      def readNPackets(n: BigInt, input: LazyList[Binary]): (LazyList[Binary], List[Packet]) = {
        @tailrec
        def loop(remain: LazyList[Binary], current: List[Packet]): (LazyList[Binary], List[Packet]) = {
          if (current.size == n) remain -> current.reverse
          else {
            val (nextRemain, nextPacket) = Packet.from(remain)
            loop(nextRemain, nextPacket :: current)
          }
        }
        loop(input, Nil)
      }
      def readNBits(n: BigInt, input: LazyList[Binary]): (LazyList[Binary], List[Packet]) = {
        val finalRemain = input.drop(n.toInt)
        @tailrec
        def loop(remain: LazyList[Binary], current: List[Packet]): (LazyList[Binary], List[Packet]) = {
          if (remain == finalRemain) finalRemain -> current.reverse
          else {
            val (nextRemain, nextPacket) = Packet.from(remain)
            loop(nextRemain, nextPacket :: current)
          }
        }
        loop(input, Nil)
      }

      def read(version: BigInt, typeId: BigInt, input: LazyList[Binary]): (LazyList[Binary], Operation) = {
        input.head match {
          case Binary.Zero =>
            val (remain, packets) = readNBits(Binary.toBigInt(input.tail.take(15)), input.tail.drop(15))
            remain -> Operation(version, typeId, packets)
          case Binary.One =>
            val (remain, packets) = readNPackets(Binary.toBigInt(input.tail.take(11)), input.tail.drop(11))
            remain -> Operation(version, typeId, packets)
        }
      }
    }

    final case class Literal(version: BigInt, value: BigInt) extends Packet {
      override def versionSum: BigInt = version
    }

    object Literal {
      def read(version: BigInt, input: LazyList[Binary]): (LazyList[Binary], Literal) = {
        @tailrec
        def loop(current: BigInt, remain: LazyList[Binary]): (LazyList[Binary], Literal) = remain match {
          case Binary.One #:: t  => loop(current * 16 + Binary.toBigInt(t.take(4)), t.drop(4))
          case Binary.Zero #:: t => t.drop(4) -> Literal(version, current * 16 + Binary.toBigInt(t.take(4)))
        }

        loop(0, input)
      }
    }
    val LiteralValue = BigInt(4)

    def from(remaining: LazyList[Binary]): (LazyList[Binary], Packet) = {
      val version = Binary.toBigInt(remaining.take(3))
      val typeId = Binary.toBigInt(remaining.slice(3, 6))
      typeId match {
        case LiteralValue => Literal.read(version, remaining.drop(6))
        case _            => Operation.read(version, typeId, remaining.drop(6))
      }
    }

    def parse(str: String): Packet = from(for {
      c <- LazyList.from(str)
      b <- Binary.from(c)
    } yield b)._2
  }

  type Parsed = Packet
  object solution extends common.Solution[Parsed]("2021", "16") {
    override def parse(input: String): Parsed = Packet.parse(input)

    def part1(input: Parsed): BigInt = input.versionSum

    def part2(input: Parsed): BigInt = input.value
  }
}
