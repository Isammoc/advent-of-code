package aoc2021

import common.algo.graph.Dijkstra
import common.grid.Point

object day15 extends common.AocApp(Day15Solution.solution)

object Day15Solution {
  object World {
    def parse(str: String): World = World(
      (for {
        (line, y) <- str.split("\n").zipWithIndex
        (c, x) <- line.zipWithIndex
      } yield Point(x, y) -> (c - '0')).toMap
    )
  }
  final case class World(costs: Map[Point, Int]) {
    val start: Point = Point(0, 0)
    val target: Point = {
      val maxX = costs.keys.map(_.x).max
      val maxY = costs.keys.map(_.y).max
      Point(maxX, maxY)
    }
    def neighbors(p: Point): List[(Point, Int)] =
      for {
        dir <- Point.OrthogonalDirections
        n = p + dir
        cost <- costs.get(n)
      } yield n -> cost
    def reachEnd: Int = Dijkstra.reach(start)(target)(this.neighbors)
    val width: Int = target.x + 1
    val height: Int = target.y + 1
  }

  final case class World2(world: World) {
    val start: Point = Point(0, 0)
    val target: Point = Point(world.width * 5 - 1, world.height * 5 - 1)
    def cost(p: Point): Option[Int] = if (0 <= p.x && p.x <= target.x && 0 <= p.y && p.y <= target.y) {
      val inc = p.x / world.width + p.y / world.height
      world.costs.get(Point(p.x % world.width, p.y % world.height)).map(cost => (cost + inc - 1) % 9 + 1)
    } else None
    def neighbors(p: Point): List[(Point, Int)] = {
      for {
        dir <- Point.OrthogonalDirections
        n = p + dir
        c <- cost(n)
      } yield n -> c
    }
    def reachEnd: Int = Dijkstra.reach(start)(target)(this.neighbors)
  }

  type Parsed = World
  object solution extends common.Solution[Parsed]("2021", "15") {
    def parse(input: String): Parsed = World.parse(input)

    def part1(input: Parsed): Int = input.reachEnd

    def part2(input: Parsed): Int = World2(input).reachEnd
  }
}
