package aoc2021

import common.grid.Point

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  final case class Line(one: Point, other: Point) {
    lazy val isVertical: Boolean = one.y == other.y
    lazy val isHorizontal: Boolean = one.x == other.x
    lazy val isFirstDiagonal: Boolean = (one.x - other.x) == (one.y - other.y)
    lazy val isSecondDiagonal: Boolean = (one.x - other.x) == -(one.y - other.y)
  }

  type Parsed = List[Line]
  object solution extends common.Solution[Parsed]("2021", "05") {
    def parse(str: String): List[Line] = {
      val LineR = "(\\d+),(\\d+)\\s+->\\s+(\\d+),(\\d+)".r
      str.split("\n").toList.map { case LineR(ax, ay, bx, by) =>
        Line(Point(ax.toInt, ay.toInt), Point(bx.toInt, by.toInt))
      }
    }

    def part1(allLines: Parsed): Int = {
      val verticalPoints = for {
        Line(Point(x1, y), Point(x2, _)) <- allLines.filter(_.isVertical)
        x <- math.min(x1, x2) to math.max(x1, x2)
      } yield Point(x, y)

      val horizontalPoints = for {
        Line(Point(x, y1), Point(_, y2)) <- allLines.filter(_.isHorizontal).filterNot(_.isVertical)
        y <- math.min(y1, y2) to math.max(y1, y2)
      } yield Point(x, y)

      val intersectionsByPoint = (verticalPoints ++ horizontalPoints).groupMapReduce(identity)(_ => 1)(_ + _)
      intersectionsByPoint.count(_._2 > 1)
    }

    def part2(allLines: Parsed): Int = {
      val verticalPoints = for {
        Line(Point(x1, y), Point(x2, _)) <- allLines.filter(_.isVertical)
        x <- math.min(x1, x2) to math.max(x1, x2)
      } yield Point(x, y)

      val horizontalPoints = for {
        Line(Point(x, y1), Point(_, y2)) <- allLines.filter(_.isHorizontal).filterNot(_.isVertical)
        y <- math.min(y1, y2) to math.max(y1, y2)
      } yield Point(x, y)

      val firstDiagonalPoints = for {
        Line(Point(x1, y1), Point(x2, _)) <- allLines.filter(_.isFirstDiagonal).filterNot(_.isVertical).filterNot(_.isHorizontal)
        d <- 0 to (x2 - x1) by (if (x2 > x1) 1 else -1)
      } yield Point(x1 + d, y1 + d)

      val secondDiagonalPoints = for {
        Line(Point(x1, y1), Point(x2, _)) <- allLines.filter(_.isSecondDiagonal).filterNot(_.isVertical).filterNot(_.isHorizontal).filterNot(_.isFirstDiagonal)
        d <- 0 to (x2 - x1) by (if (x2 > x1) 1 else -1)
      } yield Point(x1 + d, y1 - d)

      val intersectionsByPoint = (verticalPoints ++ horizontalPoints ++ firstDiagonalPoints ++ secondDiagonalPoints).groupMapReduce(identity)(_ => 1)(_ + _)
      intersectionsByPoint.count(_._2 > 1)
    }
  }
}
