package aoc2021

import scala.annotation.tailrec

object day12 extends common.AocApp(Day12Solution.solution)

object Day12Solution {
  object Graph {
    def parse(str: String): Graph =
      Graph((for {
        line <- str.split("\n")
        Array(a, b) = line.split("-")
        pair <- List(a -> b, b -> a)
      } yield pair).groupMapReduce(_._1)(e => Set(e._2))(_ ++ _))
  }

  object Node {
    def start(twoSmallCaves: Int): Node = Node("start", twoSmallCaves, Set.empty)
  }

  final case class Node(current: String, twoSmallCaves: Int, visited: Set[String]) {
    def accept(n: String): Option[Node] =
      if (n.toLowerCase != n)
        Some(Node(n, twoSmallCaves, visited + n))
      else if (n == "start")
        None
      else if (visited.contains(n)) {
        if (twoSmallCaves > 0) {
          Some(Node(n, twoSmallCaves - 1, visited))
        } else
          None
      } else
        Some(Node(n, twoSmallCaves, visited + n))
  }

  final case class Graph(neighbors: Map[String, Set[String]]) {
    def paths(initial: Node): Int = {
      @tailrec
      def loop(toVisit: List[Node], paths: Int): Int = toVisit match {
        case Nil                    => paths
        case Node("end", _, _) :: t => loop(t, paths + 1)
        case node :: t =>
          val next = for {
            n <- neighbors(node.current)
            nextNode <- node.accept(n)
          } yield nextNode

          loop(t.prependedAll(next), paths)
      }
      loop(List(initial), 0)
    }
  }

  type Parsed = Graph
  object solution extends common.Solution[Parsed]("2021", "12") {
    def parse(input: String): Parsed = Graph.parse(input)

    def part1(input: Parsed): Int = input.paths(Node.start(0))

    def part2(input: Parsed): Int = input.paths(Node.start(1))
  }

}
