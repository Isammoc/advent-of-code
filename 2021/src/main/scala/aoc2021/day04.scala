package aoc2021

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {
  case class Board(values: Array[Array[Int]]) {
    def when(numbers: List[Int]): Int = {
      val whenMarked = for {
        line <- values
      } yield for {
        v <- line
      } yield {
        val res = numbers.indexOf(v)
        if (res == -1) Int.MaxValue
        else res
      }

      val lineFinished = (for {
        line <- whenMarked
      } yield line.max).min

      val columnFinished = (for {
        column <- whenMarked.transpose
      } yield column.max).min

      math.min(lineFinished, columnFinished)
    }

    override def toString: String = values.map(_.mkString("\t")).mkString("\n")

    def toFullBoard(numbers: List[Int]): FullBoard = FullBoard(
      for {
        line <- values
      } yield for {
        v <- line
      } yield {
        val res = numbers.indexOf(v)
        if (res == -1) (v, Int.MaxValue)
        else (v, res)
      }
    )

  }

  case class Input(drawnNumbers: List[Int], boards: List[Board]) {
    def toFullBoards: List[FullBoard] = boards.map(_.toFullBoard(drawnNumbers))

  }

  case class FullBoard(values: Array[Array[(Int, Int)]]) {
    lazy val when: Int = {
      val lineFinished = (for {
        line <- values
      } yield line.map(_._2).max).min

      val columnFinished = (for {
        column <- values.transpose
      } yield column.map(_._2).max).min

      math.min(lineFinished, columnFinished)
    }

    def score(called: Int): Int = called * (for {
      line <- values
      (v, w) <- line if w > this.when
    } yield v).sum
  }

  type Parsed = Input

  object solution extends common.Solution[Parsed]("2021", "04") {
    def parse(str: String): Parsed = {
      val numbers :: boards = str.split("\\n\\n").toList

      val drawnNumbers = numbers.split(",").toList.map(_.toInt)

      Input(drawnNumbers, boards.map(b => Board(b.split("\\n").map(_.split(" ").filterNot(_.isEmpty).map(_.toInt)))))
    }

    def part1(parsedInput: Parsed): Int = {
      val fullBoards = parsedInput.toFullBoards

      val winningBoard = fullBoards.minBy(_.when)

      winningBoard.score(parsedInput.drawnNumbers(winningBoard.when))
    }

    def part2(parsedInput: Parsed): Int = {
      val fullBoards = parsedInput.toFullBoards

      val winningBoard = fullBoards.maxBy(_.when)

      winningBoard.score(parsedInput.drawnNumbers(winningBoard.when))
    }
  }
}
