package aoc2021

import common.algo.graph.Dijkstra

object day23 extends common.AocApp(Day23Solution.solution)

object Day23Solution {
  /*
#############
#ab.c.d.e.fg#
###h#j#l#n###
  #i#k#m#o#
  #########
   */
  object Point {
    val A = 1
    val B = 2
    val C = 4
    val D = 6
    val E = 8
    val F = 10
    val G = 11
  }

  type Distance = Int
  type Cost = Int

  object AmphipodKind {
    def parse(c: Char): AmphipodKind = c match {
      case 'A' => Amber
      case 'B' => Bronze
      case 'C' => Copper
      case 'D' => Desert
    }
  }
  sealed trait AmphipodKind
  case object Amber extends AmphipodKind
  case object Bronze extends AmphipodKind
  case object Copper extends AmphipodKind
  case object Desert extends AmphipodKind

  final case class Room(endingFor: AmphipodKind, inside: List[AmphipodKind]) {
    lazy val endingState: Boolean = inside.forall(_ == endingFor)
    def pop(finalSize: Int): Option[(Room, AmphipodKind, Distance)] =
      if (!endingState && inside.nonEmpty)
        Some((this.copy(inside = inside.tail), inside.head, finalSize - inside.tail.size))
      else
        None
    def push(finalSize: Int, amphipodKind: AmphipodKind): Option[(Room, Distance)] =
      if (endingState && amphipodKind == endingFor)
        Some((this.copy(inside = amphipodKind :: inside), finalSize - inside.size))
      else
        None
  }

  object World {
    val RoomsPlaces = Set(3, 5, 7, 9)
    val RoomPositionByKind: Map[AmphipodKind, Int] = Map(
      Amber -> 3,
      Bronze -> 5,
      Copper -> 7,
      Desert -> 9
    )
    val HallwayPossibleSpots = Set(1, 2, 4, 6, 8, 10, 11)
    val EnergyByKind: Map[AmphipodKind, Cost] =
      Map(
        Amber -> 1,
        Bronze -> 10,
        Copper -> 100,
        Desert -> 1000
      )

    def parse(str: String): World = {
      val lines = str.split("\n")
      World(
        Map.empty,
        Map(
          3 -> Room(Amber, List(AmphipodKind.parse(lines(2)(3)), AmphipodKind.parse(lines(3)(3)))),
          5 -> Room(Bronze, List(AmphipodKind.parse(lines(2)(5)), AmphipodKind.parse(lines(3)(5)))),
          7 -> Room(Copper, List(AmphipodKind.parse(lines(2)(7)), AmphipodKind.parse(lines(3)(7)))),
          9 -> Room(Desert, List(AmphipodKind.parse(lines(2)(9)), AmphipodKind.parse(lines(3)(9))))
        ),
        2
      )
    }
  }

  final case class World(hallwaySpots: Map[Int, AmphipodKind], rooms: Map[Int, Room], finalSize: Int) {
    import World._

    def canGo(from: Int, to: Int): Boolean =
      (to until from by (from - to).sign).forall(p => !hallwaySpots.contains(p))

    def down: Option[(World, Cost)] = (for {
      (currentPos, kind) <- hallwaySpots
      endingRoomPos = RoomPositionByKind(kind)
      if rooms(endingRoomPos).endingState && canGo(currentPos, endingRoomPos)
      (newRoom, distanceInRoom) <- rooms(endingRoomPos).push(finalSize, kind)
    } yield this.copy(
      hallwaySpots = hallwaySpots - currentPos,
      rooms = rooms + (endingRoomPos -> newRoom)
    ) -> ((distanceInRoom + (currentPos - endingRoomPos).abs) * EnergyByKind(kind))).headOption

    def up: List[(World, Cost)] = for {
      (currentRoomPos, room) <- rooms.toList
      if !room.endingState
      newPos <- HallwayPossibleSpots
      (newRoom, kind, dist) <- room.pop(finalSize)
      if canGo(currentRoomPos, newPos)
    } yield this.copy(
      hallwaySpots = hallwaySpots + (newPos -> kind),
      rooms + (currentRoomPos -> newRoom)
    ) -> ((dist + (currentRoomPos - newPos).abs) * EnergyByKind(kind))

    def next: Seq[(World, Cost)] = down.map(c => List(c)).getOrElse(up)

    def transformFor2: World =
      World(
        Map.empty,
        Map(
          3 -> rooms(3).copy(inside = List(rooms(3).inside.head, Desert, Desert, rooms(3).inside(1))),
          5 -> rooms(5).copy(inside = List(rooms(5).inside.head, Copper, Bronze, rooms(5).inside(1))),
          7 -> rooms(7).copy(inside = List(rooms(7).inside.head, Bronze, Amber, rooms(7).inside(1))),
          9 -> rooms(9).copy(inside = List(rooms(9).inside.head, Amber, Copper, rooms(9).inside(1)))
        ),
        4
      )

    final val isEnd = rooms.view.values.forall(room => room.endingState && room.inside.size == finalSize)
  }

  type Parsed = World

  object solution extends common.Solution[Parsed]("2021", "23") {
    override def parse(input: String): Parsed = World.parse(input)

    override def part1(parsed: Parsed): Any = Dijkstra.reach(parsed, (w: World) => w.next, (w: World) => w.isEnd)

    override def part2(parsed: Parsed): Any = Dijkstra.reach(parsed.transformFor2, (w: World) => w.next, (w: World) => w.isEnd)
  }
}
