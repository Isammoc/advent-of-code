package aoc2021

import scala.collection.immutable.Queue

object day21 extends common.AocApp(Day21Solution.solution)

object Day21Solution {
  final case class Player(id: Int, score: Int, position: Int) {
    def forward(n: Int): Player = {
      val nextPosition = (position + n) % 10
      Player(id, score + nextPosition + 1, nextPosition)
    }

    def deterministicNext(dice: Int): (Int, Player) = (dice + 3, forward(dice * 3 + 6))

    def diracNext: Seq[Player] =
      for {
        a <- 1 to 3
        b <- 1 to 3
        c <- 1 to 3
      } yield forward(a + b + c)
  }

  object DeterministicGame {
    def from(playerPositions: Iterable[Int]): DeterministicGame = DeterministicGame(Queue.from(playerPositions.map(pos => Player(-1, 0, pos - 1))), 0)
  }
  final case class DeterministicGame(players: Queue[Player], dice: Int) {
    def next: DeterministicGame = {
      val player +: other = players
      val (newDice, newPlayer) = player.deterministicNext(dice)
      DeterministicGame(other :+ newPlayer, newDice)
    }
    def isEnd: Boolean = players.exists(_.score >= 1000)
    def score: Int = dice * players.map(_.score).min
  }

  object DiracGame {
    def from(playerPositions: Iterable[Int]): DiracGame = DiracGame(
      Queue.from(
        for {
          (pos, id) <- playerPositions.zipWithIndex
        } yield Player(id, 0, pos - 1)
      )
    )
  }
  final case class DiracGame(players: Queue[Player]) {
    def next: Seq[DiracGame] = {
      val current +: other = players
      for {
        next <- current.diracNext
      } yield DiracGame(other :+ next)
    }
    def getWinner: Option[Player] = players.find(_.score >= 21)
  }

  type Parsed = Iterable[Int]

  object solution extends common.Solution[Parsed]("2021", "21") {
    override def parse(input: String): Parsed = input.split("\n").map(_.split(" ").last.toInt)

    override def part1(parsed: Parsed): Any = LazyList.iterate(DeterministicGame.from(parsed))(_.next).dropWhile(!_.isEnd).head.score

    override def part2(parsed: Parsed): Any = {
      val initial = DiracGame.from(parsed)

      val toVisit = collection.mutable.PriorityQueue(initial)(Ordering.by(-_.players.map(_.score).sum))
      var amounts = Map(initial -> BigInt(1))
      var results = initial.players.map(p => p.id -> BigInt(0)).toMap
      while (toVisit.nonEmpty) {
        val current = toVisit.dequeue()
        val amount = amounts(current)
        amounts = amounts - current

        current.getWinner match {
          case Some(Player(winnerId, _, _)) =>
            results = results + (winnerId -> (results(winnerId) + amount))
          case None =>
            for {
              toAdd <- current.next
            } {
              if (amounts.keySet.contains(toAdd)) {
                amounts = amounts + (toAdd -> (amounts(toAdd) + amount))
              } else {
                amounts = amounts + (toAdd -> amount)
                toVisit.enqueue(toAdd)
              }
            }
        }
      }

      results.view.values.max
    }
  }
}
