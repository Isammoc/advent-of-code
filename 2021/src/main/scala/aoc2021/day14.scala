package aoc2021

object day14 extends common.AocApp(Day14Solution.solution)

object Day14Solution {
  object Polymere {
    def parsePairs(str: String): Map[String, BigInt] = (for {
      (a, b) <- str.zip(str.tail)
    } yield s"$a$b").groupMapReduce(identity)(_ => BigInt(1))(_ + _)

    def parseRules(str: String): Map[String, List[String]] = (for {
      rule <- str.split("\n")
      Array(before, introduce) = rule.split("->")
    } yield before.trim -> List(s"${before.trim.head}${introduce.trim}", s"${introduce.trim}${before.trim.tail.head}")).groupMapReduce(_._1)(_._2)(_ ++ _)

    def parse(str: String): Polymere = {
      val Array(initial, rules) = str.split("\n\n")
      Polymere(parsePairs(initial), parseRules(rules))
    }
  }
  final case class Polymere(pairs: Map[String, BigInt], rules: Map[String, List[String]]) {
    def size: BigInt = pairs.values.sum + 1

    def next: Polymere = {
      this.copy(pairs = (for {
        (before, count) <- pairs.toList
        result <- rules.getOrElse(before, List(before))
      } yield result -> count).groupMapReduce(_._1)(_._2)(_ + _))
    }

    def score: BigInt = {
      val counts = (for {
        (pair, count) <- pairs.toList
        c <- pair
      } yield c -> count).groupMapReduce(_._1)(_._2)(_ + _)

      val max = (counts.view.values.max + 1) / 2
      val min = (counts.view.values.min + 1) / 2
      max - min
    }
  }

  type Parsed = Polymere
  object solution extends common.Solution[Parsed]("2021", "14") {
    def parse(input: String): Parsed = Polymere.parse(input)

    def part1(input: Parsed): BigInt = LazyList
      .iterate(input)(_.next)
      .drop(10)
      .head
      .score

    def part2(input: Parsed): BigInt = LazyList
      .iterate(input)(_.next)
      .drop(40)
      .head
      .score
  }
}
