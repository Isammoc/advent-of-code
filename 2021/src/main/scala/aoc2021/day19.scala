package aoc2021

import scala.annotation.tailrec

object day19 extends common.AocApp(Day19Solution.solution)

object Day19Solution {
  private val Matrix = common.matrix.Matrix.apply[Int](_)
  private type Matrix = common.matrix.Matrix[Int]

  @tailrec
  def fromFirst(toNormalize: List[Scanner], result: Set[Scanner]): Set[Scanner] =
    toNormalize match {
      case Nil => result
      case current :: t =>
        val overlaps = for {
          toConsider <- t
          overlaps <- toConsider.overlaps(current)
        } yield overlaps

        val idsFound = overlaps.toSet.map((x: Scanner) => x.id)
        val notOverlaps = t.filterNot(scanner => idsFound.contains(scanner.id))
        fromFirst(overlaps ++ notOverlaps, result + current)
    }

  object Rotation {
    def rotation(input: String): Matrix = Matrix(
      input
        .split("\n")
        .toList
        .map(_.trim.split("\\s+").toList.map(_.trim.toInt))
    )

    val Rx: Matrix = rotation(""" 1  0  0  0
                                | 0  0 -1  0
                                | 0  1  0  0
                                | 0  0  0  1""".stripMargin)

    val Ry: Matrix = rotation(""" 0  0  1  0
                                | 0  1  0  0
                                |-1  0  0  0
                                | 0  0  0  1""".stripMargin)

    val Rz: Matrix = rotation(""" 0 -1  0  0
                                | 1  0  0  0
                                | 0  0  1  0
                                | 0  0  0  1""".stripMargin)

    val I: Matrix = rotation(""" 1  0  0  0
                               | 0  1  0  0
                               | 0  0  1  0
                               | 0  0  0  1""".stripMargin)

    val All: Seq[Matrix] = for {
      facing <- List(I, Rx, Rx * Rx, Rx * Rx * Rx, Ry, Ry * Ry * Ry)
      up <- List(I, Rz, Rz * Rz, Rz * Rz * Rz)
    } yield facing * up
  }

  object Scanner {
    def parse(str: String): Scanner = {
      val name :: beacons = str.split("\n").toList

      Scanner(name.split(" ")(2).toInt, (0, 0, 0), beacons.map(Beacon.parse))
    }
  }
  case class Scanner(id: Int, position: (Int, Int, Int), beacons: List[Beacon]) {
    override def toString: String =
      s"--- scanner $id ---\n" + beacons.sortBy(b => (b.x, b.y, b.z)).mkString("\n")

    def normalize(rotation: Matrix, translation: Beacon): Scanner = {
      val transform = rotation + translation.translation
      val newPosition = (translation.x, translation.y, translation.z)
      val newBeacons = beacons.map(beacon => Beacon.fromMatrix(transform * beacon.toMatrix))
      Scanner(id, newPosition, newBeacons)
    }

    def overlaps(other: Scanner): Option[Scanner] =
      (for {
        rotation <- Rotation.All
        translations = for {
          ours <- beacons
          theirs <- other.beacons
        } yield rotation * ours.toMatrix - theirs.toMatrix
        (translation, beacons) <- translations.groupBy(identity)
        if beacons.size >= 12
      } yield (rotation, Beacon.fromMatrix(translation))).headOption
        .map((normalize _).tupled)

    def manhattan(other: Scanner): Int =
      (position._1 - other.position._1).abs + (position._2 - other.position._2).abs + (position._3 - other.position._3).abs
  }

  object Beacon {
    def parse(str: String): Beacon = {
      val Array(x, y, z) = str.split(",").map(_.toInt)
      Beacon(x, y, z)
    }

    def fromMatrix(matrix: Matrix): Beacon = {
      val List(x, y, z, _) = matrix.rows.transpose.flatten
      Beacon(x, y, z)
    }
  }
  case class Beacon(x: Int, y: Int, z: Int) {
    override def toString: String =
      s"$x,$y,$z"

    def translation: Matrix = Matrix(List(List(0, 0, 0, -x), List(0, 0, 0, -y), List(0, 0, 0, -z), List(0, 0, 0, 0)))

    def toMatrix: Matrix = Matrix(List(List(x, y, z, 1)).transpose)
  }

  type Parsed = List[Scanner]
  object solution extends common.Solution[Parsed]("2021", "19") {

    def parse(str: String): List[Scanner] = {
      str.split("\n\n").toList.map(Scanner.parse)
    }

    def part1(scanners: Parsed): Int = {
      val normalized = fromFirst(scanners, Set.empty)
      normalized.flatMap(_.beacons).size
    }

    def part2(scanners: Parsed): Int = {
      val normalized = fromFirst(scanners, Set.empty)
      normalized.toList.combinations(2).map { case List(x, y) => x.manhattan(y) }.max
    }

  }
}
