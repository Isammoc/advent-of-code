package aoc2021

import common.algo.integers.Dichotomy
import common.grid.Point

import scala.annotation.tailrec

object day17 extends common.AocApp(Day17Solution.solution)

object Day17Solution {
  final object TargetArea {
    def parse(str: String): TargetArea = {
      val TargetAreaR = "target area: x=(\\d+)..(\\d+), y=(-?\\d+)..(-?\\d+)".r
      str.trim match {
        case TargetAreaR(minX, maxX, minY, maxY) => TargetArea(minX.toInt, maxX.toInt, minY.toInt, maxY.toInt)
      }
    }
  }
  final case class TargetArea(minX: Int, maxX: Int, minY: Int, maxY: Int) {
    def verticalStep(initialVerticalVelocity: Int): Option[Int] = {
      @tailrec
      def loop(currentVelocity: Int, currentPosition: Int, currentStep: Int): Option[Int] = {
        val nextPosition = currentPosition + currentVelocity
        if (currentVelocity > 0) {
          loop(currentVelocity - 1, nextPosition, currentStep + 1)
        } else if (minY <= nextPosition && nextPosition <= maxY) {
          Some(currentStep + 1)
        } else if ((minY - currentPosition).sign != (minY - nextPosition).sign) {
          None
        } else {
          loop(currentVelocity - 1, nextPosition, currentStep + 1)
        }
      }

      loop(initialVerticalVelocity, 0, 0)
    }

    def finalHorizontalPosition(stepCount: Int, initialHorizontalVelocity: Int): Int = {
      @tailrec
      def loop(remainSteps: Int, currentVelocity: Int, currentPosition: Int): Int = {
        if (currentVelocity == 0 || remainSteps <= 0)
          currentPosition
        else loop(remainSteps - 1, currentVelocity - 1, currentPosition + currentVelocity)
      }
      loop(stepCount, initialHorizontalVelocity, 0)
    }

    def findHorizontalVelocity(stepCount: Int): Option[Int] = {
      @tailrec
      def loop(minVelocity: Int, maxVelocity: Int): Option[Int] = {
        if (maxVelocity < minVelocity) None
        else {
          val midVelocity = (maxVelocity - minVelocity) / 2 + minVelocity
          val result = finalHorizontalPosition(stepCount, midVelocity)

          if (result < minX) {
            loop(midVelocity + 1, maxVelocity)
          } else if (result < maxX) {
            Some(midVelocity)
          } else loop(minVelocity, midVelocity)
        }
      }
      loop(0, maxX)
    }

    def reachBy(initialVerticalVelocity: Int): Option[Int] = {
      for {
        step <- verticalStep(initialVerticalVelocity)
        x <- findHorizontalVelocity(step)
      } yield x
    }

    def findHighestVelocity: Point = {
      (for {
        y <- LazyList.iterate(1000)(_ - 1).take(2000)
        x <- reachBy(y)
      } yield Point(x, y)).head
    }

    def findStepsForVerticalVelocity(initialY: Int): List[Int] = {
      @tailrec
      def loop(currentVelocity: Int, currentPosition: Int, step: Int, found: List[Int]): List[Int] =
        if (currentPosition < minY) {
          found
        } else if (currentPosition <= maxY) {
          loop(currentVelocity - 1, currentPosition + currentVelocity, step + 1, step :: found)
        } else {
          loop(currentVelocity - 1, currentPosition + currentVelocity, step + 1, found)
        }
      loop(initialY, 0, 0, Nil)
    }

    def findXsForSteps(stepCount: Int): Set[Int] = {

      val minXVelocity = Dichotomy(0, maxX * 2, currentVelocity => finalHorizontalPosition(stepCount, currentVelocity) >= minX)
      val maxXVelocity = Dichotomy(0, maxX * 2, currentVelocity => finalHorizontalPosition(stepCount, currentVelocity) > maxX) - 1

      if (maxXVelocity < minXVelocity) Set.empty
      else {
        println(s"$minXVelocity to $maxXVelocity for steps: $stepCount")
        (minXVelocity to maxXVelocity).toSet
      }
    }

    def allPossible: Set[Point] = {
      val possiblesStepCount = (for {
        possibleY <- (minY * 2) to -(minY * 2)
        possibleStep <- findStepsForVerticalVelocity(possibleY)
      } yield possibleStep -> possibleY).groupMapReduce(_._1)(y => Set(y._2))(_ ++ _)

      (for {
        (step, ys) <- possiblesStepCount
        x <- findXsForSteps(step)
        y <- ys
      } yield Point(x, y)).toSet
    }
  }

  type Parsed = TargetArea
  object solution extends common.Solution[Parsed]("2021", "17") {
    override def parse(input: String): Parsed = TargetArea.parse(input)

    def part1(input: Parsed): Int = {
      val highestVelocity = input.findHighestVelocity

      if (highestVelocity.y > 0) {
        highestVelocity.y * (highestVelocity.y + 1) / 2
      } else 0
    }

    def part2(input: Parsed): Int = input.allPossible.size
  }
}
