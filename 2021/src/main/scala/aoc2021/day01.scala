package aoc2021

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed = List[Int]
  def compute(input: Parsed, indexDiff: Int): Int = input.zip(input.drop(indexDiff)).count { case (a, b) =>
    b > a
  }

  object solution extends common.Solution[Parsed]("2021", "01") {
    override def parse(input: String): Parsed = input.split("\n").toList.map(_.toInt)

    override def part1(parsed: Parsed): Int = compute(parsed, 1)

    override def part2(parsed: Parsed): Int = compute(parsed, 3)
  }
}
