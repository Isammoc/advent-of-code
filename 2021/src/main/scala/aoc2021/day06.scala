package aoc2021

object day06 extends common.AocApp(Day06Solution.solution)

object Day06Solution {

  case class Generation(fishes: Map[Int, BigInt]) {
    def next: Generation = {
      Generation((for {
        (age, count) <- fishes.toList
        next <- if (age <= 0) List(6 -> count, 8 -> count) else List((age - 1) -> count)
      } yield next).groupMapReduce(_._1)(_._2)(_ + _))
    }
    def size: BigInt = fishes.values.sum
  }

  type Parsed = Generation
  object solution extends common.Solution[Parsed]("2021", "06") {
    def parse(str: String): Parsed = {
      Generation(str.split(",").groupMapReduce(_.toInt)(_ => BigInt(1))(_ + _))
    }

    def part1(input: Parsed): BigInt = LazyList.iterate(input)(_.next).drop(80).head.size

    def part2(input: Parsed): BigInt = LazyList.iterate(input)(_.next).drop(256).head.size
  }
}
