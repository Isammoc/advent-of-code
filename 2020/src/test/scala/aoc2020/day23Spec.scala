package aoc2020

import common.AocSpec

class day23Spec extends AocSpec {

  import Day23Solution._

  "day23 2020" can {
    import solution._

    val sample = parse("389125467")

    "part1" should {
      "example" in {
        part1(sample, 10) shouldEqual "92658374"
        part1(sample) shouldEqual "67384529"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual BigInt("149245887792")
      }
    }
  }
}
