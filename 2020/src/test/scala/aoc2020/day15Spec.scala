package aoc2020
import common.AocSpec

class day15Spec extends AocSpec {
  import Day15Solution._

  "day15 2020" can {
    import solution._

    "part1" should {
      "example" in {
        part1(parse("0,3,6")) shouldEqual 436
        part1(parse("1,3,2")) shouldEqual 1
        part1(parse("2,1,3")) shouldEqual 10
        part1(parse("1,2,3")) shouldEqual 27
        part1(parse("2,3,1")) shouldEqual 78
        part1(parse("3,2,1")) shouldEqual 438
        part1(parse("3,1,2")) shouldEqual 1836
      }
    }

    "part2" should {
      "example" in {
        part2(parse("0,3,6")) shouldEqual 175594
        part2(parse("1,3,2")) shouldEqual 2578
        part2(parse("2,1,3")) shouldEqual 3544142
        part2(parse("1,2,3")) shouldEqual 261214
        part2(parse("2,3,1")) shouldEqual 6895259
        part2(parse("3,2,1")) shouldEqual 18
        part2(parse("3,1,2")) shouldEqual 362
      }
    }
  }
}
