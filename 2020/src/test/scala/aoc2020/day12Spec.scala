package aoc2020

import common.AocSpec

class day12Spec extends AocSpec {

  import Day12Solution._

  "day12 2020" can {
    import solution._
    val sample =
      parse("""F10
              |N3
              |F7
              |R90
              |F11""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 25
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 286
      }
    }
  }
}
