package aoc2020

import common.AocSpec

class day05Spec extends AocSpec {
  import Day05Solution._

  "seatID" in {
    seatID("FBFBBFFRLR") shouldEqual 357
    seatID("BFFFBBFRRR") shouldEqual 567
    seatID("FFFBBBFRRR") shouldEqual 119
    seatID("BBFFBBFRLL") shouldEqual 820
  }

  "day05 2020" can {
    import solution._

    "part1" should {
      "example" in {
        part1(parse("""FBFBBFFRLR
                      |BFFFBBFRRR
                      |FFFBBBFRRR
                      |BBFFBBFRLL
                      |""".stripMargin)) shouldEqual 820
      }
    }

    "part2" should {
      "example" in {
        part2(parse("""FBFBBFFRLL
                      |FBFBBFFRRL
                      |""".stripMargin)) shouldEqual 357
      }
    }
  }
}
