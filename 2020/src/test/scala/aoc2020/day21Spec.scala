package aoc2020

import common.AocSpec

class day21Spec extends AocSpec {

  import Day21Solution._

  "day21 2020" can {
    import solution._

    val sample = parse("""mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
                         |trh fvjkl sbzzf mxmxvkd (contains dairy)
                         |sqjhc fvjkl (contains soy)
                         |sqjhc mxmxvkd sbzzf (contains fish)""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 5
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "mxmxvkd,sqjhc,fvjkl"
      }
    }
  }
}
