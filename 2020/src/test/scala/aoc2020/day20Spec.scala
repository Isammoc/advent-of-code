package aoc2020

import common.AocSpec

class day20Spec extends AocSpec {

  import Day20Solution._

  def cut(str: String): List[List[Char]] = str.split("\n").toList.map(_.toList)

  "Tile" should {
    val sample =
      """Tile 1234:
        |#####
        |.123#
        |#456#
        |.789.
        |#..##
        |""".stripMargin

    val underTest = Tile(sample)
    "have an id" in {
      underTest.id shouldEqual 1234
    }

    "define possible borders" in {
      val borders = Set(11111L, 10011L, 11001L, 10101L, 11101L, 10111L)
      underTest.possibleBorders shouldEqual borders
    }

    "see all positions" in {
      val positionOriginal =
        """123
          |456
          |789""".stripMargin
      underTest.possible.exists(_.content == cut(positionOriginal))
      underTest.possible.exists(_.content == cut(positionOriginal).reverse)
      underTest.possible.exists(_.content == cut(positionOriginal).map(_.reverse))
      val rotateRight =
        """741
          |852
          |963
          |""".stripMargin
      underTest.possible.exists(_.content == cut(rotateRight))
      underTest.possible.exists(_.content == cut(rotateRight).reverse)
      underTest.possible.exists(_.content == cut(rotateRight).map(_.reverse))
      val rotateLeft =
        """369
          |258
          |147
          |""".stripMargin
      underTest.possible.exists(_.content == cut(rotateLeft))
      underTest.possible.exists(_.content == cut(rotateLeft).reverse)
      underTest.possible.exists(_.content == cut(rotateLeft).map(_.reverse))
      val rotateTwice =
        """987
          |654
          |321
          |""".stripMargin
      underTest.possible.exists(_.content == cut(rotateTwice))
      underTest.possible.exists(_.content == cut(rotateTwice).reverse)
      underTest.possible.exists(_.content == cut(rotateTwice).map(_.reverse))
    }
  }

  "day20 2020" can {
    import solution._

    val sample = parse("""Tile 2311:
                         |..##.#..#.
                         |##..#.....
                         |#...##..#.
                         |####.#...#
                         |##.##.###.
                         |##...#.###
                         |.#.#.#..##
                         |..#....#..
                         |###...#.#.
                         |..###..###
                         |
                         |Tile 1951:
                         |#.##...##.
                         |#.####...#
                         |.....#..##
                         |#...######
                         |.##.#....#
                         |.###.#####
                         |###.##.##.
                         |.###....#.
                         |..#.#..#.#
                         |#...##.#..
                         |
                         |Tile 1171:
                         |####...##.
                         |#..##.#..#
                         |##.#..#.#.
                         |.###.####.
                         |..###.####
                         |.##....##.
                         |.#...####.
                         |#.##.####.
                         |####..#...
                         |.....##...
                         |
                         |Tile 1427:
                         |###.##.#..
                         |.#..#.##..
                         |.#.##.#..#
                         |#.#.#.##.#
                         |....#...##
                         |...##..##.
                         |...#.#####
                         |.#.####.#.
                         |..#..###.#
                         |..##.#..#.
                         |
                         |Tile 1489:
                         |##.#.#....
                         |..##...#..
                         |.##..##...
                         |..#...#...
                         |#####...#.
                         |#..#.#.#.#
                         |...#.#.#..
                         |##.#...##.
                         |..##.##.##
                         |###.##.#..
                         |
                         |Tile 2473:
                         |#....####.
                         |#..#.##...
                         |#.##..#...
                         |######.#.#
                         |.#...#.#.#
                         |.#########
                         |.###.#..#.
                         |########.#
                         |##...##.#.
                         |..###.#.#.
                         |
                         |Tile 2971:
                         |..#.#....#
                         |#...###...
                         |#.#.###...
                         |##.##..#..
                         |.#####..##
                         |.#..####.#
                         |#..#.#..#.
                         |..####.###
                         |..#.#.###.
                         |...#.#.#.#
                         |
                         |Tile 2729:
                         |...#.#.#.#
                         |####.#....
                         |..#.#.....
                         |....#..#.#
                         |.##..##.#.
                         |.#.####...
                         |####.#.#..
                         |##.####...
                         |##..#.##..
                         |#.##...##.
                         |
                         |Tile 3079:
                         |#.#.#####.
                         |.#..######
                         |..#.......
                         |######....
                         |####.#..#.
                         |.#...#.##.
                         |#.#####.##
                         |..#.###...
                         |..#.......
                         |..#.###...""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual BigInt("20899048083289")
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 273
      }
    }
  }
}
