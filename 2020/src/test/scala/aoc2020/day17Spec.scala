package aoc2020
import common.AocSpec

class day17Spec extends AocSpec {
  import Day17Solution._

  "day17 2020" can {
    import solution._

    val sample = """.#.
                   |..#
                   |###""".stripMargin

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 112
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 848
      }
    }
  }
}
