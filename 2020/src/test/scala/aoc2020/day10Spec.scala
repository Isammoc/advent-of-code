package aoc2020

import common.AocSpec

class day10Spec extends AocSpec {

  import Day10Solution._

  "day10 2020" can {
    import solution._
    val sample1 =
      parse("""16
              |10
              |15
              |5
              |1
              |11
              |7
              |19
              |6
              |12
              |4""".stripMargin)
    val sample2 =
      parse("""28
              |33
              |18
              |42
              |31
              |14
              |46
              |20
              |48
              |47
              |24
              |23
              |49
              |45
              |19
              |38
              |39
              |11
              |1
              |32
              |25
              |35
              |8
              |17
              |7
              |9
              |4
              |2
              |34
              |10
              |3""".stripMargin)
    "part1" should {
      "example" in {
        part1(sample1) shouldEqual 35
        part1(sample2) shouldEqual 220
      }
    }

    "part2" should {
      "example" in {
        part2(sample1) shouldEqual 8
        part2(sample2) shouldEqual 19208
      }
    }
  }
}
