package aoc2020
import common.AocSpec

class day24Spec extends AocSpec {
  import Day24Solution._

  "day24 2020" can {
    import solution._

    val sample = parse("""sesenwnenenewseeswwswswwnenewsewsw
                         |neeenesenwnwwswnenewnwwsewnenwseswesw
                         |seswneswswsenwwnwse
                         |nwnwneseeswswnenewneswwnewseswneseene
                         |swweswneswnenwsewnwneneseenw
                         |eesenwseswswnenwswnwnwsewwnwsene
                         |sewnenenenesenwsewnenwwwse
                         |wenwwweseeeweswwwnwwe
                         |wsweesenenewnwwnwsenewsenwwsesesenwne
                         |neeswseenwwswnwswswnw
                         |nenwswwsewswnenenewsenwsenwnesesenew
                         |enewnwewneswsewnwswenweswnenwsenwsw
                         |sweneswneswneneenwnewenewwneswswnese
                         |swwesenesewenwneswnwwneseswwne
                         |enesenwswwswneneswsenwnewswseenwsese
                         |wnwnesenesenenwwnenwsewesewsesesew
                         |nenewswnwewswnenesenwnesewesw
                         |eneswnwswnwsenenwnwnwwseeswneewsenese
                         |neswnwewnwnwseenwseesewsenwsweewe
                         |wseweeenwnesenwwwswnew""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 10
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 2208
      }
    }
  }
}
