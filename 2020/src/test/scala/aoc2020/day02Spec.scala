package aoc2020
import common.AocSpec

class day02Spec extends AocSpec {
  import Day02Solution._

  "day02 2020" can {
    val sampleInput =
      """1-3 a: abcde
        |1-3 b: cdefg
        |2-9 c: ccccccccc""".stripMargin

    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 2
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1
      }
    }
  }
}
