package aoc2020
import common.AocSpec

class day08Spec extends AocSpec {
  import Day08Solution._

  "day08 2020" can {
    import solution._
    val sampleInput = """nop +0
                        |acc +1
                        |jmp +4
                        |acc +3
                        |jmp -3
                        |acc -99
                        |acc +1
                        |jmp -4
                        |acc +6""".stripMargin
    val sample = parse(sampleInput)
    "part1" should {
      "example" in {
        part1(sample) shouldEqual 5
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 8
      }
    }
  }
}
