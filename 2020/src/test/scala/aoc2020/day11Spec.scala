package aoc2020
import common.AocSpec

class day11Spec extends AocSpec {
  import Day11Solution._

  "day11 2020" can {
    import solution._
    val sample = parse("""L.LL.LL.LL
                         |LLLLLLL.LL
                         |L.L.L..L..
                         |LLLL.LL.LL
                         |L.LL.LL.LL
                         |L.LLLLL.LL
                         |..L.L.....
                         |LLLLLLLLLL
                         |L.LLLLLL.L
                         |L.LLLLL.LL""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 37
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 26
      }
    }
  }
}
