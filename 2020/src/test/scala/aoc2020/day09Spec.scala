package aoc2020
import common.AocSpec

class day09Spec extends AocSpec {
  import Day09Solution._

  "day09 2020" can {
    import solution._
    val sampleInput = """35
                        |20
                        |15
                        |25
                        |47
                        |40
                        |62
                        |55
                        |65
                        |95
                        |102
                        |117
                        |150
                        |182
                        |127
                        |219
                        |299
                        |277
                        |309
                        |576""".stripMargin
    val sample = parse(sampleInput)
    "part1" should {
      "example" in {
        part1(sample, 5) shouldEqual 127
      }
    }

    "part2" should {
      "example" in {
        part2(sample, 5) shouldEqual 62
      }
    }
  }
}
