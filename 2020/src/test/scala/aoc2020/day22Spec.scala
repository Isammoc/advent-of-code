package aoc2020

import common.AocSpec

class day22Spec extends AocSpec {

  import Day22Solution._

  "day22 2020" can {
    import solution._

    val sample = parse("""Player 1:
                         |9
                         |2
                         |6
                         |3
                         |1
                         |
                         |Player 2:
                         |5
                         |8
                         |4
                         |7
                         |10""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 306
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 291
      }
    }
  }
}
