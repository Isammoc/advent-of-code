package aoc2020
import common.AocSpec

class day03Spec extends AocSpec {
  import Day03Solution._

  "day03 2020" can {
    import solution._
    val sample = parse("""..##.......
                         |#...#...#..
                         |.#....#..#.
                         |..#.#...#.#
                         |.#...##..#.
                         |..#.##.....
                         |.#.#.#....#
                         |.#........#
                         |#.##...#...
                         |#...##....#
                         |.#..#...#.#
                         |""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 7
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 336
      }
    }
  }
}
