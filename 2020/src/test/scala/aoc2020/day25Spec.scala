package aoc2020

import common.AocSpec

class day25Spec extends AocSpec {

  import Day25Solution._

  "day25 2020" can {
    import solution._

    "find loop size" in {
      findLoopSize(5764801) shouldEqual 8
      findLoopSize(17807724) shouldEqual 11
    }

    val sample = parse("""5764801
                         |17807724
                         |""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 14897079
      }
    }
  }
}
