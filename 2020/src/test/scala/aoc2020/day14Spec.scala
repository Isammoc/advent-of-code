package aoc2020

import common.AocSpec

class day14Spec extends AocSpec {

  import Day14Solution._

  "day14 2020" can {
    import solution._

    "part1" should {
      val sample = parse("""mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
                           |mem[8] = 11
                           |mem[7] = 101
                           |mem[8] = 0
                           |""".stripMargin)
      "example" in {
        part1(sample) shouldEqual 165
      }
    }

    "part2" should {
      "example" in {
        val sample = parse("""mask = 000000000000000000000000000000X1001X
                             |mem[42] = 100
                             |mask = 00000000000000000000000000000000X0XX
                             |mem[26] = 1""".stripMargin)
        part2(sample) shouldEqual 208
      }

      "crafted" in {
        part2(parse("""mask = 000000000000000000000000000000000001
                      |mem[0] = 1
                      |mask = 000000000000000000000000000000000001
                      |mem[0] = 2""".stripMargin)) shouldEqual 2
        part2(parse("""mask = 000000000000000000000000000000000001
                      |mem[0] = 1
                      |mask = 00000000000000000000000000000000000X
                      |mem[0] = 2""".stripMargin)) shouldEqual 4
        part2(parse("""mask = 000000000000000000000000000000000000
                      |mem[1] = 1
                      |mask = 00000000000000000000000000000000000X
                      |mem[0] = 2""".stripMargin)) shouldEqual 4
        part2(parse("""mask = 000000000000000000000000000000000000
                      |mem[1] = 1
                      |mask = 00000000000000000000000000000000000X
                      |mem[0] = 2""".stripMargin)) shouldEqual 4
        part2(parse("""mask = 000000000000000000000000000000000000
                      |mem[1] = 1
                      |mask = 00000000000000000000000000000000000X
                      |mem[0] = 2""".stripMargin)) shouldEqual 4
        part2(parse("""mask = 00000000000000000000000000000000000X
                      |mem[1] = 1
                      |mask = 000000000000000000000000000000000000
                      |mem[0] = 2""".stripMargin)) shouldEqual 3
        part2(parse("""mask = 00000000000000000000000000000000000X
                      |mem[1] = 1
                      |mask = 000000000000000000000000000000000000
                      |mem[1] = 2""".stripMargin)) shouldEqual 3
        part2(parse("""mask = 0000000000000000000000000000000000XX
                      |mem[1] = 1
                      |mask = 000000000000000000000000000000000000
                      |mem[1] = 2""".stripMargin)) shouldEqual 5
      }
    }
  }
}
