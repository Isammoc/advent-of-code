package aoc2020
import common.AocSpec

class day13Spec extends AocSpec {
  import Day13Solution._

  "day13 2020" can {
    import solution._
    val sample = """939
                   |7,13,x,x,59,x,31,19""".stripMargin

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 295
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 1068781
      }

      "other examples" in {
        part2("""1
                |17,x,13,19
                |""".stripMargin) shouldEqual 3417
        part2("""1
                |67,7,59,61
                |""".stripMargin) shouldEqual 754018
        part2("""1
                |67,x,7,59,61
                |""".stripMargin) shouldEqual 779210
        part2("""1
                |67,7,x,59,61
                |""".stripMargin) shouldEqual 1261476
        part2("""1
                |1789,37,47,1889
                |""".stripMargin) shouldEqual 1202161486
      }
    }
  }
}
