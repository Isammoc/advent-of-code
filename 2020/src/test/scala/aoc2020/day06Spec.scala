package aoc2020
import common.AocSpec

class day06Spec extends AocSpec {
  import Day06Solution._

  "day06 2020" can {
    import solution._
    val sample = parse("""abc
                         |
                         |a
                         |b
                         |c
                         |
                         |ab
                         |ac
                         |
                         |a
                         |a
                         |a
                         |a
                         |
                         |b""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 11
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 6
      }
    }
  }
}
