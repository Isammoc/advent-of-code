package aoc2020
import common.AocSpec

class day16Spec extends AocSpec {
  import Day16Solution._

  "day16 2020" can {
    val sample = """class: 1-3 or 5-7
                   |row: 6-11 or 33-44
                   |seat: 13-40 or 45-50
                   |
                   |your ticket:
                   |7,1,14
                   |
                   |nearby tickets:
                   |7,3,47
                   |40,4,50
                   |55,2,20
                   |38,6,12""".stripMargin

    "part1" should {
      import solution._

      "example" in {
        part1(parse(sample)) shouldEqual 71
      }
      "individual nearby" in {
        val Array(definitions, _, _) = sample.split("\n\n")
        val parsers = parseDefinitions(definitions)

        validateTicket(parsers)(List(7, 3, 47)) shouldEqual true
      }
    }
  }
}
