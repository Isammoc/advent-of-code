package aoc2020
import common.AocSpec

class day01Spec extends AocSpec {
  import Day01Solution._

  val sampleInput: String =
    """1721
      |979
      |366
      |299
      |675
      |1456""".stripMargin
  "day01 2020" can {
    import solution._
    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "514579"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "241861950"
      }
    }
  }
}
