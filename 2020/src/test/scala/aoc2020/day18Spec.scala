package aoc2020

import common.AocSpec

class day18Spec extends AocSpec {

  import Day18Solution._

  "day18 2020" can {
    import solution._

    "expression should evaluate" in {
      evaluate(parseLine("2 * 3 + (4 * 5)")) shouldEqual 26
      evaluate(parseLine("5 + (8 * 3 + 9 + 3 * 4 * 3)")) shouldEqual 437
      evaluate(parseLine("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")) shouldEqual 12240
      evaluate(parseLine("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")) shouldEqual 13632
    }

    val sample =
      parse("""2 * 3 + (4 * 5)
              |5 + (8 * 3 + 9 + 3 * 4 * 3)
              |5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
              |((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
              |""".stripMargin)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual 26335
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual 693891
      }
    }
  }
}
