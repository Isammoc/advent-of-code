package aoc2020

object day05 extends common.AocApp(Day05Solution.solution)

object Day05Solution {
  def seatID(boardingPass: String): Int = {
    Integer.parseInt(
      boardingPass map {
        case 'B' => '1'
        case 'F' => '0'
        case 'R' => '1'
        case 'L' => '0'
      },
      2
    )
  }

  type Parsed = Array[Int]
  object solution extends common.Solution[Parsed]("2020", "05") {
    override def parse(input: String): Parsed = input.split("\n").map(seatID)

    def part1(input: Parsed): Int = input.max

    def part2(input: Parsed): Int = {
      val allPresents = input.toSet

      allPresents.map(_ + 1).find(id => !allPresents.contains(id) && allPresents.contains(id + 1) && allPresents.contains(id - 1)).get
    }
  }
}
