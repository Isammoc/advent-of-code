package aoc2020
import common.grid.Point

import scala.util.matching.Regex

object day12 extends common.AocApp(Day12Solution.solution)

object Day12Solution {
  case class Move(action: Char, amount: Int)

  case class Boat1(position: Point, facing: Point) {
    def next(m: Move): Boat1 = m match {
      case Move('N', amount) =>
        this.copy(position = position + Point.Up * amount)
      case Move('S', amount) =>
        this.copy(position = position + Point.Down * amount)
      case Move('E', amount) =>
        this.copy(position = position + Point.Right * amount)
      case Move('W', amount) =>
        this.copy(position = position + Point.Left * amount)
      case Move('L', amount) =>
        this.copy(facing = LazyList.iterate(facing)(_.rotateLeft)((amount / 90) % 4))
      case Move('R', amount) =>
        this.copy(facing = LazyList.iterate(facing)(_.rotateRight)((amount / 90) % 4))
      case Move('F', amount) =>
        this.copy(position = position + facing * amount)
    }
  }

  case class Boat2(position: Point, waypoint: Point) {
    def next(move: Move): Boat2 = move match {
      case Move('N', amount) =>
        this.copy(waypoint = waypoint + Point.Up * amount)
      case Move('S', amount) =>
        this.copy(waypoint = waypoint + Point.Down * amount)
      case Move('E', amount) =>
        this.copy(waypoint = waypoint + Point.Right * amount)
      case Move('W', amount) =>
        this.copy(waypoint = waypoint + Point.Left * amount)
      case Move('L', amount) =>
        this.copy(waypoint = LazyList.iterate(waypoint)(before => Point(before.y, -before.x))((amount / 90) % 4))
      case Move('R', amount) =>
        this.copy(waypoint = LazyList.iterate(waypoint)(before => Point(-before.y, before.x))((amount / 90) % 4))
      case Move('F', amount) =>
        this.copy(position = Point(position.x + waypoint.x * amount, position.y + waypoint.y * amount))
    }
  }

  val ActionR: Regex = "(.)(\\d+)".r

  type Parsed = Array[Move]
  object solution extends common.Solution[Parsed]("2020", "12") {
    override def parse(input: String): Parsed = input.split("\n").map { case ActionR(action, amount) =>
      Move(action(0), amount.toInt)
    }

    def part1(input: Parsed): Int = input
      .foldLeft(Boat1(Point(0, 0), Point.Right)) { case (boat, move) =>
        boat.next(move)
      }
      .position
      .manhattan(Point.Origin)

    def part2(input: Parsed): Int = input
      .foldLeft(Boat2(Point(0, 0), Point(10, -1))) { case (boat, move) =>
        boat.next(move)
      }
      .position
      .manhattan(Point.Origin)
  }
}
