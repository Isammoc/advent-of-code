package aoc2020

import common.grid.Point

import scala.annotation.tailrec

object day11 extends common.AocApp(Day11Solution.solution)

object Day11Solution {

  case class Grid(empty: Set[Point], occupied: Set[Point], maxX: Int, maxY: Int) {
    def next1: Grid = {
      val emptyToOccupied = empty.filter { case Point(x, y) =>
        !(for {
          dy <- -1 to 1
          dx <- -1 to 1
        } yield Point(x + dx, y + dy)).exists(occupied.contains)
      }
      val occupiedToEmpty = occupied.filter { case Point(x, y) =>
        (for {
          dy <- -1 to 1
          dx <- -1 to 1
          if dx != 0 || dy != 0
        } yield Point(x + dx, y + dy)).count(occupied.contains) >= 4
      }

      Grid(empty ++ occupiedToEmpty -- emptyToOccupied, occupied ++ emptyToOccupied -- occupiedToEmpty, maxX, maxY)
    }

    def next2: Grid = {
      def rays(from: Point) = for {
        dy <- -1 to 1
        dx <- -1 to 1
        if dx != 0 || dy != 0
      } yield LazyList.from(1).map(i => Point(from.x + i * dx, from.y + i * dy)).takeWhile(p => 0 <= p.x && p.x <= maxX && 0 <= p.y && p.y <= maxY)

      def rayOccupied(ray: Seq[Point]) = {
        @tailrec
        def loop(remain: Seq[Point]): Boolean =
          remain match {
            case Seq() => false
            case h +: t =>
              if (occupied.contains(h)) true
              else if (empty.contains(h)) false
              else loop(t)
          }

        loop(ray)
      }

      val emptyToOccupied = empty.filter(p => !rays(p).exists(rayOccupied))
      val occupiedToEmpty = occupied.filter(p => rays(p).count(rayOccupied) >= 5)

      Grid(empty ++ occupiedToEmpty -- emptyToOccupied, occupied ++ emptyToOccupied -- occupiedToEmpty, maxX, maxY)
    }
  }

  object Grid {
    def fromString(input: String): Grid = {
      val lines = input.split("\n")
      val seats = for {
        (line, y) <- lines.zipWithIndex
        (c, x) <- line.zipWithIndex if c == 'L'
      } yield Point(x, y)

      val maxY = seats.map(_.y).max
      val maxX = seats.map(_.x).max
      Grid(seats.toSet, Set.empty, maxX, maxY)
    }
  }

  type Parsed = Grid
  object solution extends common.Solution[Parsed]("2020", "11") {
    override def parse(input: String): Parsed = Grid.fromString(input)

    def part1(input: Parsed): Int = {
      @tailrec
      def loop(current: Grid): Int = {
        val next = current.next1
        if (next == current) {
          next.occupied.size
        } else {
          loop(next)
        }
      }

      loop(input)
    }

    def part2(input: Parsed): Int = {
      @tailrec
      def loop(current: Grid): Int = {
        val next = current.next2
        if (next == current) {
          next.occupied.size
        } else {
          loop(next)
        }
      }

      loop(input)
    }
  }
}
