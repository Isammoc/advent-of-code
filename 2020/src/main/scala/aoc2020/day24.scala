package aoc2020

import common.grid.Point

import scala.annotation.tailrec

object day24 extends common.AocApp(Day24Solution.solution)

object Day24Solution {

  implicit class RichPoint(thiz: Point) {
    def go(path: List[Char]): Point = path match {
      case Nil => thiz
      case 'e' :: t => (thiz + Direction.East).go(t)
      case 's' :: 'e' :: t => (thiz + Direction.SouthEast).go(t)
      case 's' :: 'w' :: t => (thiz + Direction.SouthWest).go(t)
      case 'w' :: t => (thiz + Direction.West).go(t)
      case 'n' :: 'e' :: t => (thiz + Direction.NorthEast).go(t)
      case 'n' :: 'w' :: t => (thiz + Direction.NorthWest).go(t)
    }
  }

  object Direction {
    val East: Point = Point(1, -1)
    val SouthEast: Point = Point(0, -1)
    val SouthWest: Point = Point(-1, 0)
    val West: Point = Point(-1, 1)
    val NorthEast: Point = Point(1, 0)
    val NorthWest: Point = Point(0, 1)

    val all: List[Point] = List(East, SouthEast, SouthWest, West, NorthEast, NorthWest)
  }

  case class Grid(black: Set[Point]) {
    def size: Int = black.size

    def next: Grid = {
      val minX = black.map(_.x).min
      val maxX = black.map(_.x).max
      val minY = black.map(_.y).min
      val maxY = black.map(_.y).max

      val toWhite = for {
        x <- minX to maxX
        y <- minY to maxY
        current = Point(x, y)
        neighbors = Direction.all.map(current + _).count(black.contains)
        if black.contains(current) && (neighbors == 0 || neighbors > 2)
      } yield current

      val toBlack = for {
        x <- (minX - 1) to (maxX + 1)
        y <- (minY - 1) to (maxY + 1)
        current = Point(x, y)
        neighbors = Direction.all.map(current + _).count(black.contains)
        if !black.contains(current) && neighbors == 2
      } yield current

      Grid(black -- toWhite ++ toBlack)
    }
  }

  object Grid {
    def fromInput(input: String): Grid = {
      @tailrec
      def loop(remain: List[String], black: Set[Point]): Set[Point] = remain match {
        case Nil => black
        case h :: t =>
          val current = Point(0, 0).go(h.toList)
          if (black.contains(current))
            loop(t, black - current)
          else
            loop(t, black + current)
      }

      Grid(loop(input.split("\n").toList, Set.empty))
    }
  }

  type Parsed = Grid

  object solution extends common.Solution[Parsed]("2020", "24") {
    override def parse(input: String): Parsed = Grid.fromInput(input)

    def part1(input: Parsed): Int = input.size

    def part2(input: Parsed): Int = {
      @tailrec
      def loop(remain: Int, current: Grid): Grid = if (remain == 0) current
      else loop(remain - 1, current.next)

      loop(100, input).size
    }
  }

}
