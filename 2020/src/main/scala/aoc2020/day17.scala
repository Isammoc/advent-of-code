package aoc2020

object day17 extends common.AocApp(Day17Solution.solution)

object Day17Solution {
  case class Point3(x: Int, y: Int, z: Int)

  case class Grid3(active: Set[Point3]) {
    def nextActive(p: Point3): Boolean = {
      val neighborsActive = (for {
        x <- (p.x - 1) to (p.x + 1)
        y <- (p.y - 1) to (p.y + 1)
        z <- (p.z - 1) to (p.z + 1)
        if p.x != x || p.y != y || p.z != z
        n = Point3(x, y, z)
        if active.contains(n)
      } yield n).size

      if (active.contains(p)) {
        neighborsActive == 2 || neighborsActive == 3
      } else {
        neighborsActive == 3
      }
    }

    def next: Grid3 = {
      val minX = active.map(_.x).min - 1
      val maxX = active.map(_.x).max + 1
      val minY = active.map(_.y).min - 1
      val maxY = active.map(_.y).max + 1
      val minZ = active.map(_.z).min - 1
      val maxZ = active.map(_.z).max + 1

      Grid3((for {
        x <- minX to maxX
        y <- minY to maxY
        z <- minZ to maxZ
        p = Point3(x, y, z)
        if nextActive(p)
      } yield p).toSet)
    }
  }

  case class Point4(x: Int, y: Int, z: Int, w: Int)

  case class Grid4(active: Set[Point4]) {
    def nextActive(p: Point4): Boolean = {
      val neighborsActive = (for {
        x <- (p.x - 1) to (p.x + 1)
        y <- (p.y - 1) to (p.y + 1)
        z <- (p.z - 1) to (p.z + 1)
        w <- (p.w - 1) to (p.w + 1)
        if p.x != x || p.y != y || p.z != z || p.w != w
        n = Point4(x, y, z, w)
        if active.contains(n)
      } yield n).size

      if (active.contains(p)) {
        neighborsActive == 2 || neighborsActive == 3
      } else {
        neighborsActive == 3
      }
    }

    def next: Grid4 = {
      val minX = active.map(_.x).min - 1
      val maxX = active.map(_.x).max + 1
      val minY = active.map(_.y).min - 1
      val maxY = active.map(_.y).max + 1
      val minZ = active.map(_.z).min - 1
      val maxZ = active.map(_.z).max + 1
      val minW = active.map(_.w).min - 1
      val maxW = active.map(_.w).max + 1

      Grid4((for {
        x <- minX to maxX
        y <- minY to maxY
        z <- minZ to maxZ
        w <- minW to maxW
        p = Point4(x, y, z, w)
        if nextActive(p)
      } yield p).toSet)
    }
  }

  def parseInput1(input: String): Grid3 = {
    val active = for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      if c == '#'
    } yield Point3(x, y, 0)

    Grid3(active.toSet)
  }

  def parseInput2(input: String): Grid4 = {
    val active = for {
      (line, y) <- input.split("\n").zipWithIndex
      (c, x) <- line.zipWithIndex
      if c == '#'
    } yield Point4(x, y, 0, 0)

    Grid4(active.toSet)
  }

  type Parsed = String
  object solution extends common.Solution[Parsed]("2020", "17") {
    override def parse(input: String): Parsed = input

    def part1(input: String): Int = {
      val grid = parseInput1(input)

      LazyList.iterate(grid)(_.next).drop(6).head.active.size
    }

    def part2(input: String): Int = {
      val grid = parseInput2(input)

      LazyList.iterate(grid)(_.next).drop(6).head.active.size
    }
  }
}
