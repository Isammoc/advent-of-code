package aoc2020

import scala.annotation.tailrec

object day23 extends common.AocApp(Day23Solution.solution)

object Day23Solution {
  type Parsed = List[Int]
  object solution extends common.Solution[Parsed]("2020", "23") {
    override def parse(input: String): Parsed = input.toList.map(_ - '0')

    def part1(initial: Parsed): String = part1(initial, 100)

    def part1(initial: Parsed, moves: Int): String = {

      @tailrec
      def loop(remain: Int, current: List[Int]): List[Int] = if (remain <= 0) current
      else {
        val a :: b :: c :: d :: others = current
        val pickUp = List(b, c, d)

        @tailrec
        def chooseNext(i: Int): Int = if (i == 0) chooseNext(9)
        else if (pickUp.contains(i)) {
          chooseNext(i - 1)
        } else i

        val next = chooseNext(a - 1)
        val index = others.indexOf(next)
        val before = others.take(index + 1)
        val after = others.drop(index + 1)

        loop(remain - 1, before ++ pickUp ++ after :+ a)
      }

      val result = loop(moves, initial)
      val index = result.indexOf(1)
      (result.drop(index + 1) ++ result.take(index)).mkString
    }

    override def part2(input: Parsed): BigInt = {
      val array = Array.ofDim[Int](1000000 + 1)

      for (i <- 1 until 1000000) {
        array(i) = i + 1
      }

      val initial = input ++ (10 to 1000000)

      for {
        (a, b) <- initial.zip(initial.tail)
      } {
        array(a) = b
      }
      array(initial.last) = 10
      array(1000000) = initial.head

      var current = initial.head

      for (_ <- 1 to 10000000) {
        val a = current
        val b = array(a)
        val c = array(b)
        val d = array(c)
        array(a) = array(d)
        val pickUp = List(b, c, d)

        @tailrec
        def chooseNext(i: Int): Int = if (i == 0) chooseNext(1000000)
        else if (pickUp.contains(i)) {
          chooseNext(i - 1)
        } else i

        val next = chooseNext(a - 1)

        val tmp = array(next)
        array(next) = b
        array(d) = tmp

        current = array(current)
      }

      BigInt(array(1)) * BigInt(array(array(1)))
    }
  }
}
