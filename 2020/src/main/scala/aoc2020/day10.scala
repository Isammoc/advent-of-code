package aoc2020

import scala.annotation.tailrec

object day10 extends common.AocApp(Day10Solution.solution)

object Day10Solution {
  type Parsed = List[Int]
  object solution extends common.Solution[Parsed]("2020", "10") {
    override def parse(input: String): Parsed = input.split("\n").map(_.toInt).sorted.toList

    def part1(inputs: Parsed): Int = {
      val sorted = (0 :: inputs) :+ (inputs.max + 3)

      val diffs = for {
        (a, b) <- sorted.zip(sorted.tail)
      } yield b - a

      diffs.count(_ == 1) * diffs.count(_ == 3)
    }

    def part2(inputs: Parsed): Long = {
      val sorted = inputs :+ (inputs.max + 3)

      @tailrec
      def loop(remain: List[Int], known: Map[Int, Long]): Long = remain match {
        case last :: Nil =>
          (for {
            delta <- 1 to 3 if known.contains(last - delta)
          } yield known(last - delta)).sum
        case h :: t =>
          val count = (for {
            delta <- 1 to 3 if known.contains(h - delta)
          } yield known(h - delta)).sum
          loop(t, known + (h -> count))
      }

      loop(sorted, Map(0 -> 1))
    }
  }
}
