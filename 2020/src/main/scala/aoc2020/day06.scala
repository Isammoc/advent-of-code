package aoc2020

object day06 extends common.AocApp(Day06Solution.solution)

object Day06Solution {
  type Parsed = Array[String]
  object solution extends common.Solution[Parsed]("2020", "06") {
    override def parse(input: String): Parsed = input.split("\n\n")
    def part1(input: Parsed): Int = input.map(_.replaceAll("\n", "").toSet.size).sum

    def part2(input: Parsed): Int = input.map(_.split("\n").map(_.toSet).reduce(_ & _).size).sum
  }
}
