package aoc2020

object day01 extends common.AocApp(Day01Solution.solution)

object Day01Solution {
  type Parsed = Array[Int]
  object solution extends common.Solution[Parsed]("2020", "01") {
    override def parse(input: String): Parsed = input.split("\n").map(_.toInt)

    def part1(input: Parsed): String = {
      val set = input.toSet

      val found = input.find(n => set.contains(2020 - n))
      found match {
        case Some(f) => (f * (2020 - f)).toString
        case _       => "IMPOSSIBLE"
      }
    }

    def part2(input: Parsed): String = {
      val possible = for {
        n1 <- input
        n2 <- input if n1 < n2
        n3 <- input if n2 < n3 && n1 + n2 + n3 == 2020
      } yield n1 * n2 * n3

      if (possible.length == 0) {
        "IMPOSSIBLE"
      } else if (possible.length == 1) {
        possible(0).toString
      } else {
        "Too many results"
      }
    }
  }
}
