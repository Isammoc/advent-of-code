package aoc2020

import scala.annotation.tailrec

object day15 extends common.AocApp(Day15Solution.solution)

object Day15Solution {
  type Parsed = Array[(Int, Int)]
  object solution extends common.Solution[Parsed]("2020", "15") {
    override def parse(input: String): Parsed = input
      .split(",")
      .map(_.toInt)
      .zipWithIndex
      .map { case (a, i) =>
        a -> (i + 1)
      }
      .reverse

    def part1(input: Parsed): Int = {
      val target = 2020

      @tailrec
      def loop(current: Int, last: Int, memory: Map[Int, Int]): Int = if (current == target) last
      else {
        if (memory.contains(last)) {
          val speech = current - memory(last)
          loop(current + 1, speech, memory + (last -> current))
        } else {
          loop(current + 1, 0, memory + (last -> current))
        }
      }

      val (last, current) = input.head
      loop(current, last, input.tail.reverse.toMap)
    }

    def part2(input: Parsed): Int = {

      val target = 30000000

      val memory = Array.fill(target)(0)
      input.tail.reverse.foreach { case (a, i) =>
        memory(a) = i
      }

      @tailrec
      def loop(current: Int, last: Int): Int = if (target == current) last
      else {
        if (memory(last) != 0) {
          val speech = current - memory(last)
          memory(last) = current
          loop(current + 1, speech)
        } else {
          memory(last) = current
          loop(current + 1, 0)
        }
      }

      val (last, current) = input.head
      loop(current, last)
    }
  }
}
