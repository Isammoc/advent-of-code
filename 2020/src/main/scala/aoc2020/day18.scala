package aoc2020

import scala.annotation.tailrec

object day18 extends common.AocApp(Day18Solution.solution)

object Day18Solution {

  sealed trait Token extends Product with Serializable

  case class Number(value: Long) extends Token

  sealed trait Operator extends Token {
    def apply(left: Long, right: Long): Number
  }

  case object Plus extends Operator {
    override def apply(left: Long, right: Long): Number = Number(left + right)
  }

  case object Times extends Operator {
    override def apply(left: Long, right: Long): Number = Number(left * right)
  }

  case object OpenParenthesis extends Token

  case object CloseParenthesis extends Token

  def parseLine(line: String): List[Token] = {
    @tailrec
    def toAtom(remain: List[Char], current: List[Token]): List[Token] = remain match {
      case Nil      => current.reverse
      case ' ' :: t => toAtom(t, current)
      case '+' :: t => toAtom(t, Plus :: current)
      case '*' :: t => toAtom(t, Times :: current)
      case '(' :: t => toAtom(t, OpenParenthesis :: current)
      case ')' :: t => toAtom(t, CloseParenthesis :: current)
      case c :: t   => toAtom(t, Number(c - '0') :: current)
    }

    @tailrec
    def toToken(remain: List[Token], current: List[Token]): List[Token] = remain match {
      case Nil                         => current.reverse
      case Number(a) :: Number(b) :: t => toToken(Number(a * 10 + b) :: t, current)
      case h :: t                      => toToken(t, h :: current)
    }

    toToken(toAtom(line.toList, Nil), Nil)
  }

  def eval(tokens: List[Token]): Long = {
    @tailrec
    def product(remain: List[Token]): Long = remain match {
      case Number(res) :: Nil                   => res
      case Number(a) :: Times :: Number(b) :: t => product(Number(a * b) :: t)
    }

    @tailrec
    def addition(remain: List[Token], stack: List[Token]): Long = remain match {
      case Nil                                 => product(stack)
      case Number(a) :: Plus :: Number(b) :: t => addition(Number(a + b) :: t, stack)
      case h :: t                              => addition(t, h :: stack)
    }

    def parenthesis(remain: List[Token], stack: List[Token]): Long = remain match {
      case Nil => addition(stack, Nil)
      case CloseParenthesis :: t =>
        val (ready, nextStack) = stack.span(_ != OpenParenthesis)
        parenthesis(Number(parenthesis(ready, Nil)) :: t, nextStack.tail)
      case h :: t => parenthesis(t, h :: stack)
    }

    parenthesis(tokens, Nil)
  }

  def evaluate(tokens: List[Token]): Long = {
    def loop(remain: List[Token]): (Long, List[Token]) = remain match {
      case Number(res) :: Nil                   => (res, Nil)
      case Number(res) :: CloseParenthesis :: t => (res, t)
      case Number(a) :: Plus :: Number(b) :: t  => loop(Number(a + b) :: t)
      case Number(a) :: Times :: Number(b) :: t => loop(Number(a * b) :: t)
      case OpenParenthesis :: t =>
        val (b, r) = loop(t)
        loop(Number(b) :: r)
      case (n: Number) :: (op: Operator) :: OpenParenthesis :: t =>
        val (b, r) = loop(t)
        loop(n :: op :: Number(b) :: r)
    }

    loop(tokens)._1
  }

  type Parsed = Array[List[Token]]
  object solution extends common.Solution[Parsed]("2020", "18") {
    override def parse(input: String): Parsed = input.split("\n").map(parseLine)

    def part1(input: Parsed): Long = input.map(evaluate).sum

    def part2(input: Parsed): Long = input.map(eval).sum
  }

}
