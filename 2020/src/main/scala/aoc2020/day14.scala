package aoc2020

import scala.annotation.tailrec

object day14 extends common.AocApp(Day14Solution.solution)

object Day14Solution {

  sealed abstract class Line extends Product with Serializable

  case class Mask(mask: String) extends Line

  case class Mem(address: BigInt, value: BigInt) extends Line

  sealed trait MemoryNode extends Product with Serializable {
    def value: BigInt

    def insert(path: List[Char], value: BigInt): MemoryNode = {
      (this, path) match {
        case (_, Nil) => Value(value)
        case (Value(_), 'X' :: t) =>
          val child = Value(0).insert(t, value)
          Node(child, child)
        case (Value(_), '0' :: t) => Node(Value(0).insert(t, value), Value(0))
        case (Value(_), '1' :: t) => Node(Value(0), Value(0).insert(t, value))
        case (Node(left, right), 'X' :: t) if left == right =>
          val child = left.insert(t, value)
          Node(child, child)
        case (Node(left, right), 'X' :: t) =>
          Node(left.insert(t, value), right.insert(t, value))
        case (Node(left, right), '0' :: t) => Node(left.insert(t, value), right)
        case (Node(left, right), '1' :: t) => Node(left, right.insert(t, value))
      }
    }
  }

  case class Value(value: BigInt) extends MemoryNode

  case class Node(child0: MemoryNode, child1: MemoryNode) extends MemoryNode {
    override def value: BigInt = if (child0 == child1) child0.value * 2
    else {
      child0.value + child1.value
    }
  }

  def addressMask(mask: String, addr: BigInt): String = {
    mask.reverse.zip(LazyList.concat(addr.toString(2).reverse, LazyList.continually('0'))).map {
      case ('0', v) => v
      case (m, _)   => m
    }
  }.reverse.mkString

  type Parsed = List[Line]
  object solution extends common.Solution[Parsed]("2020", "14") {
    def parse(input: String): List[Line] = {
      val MaskR = "mask = (.+)".r
      val MemR = "mem\\[(\\d+)] = (\\d+)".r

      input
        .split("\n")
        .map {
          case MaskR(mask)          => Mask(mask)
          case MemR(address, value) => Mem(address.toInt, value.toLong)
        }
        .toList
    }

    def part1(input: Parsed): BigInt = {

      @tailrec
      def loop(remain: List[Line], mask: String, memories: Map[BigInt, BigInt]): BigInt = remain match {
        case Nil => memories.values.sum
        case Mask(mask) :: t =>
          loop(t, mask.reverse, memories)
        case Mem(address, value) :: t =>
          val valueStr = value.toString(2).reverse
          val realValue =
            BigInt(
              mask
                .zip(LazyList.concat(valueStr, LazyList.continually('0')))
                .map {
                  case ('X', v) => v
                  case (m, _)   => m
                }
                .reverse
                .mkString,
              2
            )
          loop(t, mask, memories + (address -> realValue))
      }

      loop(input, "", Map.empty)
    }

    def part2(input: Parsed): BigInt = {
      @tailrec
      def loop(remain: List[Line], mask: String, memories: MemoryNode): BigInt = remain match {
        case Nil             => memories.value
        case Mask(mask) :: t => loop(t, mask, memories)
        case Mem(address, value) :: t =>
          loop(t, mask, memories.insert(addressMask(mask, address).toList, value))
      }

      loop(input, "", Value(0))
    }
  }
}
