package aoc2020

object day03 extends common.AocApp(Day03Solution.solution)

object Day03Solution {

  def slope(dx: Int, dy: Int)(trees: Array[String]): Long = {
    var y = 0
    var x = 0

    var count = 0

    while (y < trees.length) {
      if (trees(y)(x % trees(y).length) == '#') {
        count += 1
      }

      x += dx
      y += dy
    }

    count
  }

  type Parsed = Array[String]
  object solution extends common.Solution[Parsed]("2020", "03") {
    override def parse(input: String): Parsed = input.split("\n")

    def part1(trees: Parsed): Long = slope(3, 1)(trees)

    def part2(trees: Parsed): Long =
      slope(1, 1)(trees) *
        slope(3, 1)(trees) *
        slope(5, 1)(trees) *
        slope(7, 1)(trees) *
        slope(1, 2)(trees)
  }
}
