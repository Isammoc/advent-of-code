package aoc2020

import common.util.Memoize

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day22 extends common.AocApp(Day22Solution.solution)

object Day22Solution {
  type Parsed = (Queue[Int], Queue[Int])
  object solution extends common.Solution[Parsed]("2020", "22") {
    override def parse(input: String): Parsed = {
      val Array(deckA, deckB) = input.split("\n\n").map(one => Queue.from(one.split("\n").tail.map(_.toInt)))
      (deckA, deckB)
    }

    def part1(input: Parsed): Long = {

      @tailrec
      def loop(decks: (Queue[Int], Queue[Int])): Long = decks match {
        case (a +: as, b +: bs) =>
          if (a > b)
            loop((as :+ a :+ b, bs))
          else {
            loop((as, bs :+ b :+ a))
          }
        case (Queue(), bs) => bs.reverse.zipWithIndex.map { case (a, i) => a * (i + 1) }.sum
        case (as, Queue()) => as.reverse.zipWithIndex.map { case (a, i) => a * (i + 1) }.sum
      }

      loop(input)
    }

    def part2(input: Parsed): Int = {
      sealed trait Player
      case object Player1 extends Player
      case object Player2 extends Player

      lazy val loop: ((Queue[Int], Queue[Int])) => (Player, Queue[Int]) = Memoize.memoize { decks: (Queue[Int], Queue[Int]) =>
        @tailrec
        def inner(decks: (Queue[Int], Queue[Int]), previous: Set[(Player, Queue[Int])]): (Player, Queue[Int]) = {
          decks match {
            case (player1, player2) if previous.contains(Player1 -> player1) || previous.contains(Player2 -> player2) => Player1 -> player1
            case (a +: as, b +: bs) =>
              if (as.size >= a && bs.size >= b) {
                loop((as.take(a), bs.take(b))) match {
                  case (Player1, _) => inner((as :+ a :+ b, bs), previous + (Player1 -> (a +: as)) + (Player2 -> (b +: bs)))
                  case (Player2, _) => inner((as, bs :+ b :+ a), previous + (Player1 -> (a +: as)) + (Player2 -> (b +: bs)))
                }
              } else if (a > b) {
                inner((as :+ a :+ b, bs), previous + (Player1 -> (a +: as)) + (Player2 -> (b +: bs)))
              } else {
                inner((as, bs :+ b :+ a), previous + (Player1 -> (a +: as)) + (Player2 -> (b +: bs)))
              }
            case (Queue(), bs) => Player2 -> bs
            case (as, Queue()) => Player1 -> as
          }
        }

        inner(decks, Set.empty)
      }

      val (_, winDeck) = loop(input)
      winDeck.reverse.zipWithIndex.map { case (a, i) => a * (i + 1) }.sum
    }
  }
}
