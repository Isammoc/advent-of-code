package aoc2020

import common.grid.Point

object day20 extends common.AocApp(Day20Solution.solution)

object Day20Solution {
  def rotateLeft[T](current: List[List[T]]): List[List[T]] = current.transpose.reverse

  def allPositions[T](original: List[List[T]]): Set[List[List[T]]] = (for {
    one <- List(original.map(_.toList), original.map(_.toList).reverse, original.map(_.toList).transpose)
    current <- List(one, rotateLeft(one), rotateLeft(rotateLeft(one)), rotateLeft(rotateLeft(rotateLeft(one))))
  } yield current).toSet

  case class OrientedTile(borders: Map[Point, Long], content: List[List[Char]]) {
    def acceptConstraints(constraints: Iterable[(Point, Long)]): Boolean = constraints.forall { case (dir, value) =>
      borders(dir) == value
    }
  }

  case class Tile(id: Int, possible: Set[OrientedTile]) {
    lazy val possibleBorders: Set[Long] = for {
      orientedTile <- possible
      border <- orientedTile.borders.values
    } yield border

    def withConstraints(constraints: Iterable[(Point, Long)]): List[OrientedTile] =
      if (constraints.map(_._2).forall(possibleBorders.contains)) {
        for {
          oriented <- possible.toList
          if oriented.acceptConstraints(constraints)
        } yield oriented
      } else Nil
  }

  object Tile {
    def lineToLong(str: List[Char]): Long = str.mkString.map {
      case '#' => '1'
      case '.' => '0'
    }.toLong

    def apply(str: String): Tile = {
      val TileR = "Tile (\\d+):".r
      val first :: others = str.split("\n").toList

      val id = first match {
        case TileR(id) => id.toInt
      }

      val possible = for {
        current <- allPositions(others.map(_.toList))
      } yield {
        OrientedTile(
          Map(
            Point.Up -> lineToLong(current.head),
            Point.Right -> lineToLong(current.map(_.last)),
            Point.Down -> lineToLong(current.last),
            Point.Left -> lineToLong(current.map(_.head))
          ),
          current.tail.take(current.size - 2).map(line => line.tail.take(line.size - 2))
        )
      }
      Tile(id, possible)
    }
  }

  val monsters: Set[Set[Point]] = {
    val text =
      """..................#.
        |#....##....##....###
        |.#..#..#..#..#..#...""".stripMargin

    val ll = text.split("\n").toList.map(_.toList)

    for {
      one <- allPositions(ll)
    } yield {
      val head :: tail = for {
        (line, y) <- one.zipWithIndex
        (c, x) <- line.zipWithIndex
        if c == '#'
      } yield Point(x, y)

      tail.map(_ + (head * -1)).toSet
    }
  }

  def foundToImage(found: Map[Point, OrientedTile]): Set[Point] = {
    val minX = found.keys.map(_.x).min
    val maxX = found.keys.map(_.x).max
    val minY = found.keys.map(_.y).min
    val maxY = found.keys.map(_.y).max

    val length = found.head._2.content.head.size

    (for {
      y <- minY to maxY
      x <- minX to maxX
      if found.contains(Point(x, y))
      oriented = found(Point(x, y))
      yy <- 0 until length
      xx <- 0 until length
      if oriented.content(yy)(xx) == '#'
    } yield Point((x - minX) * length + xx, (y - minY) * length + yy)).toSet
  }

  type Parsed = List[Tile]
  object solution extends common.Solution[Parsed]("2020", "20") {
    override def parse(input: String): Parsed = input.split("\n\n").map(Tile.apply).toList

    def part1(tiles: Parsed): BigInt = {
      val byOccurrences = (for {
        t <- tiles
        v <- t.possibleBorders
      } yield v)
        .groupBy(identity)
        .map { case (v, l) =>
          v -> l.size
        }
        .toList
        .sortBy(_._2)
        .map(_._1)

      val tileByBorderOccurrence = byOccurrences.map { value =>
        tiles.filter(tile => tile.possibleBorders.contains(value))
      }

      tileByBorderOccurrence.filter(_.size == 1).flatten.groupBy(_.id).filter(_._2.size == 4).map(BigInt apply _._1).product
    }

    def part2(tiles: Parsed): Long = {
      def loop(remain: Set[Tile], found: Map[Point, OrientedTile]): Map[Point, OrientedTile] = {
        if (remain.isEmpty) {
          found
        } else {
          val pointWithConstraints = for {
            p <- found.keys
            pToVisit <- Point.OrthogonalDirections.map(p + _)
            if !found.contains(pToVisit)
          } yield {
            pToVisit -> (for {
              dir <- Point.OrthogonalDirections
              if found.contains(pToVisit + dir)
            } yield {
              dir -> found(pToVisit + dir).borders(dir.opposite)
            })
          }

          val pointWithPossibleTiles = for {
            (p, constraints) <- pointWithConstraints
            tile <- remain
            orienteds = tile.withConstraints(constraints)
            if orienteds.nonEmpty
          } yield (p, tile, orienteds)

          if (pointWithPossibleTiles.isEmpty) {
            Map.empty
          } else {
            val (pToConsider, list) = pointWithPossibleTiles.groupBy(_._1).minBy(_._2.size)
            val possibleResult = for {
              (_, tile, orienteds) <- list
              oriented <- orienteds
              result = loop(remain - tile, found + (pToConsider -> oriented))
              if result.nonEmpty
            } yield result

            if (possibleResult.isEmpty) Map.empty
            else possibleResult.head
          }
        }
      }

      val first :: others = tiles

      val wholeImage = foundToImage(loop(others.toSet, Map(Point(0, 0) -> first.possible.head)))

      (for {
        monster <- monsters
      } yield {
        val partOfMonster = for {
          origin <- wholeImage
          currentMonster = monster.map(_ + origin)
          if currentMonster.forall(wholeImage.contains)
          p <- currentMonster + origin
        } yield p

        (wholeImage -- partOfMonster).size
      }).min
    }
  }
}
