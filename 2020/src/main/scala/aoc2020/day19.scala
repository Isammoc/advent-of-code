package aoc2020

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day19 extends common.AocApp(Day19Solution.solution)

object Day19Solution {

  sealed trait Node {
    def accept(c: Char): List[Node]
  }

  case object Complete extends Node {
    override def accept(c: Char): List[Node] = Nil
  }

  case class CharNode(c: Char) extends Node {
    override def accept(c: Char): List[Node] =
      if (c == this.c) List(Complete)
      else Nil
  }

  case class OrNode(children: Set[Node]) extends Node {
    def fromChildren(children: Set[Node]): Node = if (children.size == 1) children.head else OrNode(children)

    override def accept(c: Char): List[Node] = {
      val newChildren = for {
        child <- children
        newChild <- child.accept(c)
      } yield newChild

      if (newChildren.isEmpty) Nil
      else if (newChildren == Set(Complete)) List(Complete)
      else if (newChildren.contains(Complete)) List(Complete, fromChildren(newChildren - Complete))
      else List(fromChildren(newChildren))
    }
  }

  case class ChainNode(children: List[Node]) extends Node {
    override def accept(c: Char): List[Node] = children match {
      case Nil      => Nil
      case h :: Nil => h.accept(c)
      case h :: t =>
        val head = h.accept(c)

        if (head.isEmpty) Nil
        else if (head.contains(Complete)) {
          val remain = ChainNode(t)
          val other = head.filterNot(_ == Complete)

          if (other.isEmpty) List(remain)
          else remain :: other.map(_ :: t).map(ChainNode)
        } else {
          head.map(_ :: t).map(ChainNode)
        }
    }
  }

  case object Self extends Node {
    override def accept(c: Char): List[Node] = ???
  }

  case class RecursiveNode(original: Set[Node], children: Set[Node]) extends Node {
    def fromChild(child: Node): Node = child match {
      case Self                 => RecursiveNode(original, original)
      case ChainNode(Self :: t) => ChainNode(RecursiveNode(original, original) :: t)
      case a                    => a
    }

    def fromChildren(children: Set[Node]): Node =
      if (children.size == 1)
        fromChild(children.head)
      else
        RecursiveNode(original, children.map(fromChild))

    override def accept(c: Char): List[Node] = {
      val newChildren = for {
        child <- children
        newChild <- child.accept(c)
      } yield newChild

      if (newChildren.isEmpty) Nil
      else if (newChildren == Set(Complete)) List(Complete)
      else if (newChildren.contains(Complete)) List(Complete, fromChildren(newChildren - Complete))
      else List(fromChildren(newChildren))
    }
  }

  object Node {
    def fromRules(rules: Map[Int, Rule]): Node = {
      @tailrec
      def loop(remain: Queue[Rule], found: Map[Int, Node]): Node = if (found.contains(0)) {
        found(0)
      } else {
        remain match {
          case CharRule(id, c) +: xs                                             => loop(xs, found + (id -> CharNode(c)))
          case SimpleRule(id, contains) +: xs if contains.forall(found.contains) => loop(xs, found + (id -> ChainNode(contains.map(found))))
          case OrRule(id, left, right) +: xs if left.forall(found.contains) && right.forall(found.contains) =>
            loop(xs, found + (id -> OrNode(Set(ChainNode(left.map(found)), ChainNode(right.map(found))))))
          case RecursiveRule(id, left, right) +: xs if left.forall(found.contains) && right.flatten.forall(found.contains) =>
            val children: Set[Node] = Set(
              ChainNode(left.map(found)),
              ChainNode(right.map {
                case Some(i) => found(i)
                case None    => Self
              })
            )

            loop(xs, found + (id -> RecursiveNode(children, children)))
          case x +: xs =>
            loop(xs :+ x, found)
        }
      }

      loop(Queue.empty ++ rules.values, Map.empty)
    }
  }

  case class Engine(node: Node) {

    def validate(msg: String): Boolean = {
      @tailrec
      def loop(remain: List[Char], current: Set[Node]): Boolean = remain match {
        case Nil    => current.contains(Complete)
        case h :: t => loop(t, current.flatMap(_.accept(h)))
      }

      loop(msg.toList, Set(node))
    }
  }

  sealed abstract class Rule(val Id: Int) extends Product with Serializable

  case class CharRule(id: Int, c: Char) extends Rule(id)

  case class OrRule(id: Int, left: List[Int], right: List[Int]) extends Rule(id)

  case class SimpleRule(id: Int, contains: List[Int]) extends Rule(id)

  case class RecursiveRule(id: Int, left: List[Int], right: List[Option[Int]]) extends Rule(id)

  private def parseRules(input: String) = {
    val CharR = "(\\d+): \"(.)\"".r
    val OrR = "(\\d+): (.*) \\| (.*)".r
    val SimpleR = "(\\d+): (.*)".r

    val rules = input.split("\n").map {
      case CharR(id, c)          => CharRule(id.toInt, c(0))
      case OrR(id, left, right)  => OrRule(id.toInt, left.split(" ").map(_.toInt).toList, right.split(" ").map(_.toInt).toList)
      case SimpleR(id, contains) => SimpleRule(id.toInt, contains.split(" ").map(_.toInt).toList)
    }
    rules
  }

  type Parsed = (Array[Rule], Array[String])
  object solution extends common.Solution[Parsed]("2020", "19") {
    override def parse(input: String): Parsed = {
      val Array(inputRules, inputMessages) = input.split("\n\n")
      (parseRules(inputRules), inputMessages.split("\n"))
    }

    def part1(input: Parsed): Int = {
      val (rules, messages) = input

      val engine = Engine(Node.fromRules(rules.map(rule => rule.Id -> rule).toMap))

      messages.count(engine.validate)
    }

    def part2(input: Parsed): Int = {
      val (rules, messages) = input
      val rule8 = RecursiveRule(8, List(42), List(Some(42), None))
      val rule11 = RecursiveRule(11, List(42, 31), List(Some(42), None, Some(31)))
      val engine = Engine(Node.fromRules(LazyList.concat(rules, List(rule8, rule11)).map(rule => rule.Id -> rule).toMap))

      messages.count(engine.validate)
    }
  }
}
