package aoc2020

import scala.annotation.tailrec

object day08 extends common.AocApp(Day08Solution.solution)

object Day08Solution {
  type Parsed = DeviceState
  object solution extends common.Solution[Parsed]("2020", "08") {
    override def parse(input: String): Parsed = Device.fromScript(input)

    def part1(input: Parsed): Int = {
      @tailrec
      def loop(current: DeviceState, visited: Set[Int]): Int = if (visited contains current.instructionPointer) {
        current.accumulator
      } else {
        loop(current.step, visited + current.instructionPointer)
      }

      loop(input, Set.empty)
    }

    def part2(original: Parsed): Int = {
      val possible = for {
        i: Int <- original.instructions.indices.toSet
      } yield original.instructions(i) match {
        case Nop(value) => original.copy(original.instructions.updated(i, Jmp(value)))
        case Jmp(value) => original.copy(original.instructions.updated(i, Nop(value)))
        case _          => original
      }

      @tailrec
      def loop(current: DeviceState, visited: Set[Int]): Option[Int] = {
        current.status match {
          case Running =>
            if (visited contains current.instructionPointer) {
              None
            } else {
              loop(current.step, visited + current.instructionPointer)
            }
          case EndOfScript => Some(current.accumulator)
          case _           => None
        }
      }

      possible.map(state => loop(state, Set.empty)).find(_.isDefined).get.get
    }
  }
}
