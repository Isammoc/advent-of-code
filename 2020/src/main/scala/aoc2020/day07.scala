package aoc2020

import common.util.Memoize

import scala.annotation.tailrec

object day07 extends common.AocApp(Day07Solution.solution)

object Day07Solution {
  case class Rule(outermost: String, inside: List[(String, Int)])

  def parseRules(input: String): Seq[Rule] = {
    val RuleR = "(.*) bags contain (.*).".r
    val InsideR = "(\\d+) (.+) bags?".r

    input
      .split("\n")
      .map { case RuleR(outermost, inside) =>
        Rule(
          outermost,
          inside match {
            case "no other bags" => Nil
            case _ =>
              inside.split(", ").toList.map { case InsideR(count, bag) =>
                bag -> count.toInt
              }
          }
        )
      }
      .toList
  }
  type Parsed = Seq[Rule]
  object solution extends common.Solution[Parsed]("2020", "07") {
    override def parse(input: String): Parsed = parseRules(input)

    def part1(input: Parsed): Int = {
      val reverseRules = (for {
        Rule(outermost, inside) <- input
        (name, _) <- inside
      } yield name -> outermost).groupBy(_._1).view.mapValues(_.map(_._2)).toMap

      @tailrec
      def loop(toVisit: Seq[String], visited: Set[String]): Int = toVisit match {
        case Nil                           => visited.size
        case h :: t if visited.contains(h) => loop(t, visited)
        case h :: t =>
          loop(reverseRules.getOrElse(h, Nil) ++ t, visited + h)
      }

      loop(List("shiny gold"), Set.empty) - 1
    }

    def part2(input: Parsed): Int = {
      val strRules = input.groupBy(_.outermost).view.mapValues(_.flatMap(_.inside))

      lazy val inside: String => Int = Memoize.memoize { current =>
        (for {
          (name, count) <- strRules(current)
        } yield count * inside(name)).sum + 1
      }

      inside("shiny gold") - 1
    }
  }
}
