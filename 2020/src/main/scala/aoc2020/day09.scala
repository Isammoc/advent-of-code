package aoc2020

import scala.annotation.tailrec

object day09 extends common.AocApp(Day09Solution.solution)

object Day09Solution {
  type Parsed = List[Long]
  object solution extends common.Solution[Parsed]("2020", "09") {
    override def parse(input: String): Parsed = input.split("\n").map(_.toLong).toList

    def part1(input: Parsed, preambleSize: Int): Long = {
      @tailrec
      def loop(current: List[Long]): Long = {
        val preambles = current.take(preambleSize).toSet
        val toTest = current(preambleSize)
        if (preambles.exists(p => preambles contains (toTest - p))) {
          loop(current.tail)
        } else {
          toTest
        }
      }

      loop(input)
    }

    def part2(input: Parsed, preambleSize: Int): Long = {
      val toFound = part1(input, preambleSize)

      @tailrec
      def loop(remain: List[Long], considering: List[Long], sum: Long): Long = if (sum == toFound) {
        considering.min + considering.max
      } else if (sum > toFound) {
        loop(remain, considering.tail, sum - considering.head)
      } else {
        remain match {
          case h :: t => loop(t, considering :+ h, sum + h)
        }
      }

      loop(input, Nil, 0)
    }

    override def part1(parsed: Parsed): Any = part1(parsed, 25)

    override def part2(parsed: Parsed): Any = part2(parsed, 25)
  }
}
