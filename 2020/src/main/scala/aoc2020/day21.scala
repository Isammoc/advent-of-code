package aoc2020

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object day21 extends common.AocApp(Day21Solution.solution)

object Day21Solution {

  case class Food(ingredients: Set[String], allergens: Set[String]) {
    def without(ingredient: String, allergen: String): Food = Food(ingredients - ingredient, allergens - allergen)
  }

  case class Grocery(foods: List[Food], ingredients: Set[String], allergens: Set[String], found: Map[String, String]) {
    def without(ingredient: String, allergen: String): Grocery =
      Grocery(foods.map(_.without(ingredient, allergen)), ingredients - ingredient, allergens - allergen, found + (allergen -> ingredient))
  }

  object Grocery {
    def apply(str: String): Grocery = {
      val FoodR = "(.*) (?:\\(contains (.*)\\))?".r

      val foods = str
        .split("\n")
        .map { case FoodR(ingredients, allergens) =>
          Food(ingredients.split(" ").toSet, allergens.split(", ").toSet)
        }
        .toList

      Grocery(foods, foods.map(_.ingredients).fold(Set.empty)(_ ++ _), foods.map(_.allergens).fold(Set.empty)(_ ++ _), Map.empty)
    }
  }

  @tailrec
  def loop(toVisit: Queue[Grocery]): Grocery = toVisit match {
    case current +: xs =>
      if (current.allergens.isEmpty) current
      else {
        val possible = for {
          allergen <- current.allergens
        } yield {
          val allIngredients = for {
            food <- current.foods
            if food.allergens.contains(allergen)
          } yield food.ingredients
          allergen -> allIngredients.fold(allIngredients.head)(_ intersect _)
        }
        val (allergen, ingredients) = possible.minBy(_._2.size)

        val next = for {
          ingredient <- ingredients
        } yield current.without(ingredient, allergen)

        loop(xs ++ next)
      }
  }

  type Parsed = Grocery
  object solution extends common.Solution[Parsed]("2020", "21") {
    override def parse(input: String): Parsed = Grocery(input)

    def part1(grocery: Parsed): Int = {
      val result = loop(Queue(grocery))

      result.foods.map(_.ingredients.size).sum
    }

    def part2(grocery: Parsed): String = {
      val result = loop(Queue(grocery))

      result.found.toList.sortBy(_._1).map(_._2).mkString(",")
    }
  }
}
