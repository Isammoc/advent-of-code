package aoc2020

import scala.annotation.tailrec

object day25 extends common.AocApp(Day25Solution.solution)

object Day25Solution {

  def step(value: Int, subjectNumber: Int): Int = ((value.toLong * subjectNumber.toLong) % 20201227).toInt

  def transform(subjectNumber: Int, loopSize: Int): Int = (1 to loopSize).foldLeft(1) { case (value, _) => step(value, subjectNumber) }

  def findLoopSize(publicKey: Int): Int = {
    @tailrec
    def loop(current: Int, value: Int): Int = if (value == publicKey) current
    else loop(current + 1, step(value, 7))

    loop(0, 1)
  }

  type Parsed = Array[Int]
  object solution extends common.Solution[Parsed]("2020", "25") {
    override def parse(input: String): Parsed = input.split("\n").map(_.toInt)

    def part1(input: Parsed): Int = {
      val Array(cardPublicKey, doorPublicKey) = input

      val doorLoopSize = findLoopSize(doorPublicKey)

      transform(cardPublicKey, doorLoopSize)
    }

    override def part2(parsed: Parsed): Any = "CHRISTMAS GIFT"
  }

}
