package aoc2020

sealed trait Instruction extends Product with Serializable

case class Nop(value: Int) extends Instruction

case class Acc(value: Int) extends Instruction

case class Jmp(value: Int) extends Instruction

sealed trait DeviceStatus extends Product with Serializable {
  def next(current: DeviceState): DeviceState
}

case object Running extends DeviceStatus {
  override def next(current: DeviceState): DeviceState = {
    if (current.instructionPointer == current.instructions.length) {
      current.copy(status = EndOfScript)
    } else if (current.instructionPointer < 0 || current.instructionPointer >= current.instructions.length)
      current.copy(status = OutOfBoundInstructionPointer)
    else {
      val currentInstruction = current.instructions(current.instructionPointer)
      currentInstruction match {
        case Nop(_)     => current.nop
        case Acc(value) => current.acc(value)
        case Jmp(value) => current.jmp(value)
      }
    }
  }
}

case object EndOfScript extends DeviceStatus {
  override def next(current: DeviceState): DeviceState = current
}

sealed trait ErrorStatus extends DeviceStatus {
  final def next(current: DeviceState): DeviceState = current
}

case object OutOfBoundInstructionPointer extends ErrorStatus

case class DeviceState(instructions: Array[Instruction], status: DeviceStatus = Running, accumulator: Int = 0, instructionPointer: Int = 0) {
  def nop: DeviceState = this.copy(instructionPointer = instructionPointer + 1)

  def acc(value: Int): DeviceState = this.copy(accumulator = accumulator + value, instructionPointer = instructionPointer + 1)

  def jmp(value: Int): DeviceState = this.copy(instructionPointer = instructionPointer + value)

  def step: DeviceState = status.next(this)
}

object Device {
  def fromScript(script: String): DeviceState = {
    val NopR = "nop ([+-]\\d+)".r
    val AccR = "acc ([+-]\\d+)".r
    val JmpR = "jmp ([+-]\\d+)".r

    val instructions = script.split("\n").map {
      case NopR(value) => Nop(value.toInt)
      case AccR(value) => Acc(value.toInt)
      case JmpR(value) => Jmp(value.toInt)
    }
    DeviceState(instructions)
  }
}
