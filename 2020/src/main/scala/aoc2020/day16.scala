package aoc2020

object day16 extends common.AocApp(Day16Solution.solution)

object Day16Solution {

  sealed trait Parser {
    val name = ""

    def validate(value: Int): Boolean
  }

  case class Interval(start: Int, end: Int) extends Parser {
    override def validate(value: Int): Boolean = start <= value && value <= end
  }

  case class Or(override val name: String, left: Parser, right: Parser) extends Parser {
    override def validate(value: Int): Boolean = left.validate(value) || right.validate(value)
  }

  def parseDefinitions(definitions: String): Set[Parser] = {
    val DefR = "(.*): (\\d+)-(\\d+) or (\\d+)-(\\d+)".r

    definitions
      .split("\n")
      .map { case DefR(name, s1, e1, s2, e2) =>
        Or(name, Interval(s1.toInt, e1.toInt), Interval(s2.toInt, e2.toInt))
      }
      .toSet
  }

  def validateTicket(parsers: Set[Parser])(remain: List[Int]): Boolean =
    remain.forall(value => parsers.exists(_.validate(value)))

  type Parsed = (Set[Parser], List[Int], List[List[Int]])

  object solution extends common.Solution[Parsed]("2020", "16") {
    override def parse(input: String): (Set[Parser], List[Int], List[List[Int]]) = {
      val Array(definitions, myTicket, nearbyTickets) = input.split("\n\n")
      (
        parseDefinitions(definitions),
        myTicket.split("\n").tail.head.split(",").toList.map(_.toInt),
        nearbyTickets.split("\n").tail.map(_.split(",").map(_.toInt).toList).toList
      )
    }

    def part1(input: Parsed): Int = {
      val (parsers, _, tickets) = input
      tickets.flatten.filterNot(ticket => parsers.exists(_.validate(ticket))).sum
    }

    def part2(input: Parsed): Long = {
      val (parsers, myTickets, tickets) = input

      val validTickets: List[List[Int]] = myTickets :: tickets.filter(
        validateTicket(parsers)
      )

      case class ColumnParser(parsers: Set[Parser], column: List[Int])

      def loop(columnParsers: Set[ColumnParser], result: Long = 1): Long = {
        if (columnParsers.isEmpty) result
        else {
          val toConsider = columnParsers.minBy(_.parsers.size)

          if (toConsider.parsers.isEmpty) {
            1
          } else {
            for {
              parser <- toConsider.parsers
              current = result * (if (parser.name.startsWith("departure")) toConsider.column.head else 1)
            } yield loop(
              (columnParsers - toConsider).map { case ColumnParser(oldParsers, column) =>
                ColumnParser(oldParsers - parser, column)
              },
              current
            )
          }.product
        }
      }

      loop(for {
        column <- validTickets.transpose.toSet
      } yield ColumnParser(parsers.filter(parser => column.forall(parser.validate)), column))
    }
  }
}
