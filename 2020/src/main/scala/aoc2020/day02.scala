package aoc2020

object day02 extends common.AocApp(Day02Solution.solution)

object Day02Solution {
  case class Line(min: Int, max: Int, letter: Char, password: String)

  type Parsed = Seq[Line]
  object solution extends common.Solution[Parsed]("2020", "02") {
    def parse(input: String): Parsed = {
      val LineR = "(\\d+)-(\\d+) (.): (.+)".r
      input
        .split("\n")
        .map { case LineR(min, max, letter, password) =>
          Line(min.toInt, max.toInt, letter(0), password)
        }
        .toSeq
    }

    def part1(input: Parsed): Int = {
      input.count(line => {
        val count = line.password.count(_ == line.letter)
        line.min <= count && count <= line.max
      })
    }

    def part2(input: Parsed): Int =
      input.count(line => {
        val first = line.password.length >= line.min && (line.password.charAt(line.min - 1) == line.letter)
        val second = line.password.length >= line.max && (line.password.charAt(line.max - 1) == line.letter)
        first ^ second
      })
  }
}
