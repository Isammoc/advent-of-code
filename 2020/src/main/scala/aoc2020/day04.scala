package aoc2020

import java.util.function.Predicate
import scala.util.matching.Regex

object day04 extends common.AocApp(Day04Solution.solution)

object Day04Solution {

  type Validator = Predicate[String]

  val Byr: Regex = "byr:(\\d\\d\\d\\d)".r
  val Iyr: Regex = "iyr:(\\d\\d\\d\\d)".r
  val Eyr: Regex = "eyr:(\\d\\d\\d\\d)".r
  val HgtCm: Regex = "hgt:(\\d\\d\\d)cm".r
  val HgtIn: Regex = "hgt:(\\d\\d)in".r
  val Hcl: Regex = "hcl:#[0-9a-f]{6}".r
  val Ecl: Regex = "ecl:(?:amb|blu|brn|gry|grn|hzl|oth)".r
  val Pid: Regex = "pid:[0-9]{9}".r

  val validators: List[Validator] = List(
    {
      case Byr(byr) => 1920 <= byr.toInt && byr.toInt <= 2002
      case _        => false
    },
    {
      case Iyr(iyr) => 2010 <= iyr.toInt && iyr.toInt <= 2020
      case _        => false
    },
    {
      case Eyr(eyr) => 2020 <= eyr.toInt && eyr.toInt <= 2030
      case _        => false
    },
    {
      case HgtCm(cm) => 150 <= cm.toInt && cm.toInt <= 193
      case HgtIn(in) => 59 <= in.toInt && in.toInt <= 76
      case _         => false
    },
    Hcl.matches,
    Ecl.matches,
    Pid.matches
  )

  val identifiers = List("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

  val FourDigits: Regex = "(\\d\\d\\d\\d)".r

  val validations = Map(
    "byr" -> { str: String =>
      str
    }
  )

  type Parsed = Array[Array[String]]

  object solution extends common.Solution[Parsed]("2020", "04") {
    override def parse(input: String): Parsed = input.split("\n\n").map(_.split("[ \n]"))

    def part1(input: Parsed): Int = input
      .count(passport => identifiers.forall(passport.map(_.split(':')).map(_(0)).toSet.contains))

    def part2(input: Parsed): Int = input.count(passport => validators.forall(validator => passport.exists(validator.test)))
  }
}
