package aoc2020

object day13 extends common.AocApp(Day13Solution.solution)

object Day13Solution {

  def nextDepart(timestamp: Int)(busID: Int): Int = {
    val k = timestamp / busID
    (k + 1) * busID - timestamp
  }

  type Parsed = String
  object solution extends common.Solution[Parsed]("2020", "13") {
    override def parse(input: String): Parsed = input
    def part1(input: String): Int = {
      val Array(timestampStr, busIDsLine) = input.split("\n")
      val timestamp = timestampStr.toInt
      val busIDs = busIDsLine.split(",").filterNot(_ == "x").map(_.toInt)

      val busID = busIDs.minBy(nextDepart(timestamp))
      busID * nextDepart(timestamp)(busID)
    }

    def part2(input: String): BigInt = {
      val Array(_, busIDsLine) = input.split("\n")
      case class Bus(id: Int, index: Int) {
        def isDepart(timestamp: BigInt): Boolean = (timestamp + index) % id == 0
      }
      val buses = busIDsLine.split(",").zipWithIndex.filterNot(_._1 == "x").map { case (busId, index) =>
        Bus(busId.toInt, index)
      }

      buses
        .foldLeft(LazyList.iterate(BigInt(1))(_ + 1))((current, bus) => {
          val tmp = current.filter(bus.isDepart)
          val step = tmp.zip(tmp.tail).map { case (a, b) => b - a }.head
          LazyList.iterate(tmp.head)(_ + step)
        })
        .head
    }
  }
}
