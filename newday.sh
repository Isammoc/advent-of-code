#!/bin/bash

DEFAULT_YEAR=2024

function help {
  echo "use with as:"
  echo "  $0 [<year>] <day>"
  echo "sample:"
  echo "  $0 02"
  echo "  $0 2017 24"
}


function write {
  if [ ! -d "${YEAR}/src/main/scala/aoc${YEAR}" ]; then
    mkdir -p $YEAR/src/main/scala/aoc$YEAR
  fi
  cat > $YEAR/src/main/scala/aoc$YEAR/day$DAY.scala << EOF
package aoc${YEAR}

object day${DAY} extends common.AocApp(Day${DAY}Solution.solution)

object Day${DAY}Solution {
  type Parsed = String

  object solution extends common.Solution[Parsed]("${YEAR}", "${DAY}") {
    override def parse(input: String): Parsed = input

    override def part1(parsed: Parsed): Any = "NOT IMPLEMENTED"

    override def part2(parsed: Parsed): Any = "NOT IMPLEMENTED"
  }
}

EOF

  if [ ! -d $YEAR/src/test/scala/aoc$YEAR ]; then
    mkdir -p $YEAR/src/test/scala/aoc$YEAR
  fi
  cat > $YEAR/src/test/scala/aoc$YEAR/day${DAY}Spec.scala << EOF
package aoc${YEAR}

class day${DAY}Spec extends common.AocSpec {
  import Day${DAY}Solution._

  val sampleInput: String =
    """""".stripMargin

  "day${DAY} ${YEAR}" can {
    import solution._

    val sample = parse(sampleInput)

    "part1" should {
      "example" in {
        part1(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }

    "part2" should {
      "example" in {
        part2(sample) shouldEqual "NOT IMPLEMENTED"
      }
    }
  }
}

EOF
}

if [ $# -eq 1 ]; then
  YEAR=$DEFAULT_YEAR
  DAY=$1
elif [ $# -eq 2 ]; then
  YEAR=$1
  DAY=$2
else
  help
fi
write
